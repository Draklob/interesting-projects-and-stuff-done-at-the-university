package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Random;

/**
 * Cada individuo genera un determinado n�mero de valores aleatorios de un tipo determinado ( float, int, etc... )
 * entre 0 y un valor m�ximo, o entre un m�nimo y un valor m�ximo
 * Estas variaciones se determinan en funci�n del fenotipo elegido para resolver el problema planteado
 * En funci�n de los valores generados tendr� un fitness determinado de acuerdo al tipo de problema y su soluci�n.
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class AG_Individual extends Individual<ArrayList<Number>>
{
	
	/**
     * Constructor para la clase ArrayFloat_Individual 
     * @param _abstractProblem El tipo de problema para el que se genera este individuo
     */
	public AG_Individual( Abstract_Problem<ArrayList<Number>> _abstractProblem ) 
	{
		// Inicializamos los atributos del padre
		super();
		
		// Se crea un objeto de tipo Random para generar tantos valores aleatorios para este individuo
		// como requiera el problema 
		Random rd = new Random();
		
		// Se inicializa el genoma ( ArrayList )
		this.genoma = new ArrayList<Number>();
		
		// Se generan valores aleatorios para este individuo 
		for( int i=0; i<_abstractProblem.varCount; i++ )
		{
			Number newValue = 0;
			if( _abstractProblem.mode == Abstract_Problem.Mode.TEST1 )
			{
				newValue = 1;
			}
			else if( _abstractProblem.varType == Abstract_Problem.VarType.FLOAT )
			{
				newValue = rd.nextFloat() * ( _abstractProblem.varMax - _abstractProblem.varMin ) +  _abstractProblem.varMin;
			}
			else if( _abstractProblem.varType == Abstract_Problem.VarType.INTEGER )
			{
				newValue = rd.nextInt( _abstractProblem.varMax - _abstractProblem.varMin + 1 ) +  _abstractProblem.varMin;
			}
			else if( _abstractProblem.varType == Abstract_Problem.VarType.BINARY )
			{
				// Damos m�s prioridad al cero para crear menos conexiones y obligar en la evoluci�n a elegir las necesarias
				if( rd.nextFloat() > 0.2 ) newValue = 0;
				else newValue = 1;
				//newValue = rd.nextInt( 2 );
			}
			this.genoma.add(newValue);
		}

	}// Fin del constructor 
	
	/**
	 * Constructor copia
	 * @param individual El individuo que se usa para construir este 
	 */
	public AG_Individual( Individual<ArrayList<Number>> individual )
	{
		// Inicializamos los atributos del padre
		super();
		
		// Se inicializan los atributos copiando el valor del individuo que se quiere clonar
		this.genoma = new ArrayList<Number>();
		for( Number numb: individual.genoma )
		{
			this.genoma.add(numb);
		}
		this.fitness = individual.fitness;
	}
	
	/**
	 * Muestra por pantalla el genoma del individuo para debuguear
	 */
	public void printGenoma()
	{
		int index = 0;
		for( int i=0; i<(this.genoma).size(); i++ )
		{
			System.out.println( "var : " + index + " - valor = " + this.genoma.get(index) );
			index++;
		}
	}


}
