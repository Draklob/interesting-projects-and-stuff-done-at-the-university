package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Random;

/**
 * Clase que encapsula diferentes m�todos para c�lculos usados en diferentes clases
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class Utils 
{
	
	/**
	 * Devuelve un n�mero aleatorio ( tipo int ) entre un valor inicial y uno final
	 * @param valorInicial
	 * @param valorFinal
	 * @return
	 */
	@SuppressWarnings("unused")
	private int numeroAleatorio( int valorInicial, int valorFinal)
	{
	    return (int)(Math.random()*(valorFinal-valorInicial+1)+valorInicial);
	}
	    
	/**
	 * Devuelve una lista de un n�mero determinado de enteros ( valuesCount )
	 * aleatorios entre 0 inclusive y limit no inclusive
	 * @param valuesCount El n�mero de elementos del array de enteros devuelto
	 * @param limit El m�ximo valor que puede alcanzar cada valor del array de enteros devuelto
	 * @return El array de enteros de tama�o valuesCount generado aleatoriamente de tipo entero entre 0 y limit
	 */
	static public ArrayList<Integer> generateDifferentValues( int valuesCount, int limit )
	{
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		
		Random rd = new Random();
		
		// Mientras que no se generen todos los numeros
		while( arrayList.size() < valuesCount )
		{
			int numero = rd.nextInt( limit );//genero un numero
			if(arrayList.isEmpty())//si la lista esta vacia
				arrayList.add(numero);
			else
			{
				if( ! arrayList.contains(numero))
				{
					arrayList.add(numero);
				}
			}
		}
	 
	   return arrayList;
	}
	
	/**
	 * Devuelve el valor de la funci�n sigmoide aplicada al par�metro x
	 * @param x El valor sobre el que calcular la sigmoide
	 * @return El valor de la sigmoide aplicada a x
	 */
	static public float sigmoid( float x ) 
	{
        return (float) ( 1.0 / ( 1.0 + ( Math.exp(-x) ) ) );
    }
	
	/**
	 * Devuelve el valor de la funci�n sigmoide aplicada al par�metro x
	 * @param x El valor sobre el que aplicar la hiperb�lica
	 * @return El valor de la hiperb�lica aplicada a x
	 */
	static public float hyperbolic( float x ) 
	{
       // return (float) ( ( 1.0 - ( Math.exp(-x) ) ) / ( 1.0 + ( Math.exp(-x) ) ) );
		
		return (float) ( ( Math.exp(x) - ( Math.exp(-x) ) ) / ( Math.exp(x) + ( Math.exp(-x) ) ) );
    }
	
	/**
	 * Devuelve el valor de la derivada de la funci�n sigmoide aplicada al par�metro x
	 * @param x El valor sobre el que aplicar la derivada de la sigmoide
	 * @return El valor de la derivada de la sigmoide aplicada a x
	 */
	static public float sigmoid_derived( float x ) 
	{
		return x * ( 1 - x );
    }
	
	/**
	 * Devuelve el valor de la derivada de la funci�n sigmoide aplicada al par�metro x
	 * @param x El valor sobre el que aplicar la derivada de la hiperb�lica
	 * @return El valor de la derivada de la hiperb�lica aplicada a x
	 */
	static public float hiperbolic_derived( float x ) 
	{
		return ( 1 - ( hyperbolic( x ) * hyperbolic( x ) ) );
    }
	
	/**
	 * Muestra los valores de un arraylist de tipo float por pantalla
	 * @param _trainingData el arraylist 
	 */
	static public void showArrayList( ArrayList<Float> _trainingData ) 
	{
		System.out.println( "------------------------------------------");
		int count = 0;
		for( Number value : _trainingData )
		{
			System.out.println( "Value => " + count + " = " + value );
		}
		System.out.println( "------------------------------------------");
    }

}
