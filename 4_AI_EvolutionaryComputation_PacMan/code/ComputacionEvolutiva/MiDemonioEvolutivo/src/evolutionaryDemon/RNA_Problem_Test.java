package evolutionaryDemon;

import java.util.ArrayList;


/**
 * Clase que usamos para testear que la clase RNA_Problem funciona correctamente
 * El Test1 es simplemente para comprobar que se calcula bien el resultado final de una red neuronal pas�ndo una tupla conocida
 * En el Test2_OR se usa la compuerta l�gica OR como funci�n de entrenamiento
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 *
 */
public class RNA_Problem_Test extends RNA_Problem
{
	public enum TestEnum{ Test1, Test2_OR, Test2_AND, Test2_XOR, Test3 };
	/**
	 * El tipo de test
	 */
	private TestEnum testEnum;
	
	/**
	 * Para llevar un contador de los inputs generados para las compuertas l�gicas
	 * En total cada compuerta tiene cuatro inputs diferentes 
	 */
	private int inputNumber = 0;

	/**
	 * Constructor
	 * @param _mode _mode Si se ejecuta en modo normal o en Test1 ( esto hay que mejorarlo, queda un poco sucio o desordenado )
	 * @param _testEnum El test que se va a llevar a cabo
	 * @param _enterLayerNeurons El n�mero de neuronas de la capa de entrada
	 * @param _hiddenLayerNeurons El n�mero de neuronas de la capa oculta
	 * @param _outLayerNeurons El n�mero de neuronas de la capa de salida
	 * @param _activationFunction La funci�n de activaci�n que se usar� en el algoritmo de backpropagation
	 * @param _minBackPropIterations El m�nimo de iteraciones del algoritmo de backpropagation
	 * @param _maxBadIterations El m�ximo de iteraciones del algoritmo de backpropagation
	 * @param _learningCons La constante de aprendizaje para el algoritmo de backpropagation
	 * @param _marginError Si el fitness del mejor individuo mejora este error se detiene el algoritmo
	 * @param _maxTotalIterations El m�ximo de iteraciones permitidas
	 */
	// mode, enterLayer, hiddenLayer, outputLayer, activationFunction, minBackPropIterations, maxBadIterations, learningCons, marginError, maxTotalIterations, iterationsPerFitness 
	public RNA_Problem_Test( Mode _mode, TestEnum _testEnum, int _enterLayerNeurons, int _hiddenLayerNeurons, int _outLayerNeurons, ActivationFunction _activationFunction, 
			int _minBackPropIterations, int _maxBadIterations, float _learningCons, float _marginError, int _maxTotalIterations ) 
	{
		// Se construye el padre
		// mode, enterLayer, hiddenLayer, outputLayer, activationFunction, minBackPropIterations, maxBadIterations, learningCons, marginError, maxTotalIterations, iterationsPerFitness 
		super( _mode, _enterLayerNeurons, _hiddenLayerNeurons, _outLayerNeurons, _activationFunction,
				_minBackPropIterations, _maxBadIterations, _learningCons, _marginError, _maxTotalIterations, 4 );
		
		// El tipo de test escogido
		this.testEnum = _testEnum;
		
		/**
		 * Segunda prueba del sitema de evoluci�n de las redes neuronales. Se prueba evolucionando una poblaci�n
		 * para encontrar una red neuronal que se comporte igual que una compuerta l�gica 
		 */
		if( this.testEnum == TestEnum.Test2_OR || this.testEnum == TestEnum.Test2_AND || this.testEnum == TestEnum.Test2_XOR )
		{
			this.getBestIndividual();
		}
		
	}

	/**
	 * Evoluciona una poblaci�n de redes neuronales para encontrar la que mejor se adapte al problema planteado
	 * y por tanto tenga mejor fitness en ese �mbito
	 */
	@Override
	protected void getBestIndividual() 
	{
		// Se crea un demonio evolutivo gen�rico, en este caso de tipo algoritmo gen�tico
		// que evoluciona una red neuronal que ofrezca estrategias al movimiento de Ms.Pacman
		EvoDemon<ArrayList<Number>> my_ED = new ED_AG( 
				this, 		 								  // El �mbito del poblema que se quiere resolver
				this.marginError,							  // El margen de error respecto a la soluci�n requerida
				50, 										  // El tama�o de la poblaci�n estable para resolver el problema
				ED_AG.SelectionType.Tournament,               // El m�todo de selecci�n usado 
				ED_AG.CroosoverType.Uniforme,                 // El m�todo de croosover usado
				ED_AG.MutationType.Uniforme,				  // El m�todo de mutaci�n usado
				ED_AG.ReplacementType.Worst,				  // El m�todo de reemplazo usado
				1000,										  // El m�nimo de iteraciones
				10,											  // El m�ximo de iteraciones fallidas permitidas
				2											  // El n�mero de inmigraciones por iteraci�n
			); 			  
		
		// Se inicia el demonio evolutivo
		my_ED.evolutiveProcess();
		// Si se sale del bucle del demonio evolutivo es que se ha encontrado la soluci�n
		// habr�a que hacer aqu� un control de errores
		this.bestIndividualFound = true; 
		
	}
	
	/**
	 * Testea un individuo calculando las salidas que produce respecto a unas entradas
	 * y compar�ndolas con las esperadas. Muestra por pantalla el resultado
	 */
	@Override
	protected void testIndividual(Individual<ArrayList<Number>> _individual)
	{
		// Se castea _individual a red neuronal
		// Aqu� habr�a que controlar las posibles excepciones
		AG_Individual_RNA individual_RNA = ( ( AG_Individual_RNA ) _individual );
		
		if( this.testEnum == TestEnum.Test2_OR || this.testEnum == TestEnum.Test2_AND || this.testEnum == TestEnum.Test2_XOR )
		{
			System.out.println("-----------------------------------------" );
			System.out.println("-----------TEST DE 10 VALORES------------" );
			System.out.println("-----------------------------------------" );
			// Teteamos la red neuronal con 10 valores aleatorios
			for( int x=0; x<10; x++ )
			{
				ArrayList<Float> trainingData = this.generateTrainingData();
				// Extraemos tan solo los inputs
				ArrayList<Float> input = new ArrayList<Float>();
				int count = 0;
				for( Float value: trainingData )
				{
					if( count < individual_RNA.enterLayer.size() )
						input.add(value);
					count++;
				}
				// Se muestran por pantalla todos los input del trainingData
				for( Float value: input )
				{
					System.out.println("InputValue - " + x + " => " + value );
				}
				System.out.println("Salida esperada => " + trainingData.get(individual_RNA.enterLayer.size()) );
				ArrayList<Float> result = this.getOutput( individual_RNA, input );
				System.out.println("Salida devuelta => " + result.get(0) );
				System.out.println("-----------------------------------------" );
			}
		}
	}
	
	/**
	 * Genera las tuplas de entrenamiento para resolver las compuertas l�gicas pero extendi�ndolas a varios inputs
	 * en vez de a dos solos ( es decir, por ejemplo, se podr�a generar un input que fuera ( 0, 1, 0, 1 ) y el resultado de la compuerta l�gica XOR 
	 * ser�a 0.
	 * @return el arraylist con los valores de entrenamiento para una compuerta, de entrada aleatorios con su correspondiente salida
	 */
	@Override
	protected ArrayList<Float> generateTrainingData() 
	{
		// En este array se a�aden tanto las entradas como las salidas esperadas
		ArrayList<Float> trainingData = new ArrayList<Float>();

		this.inputNumber ++;
		// En funci�n del contador de input se genera una entrada u otra
		if( this.inputNumber == 1 )
		{
			trainingData.add(0.f);
			trainingData.add(0.f);
		}else if( this.inputNumber == 2 )
		{
			trainingData.add(0.f);
			trainingData.add(1.f);
		}else if( this.inputNumber == 3 )
		{
			trainingData.add(1.f);
			trainingData.add(0.f);
		}else if( this.inputNumber == 4 )
		{
			trainingData.add(1.f);
			trainingData.add(1.f);
			this.inputNumber = 0;
		}
		
		// Se generan la salida esperada para la compuerta l�gica OR
		if( this.testEnum == TestEnum.Test2_OR )
		{
			// Aplicando la compuerta l�gica OR sobre los datos de entrada calculamos la salida de la �nica neurona de salida
			for( Float value: trainingData )
			{
				// Con que un valor valga 1 la compuerta debe devolver 1
				if( value == 1 )
				{
					trainingData.add(1.f);
					return trainingData;
				}
			}
			trainingData.add(0.f);
			return trainingData;
		}
		else if( this.testEnum == TestEnum.Test2_AND )
		{
			// Aplicando la compuerta l�gica OR sobre los datos de entrada calculamos la salida de la �nica neurona de salida
			for( Float value: trainingData )
			{
				// Con que un valor valga 0 la compuerta debe devolver 0
				if( value == 0 )
				{
					trainingData.add(0.f);
					return trainingData;
				}
			}
			trainingData.add(1.f);
			return trainingData;
		}
		else if( this.testEnum == TestEnum.Test2_XOR )
		{
			boolean hasCero = false;
			boolean hasOne = false;
			// Aplicando la compuerta l�gica OR sobre los datos de entrada calculamos la salida esperada de la �nica neurona de salida
			for( Float value: trainingData )
			{
				// Si existe un valor 0 y un valor 1 el resultado tiene que ser cero
				if( value == 0 )
				{
					hasCero = true;
				}
				else if( value == 1 )
				{
					hasOne = true;
				}
			}
			if( hasCero && hasOne )
				trainingData.add(0.f);
			else trainingData.add(1.f);
			return trainingData;
		}
		return null;
	}

}
