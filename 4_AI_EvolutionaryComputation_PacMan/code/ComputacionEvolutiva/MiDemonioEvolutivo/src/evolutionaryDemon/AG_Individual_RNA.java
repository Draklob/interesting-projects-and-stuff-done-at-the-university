package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Random;


/**
 * Esta clase guarda los datos que codifican una red neuronal
 * Hereda de AG_Individual
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class AG_Individual_RNA extends AG_Individual
{
	/**
	 * La capa de neuronas que reciben las entradas
	 */
	ArrayList<Neuron> enterLayer;
	/**
	 * La capa de neuronas oculta
	 */
    ArrayList<Neuron> hiddenLayer;
    /**
     * La capa de neuronas de salida
     */
    ArrayList<Neuron> outputLayer;
    /**
     * El bias que le aplicamos a todas las neuronas que forman la red neuronal
     * En principio tomar� el valor 1, luego se podr� ir modificando para ajustar la red neuronal
     */
    float bias = 0.5f;
    /**
     * Para saber si est� entrenada o no y utilizar por tanto los pesos finales o los normales
     * para calcular el output
     */
    boolean trained = false;
    
	/**
	 * Constructor
	 * @param _RNA_Problem La clase que define el problema que se trata de solucionar con la red neuronal
	 */
	public AG_Individual_RNA( RNA_Problem _RNA_Problem ) 
	{
		
		// En el padre se genera aleatoriamente una cadena de ceros y unos de longitud suficiente
		// para poder codificar todas las redes neuronales posibles que se pueden llegar a formar con el n�mero indicado de 
		// neuronas de entrada, capa oculta y capa de salida
		super( _RNA_Problem );
		// Se inicializa la red neuronal
		initialize( _RNA_Problem );
		
	}// Fin del constructor
	
	/**
	 * Constructor copia
	 * @param individual El individuo que se usa para construir este
	 * @param _RNA_Problem El problema en el que se construye el individuo
	 */
	public AG_Individual_RNA( RNA_Problem _RNA_Problem, AG_Individual_RNA individual ) 
	{
		// Se invoca el constructor copia del padre
		super( individual );
		// Se inicializa la red neuronal
		initialize( _RNA_Problem );
	}
	
	/**
	 * Se crea la base que tendr�n todas las redes neuronales por igual
	 * en funci�n del tipo de problema que se establezca
	 * Es decir, se crea la base para luego en funcion del genoma poder calcular el fenotipo
	 * @param _RNA_Problem El �mbito del problema en el que se inicializa el individuo
	 */
	private void initialize( RNA_Problem _RNA_Problem )
	{
		
		// Inicializamos los arrays de neuronas para las diferentes capas
		enterLayer = new ArrayList<Neuron>();
		hiddenLayer = new ArrayList<Neuron>();
		outputLayer = new ArrayList<Neuron>();
		
		// La neurona para el bias tiene un id = 0;
		// Esta neurona ser� un input para todas las neuronas, pero en cada 
		// una de ellas estar� conectada con un peso diferente
		Neuron biasNeuron = new Neuron( 0 );
		biasNeuron.output = this.bias;
				
		// Los id de las neuronas se forma por el n�mero de capa m�s el n�mero que le corresponda dentro de dicha capa
		// ( por ejemplo 103 ser� la neurona 3 de la capa de entrada )
		// enterLayer -> 100 - hiddenLayer -> 200 - outLayer -> 300	
		int neuronID = 100;
		
		// Se crean las neuronas de la capa de entrada
		for( int i=0; i<_RNA_Problem.enterLayerBits; i++ )
		{
			Neuron neuron = new Neuron( neuronID + i );
			enterLayer.add( neuron );
		}
		
		// enterLayer -> 100 - hiddenLayer -> 200 - outLayer -> 300
		neuronID = 200;
		
		// Se crean las neuronas de la capa oculta
		for( int i=0; i<_RNA_Problem.hiddenLayerBits; i++ )
		{
			Neuron neuron = new Neuron( neuronID + i );
			if( _RNA_Problem.mode == Abstract_Problem.Mode.TEST1 )
				neuron.addInputConnection( biasNeuron, 1.f ); // Le conectamos el bias con peso 1
			else neuron.addInputConnection( biasNeuron, getRandom() ); // Le conectamos el bias con peso random
			hiddenLayer.add( neuron );
		}
		
		// enterLayer -> 100 - hiddenLayer -> 200 - outLayer -> 300
		neuronID = 300;
		
		// Se crean las neuronas de la capa de salida
		for( int i=0; i<_RNA_Problem.outLayerBits; i++ )
		{
			Neuron neuron = new Neuron( neuronID + i );
			if( _RNA_Problem.mode == Abstract_Problem.Mode.TEST1 )
				neuron.addInputConnection( biasNeuron, 1.f ); // Le conectamos el bias con peso 1
			else neuron.addInputConnection( biasNeuron, getRandom() ); // Le conectamos el bias con peso random
			outputLayer.add( neuron );
		}
		
	}
	
	/**
	 * Calcula el fenotipo de la red neuronal, es decir, decodifica la cadena de ceros y unos del genoma,
	 * estableciendo las conexiones entre las diferentes neuronas de las diferentes capas
	 * en funci�n de estos ceros y unos, y asign�ndoles valores aleatorios para los pesos entre 0 y 1
	 * @param _RNA_Problem El �mbito del problema en el que se calcula el fenotipo del individuo
	 */
	public void calculatePhenotype( RNA_Problem _RNA_Problem )
	{
		// Este contador nos informa que bit estamos analizando de la cadena que forma el genoma de la red neuronal para decodificarla
		int bitCount = 0;
				
		// Se analizan los bits de las conexiones directas
		// Por cada neurona de entrada se analizan sus posibles conexiones con
		// las neuronas de salida ( 
		for( int i=0; i<enterLayer.size(); i++ )
		{
			for( int j=0; j<outputLayer.size(); j++ )
			{
				// Cuando valga 1 habr� conexi�n entre la neurona de entrada que indique i
				// y la neurona de salida que indique j
				if( this.genoma.get(bitCount).intValue() == 1 )
				{
					// Cuando haya conexi�n, si se est� en modo TESTING, se inicializar� el peso con un valor 1
					if( _RNA_Problem.mode == Abstract_Problem.Mode.TEST1 )
						this.outputLayer.get(j).addInputConnection( this.enterLayer.get(i), 1.f );
					else
					{
						// Cuando haya conexi�n se inicializar� el peso con un valor aleatorio entre 0 y 1 
						this.outputLayer.get(j).addInputConnection( this.enterLayer.get(i), getRandom() );
					}
				}
				bitCount++;
			}
		}
		
		// Se analizan los bits de las conexiones indirectas
		// Por cada neurona de la capa de entrada analizamos las posibles conexiones que 
		// pasen por cada neurona de la capa oculta hacia las neuronas de la capa de salida
		for( int i=0; i<enterLayer.size(); i++)
		{
			for( int j=0; j<hiddenLayer.size(); j++)
			{
				for( int k=0; k<outputLayer.size(); k++)
				{
					// Cuando valga 1 habr� conexi�n entre la neurona de entrada que indique i
					// y la neurona de la capa oculta que indique j
					// y tambi�n conexi�n, entre la neurona oculta que indica j
					// y la neurona de la capa de salida que indique k
					if( this.genoma.get(bitCount).intValue() == 1 )
					{
						if( _RNA_Problem.mode == Abstract_Problem.Mode.TEST1 )
						{
							// Cuando haya conexi�n, si se est� en modo TESTING, se inicializar�n los pesos con un valor 1 
							this.hiddenLayer.get(j).addInputConnection( this.enterLayer.get(i), 1.f );
							this.outputLayer.get(k).addInputConnection( this.hiddenLayer.get(j), 1.f );
						}
						else
						{
							// Cuando haya conexi�n se inicializar�n los pesos con un valor aleatorio entre 0 y 1 
							this.hiddenLayer.get(j).addInputConnection( this.enterLayer.get(i), getRandom() );
							this.outputLayer.get(k).addInputConnection( this.hiddenLayer.get(j), getRandom() );
						}
					}
					bitCount++;
				}
			}
		}

	}
	
	/**
	 *  Devuelve un n�mero aleatorio de tipo float entre -1 y 1
	 *  Usamos esta funci�n para establecer pesos aleatorios iniciales en cada conexi�n
	 * @return Float - [-1;1[
	 */
    float getRandom() 
    {
    	Random rand = new Random();
    	return rand.nextFloat() * 2 - 1; 
    }
	


}
