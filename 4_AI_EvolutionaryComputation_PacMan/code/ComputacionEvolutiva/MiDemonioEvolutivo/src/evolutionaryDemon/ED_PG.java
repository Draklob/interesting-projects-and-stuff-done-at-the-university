package evolutionaryDemon;

import java.util.ArrayList;

/**
 * Clase que representa un algoritmo evolutivo que se usar�a para evolucionar una poblaci�n de individuos cuyo genoma fuera un programa.
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class ED_PG extends EvoDemon<ArrayList<Float>> 
{

	/**
	 * Inicia la poblaci�n
	 */
	@Override
	protected void populationGeneration() 
	{
		// TODO Auto-generated method stub
		
	}

	/**
	 * Selecciona unos un grupo de individuos de la poblaci�n actual bas�ndose en alg�n algoritmo concreto
	 * Estos algoritmos pueden ser : Ranking, Tournament, Roulette
	 * @return Devuelve la lista de individuos seleccionados
	 */
	@Override
	protected ArrayList<Individual<ArrayList<Float>>> selection() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Cruza varios individuos para obtener otros nuevos
	 * @param parents Trabaja sobre la poblaci�n resultante del m�todo de selecci�n
	 * @return Devuelve la poblaci�n resultante despu�s de hacer los cruces entre individuos
	 */
	@Override
	protected ArrayList<Individual<ArrayList<Float>>> crossover (ArrayList<Individual<ArrayList<Float>>> parents) 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Muta al azar varios individuos para obtener otros nuevos
	 * @param parents Trabaja sobre la poblaci�n resultante del m�todo de crossover
	 * @return Devuelve la poblaci�n resultante despu�s de hacer las mutaciones de individuos
	 */
	@Override
	protected ArrayList<Individual<ArrayList<Float>>> mutation( ArrayList<Individual<ArrayList<Float>>> parents ) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Remplaza los individuos modificados o evolucionados en la poblaci�n actual 
	 * @param newGeneration Redibe el grupo de individuos modificados despu�s de ser mutados
	 */
	@Override
	protected void replacement(ArrayList<Individual<ArrayList<Float>>> newGeneration) 
	{
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Calcula el fitness de un individuo concreto
	 * @param individual
	 */
	@Override
	protected void calculateFitness(Individual<ArrayList<Float>> individual) 
	{
		// TODO Auto-generated method stub
	}
	
	/**
	 * Establece la condici�n de parada para terminar la evoluci�n
	 * @return Devuelve True si se cumple la condici�n de parada o convergencia
	 */
	@Override
	protected boolean converge() 
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Ordena la poblaci�n en funci�n del fitness de menor a mayor ( de mejor a peor )
	 */
	@Override
	protected void orderPopulation()
	{
	
	}


	

	
		
	
}
