package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Random;

/**
 * Hereda de RNA_Problem e incluye aquellos m�todos espec�ficos y necesarios para abordar
 * el problema de devolver un movimiento adecuado para Pacman usando redes neuronales 
 * y que sea similar al que devolver�a la funci�n de entrenamiento asignada
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class RNA_Problem_Pacman extends RNA_Problem
{
	
	/**
	 * La funci�n que se usa para el entrenamiento de la red neuronal
	 */
	public enum TraningFunction{ CustomFunction, CustomFunction_Version2, CustomFunctionHiperbolic, StarterPacmanFunction };
	/**
	 * La funci�n que se usa para el entrenamiento de la red neuronal
	 */
	public TraningFunction traningFunction;
	
	
	/**
	 * Constructor
	 * @param _traningFunction Recibe el tipo de funci�n de entrenamiento que se quiera usar
	 */
	public RNA_Problem_Pacman( TraningFunction _traningFunction) 
	{
		
		// Se pasan los par�metros apropiados para la creaci�n de las redes neuronales adpatadas 
		// al problema de resolver el movimiento de Pacman
		// mode, enterLayer, hiddenLayer, outputLayer, activationFunction, minBackPropIterations, maxBadIterations, learningCons, marginError, maxTotalIterations, iterationsPerFitness 
		super( Abstract_Problem.Mode.NORMAL, 8, 14, 5, ActivationFunction.Hyperbolic, 2000, 20, 0.1f, 0.00005f, 10000, 100 );
		
		this.traningFunction = _traningFunction;
		
	}// Fin del constructor 
	
	
	/**
	 * Inicializa las redes neuronales y las evoluciona buscando la que se adapate mejor a las tuplas
	 * de entrenamiento que le proporcionamos, que simulan las que se le pasar�n desde el juego de MsPacman
	 * Para ello crea un algoritmo evolutivo que a su vez har� uso del algoritmo de backpropagation 
	 * para ajustar cada red neuronal
	 */
	@Override
	public void getBestIndividual()
	{
		// Se crea un demonio evolutivo gen�rico, en este caso de tipo algoritmo gen�tico
		// que evoluciona una red neuronal que ofrezca estrategias al movimiento de Ms.Pacman
		EvoDemon<ArrayList<Number>> my_ED = new ED_AG( 
				this, 		 								  // El �mbito del poblema que se quiere resolver
				this.marginError,							  // El margen de error respecto a la soluci�n requerida
				40, 										  // El tama�o de la poblaci�n estable para resolver el problema
				ED_AG.SelectionType.Tournament,               // El m�todo de selecci�n usado 
				ED_AG.CroosoverType.Bernoulli,                // El m�todo de croosover usado
				ED_AG.MutationType.Uniforme,				  // El m�todo de mutaci�n usado
				ED_AG.ReplacementType.Worst,			  	  // El m�todo de reemplazo usado
				1000,										  // El m�nimo de iteraciones
				40,											  // El m�ximo de iteraciones fallidas permitidas
				2											  // El n�mero de inmigraciones por iteraci�n
			); 			  
		
		// Se inicia el demonio evolutivo
		my_ED.evolutiveProcess();
		// Si se sale del bucle del demonio evolutivo es que se ha encontrado la soluci�n
		// habr�a que hacer aqu� un control de errores
		this.bestIndividualFound = true; 

	}
	
	/**
	 * Testea un individuo calculando las salidas que produce respecto a unas entradas
	 * y compar�ndolas con las esperadas. Muestra por pantalla el resultado
	 */
	@Override
	protected void testIndividual(Individual<ArrayList<Number>> _individual)
	{
		
	}
	
	/**
	 * Genera unas entradas aleatorias con sus correspondientes salidas esperadas
	 * para entrenar cada red neuronal y poder calcular as� su fitness mediante backpropagation
	 * @return Un array con ocho valores aleatorios para las entradas y cinco para las salidas esperadas
	 * Todos los valores estar�n en el rango [ 0, 1 [
	 */
	@Override
	public ArrayList<Float> generateTrainingData()
	{
		// Se crea un objeto random para generar aleatoriedad en los datos
		Random rd = new Random();
		ArrayList<Float> trainingData = new ArrayList<Float>();
		// Se a�aden 8 valores aleatorios de tipo float comprendidos entre 0 y 1
		// Los cuatro primeros corresponden a la distancia a la que est�n los fantasmas de pacman
		// y los otros cuatro al tiempo que le quedan por ser comestibles ( "cero" significa que ya no son comestibles )
		int count = 0;
		while( count < 8 )
		{
			// Apa�o que hay que testear para que los tiempos de los fantasmas tengan preferentemente valor 0
			if( count > 3 )
			{
				if( rd.nextFloat() >= 0.5f ) trainingData.add( 0.f );
				else trainingData.add( rd.nextFloat() );
			}
			else trainingData.add( rd.nextFloat() );
			count++;
		}
		
		// Se calculan las salidas para las entradas aleatorias
		// Esto se hace en una funci�n aparte para poder probarla directamente con el controlador
		ArrayList<Float> outputs = new ArrayList<Float>();
		if( this.traningFunction == TraningFunction.CustomFunction )
		{
			outputs = this.getCustomTrainingOutputs(trainingData);
		}
		else if( this.traningFunction == TraningFunction.CustomFunction_Version2 ) 
		{
			//outputs = this.getTrainingOutputsHiperbolic( this.getCustomTrainingOutputs(trainingData) );
			outputs = this.getCustomTrainingOutputs_version_2( trainingData );
		}
		else if( this.traningFunction == TraningFunction.StarterPacmanFunction )
		{
			outputs = this.getStarterPacmanTrainingOutputs(trainingData);
		}
		else if( this.traningFunction == TraningFunction.CustomFunctionHiperbolic ) 
		{
			//outputs = this.getTrainingOutputsHiperbolic( this.getCustomTrainingOutputs(trainingData) );
			outputs = this.getTrainingOutputsHiperbolic( trainingData );
		}
			
		
		// Agregamos las salidas al array de TrainingData
		for( Float value: outputs )
		{
			trainingData.add(value);
		}
		
		return trainingData;
	}
	
	/**
	 * Si se selecciona la opci�n de Outputs Hiperbolic, pasamos las salidas de la funci�n de entrenamiento custom
	 * por la funci�n hiperb�lica para asemejar los valores de salida a la funci�n de activaci�n de las neuronas
	 * @param input Los valores de la tupla de entrenamiento para el input
	 * @return Devuelve el output para esa tupla de entrenamiento pero modificados por la funci�n de entrenamiento
	 */
	public ArrayList<Float> getTrainingOutputsHiperbolic( ArrayList<Float> input )
	{
		// Primero se obtienen las salidas normales que produce la funci�n getCustomTrainingOutputs 
		ArrayList<Float> outputs = this.getCustomTrainingOutputs(input);
		
		// Y luego esas salidas se modifican con la funci�n hiperb�lica
		int index = 0;
		for( float value: outputs )
		{
			outputs.set(index, Utils.hyperbolic(value));
			index++;
		}
		// Se devuelven las salidas modificadas con la funci�n hiperb�lica
		return outputs;
	}
	
	/**
	 * Se devuelven 5 valores con las salidas esperadas en funci�n del array de entradas
	 * 1 huye del fantasma y -1 va hacia el fantasma
	 * @param input el array de 8 valores de entrada
	 * @return Devuelve un ArrayList de tipo float con las salidas esperadas 
	 * ( o sea, c�mo se activa cada neurona de la capa de salida en funci�n de las entradas pasadas por par�metro )
	 */
	public ArrayList<Float> getCustomTrainingOutputs( ArrayList<Float> input )
	{
		// Se calculan las salidas para las entradas pasadas por par�metro
		ArrayList<Float> outputs = new ArrayList<Float>();
		
		// Se calcula cu�nto se deber�a de activar cada neurona de la capa de salida
		// o sea, cada estrategia, en funci�n del array de valores pasados por par�metro ( input )
		float EG1 = 0.f;
		if( input.get(4) <= 0.f )
		{
			EG1 = (float) ( 1 - Math.pow(input.get(0), 0.3) ); // Cuando no est� edible ( en funci�n de la distancia al fantasma )
			
		}
		else EG1 = (float) ( ( ( 2 * ( Math.pow(input.get(4), 0.3) )  ) - 1 ) * ( Math.pow(input.get(0), 3) - 1 ) ); // Cuando est� edible ( en funci�n del tiempo que le queda de ser comestible y la distancia )
	
		outputs.add( EG1 );
		
		float EG2 = 0.f;
		if( input.get(5) <= 0.f )
		{
			EG2 = (float) ( 1 - Math.pow(input.get(1), 0.3) ); // Cuando no est� edible
		}
		else EG2 = (float) ( ( ( 2 * ( Math.pow(input.get(5), 0.3) )  ) - 1 ) * ( Math.pow(input.get(1), 3) - 1 ) ); // Cuando est� edible
		
		outputs.add( EG2 );
		
		float EG3 = 0.f;
		if( input.get(6) <= 0.f )
		{
			EG3 = (float) ( 1 - Math.pow(input.get(2), 0.3) ); // Cuando no est� edible
		}
		else EG3 = (float) ( ( ( 2 * ( Math.pow(input.get(6), 0.3)  )  ) - 1 ) * ( Math.pow(input.get(2), 3) - 1 ) ); // Cuando est� edible
		
		outputs.add( EG3 );
		
		float EG4 = 0.f;
		if( input.get(7) <= 0.f )
		{
			EG4 = (float) ( 1 - Math.pow(input.get(3), 0.3) ); // Cuando no est� edible
		}
		else EG4 = (float) ( ( ( 2 * ( Math.pow(input.get(7), 0.3) )  ) - 1 ) * ( Math.pow(input.get(3), 3) - 1 ) ); // Cuando est� edible
		
		outputs.add( EG4 );
		
		// La estrategia de ir a por la p�ldora
		// Se calcula la diferencia menor, tan solo si no hay una estrategia definida con respecto a un fantasma se activar� la de ir a por la p�ldora m�s cercana
		float EP = 1 - Math.abs(EG1);
		if( ( 1 - Math.abs(EG2) ) < EP ) EP = 1 - Math.abs(EG2);
		if( ( 1 - Math.abs(EG3) ) < EP ) EP = 1 - Math.abs(EG3);
		if( ( 1 - Math.abs(EG4) ) < EP ) EP = 1 - Math.abs(EG4);
		
		outputs.add( EP );
		
		return outputs;
	}
	
	/**
	 * Se devuelven 5 valores con las salidas esperadas en funci�n del array de entradas
	 * 1 huye del fantasma y -1 va hacia el fantasma
	 * @param input el array de 8 valores de entrada
	 * @return Devuelve un ArrayList de tipo float con las salidas esperadas 
	 * ( o sea, c�mo se activa cada neurona de la capa de salida en funci�n de las entradas pasadas por par�metro )
	 */
	public ArrayList<Float> getCustomTrainingOutputs_version_2( ArrayList<Float> input )
	{
		// Se calculan las salidas para las entradas pasadas por par�metro
		ArrayList<Float> outputs = new ArrayList<Float>();
		
		// Se calcula cu�nto se deber�a activar cada neurona de la capa de salida
		// o sea, cada estrategia, en funci�n del array de valores pasados por par�metro ( input )
	
		float EG1 = (float) ( ( 1 - Math.pow(input.get(0), 1) ) * ( 1 - 2 * Math.pow(input.get(4), 1) ) ); // Cuando no est� edible ( en funci�n de la distancia al fantasma )
		outputs.add( EG1 );
		
		float EG2 = (float) ( ( 1 - Math.pow(input.get(1), 1) ) * ( 1 - 2 * Math.pow(input.get(5), 1) ) ); // Cuando no est� edible ( en funci�n de la distancia al fantasma )
		outputs.add( EG2 );
		
		float EG3 = (float) ( ( 1 - Math.pow(input.get(2), 1) ) * ( 1 - 2 * Math.pow(input.get(6), 1) ) ); // Cuando no est� edible ( en funci�n de la distancia al fantasma )
		outputs.add( EG3 );
		
		float EG4 = (float) ( ( 1 - Math.pow(input.get(3), 1) ) * ( 1 - 2 * Math.pow(input.get(7), 1) ) ); // Cuando no est� edible ( en funci�n de la distancia al fantasma )
		outputs.add( EG4 );
		
		// La estrategia de ir a por la p�ldora
		// Se calcula la diferencia menor, tan solo si no hay una estrategia definida con respecto a un fantasma se activar� la de ir a por la p�ldora m�s cercana
		float EP = 1 - Math.abs(EG1);
		if( ( 1 - Math.abs(EG2) ) < EP ) EP = 1 - Math.abs(EG2);
		if( ( 1 - Math.abs(EG3) ) < EP ) EP = 1 - Math.abs(EG3);
		if( ( 1 - Math.abs(EG4) ) < EP ) EP = 1 - Math.abs(EG4);
		
		outputs.add( EP );
		
		return outputs;
	}
	
	/**
	 * Se devuelven 5 valores con las salidas que se corresponden con el input pasado por par�metro
	 * Trata de emular el comportamiento del controlador de StarterPacman
	 * 1 huye del fantasma y -1 va hacia el fantasma
	 * @param input el array de 8 valores de entrada
	 * @return el array de 5 valores que se corresponden con los 8 de entrada
	 */
	public ArrayList<Float> getStarterPacmanTrainingOutputs( ArrayList<Float> input )
	{
		// Se calculan las salidas para las entradas pasadas por par�metro
		ArrayList<Float> outputs = new ArrayList<Float>();
		
		// Se inician todos los valores a cero, ya que el controlador tipo StarterPacman tan solo devuelve
		for(int i=0; i<5; i++)
		{
			outputs.add(0.f);
		}
	
		// La distancia m�nima a la que huye de los fantasmas
		float MIN_DISTANCE = 0.2f;
		
		// Strategy 1: if any non-edible ghost is too close (less than MIN_DISTANCE), run away from him
		// Se recorren todos los fantasmas para ver si hay que huir de alguno de ellos
		int ghostIndex = 0;
		while( ghostIndex < 4 )
		{
			if( input.get( ghostIndex + 4 ) <= 0 ) 
			{
				if( input.get( ghostIndex ) <= MIN_DISTANCE ) 
				{
					outputs.set(ghostIndex, 1.f);
					return outputs;
				}
			}
			ghostIndex++;
		}
		
		// Strategy 2: find the nearest edible ghost and go after him 
		float minDistance = 2.f;
		int ghostCloser = -1;
		// Si no huimos de ning�n fantasma, recorremos otra vez todos los fantasmsa
		// para ver si pacman se puede comer alguno
		ghostIndex = 0;
		while( ghostIndex < 4 )
		{
			if( input.get( ghostIndex + 4 ) > 0 ) // Es comestible
			{
				// Se busca de todos los comestibles el que est� m�s cercano
				float distance = input.get( ghostIndex );
 				if( distance <= minDistance ) 
				{
					minDistance=distance;
					ghostCloser=ghostIndex;
				}
			}
			ghostIndex++;
		}
		
		if(ghostCloser!=-1)
		{
			// we found an edible ghost
			outputs.set(ghostCloser, -1.f);
			return outputs;
		}
			
		// Strategy 3: go after the pills and power pills
		outputs.set(4, 1.f);
		return outputs;
		
	}

	
}
