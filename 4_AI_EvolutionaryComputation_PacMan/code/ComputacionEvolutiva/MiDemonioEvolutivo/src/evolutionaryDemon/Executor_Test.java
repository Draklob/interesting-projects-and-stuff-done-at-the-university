package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import evolutionaryDemon.RNA_Problem.ActivationFunction;


/**
 * Clase para probar el demonio evolutivo antes de ejecutarlo desde Pacman
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class Executor_Test 
{
	//El objeto de la clase Scanner para poder leer del teclado
	static Scanner sc;
	

	/**
	 * Ejecuta la aplicaci�n
	 * @param args Argumentos de ejecuci�n
	 */
	public static void main(String[] args)
	{
		// Inicializo la clase scanner:
		sc = new Scanner( System.in );
		
		// Se invoca el men�
		menu();
		
	}
	
	/** 
     *	El men� principal para la configuraci�n de la ejecuci�n
     */
	public static void menu()
	{
		
		/*variable para recoger la opcion elegida en el menu principal*/
		int option;	
		
		do{
			/*Menu principal del programa*/
			System.out.println( "\n\n____________________________________________________________________________________\n\n" );
			System.out.println( "                       Men� de Opciones de Tests\n" );
			System.out.println( "   1. Se aplica el algoritmo gen�tico para resolver ecuaciones formadas por monomios multivariable" );
			System.out.println( "   2. Test simple para chequear que las redes neuronales se construyen de manera correcta" );
			System.out.println( "   3. Para crear una compuerta OR, AND � XOR " );
			System.out.println( "   5. Salir\n" );
			
			option = sc.nextInt();
		
			switch( option )
			{
				case 1:// Opcion 1 - Chequea que se construyen bien las redes neuronales
					polynomialTest();
					break;
				case 2:// Opcion 2 - Chequea que se construyen bien las redes neuronales
					simpleTestRNA();
					break;
				case 3:// Opcion 2 - Chequea la evoluci�n de una poblaci�n de redes neuronales 
					// para conseguir simular el funcionamiento de una compuerta l�gica 
					testLogicGate();
					break;
				case 5:// Opcion 5 - Crear Tablero usando Aut�matas Celulares Simples
			}
				
		}while( option != 5 );//Bucle infinito hasta que introduzca 5 y salga del programa
		
	}
	
	/**
	 * Testea el algoritmo gen�tico resolviendo ecuaciones de monomios multivariable
	 */
	public static void polynomialTest()
	{
		// Se crea la clase que genera y evoluciona una poblaci�n de arrays de valores
		// que buscan la soluci�n m�s �ptima a una ecuaci�n con polinomios de varias variables
		Polynomial_Problem myProblem = new Polynomial_Problem( Abstract_Problem.Mode.NORMAL );		
	
		// Se busca el mejor individuo que se adapte al planteamiento del problema
		myProblem.getBestIndividual();
	}
	
	/**
	 * Se ponen a prueba el correcto funcionamiento de diferentes m�todos y algoritmos relativos a las redes neuronales y su entrenamiento
	 */
	public static void simpleTestRNA ()
	{
		/**
		 * Primera prueba en la que se comprueba el correcto funcionamiento de la construcci�n de una red neuronal  
		 * En el modo Test1 se establecen todas las conexiones posibles de la red neuronal y todos los pesos de dichas conexiones se inicializan a 1
		 */
		// Variables que indicar�n el n�mero de neuronas de cada capa
		int inputNeuronsCount = 0;
		int hiddenNeuronsCount = 0;
		int outputNeuronsCount = 0;
		
		System.out.println( "\n\n____________________________________________________________________________________\n\n" );
		System.out.println( "                       Simple Test\n" );
		System.out.println( "Se establecen todas las conexiones posibles y todos los pesos toman el valor entero 1, inclusive el bias.\n" );
		
		// Men� para configurar la capa de entrada
		inputNeuronsCount = settingLayersMenu( "entrada", 1, 5 );
		
		// Men� para configurar la capa oculta
		hiddenNeuronsCount = settingLayersMenu( "oculta", 1, 13 );
		
		// Men� para configurar la capa oculta
		outputNeuronsCount = settingLayersMenu( "salida", 1, 5 );
				
		System.out.println( "____________________________________________________________________________________\n\n" );
	
		// Se muestra el men� para elegir la funci�n de activaci�n
		RNA_Problem.ActivationFunction activationFunctionSelected = activationFunctionMenu();
	
		// Al crearse en modo TEST1, se establecer�n todas las conexiones posibles y los pesos de dichas conexiones se inicializar�n a uno para facilitar el c�lculo de la salida.
		// De esta forma ser� f�cil cotejar los resultados obtenidos en los outputs con los esperados.
		// mode, testEnum, enterLayer, hiddenLayer, outputLayer, activationFunction, minBackPropIterations, maxBadIterations, learningCons, marginError, maxTotalIterations 
		RNA_Problem_Test myRNA_Problem_Test = new RNA_Problem_Test( Abstract_Problem.Mode.TEST1, RNA_Problem_Test.TestEnum.Test1, 
				inputNeuronsCount, hiddenNeuronsCount, outputNeuronsCount, activationFunctionSelected, 1000, 1, 0.1f, 0.0005f, 1000 );	
		
		// Se crea un individuo para este tipo de problema neuronal
		AG_Individual_RNA myAG_Individual_RNA = new AG_Individual_RNA( myRNA_Problem_Test );
		// Se calcula el fenotipo del individuo en base al genoma reci�n creado
		myAG_Individual_RNA.calculatePhenotype( myRNA_Problem_Test );
		
		// Bucle para poder testear el individuo con diferentes valores
		int option = 0;
		do{
			System.out.println( "____________________________________________________________________________________\n\n" );
			System.out.println( "                       Elija una opci�n\n" );
			System.out.println( "   1. Testear" );
			System.out.println( "   2. Salir\n" );
			
			option = sc.nextInt();
			System.out.println( "" );
		
			switch( option )
			{
				case 1:// Opcion 1
					/*Testeando*/
					System.out.println( "____________________________________________________________________________________\n\n" );
					System.out.println( "                       Testeando\n" );
					// Se solicita un input para comprobar su correcto funcionamiento
					ArrayList<Float> input = new ArrayList<Float>();
					int count = 0;
					for( int x=0; x<inputNeuronsCount; x++)
					{
						System.out.println( "Introduzca un valor para la entrada numero : " + count + "\n" );
						float value = sc.nextFloat();
						input.add(value);
						count++;
					}
					// Se calcula el output para esas entradas
					System.out.println( "\n                       Salida\n" );
					ArrayList<Float> outputs = myRNA_Problem_Test.getOutput( myAG_Individual_RNA, input);
					count = 0;
					for( Float value: outputs )
					{
						// Se muestra finalmente el valor del output ( para que se pueda ver si corresponde con el esperado )
						System.out.println( "output: " + count + " => " + value );
						count++;
					}
					
					System.out.println();
					break;
				case 2:// Opcion 2 - Vuelve al men� principal
					break;
			}
			
		}while( option != 2 );//Bucle infinito hasta que introduzca 2 
	}
	
	/**
	 * Evoluciona una poblaci�n de redes neuronales para obtener una que se comporte como una compuerte l�gica
	 * Es complicado ajustarla bien usando la funci�n de activaci�n hiperb�lica, que es la que usa Pacman
	 */
	public static void testLogicGate()
	{
		// Para una compuerta l�gica el requisito es que la capa de salida solo puede tener una neurona.
		// Solicitamos el n�mero de neuronas de la capa de entrada y de la capa oculta
		int inputNeuronsCount = 2;
		int hiddenNeuronsCount = 0;
		int outputNeuronsCount = 1;
		
		/*variable para recoger la opcion elegida en el menu principal*/
		int logicGateSelectedAsIndex;	
		do{
			/*Menu principal para elegir tipo de compuerta*/
			System.out.println( "\n\n____________________________________________________________________________________\n\n" );
			System.out.println( "                       Qu� compuerta l�gica deseas simular\n" );
			System.out.println( "   1. Compuerta L�gica OR" );
			System.out.println( "   2. Compuerta L�gica AND" );
			System.out.println( "   3. Compuerta L�gica XOR\n" );
			
			logicGateSelectedAsIndex = sc.nextInt();
	
		}while( logicGateSelectedAsIndex != 1 && logicGateSelectedAsIndex != 2 && logicGateSelectedAsIndex != 3 );//Bucle infinito hasta que introduzca un valor correcto
	
		RNA_Problem_Test.TestEnum logicGateSelected = null;
		System.out.println( "\n____________________________________________________________________________________\n\n" );
		if( logicGateSelectedAsIndex == 1 )
		{
			System.out.println( "                       Test de la compuerta OR\n" );
			logicGateSelected = RNA_Problem_Test.TestEnum.Test2_OR;
		}
		else if( logicGateSelectedAsIndex == 2 )
		{
			System.out.println( "                       Test de la compuerta AND\n" );
			logicGateSelected = RNA_Problem_Test.TestEnum.Test2_AND;
		}
		else if( logicGateSelectedAsIndex == 3 )
		{
			System.out.println( "                       Test de la compuerta XOR\n" );
			logicGateSelected = RNA_Problem_Test.TestEnum.Test2_XOR;
		}
		
		// Men� para configurar la capa oculta
		hiddenNeuronsCount = settingLayersMenu( "oculta", 1, 13 );
		
		System.out.println( "\n____________________________________________________________________________________\n\n" );
	
		// Se muestra el men� para elegir la funci�n de activaci�n
		RNA_Problem.ActivationFunction activationFunctionSelected = activationFunctionMenu();
		
		// Se construye el �mbito del problema para resolver la compuerta l�gica seleccionada usando este tipo de redes neuronales
		// mode, testEnum, enterLayer, hiddenLayer, outputLayer, activationFunction, minBackPropIterations, maxBadIterations, learningCons, marginError, maxTotalIterations  
		RNA_Problem_Test myRNA_Problem_Test = new RNA_Problem_Test( Abstract_Problem.Mode.NORMAL, logicGateSelected, inputNeuronsCount, 
				hiddenNeuronsCount, outputNeuronsCount, activationFunctionSelected, 5000, 5, 0.5f, 0.00005f, 25000 );
		
		// Una vez que se ha seleccionado una red neuronal despu�s de aplicar el algoritmo gen�tico la probamos
		
		int option;
		do{
			System.out.println( "\n\n____________________________________________________________________________________\n\n" );
			System.out.println( "                       Elija una opci�n\n" );
			System.out.println( "   1. Testear la compuerta l�gica manualmente" );
			System.out.println( "   2. Testear la compuerta l�gica autom�ticamente" );
			System.out.println( "   3. Salir\n" );
			
			option = sc.nextInt();
			System.out.println( "\n" );
			
			switch( option )
			{
				case 1:// Opcion 1 - Solicita unos valores de entrada y comprueba que la compuerta funcione correctamente
					ArrayList<Float> input = new ArrayList<Float>();
					int count = 0;
					for( int x=0; x<inputNeuronsCount; x++)
					{
						Float value = -1.f;
						while( value != 0 && value != 1 )
						{
							System.out.println( "Introduzca un valor binario para la entrada : " + count );
							value = (float) sc.nextInt();
						}
						input.add(value);
						count++;
					}
					
					ArrayList<Float> result = myRNA_Problem_Test.getOutput(input);
					System.out.println("El resultado es : " + result.get(0) );
					
					break;
				case 2:// Opcion 2 - Genera el input automaticamente
					count = 0;
					
					do
					{
						ArrayList<Float> automaticInput = new ArrayList<Float>();
						Random rd = new Random();
						for( int x=0; x<inputNeuronsCount; x++)
						{
							int value = 0;
							
							if( count == 0 ) value = 0; // Nos aseguramos que al menos una tupla sean todos ceros para probar con este input concreto
							else if( count == 1 ) value = 1; // Nos aseguramos que al menos una tupla sean todos unos para probar con este input concreto
							else value = rd.nextInt(2);
							
							System.out.println("InputValue - " + x + " => " + value );
							automaticInput.add( (float)value );
						}
						result = myRNA_Problem_Test.getOutput(automaticInput);
						System.out.println("El resultado es : " + result.get(0) );
						System.out.println("----------------------");
						count++;
					}while( count < 10 );
					
					break;
				case 3:// Opcion 3 - Vuelve al men� principal
					break;
			}
				
		}while( option != 3 );//Bucle infinito hasta que introduzca 3 
	}
	
	/**
	 * Permite al usuario introducir el n�mero de neurona que tendr� una capa determinada
	 * @param layerName el nombre de la capa
	 * @param min el m�nimo de neuronas que puede tener la capa
	 * @param max el m�ximo de neuronas que puede tener la capa
	 * @return valor de tipo int que se corresponde con el n�mero de neuronas para una capa determinada
	 */
	private static int settingLayersMenu( String layerName, int min, int max )
	{
		
		int neuronsCount = 0;
		while( neuronsCount < min || neuronsCount > max )
		{
			System.out.println("Introduzca el n�mero de neuronas de la capa " + layerName + " ( mayor que " + min + " y menor que " + max + " )\n");
			neuronsCount = sc.nextInt();
		}
		
		System.out.println( "" );
		return neuronsCount;
		
	}
	
	/**
	 * Men� para seleccionar la funci�n de activaci�n usada en el test
	 * @return devuelve el tipo de enumerado que se corresponde con la funci�n de activaci�n seleccionada
	 */
	private static RNA_Problem.ActivationFunction activationFunctionMenu()
	{
		int aFSelectedAsIndex;
		do{
			/*Menu principal para elegir el tipo de funci�n de activaci�n que debe usar la neurona*/
			System.out.println( "                       Qu� funci�n de activaci�n debe ser usada en las neuronas\n" );
			System.out.println( "   1. Threshold" );
			System.out.println( "   2. Sigmoid" );
			System.out.println( "   3. Hyperbolic\n" );
			
			aFSelectedAsIndex = sc.nextInt();
			System.out.println( "" );
			
		}while( aFSelectedAsIndex != 1 && aFSelectedAsIndex != 2 && aFSelectedAsIndex != 3 );//Bucle infinito hasta que introduzca un valor correcto
		
		// Se convierte el �ndice seleccionado al tipo de enumerado que indica el tipo de funci�n de activaci�n seleccionada
		RNA_Problem.ActivationFunction activationFunctionSelected = null;
		if( aFSelectedAsIndex == 1 ) activationFunctionSelected = ActivationFunction.Threshold;
		else if( aFSelectedAsIndex == 2 ) activationFunctionSelected = ActivationFunction.Sigmoid;
		else if( aFSelectedAsIndex == 3 ) activationFunctionSelected = ActivationFunction.Hyperbolic;
		
		return activationFunctionSelected;
	}
	
}

