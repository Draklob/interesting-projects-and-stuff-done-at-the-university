package evolutionaryDemon;

import java.util.ArrayList;

/**
 * Clase gen�rica abstracta que representa un demonio evolutivo
 * De ella puede derivar un algoritmo gen�tico o una clase que represente una programaci�n gen�tica
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 * @param <Genoma> Para el ejemplo de MsPacman el genoma es una cadena de ceros y unos
 */
public abstract class EvoDemon <Genoma> 
{
	
	/**
	 * El n�mero de individuos
	 */
	protected int populationSize;
	
	/**
	 * La lista de individuos
	 */
	protected ArrayList<Individual<Genoma>> population;
		
	/******* METODOS *******/
	
	/**
	 * Inicia la poblaci�n
	 */
	protected abstract void populationGeneration();
	
	/**
	 * Selecciona unos un grupo de individuos de la poblaci�n actual bas�ndose en alg�n algoritmo concreto
	 * Estos algoritmos pueden ser : Ranking, Tournament, Roulette
	 * @return Devuelve la lista de individuos seleccionados
	 */
	protected abstract ArrayList<Individual<Genoma>> selection();
	
	/**
	 * Cruza varios individuos para obtener otros nuevos
	 * @param parents Trabaja sobre la poblaci�n resultante del m�todo de selecci�n
	 * @return Devuelve la poblaci�n resultante despu�s de hacer los cruces entre individuos
	 */
	protected abstract ArrayList<Individual<Genoma>> crossover (ArrayList<Individual<Genoma>> parents);
	
	/**
	 * Muta al azar varios individuos para obtener otros nuevos
	 * @param parents Trabaja sobre la poblaci�n resultante del m�todo de crossover
	 * @return Devuelve la poblaci�n resultante despu�s de hacer las mutaciones de individuos
	 */
	protected abstract ArrayList<Individual<Genoma>> mutation (ArrayList<Individual<Genoma>> parents);
	
	/**
	 * Remplaza los individuos modificados o evolucionados en la poblaci�n actual 
	 * @param newGeneration Redibe el grupo de individuos modificados despu�s de ser mutados
	 */
	protected abstract void replacement(ArrayList<Individual<Genoma>> newGeneration);
	
	/**
	 * Calcula el fitness de un individuo concreto
	 * @param individual
	 */
	protected abstract void calculateFitness(Individual<Genoma> individual);
	
	/**
	 * Establece la condici�n de parada para terminar la evoluci�n
	 * @return Devuelve True si se cumple la condici�n de parada o convergencia
	 */
	protected abstract boolean converge();
	
	/**
	 * Ordena la poblaci�n en funci�n del fitness de menor a mayor ( de mejor a peor )
	 */
	protected abstract void orderPopulation();
	
	/**
	 * Genera el bucle que evoluciona el demonio evolutivo buscando el individuo que representa la soluci�n al problema
	 * Si se cumple la condici�n de convergencia se sale del m�todo
	 * @return Devuelve True cuando termina la evoluci�n. Pendiente de implementar un control de error para saber cu�ndo se 
	 * consigue evolucionar positivamente una poblaci�n o no.
	 */
	public boolean evolutiveProcess()
	{
		// Se crea primero la poblaci�n de partida sobre la que primero
		// se comprobar� la condici�n de parada antes de comenzar a evolucionar dicha poblaci�n
		populationGeneration();
		
		// Se mantiene en el bucle ( ciclo de evoluci�n ) hasta que se consigue la condici�n de parada
		while ( ! converge() )
		{
			populationGeneration();
			replacement(mutation(crossover(selection())));
		}
		
		return true;
	}
	
}


