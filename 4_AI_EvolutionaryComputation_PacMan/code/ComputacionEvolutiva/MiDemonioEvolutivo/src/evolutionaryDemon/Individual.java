package evolutionaryDemon;

/**
 * Representa un individuo general con un genoma general que podr� ser cualquier objeto, 
 * como por ejemplo un array de n�mero, y un fitness, o sea, el grado de adaptaci�n de dicho individuo
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 * @param <Genoma> El genoma del individuo
 */
public abstract class Individual<Genoma> 
{
	/**
	 * El genoma de este individuo
	 */
	protected Genoma genoma;
	
	/**
	 * Indica el grado de adaptaci�n de este individuo
	 */
	protected float fitness;
	
}
