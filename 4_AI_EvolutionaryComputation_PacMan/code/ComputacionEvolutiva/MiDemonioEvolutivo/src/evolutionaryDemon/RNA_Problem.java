package evolutionaryDemon;

import java.util.ArrayList;

/**
 * Clase que incluye todas las herramientas para trabajar con redes neuronales
 * Todos los m�todos son generalizables a cualquier tipo de redes neuronales multicapa de tres capas ( entrada, oculta y de salida ).
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public abstract class RNA_Problem extends Abstract_Problem<ArrayList<Number>>
{

	/**
	 * En principio probamos con 8 neuronas de entrada
	 * 4 para las distancias entre pacman y los fantasmas
	 * 4 para el tiempo restante para dejar de ser comestibles 
	 * ( si es 0 indica que no podemos comerlos, por lo que habr� que huir de ellos )
	 */
	public int enterLayerBits;
	/**
	 * Las neuronas ocultas
	 * ( en principio probaremos con 12 )
	 */
	public int hiddenLayerBits;
	/**
	 * Las neuronas de salida
	 * Para este caso, representan las nueve estrategias posibles,
	 * huir de cada uno de los fantasmas, perseguir a cada uno de los fantasmas
	 * o ir a por la p�ldora m�s cercana.
	 * Aplicando la funci�n de activiaci�n hip�rbolica las dos estrategias de cada fantasma se agrupan 
	 * en una sola neurona, reduciendo el n�mero de neuronas de salida a un total de 5
	 */
	public int outLayerBits;
	/**
	 * El tipo de funci�n de activaci�n que se use
	 * En este caso ( Pacman ) se usa la funci�n hiperb�lica.
	 */
	public enum ActivationFunction { Threshold, Sigmoid, Hyperbolic };
	/**
	 * El tipo de funci�n de activaci�n que se use.
	 */
	ActivationFunction activationFunction;
	/**
	 * El n�mero de iteraciones minimas que ejecuta el algoritmo de backpropagation 
	 * antes de dar por v�lida una distribuci�n de pesos.
	 */
	public int minBackPropIterations;
	/**
	 * El m�ximo de iteraciones consecutivas que se permite que se ejecute 
	 * el algoritmo de backpropagation sin que se rebaje la tasa de error
	 */
	public int maxBadIterations;
	/**
	 * La constante de aprendizaje
	 * En principio ser� constante
	 */
	public float learningCons;
	/**
	 * El n�mero de iteraciones que se usan para promediar el fitness 
	 * Se calcula el error cuadr�tico medio de estas iteraciones
	 */
	private float iterationsPerFitness = 0;

	/**
	 * Constructor
	 * @param _mode Si se ejecuta en modo normal o en Test1 ( esto hay que mejorarlo, queda un poco sucio o desordenado )
	 * @param _enterLayerNeurons El n�mero de neuronas de la capa de entrada
	 * @param _hiddenLayerNeurons El n�mero de neuronas de la capa oculta
	 * @param _outLayerNeurons El n�mero de neuronas de la capa de salida
	 * @param _activationFunction La funci�n de activaci�n que se usar� en el algoritmo de backpropagation
	 * @param _minBackPropIterations El m�nimo de iteraciones que se har�n en el algoritmo de backpropagation antes de detenerlo
	 * @param _maxBadIterations El m�ximo de iteraciones permitidas sin conseguir rebajar el error 
	 * @param _learningCons La constante de aprendizaje
	 * @param _marginError El error permitido para el fitness  ( si se consigue un individuo con un fitness menor a este error se termina la b�squeda )
	 * @param _maxTotalIterations El m�ximo total de iteraciones para el backpropagation 
	 * @param _iterationsPerFitness Las tandas de iteraciones despu�s de las cuales se eval�a el incremento o decremento del fitness o error
	 */
	// mode, enterLayer, hiddenLayer, outputLayer, activationFunction, minBackPropIterations, maxBadIterations, learningCons, marginError, maxTotalIterations, iterationsPerFitness 
	public RNA_Problem( Mode _mode, int _enterLayerNeurons, int _hiddenLayerNeurons, int _outLayerNeurons, ActivationFunction _activationFunction,
						int _minBackPropIterations, int _maxBadIterations, float _learningCons, float _marginError, int _maxTotalIterations, float _iterationsPerFitness )
	{
		// Se construye el padre pas�ndole el modo en el que se ejecuta la aplicaci�n
		super( _mode );
		
		this.varType = VarType.BINARY;
		this.enterLayerBits = _enterLayerNeurons;
		this.hiddenLayerBits = _hiddenLayerNeurons;
		this.outLayerBits = _outLayerNeurons; 
		// En el caso de Ms. Pacman se agrupan las estrategias 
		// ( 4 neuronas para las estrategias de huir o ir a por los fantasmas y 1 para ir a por la p�ldora m�s cercana ).
		// Para las estrategias agrupadas usamos la funci�n de activaci�n hiperb�lica
		// Todas las dem�s neuronas tambi�n usar�n esta misma funci�n.
		this.activationFunction = _activationFunction;
		// Se inicializa el n�mero de variables que tendr� el genoma
		this.varCount = this.enterLayerBits * ( this.hiddenLayerBits + 1 ) * this.outLayerBits;
		
		this.minBackPropIterations = _minBackPropIterations;   // Este valor habr� que irlo ajustando probando
		this.maxBadIterations = _maxBadIterations; 			   // Este valor habr� que irlo ajustando probando
		this.learningCons = _learningCons;		               // Este valor habr� que irlo ajustando probando
		this.bestIndividualFound = false;
		this.iterationsPerFitness = _iterationsPerFitness;
		this.marginError = _marginError;
		this.maxTotalIterations = _maxTotalIterations;
		
	}// Fin del constructor 
	
	/**
	 * Crea y devuelve una red neuronal acorde con el problema 
	 * @return La red neuronal reci�n creada
	 */
	@Override
	public AG_Individual_RNA getNewIndividual()
	{
		return new AG_Individual_RNA( this );
	}
	
	/**
	 * Crea y devuelve una red neuronal acorde con el problema 
	 * y creado como copia de una pasada por par�metro
	 * @return La red neuronal reci�n creada
	 */
	@Override
	public AG_Individual_RNA getIndividualCopy( Individual<ArrayList<Number>> _origin)
	{
		return new AG_Individual_RNA( this, (AG_Individual_RNA) _origin );
	}
	
	/**
	 * Genera unas entradas aleatorias con sus correspondientes salidas esperadas
	 * para entrenar cada red neuronal y poder calcular as� su fitness mediante backpropagation
	 * @return Un array con los valores de entrada y salida
	 */
	protected abstract ArrayList<Float> generateTrainingData();
	
	/**
	 * Testea un individuo calculando las salidas que produce respecto a unas entradas
	 * y compar�ndolas con las esperadas. Muestra por pantalla el resultado
	 */
	protected abstract void testIndividual( Individual<ArrayList<Number>> _individual );
	
	/**
	 * Calcula el fitness de cada red neuronal usando el algoritmo de backpropagation
	 * Para ello usa el m�todo de generar datos aleatorios de entrenamiento
	 * @param _individual La red neuronal a la que se quiere calcular su fitness
	 * @return El fitness de la red neuronal
	 */
	@Override
	public float calculateFitness( Individual<ArrayList<Number>> _individual )
	{
		
		// Se castea _individual a red neuronal
		// Aqu� habr�a que controlar las posibles excepciones
		AG_Individual_RNA individual_RNA = ( ( AG_Individual_RNA ) _individual );
		
		// Se indica que no est� entrenada
		individual_RNA.trained = false;
		
		// Se decodifica el genotipo para determinar las conexiones neuronales
		individual_RNA.calculatePhenotype( this );
		
		// Se inicializan las variables en las que se almacenan los valores comparativos de los errores
		float beforeError = 100.f; // El valor previo se inicializa a un valor alto para la primera comparaci�n
		float currentError = 0.f;
	
		// Se mantiene en el bucle siempre que :
		// no se hayan realizado aun un n�mero m�nimo de iteraciones �
		// no se haya sobrepasado un m�ximo de grupo de iteraciones permitidas sin disminuir el error
		int iterationsCount = 0;
		int badIterationsCount = 0; // Se incrementa por cada grupo de iteraciones que no rebaje el error del grupo anterior
		
		// Se pone a cero el valor para el n�mero de iteraciones que se tiene en cuenta para calcular el fitness
		int iterationsPerFitnessCount = 0;
		
		// El valor de fitness que se devolver� para esta red neuronal despu�s de entrenarla
		float fitness = 100.f;
		
		
		/**** BUCLE DE ENTRENAMIENTO ONLINE ****/
		
		do
		{
			
			// El error obtenido se va acumulando para luego poder obtener el promedio
			currentError += applyBackPropagation( individual_RNA ); // Se aplica el algoritmo de backpropagation !!!
			
			iterationsCount++; 				// El n�mero de iteraciones total
			iterationsPerFitnessCount++; 	// El n�mero de iteraciones que se tienen en cuenta para calcular el fitness
			
			// Si se ha superado el n�mero de iteraciones predefinido para calcular el valor del fitness
			if( iterationsPerFitnessCount >= this.iterationsPerFitness )
			{
				// Si se ha rebajado el error anterior
				if( currentError < beforeError )
				{
					fitness = currentError / this.iterationsPerFitness;
					beforeError = currentError; // no hace falta usar el error promedio, vale con comparar la suma sobre todas las iteraciones
					
					badIterationsCount = 0; // Se resetea a cero el contador de badIterations
					// Se confirman los pesos que se tengan en este momento
					commitWeights( individual_RNA );
				}
				else badIterationsCount++; // Si no se ha mejorado la tasa de error
				
				iterationsPerFitnessCount = 0;
				currentError = 0;
			}
			
			// Si se ha conseguido rebajar el margen de error del problema se detiene el algoritmo
			// Tambi�n se detiene si se ha sobrepasado el l�mite de iteraciones totales permitido
			// Se podr�a considerar detener el algoritmo cuando la tasa de mejora bajara de un umbral
			if( fitness <= this.marginError || iterationsCount >= this.maxTotalIterations )
			{
				break;
			}
			
		}while( iterationsCount < this.minBackPropIterations || badIterationsCount < this.maxBadIterations );
		
		/**** FIN DEL BUCLE DE ENTRENAMIENTO ONLINE ****/
		
		
		// Al final los pesos que se quedar�n como definitivos ser�n los �ltimos confirmados
		// y el fitness el �ltimo obtenido
		// Se indica que ya est� entrenada
		individual_RNA.trained = true;
		
		System.out.println( "********************" );
		System.out.println( "Backpropagation iterations : " + iterationsCount );
		System.out.println( "fitness : " + fitness );
		//individual_RNA.printGenoma();
		System.out.println( "********************" );
		// Testeamos el individuo con un conjunto de tuplas para ver c�mo va.
		//this.testIndividual(individual_RNA);
		return fitness;
	}
	
	/**
	 * Aplica el algoritmo de backpropagation a una red neuronal y una tupla de datos concreta
	 * @param _individual_RNA La red neuronal que se est� entrenando
	 * @return La tasa de error que se obtienen sobre dicha tupla y los pesos optimizados tras el algoritmo
	 */
	private float applyBackPropagation( AG_Individual_RNA _individual_RNA )
	{
		// 1.- Se inician los incrementos de los pesos a cero en todas las conexiones de las neuronas
		resetDeltaWeights( _individual_RNA );
		// 2.- Se genera una tupa aleatoria de entrenamiento con sus correspondientes salidas
		ArrayList<Float> trainingData = generateTrainingData();
		// 3.- Se calcula el valor del output de cara neurona de la capa oculta y de salida en funci�n 
		// de la salida de las neuronas de la capa de entrada usando los pesos provisionales que tenga en ese momento la red neuronal
		calculateOutput( _individual_RNA, trainingData );
		// 4.- Se calcula el error de las salidas de las neuronas de la capa de salida.
		calculateOutputLayerErrors( _individual_RNA, trainingData );
		// 5.- Se calculan los incrementos de los pesos en las conexiones entre la capa oculta y la capa de salida
		// y la capa de entrada y la capa de salida ( conexiones directas )
		calculateDeltaWeights( _individual_RNA, _individual_RNA.outputLayer );
		// 6,- Se calculan los errores de la capa oculta
		calculateHiddenLayerErrors( _individual_RNA );
		// 7.- Se calculan los incrementos de los pesos en las conexiones entre la capa de entrada y la capa oculta
		calculateDeltaWeights( _individual_RNA, _individual_RNA.hiddenLayer );
		// 8.- Por �ltimo se actualizan los pesos de la red
		updateWeights( _individual_RNA );
		// 9.- Se devuelve la tasa de error calculado como error cuadr�tico medio aplicado 
		// a las salidas de todas las neuronas de la capa de salida
		return calculateAverageSquaredError( _individual_RNA, trainingData );
	}
	
	/**
	 * M�todo que confirma los pesos cuando se hayan hecho unas iteraciones
	 * determinadas y se haya conseguido mejorar el fitness
	 */
	private void commitWeights( AG_Individual_RNA _individual_RNA )
	{
		for( Neuron neuron: _individual_RNA.hiddenLayer )
		{
			neuron.commitWeights();
		}
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			neuron.commitWeights();
		}
	}

	/**
	 * Actualiza los pesos de las conexiones entrantes de la capa oculta y la capa de salida
	 * @param _individual_RNA La red neuronal a la que se actualizan los pesos
	 */
	private void updateWeights( AG_Individual_RNA _individual_RNA )
	{
		for( Neuron neuron: _individual_RNA.hiddenLayer )
		{
			neuron.updateWeights();
		}
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			neuron.updateWeights();
		}
	}
	
	/**
	 * Calcula los incrementos de pesos de todas las neuronas correspondientes a una capa
	 * @param _individual_RNA La red neuronal a la que se calcula el incremento de pesos
	 * @param _layer La capa de neuronas 
	 */
	private void calculateDeltaWeights( AG_Individual_RNA _individual_RNA, ArrayList<Neuron> _layer ) 
	{
		for( Neuron neuron: _layer )
		{
			neuron.calculateDeltaWeights( this.learningCons );
		}
	}
	
	/**
	 * Calcula el error cuadr�tico medio de las salidas de las neuronas con respecto a las esperadas para una tupla de entrenamiento dada
	 * @param _individual_RNA La red neuronal que se est� entrenando
	 * @param _trainingData La tupla de entrenamiento
	 * @return El error cuadr�tico medio que en nuestro contexto es el fitness de la red neuronal en ese momento para los pesos actuales
	 */
	private float calculateAverageSquaredError( AG_Individual_RNA _individual_RNA, ArrayList<Float> _trainingData )
	{
		int count = 0;
		float errorsSum = 0;
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			// index nos da el valor dentro del array de datos de entrenamiento correspondiente a la salida
			// esperada para esta neurona ( se podr�a hacer m�s f�cil guardando los datos de salida en un array aparte )
			int index = _trainingData.size() - this.outLayerBits + count; // hay que debuguearlo
			// Se va sumando el cuadrado de la diferencia entre el valor esperado y el obtenido en valor absoluto
			errorsSum += Math.pow( ( _trainingData.get( index ) - neuron.output ), 2 ) ;
			count++;
		}
		// Y luego se divide entre el n�mero total de neuronas de salida 
		errorsSum = errorsSum / _individual_RNA.outputLayer.size();
		
		return errorsSum;
	}

	/**
	 * Se calcula el error de cada neurona de la capa de salida 
	 * en funci�n de los datos esperados dentro del array de valores pasado por par�metro ( trainingData )
	 * @param _individual_RNA La red neuronal a la que se calcula los errores de la capa de salida
	 * @param _trainingData La tupla de entrenamiento
	 */
	private void calculateOutputLayerErrors( AG_Individual_RNA _individual_RNA, ArrayList<Float> _trainingData )
	{
		int count = 0;
		
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			// index nos da el valor dentro del array de datos de entrenamiento correspondiente a la salida
			// esperada para esta neurona ( se podr�a hacer m�s f�cil guardando los datos de salida en un array aparte )
			int index = _trainingData.size() - this.outLayerBits + count; // hay que debuguearlo
			// Se calcula el error de cada neurona de la capa de salida en relaci�n al valor esperado de la tupla de entrenamiento
			neuron.calculateErrorInOutputLayer( _trainingData.get( index ), this );
			count++;
		}
	}
	
	/**
	 * Se calcula el error de cada neurona de la capa oculta
	 * @param _individual_RNA La red neuronal a la que se calcula los errores de la capa oculta
	 */
	private void calculateHiddenLayerErrors( AG_Individual_RNA _individual_RNA )
	{
		for( Neuron neuron: _individual_RNA.hiddenLayer )
		{
			neuron.calculateErrorInHiddenLayer( _individual_RNA.outputLayer, this );
		}
	}
	
	/**
	 * Se inician los incrementos de los pesos a cero en todas las conexiones de las neuronas
	 * @param _individual_RNA La red neuronal a la que se resetea los incrementos de los pesos para ponerlos a cero
	 */
	private void resetDeltaWeights( AG_Individual_RNA _individual_RNA )
	{
		for( Neuron neuron: _individual_RNA.hiddenLayer )
		{
			neuron.resetDeltaWeights();
		}
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			neuron.resetDeltaWeights();
		}
	}
	
	/**
	 * Se calcula el output de cada neurona de la red neuronal pasada por par�metro,
	 * en funci�n de las entradas que se le pasen por par�metro,
	 * y los pesos definitivos que guardara al alcanzar el mejor fitness.
	 * @param La red neuronal a la que se le calcula el output
	 * @param input Array de datos de entrada ( puede ser el training data entero del cual tan solo se tomar�n los datos de entrada )
	 */
	private void calculateOutput( AG_Individual_RNA _individual_RNA, ArrayList<Float> input )
	{
		int count = 0;
		for( Neuron neuron: _individual_RNA.enterLayer )
		{
			neuron.output = input.get(count);
			count++;
		}
		for( Neuron neuron: _individual_RNA.hiddenLayer )
		{
			neuron.calculateOutput( this, _individual_RNA.trained );
		}
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			neuron.calculateOutput( this, _individual_RNA.trained );
		}
	}
	
	/**
	 * En funci�n de las entradas devolver� un array con los valores de salida de las 
	 * neuronas de salida de la red neuronal seleccionada ( this.finalIndividual )
	 * @param input Array de datos de entrada
	 * ( Para MsPacman ser�n las 4 distancias a los fantasmas y los 4 tiempos de edible )
	 * @return un array de float con las salidas de las neuronas de salidas
	 */
	public ArrayList<Float> getOutput( ArrayList<Float> input )
	{
		// Se calculan las salidas para las entradas pasadas por par�metro
		this.calculateOutput( (AG_Individual_RNA) this.finalIndividual, input );
		ArrayList<Float> outputs = new ArrayList<Float>();
		for( Neuron neuron: ( (AG_Individual_RNA) this.finalIndividual ).outputLayer )
		{
			outputs.add( neuron.output );
		}
		return outputs;
	}	
	
	/**
	 * En funci�n de las entradas y la red neuronal pasada por par�metro,
	 * devolver� un array con los valores de salida de dicha red neuronal
	 * En principio este m�todo se usa para debuguear cualquier red neuronal
	 * @param _individual_RNA La red neuronal a la que se calcula sus valores de salida
	 * @param input Array de datos de entrada
	 * @return un array de float con las salidas de las neuronas de salidas
	 */
	public ArrayList<Float> getOutput( AG_Individual_RNA _individual_RNA, ArrayList<Float> input )
	{
		// Se calculan las salidas para las entradas pasadas por par�metro
		this.calculateOutput( _individual_RNA, input );
		ArrayList<Float> outputs = new ArrayList<Float>();
		for( Neuron neuron: _individual_RNA.outputLayer )
		{
			outputs.add( neuron.output );
		}
		return outputs;
	}	

}
