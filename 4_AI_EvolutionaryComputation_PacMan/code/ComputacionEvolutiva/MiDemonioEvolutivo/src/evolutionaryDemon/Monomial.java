package evolutionaryDemon;

import java.util.ArrayList;

/**
 * Representa un monomio usado para componer un polinomio multivariable
 * Se compone de un coeficiente y un array de exponentes a los que se elevar�n en cada caso 
 * las variables pasadas por par�metro siempre que se tenga que calcular el valor de dicho monomio
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class Monomial 
{
	/**
	 * El coeficiente por el que se multiplica el monomio al sumarlo con otros ( 1, -1, 3, 0.5, etc... )
	 */
	float coef;
	
	/**
	 * Cada variable se eleva al exponente indicado en este array
	 */
	ArrayList<Float> expArray;
	
	/**
	 * Constructor
	 * @param _coef El coeficiente para este monomio
	 * @param _expArray El array de exponentes 
	 */
	public Monomial( float _coef, ArrayList<Float> _expArray ) 
	{
		
		this.coef = _coef;
		this.expArray = _expArray;
		
	}// Fin del constructor
	
	/**
	 * Calcula el valor de e ste monomio
	 * @param _varType El tipo de de variable ( integer, float, ... )
	 * @param _varsArray El array de variables que se elevaran usando el array de exponentes de este monomio
	 * y se multiplicar�n por el coeficiente
	 * @return El valor de dicho monomio
	 */
	public float CalculateValue( Abstract_Problem.VarType _varType, ArrayList<Number> _varsArray )
	{
		float result = this.coef;
		// Contador para saber a qu� exponente elevar la variable
		int count = 0;
		// Se recorren todas las variables
		for( Number var: _varsArray )
		{
			/*float varValue = 1.f; // Si el exponente es cero se multiplica por 1
			for( int i=0; i<this.expArray.get(count); i++)
			{
				if( i == 0 ) varValue = var.floatValue();
				else varValue *= var.floatValue();
			}*/
			
			double doubleVar = 0;
			if( _varType == Abstract_Problem.VarType.INTEGER )
				doubleVar = (double) var.intValue();
			else if( _varType == Abstract_Problem.VarType.FLOAT )
				doubleVar = (double) var.floatValue();
			
			double exp = this.expArray.get(count).doubleValue();
			
			double varValue = Math.pow( doubleVar, exp );
			result *= varValue;
			count++;
		}
		
		return result;
	}
}
