package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Representa un �mbito para resolver ecuaciones de polinomios multivariable
 * Incorpora un sistema de codificaci�n para poder definir los polinomios din�micamente ( por ejemplo de manera aleatoria )
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class Polynomial_Problem extends Abstract_Problem<ArrayList<Number>>
{
	/**
	 * La lista de monimios que tiene este fenotipo
	 */
	ArrayList<Monomial> monomialArrayList;
	
	/**
	 * La soluci�n para calcular luego el fitness de cada polinomio ( individuo ) 
	 * o sea el grado de adaptaci�n de cada individuo
	 */
	float problemSolution;
	
	//El objeto de la clase Scanner para poder leer del teclado
	Scanner sc;

	/**
	 * Constructor 
	 * @param _mode Si se ejecuta en modo normal o en Test1 ( esto hay que mejorarlo, queda un poco sucio o desordenado )
	 */
	public Polynomial_Problem( Mode _mode )
	{
		// Se construye el padre pas�ndole el modo en el que se ejecuta la aplicaci�n
		super( _mode );
		
		// Inicializo la clase scanner:
		sc = new Scanner( System.in );
		
		System.out.println( "\n____________________________________________________________________________________" );
		System.out.println( "\n\n                       Configuraci�n del polinomio" );
		System.out.println( "                    _________________________________\n\n" );
		
		// Se selecciona el tipo de variable
		int varTypeSelectedAsIndex;
		do{
			/*Menu principal para elegir el tipo de variable con el que trabajar� el polinomio*/
			System.out.println( "                       Elija el tipo de variable\n" );
			System.out.println( "   1. Integer" );
			System.out.println( "   2. Float\n" );

			varTypeSelectedAsIndex = sc.nextInt();
			System.out.println( "" );
			
		}while( varTypeSelectedAsIndex != 1 && varTypeSelectedAsIndex != 2 );//Bucle infinito hasta que introduzca un valor correcto
		
		// Se convierte el �ndice seleccionado al tipo de enumerado 
		if( varTypeSelectedAsIndex == 1 ) this.varType = Abstract_Problem.VarType.INTEGER;
		else if( varTypeSelectedAsIndex == 2 ) this.varType = Abstract_Problem.VarType.FLOAT;
		
		// Se solicita el n�mero de variables que tendr� el polinomio ( genoma )
		int varCount = -1;
		while( varCount < 0 || varCount > 10 )
		{
			System.out.println("Introduzca el n�mero de variables ( mayor que " + 0 + " y menor que " + 10 + " )\n");
			varCount = sc.nextInt();
		}
		// Se asigna al atributo de la clase padre
		this.varCount = varCount;
		
		System.out.println( "" );
	
		// En principio se inicializan las variables aleatoriamente con este margen de valores
		this.varMax = 20;
		this.varMin = -20;
		
		// Se solicita la soluci�n al problema ( el resultado de la suma de los monomios )
		System.out.println("Introduzca el resultado de sumar todos los monomios\n");
		this.problemSolution = sc.nextInt();
	
		
		monomialArrayList = new ArrayList<Monomial>(); // La lista de monomios
		
		// Bucle para crear los monomimos que se necesiten para construir el polinomio
		int option = 0;
		do{
			System.out.println( "____________________________________________________________________________________\n\n" );
			System.out.println( "                       Agregue los monomios que necesite\n" );
			System.out.println( "   1. A�adir nuevo monomio" );
			System.out.println( "   2. Salir\n" );
			
			option = sc.nextInt();
			System.out.println( "" );
		
			switch( option )
			{
				case 1:// Opcion 1
					/*Testeando*/
					System.out.println( "____________________________________________________________________________________\n\n" );
					System.out.println( "                       Configurando el monomio " + ( this.monomialArrayList.size() + 1 ) + "\n" );
					addNewMonomial();
					System.out.println();
					break;
				case 2:// Opcion 2 - Sale del bucle
					break;
			}
			
		}while( option != 2 );//Bucle infinito hasta que introduzca 2 
		
		this.bestIndividualFound = false;
		
	}// Fin del constructor 
	
	/**
	 * Crea y devuelve un individuo con un array de n�meros acordes con el polinomio planteado
	 * @return El individuo reci�n creado
	 */
	@Override
	public AG_Individual getNewIndividual()
	{
		return new AG_Individual( this );
	}
	
	/**
	 * Crea y devuelve un individuo con un array de n�meros acordes con el polinomio planteado
	 * y creado como copia de una pasada por par�metro
	 * @return El individuo reci�n creado
	 */
	@Override
	public AG_Individual getIndividualCopy( Individual<ArrayList<Number>> origin )
	{
		return new AG_Individual( origin );
	}
	
	/**
	 * Inicializa la poblaci�n de individuos y los evoluciona buscando aquel que ofrezca la soluci�n a la ecuaci�n
	 */
	@Override
	public void getBestIndividual()
	{
		// Se crea un demonio evolutivo gen�rico, en este caso de tipo algoritmo gen�tico
		// que evoluciona una red neuronal que ofrezca estrategias al movimiento de Ms.Pacman
		EvoDemon<ArrayList<Number>> my_ED = new ED_AG( 
				this, 		 								  // El �mbito del poblema que se quiere resolver
				0.5f,									  	  // El margen de error respecto a la soluci�n requerida
				50, 										  // El tama�o de la poblaci�n estable para resolver el problema
				ED_AG.SelectionType.Tournament,               // El m�todo de selecci�n usado 
				ED_AG.CroosoverType.Uniforme,                 // El m�todo de croosover usado
				ED_AG.MutationType.Uniforme,				  // El m�todo de mutaci�n usado
				ED_AG.ReplacementType.Worst,				  // El m�todo de reemplazo usado
				100,										  // El m�nimo de iteraciones
				10,											  // El m�ximo de iteraciones fallidas permitidas
				0											  // El n�mero de inmigraciones por iteraci�n
			); 			  
		
		// Se inicia el demonio evolutivo
		my_ED.evolutiveProcess();
		
		this.bestIndividualFound = true; // Hay que implementar el control de acierto o fracaso del demonio evolutivo
	}
	
	/**
	 * Calcula el fitness del individuo pasado por par�metro en funci�n del �mbito del problema planteado
	 */
	@Override
	public float calculateFitness( Individual<ArrayList<Number>> _individual )
	{
		float phenotype = 0;
		
		for( Monomial newMonomial: this.monomialArrayList )
		{
			phenotype += newMonomial.CalculateValue( this.varType, _individual.genoma );
		}
		
		float fitness = this.problemSolution - phenotype;
		if( fitness < 0 ) fitness = - fitness;

		return fitness;
	}
	
	/**
	 * Se solicitan los valores apropiados para a�adir un nuevo monomio al polinomio
	 */
	private void addNewMonomial()
	{
		// Creamos un monomio y lo a�adimos a la lista de monomios
		ArrayList<Float> expList = new ArrayList<Float>();
		// Se solicita el coeficiente para este monomio
		System.out.println("Introduzca el coeficiente para este monomio\n");
		float coef = sc.nextFloat();
		System.out.println("");
		// Se solicitan el exponente al que se elevara cada variable
		int count = 0;
		while( count < this.varCount )
		{
			// Se solicita un exponente para la variable correspondiente
			System.out.println("Introduzca el exponente para la variable numero : " + ( count + 1 ) + "\n");
			float exp = sc.nextFloat();
			expList.add(exp);
			count++;
		}
		
		Monomial newMonomial = new Monomial( coef, expList );
		this.monomialArrayList.add(newMonomial);
	}
	
	

}
