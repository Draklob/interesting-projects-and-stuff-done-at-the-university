package evolutionaryDemon;

/**
 * Cada neurona tiene un array de conexiones
 * cada conexi�n guarda una referencia a la neurona que proporciona el input en esta conexi�n
 * y el peso 
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class Connection 
{
	/**
	 * La neurona que proporciona el input en la conexi�n
	 */
	public Neuron inputNeuron;
	
	/**
	 * Los pesos definitivos
	 */
	public float finalWeight = 0;
	
	/**
	 * El peso de esta conexi�n
	 */
	public float weight = 0;

	/**
	 * El incremento del peso para aplicar el algoritmo de backpropagation
	 */
	public float deltaWeight = 0;
	
	/**
	 * Constructor de la clase Connection
	 * @param _neuron La neurona que sirve de input en esta conexi�n
	 * @param _initialWeight El peso inicial de esta conexi�n
	 */
	public Connection( Neuron _neuron, float _initialWeight ) 
	{
		this.inputNeuron = _neuron;
		this.weight = _initialWeight;
		this.deltaWeight = 0;
    }
	
}
