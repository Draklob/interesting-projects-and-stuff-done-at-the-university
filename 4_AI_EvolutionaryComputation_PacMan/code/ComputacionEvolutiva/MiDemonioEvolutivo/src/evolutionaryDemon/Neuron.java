package evolutionaryDemon;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Representa una neurona
 * Tiene todos los m�todos necesarios para poder aplicar el algoritmo
 * de backpropagation a la red neuronal que la incluye.
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class Neuron 
{
	/**
	 * El id de la neurona para poder identificarlas y as� ser m�s f�cil el debuguear
	 */
    final public int id;  
    /**
     * Las conexiones entrantes que tiene cada neurona
     */
	ArrayList<Connection> inputConnections = new ArrayList<Connection>();
	/**
	 * Diccionario que permitir� encontrar con facilidad todas conexiones de entrada en las neuronas de salida
	 * que est�n asociadas a una neurona dada de la capa oculta ( se podr�a haber prescindido de inputConnections )
	 */
	public HashMap<Integer,Connection> connectionHashMap = new HashMap<Integer,Connection>();
	/**
	 * La salida de cada neurona
	 */
	public float output = 0;
	/**
	 * El error provisional en la salida de esta neurona al aplicarle el algoritmo de backpropagation
	 */
	public float error = 0;
	
	/**
	 * Constructor para la clase Neuron
	 * @param _id El identificador de la neurona
	 */
	public Neuron( int _id )
	{
		
		this.id = _id;
		
	}// Fin del constructor
	
	/**
	 * A�ade una conexi�n de entrada con una neurona pasada por par�metro y
	 * con un peso inicial tambi�n pasado por par�metro.
	 * Se comprueba que no existiera ya una conexi�n con dicha neurona.
	 * @param _neuron La neurona que proporcionar� el valor ( su output ) para esta conexi�n de entrada
	 * @param _initialWeight El peso inicial para esta conexi�n ( un valor aleatorio entre 0 y 1 en este caso )
	 */
	public void addInputConnection( Neuron _neuron, float _initialWeight )
	{
		// Se comprueba primero si ya existe una conexi�n con esa neurona para no tener conexiones duplicadas
		
		for( Connection con: inputConnections )
		{
			if( con.inputNeuron.id == _neuron.id )
				return;
		}
		// Si no existe aun se crea
		Connection con = new Connection( _neuron, _initialWeight );
		// Se agrega la nueva conexi�n tanto al array como al hashmap
        inputConnections.add(con);
        this.connectionHashMap.put( _neuron.id, con );
    }
	
	/**
	 * Resetea a cero los incrementos de los pesos en las conexiones de la neurona
	 */
	public void resetDeltaWeights()
	{
		for( Connection con: this.inputConnections )
		{
			con.deltaWeight = 0;
		}
	}
	
	/**
	 * Calcula el output para esta neurona en funci�n de las conexiones entrantes que tenga
	 * y la funci�n de activaci�n seleccionada en la red neuronal pasada por par�metro.
	 * Para calcular los outputs de las neuronas de la capa de salida se tendr�n que haber calculado
	 * antes los outputs de las neuronas de la capa oculta.
	 * El par�metro _final indica si se est� entrenando a la red neuronal y por tanto se tienen que usar los
	 * pesos provisionales o si por el contrario se quiere saber un output definitivo, en cuyo caso se usar�n los pesos finales.
	 * @param _RNA_Problem para saber qu� tipo de funci�n de activaci�n usar
	 * @param _trained Indica si la red neuronal est� ya entrenada o no
	 */
	public void calculateOutput( RNA_Problem _RNA_Problem, boolean _trained )
	{
		float sum = 0;
		// Se calcula la sumatoria de las entradas ponderadas por los pesos respectivos a cada conexi�n
		for( Connection con : this.inputConnections )
		{
			// Se usan unos pesos u otros en funci�n de si est� ya entrenada o no
			if( _trained == true )
				sum += con.inputNeuron.output * con.finalWeight;
			else
				sum += con.inputNeuron.output * con.weight;
		}
		// Se usa una u otra funci�n de activaci�n
		if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Threshold )
		{
			if( sum <= 0 ) this.output = 0;
			else this.output = 1;
		}
		else if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Sigmoid )
			this.output = Utils.sigmoid(sum);
		else if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Hyperbolic )
			this.output = Utils.hyperbolic(sum);
	}
	
	/**
	 * Se calcula el error de salida de la neurona con respecto a la salida esperada
	 * en el caso de que sea una neurona de la capa de salida
	 * Derivada de funci�n hiperb�lica - http://en.wikibooks.org/wiki/Artificial_Neural_Networks/Activation_Functions    
	 * @param _outputExpected El valor esperado
	 * @param _RNA_Problem El �mbito del problema para el que se ha creado la red neuronal que contiene esta neurona
	 */
	public void calculateErrorInOutputLayer( float _outputExpected, RNA_Problem _RNA_Problem )
	{
		// Se usa una u otra funci�n de activaci�n
		float derived = 0.f;
		if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Threshold )
			derived = 1;
		else if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Sigmoid )
			derived = Utils.sigmoid_derived( this.output );
		else if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Hyperbolic )
			derived = Utils.hiperbolic_derived( this.output );
		// Se asigna el valor obtenido para el error
		this.error = derived * ( _outputExpected - this.output );
	}
	
	/**
	 * Se calcula el error de salida de la neurona con respecto a la salida esperada
	 * en el caso de que sea una neurona de la capa oculta
	 * Derivada de funci�n hiperb�lica - http://en.wikibooks.org/wiki/Artificial_Neural_Networks/Activation_Functions    
	 * @param _outputLayer El array de neuronas de la capa de salida
	 * @param _RNA_Problem El problema para el que se genera la red neuronal que contiene esta neurona
	 */
	public void calculateErrorInHiddenLayer( ArrayList<Neuron> _outputLayer, RNA_Problem _RNA_Problem )
	{
		this.error = 0.f;
		// Se usa una u otra funci�n de activaci�n
		float derived = 0.f;
		if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Threshold )
			derived = 1;
		else if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Sigmoid )
			derived = Utils.sigmoid_derived( this.output );
		else if( _RNA_Problem.activationFunction == RNA_Problem.ActivationFunction.Hyperbolic )
			derived = Utils.hiperbolic_derived( this.output );
		// Ahora hay que calcular la sumatoria de las salidas de las neuronas de la capa de salida
		// con las que est� conectada esta neurona ( en el caso de que sea de la capa oculta )
		// multiplicado por el peso de dicha conexi�n
		for( Neuron neuron: _outputLayer )
		{
			// Se busca en el diccionario de cada neurona de salida si tienen alguna conexi�n con esta neurona
			if( neuron.connectionHashMap.get(this.id) != null )
			{
				Connection con = neuron.connectionHashMap.get(this.id);
				this.error += con.weight * neuron.error;
			}
		}
		// Se asigna el valor obtenido para el error
		this.error *= derived;
	}
	
	/**
	 * Calcula el incremento de los pesos
	 * Hay que esperar a asign�rselos al peso provisional, ya que aun hay que calcular con este valor el error de las
	 * neuronas de la capa oculta
	 * @param _learningCons La constante de aprendizaje
	 */
	public void calculateDeltaWeights( float _learningCons )
	{
		for( Connection con : this.inputConnections )
		{
			con.deltaWeight += _learningCons * this.error * con.inputNeuron.output; 
		}
	}
	
	/**
	 * Actualiza los pesos provisionales de las conexiones entrantes de esta neurona
	 * sum�ndoles el incremento que se haya producido tras el entrenamiento con una tupla determinada de datos
	 */
	public void updateWeights()
	{
		for( Connection con : this.inputConnections )
		{
			con.weight += con.deltaWeight;
		}
	}
	
	/**
	 * Asigna los pesos actuales a los finales en cada conexi�n de esta neurona
	 */
	public void commitWeights()
	{
		for( Connection con : this.inputConnections )
		{
			con.finalWeight = con.weight;
		}
	}
	
	
}
