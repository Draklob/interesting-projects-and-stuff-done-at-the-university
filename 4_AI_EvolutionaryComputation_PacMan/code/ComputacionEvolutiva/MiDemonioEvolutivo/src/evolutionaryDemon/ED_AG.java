package evolutionaryDemon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


/**
 * Clase que representa un algoritmo gen�tico para individuos con genoma de tipo ArrayList de objetos de tipo Number
 * Puede evolucionar individuos para buscar la soluci�n a un polinomio, o bien
 * evolucionar redes neuronales que optimicen un resultado buscado para un tipo de entrada
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 * Podemos construir la clase como Integer, Float, etc...
 */
public final class ED_AG extends EvoDemon<ArrayList<Number>>
{

	/**
	 * El tipo de algoritmo de selecci�n de individuos que se aplica
	 */
	public enum SelectionType{ Ranking, Tournament, Roulette };
	/**
	 * El tipo de algoritmo de selecci�n de individuos que se aplica
	 */
	SelectionType selectionType;
	
	/**
	 * El tipo de algoritmo de crossover de individuos que se aplica
	 */
	public enum CroosoverType{ Uniforme, Plano, CruceCombinado, CruceAritmetico, Bernoulli };
	/**
	 * El tipo de algoritmo de crossover de individuos que se aplica
	 */
	CroosoverType croosoverType;
	
	/**
	 * El tipo de algoritmo de mutaci�n de individuos que se aplica
	 */
	public enum MutationType{ None, Uniforme, Intercambio };
	/**
	 * El tipo de algoritmo de mutaci�n de individuos que se aplica
	 */
	MutationType mutationType;
	
	/**
	 * Antes de la funci�n de reemplazo se podr�n incluir m�s individuos que aporten diversidad
	 */
	public int inmigrationNumber;
	
	/**
	 * El tipo de algoritmo de reemplazo de individuos que se aplica
	 */
	public enum ReplacementType{ Worst, Random, Tournament, Generational };
	/**
	 * El tipo de algoritmo de reemplazo de individuos que se aplica
	 */
	ReplacementType replacementType;
	
	/**
	 * Se guarda una referencia a la clase que posee las herramientas necesarias
	 * para resolver un problema dado, ya sea usando un polinomio de varias variables
	 * o una red neuronal artificial
	 */
	Abstract_Problem<ArrayList<Number>> problemScope;
	
	/**
	 * El margen de error que se permite como condici�n de parada respecto a la soluci�n esperada
	 */
	float errorMargin;
	
	/**
	 * La m�scara binaria para los cruces ( un array de enteros binarios )
	 * Se genera aleatoriamente en el constructor del algoritmo gen�tico
	 */
	ArrayList<Integer> croosOverMask;
	
	/**
	 * Se guarda el valor del mejor fitness conseguido para ir calculando si se rebaja o no en cada iteraci�n
	 */
	float bestFitness;
	
	/**
	 * El n�mero de iteraciones minimas que ejecuta el algoritmo evolutivo antes de dar por v�lido el mejor fitness alcanzado
	 */
	private int min_ED_AG_Iterations;
	
	/**
	 * El contador de iteraciones;
	 */
	private int iterationsCount;
	
	/**
	 * El m�ximo de iteraciones consecutivas que se permite que se ejecute el algoritmo evolutivo sin que se mejore el mejor fitness 
	 */
	private int maxBadIterations;
	
	/**
	 * El contador de iteraciones sin rebajar el mejor fitness
	 */
	private int badIterationsCount;
	
	/**
	 * Constructor para la clase ED_AG
	 * @param _problemScope El �mbito del problema para los que se genera y evoluciona la poblaci�n de individuos
	 * @param _errorMargin El margen de error admisible para detener el demonio evolutivo
	 * @param _populationSize El tama�o de poblaci�n que se mantiene estable durante el proceso evolutivo
	 * @param _selectionType El algoritmo de selecci�n que se usa durante el algoritmo gen�tico
	 * @param _croosoverType El algoritmo de cruce que se usa durante el algoritmo gen�tico
	 * @param _mutationType El algoritmo de mutaci�n que se usa durante el algoritmo gen�tico
	 * @param _replacementType El algoritmo de reemplazo que se usa durante el algoritmo gen�tico
	 * @param _minIterations El m�nimo de iteraciones que ejecuta el algoritmo gen�tico
	 * @param _maxBadIterations El m�ximo de iteraciones que ejecuta el algoritmo gen�tico
	 * @param _inmigrationNumber El n�mero de inmigraciones que se realizan por iteraci�n
	 */
	public ED_AG( Abstract_Problem<ArrayList<Number>> _problemScope, float _errorMargin, int _populationSize, 
			SelectionType _selectionType, CroosoverType _croosoverType, MutationType _mutationType, ReplacementType _replacementType,
			int _minIterations, int _maxBadIterations, int _inmigrationNumber ) 
	{
		// Inicializamos los atributos del padre
		super();
		
		// Se asigna el �mbito del problema a tratar
		this.problemScope = _problemScope;
			
		this.errorMargin = _errorMargin;
		this.populationSize = _populationSize;
		this.selectionType = _selectionType;
		this.croosoverType = _croosoverType;
		this.mutationType = _mutationType;
		this.replacementType = _replacementType;
		
		this.min_ED_AG_Iterations = _minIterations;
		this.iterationsCount = 0;
		this.maxBadIterations = _maxBadIterations;
		this.badIterationsCount = 0;
		
		this.inmigrationNumber = _inmigrationNumber;
		
		// Inicializamos aqu� el arraylist para que la primera vez que comprueba la convergencia no pete
		this.population = new ArrayList<Individual<ArrayList<Number>>>();
		
		//Se inicia el mejor fitness a un valor alto para que se supere seguro en el primer ciclo de evoluci�n 
		bestFitness = 100.f;
		
	}// Fin del constructor 
	
	/**
	 * Se invoca antes del comienzo del ciclo del demonio evolutivo y luego,
	 * ya dentro de dicho ciclo, despu�s de haber seleccionado, cruzado y mutado los individuos
	 * y de haber comprobado la condici�n de parada.
	 * Genera tantos individuos como sea necesario para mantener el tama�o de la poblaci�n.
	 * Inicialmente se crear�n todos los individuos, pero luego tan solo los que hayan sido
	 * eliminados en el proceso de evoluci�n.
	 */
	@Override
	protected void populationGeneration() 
	{
		
		int count = this.population.size();
		while( count < this.populationSize )
		{
			// Dependiendo del tipo de problema se crean unos individuos u otros
			Individual<ArrayList<Number>> newIndividual = this.problemScope.getNewIndividual();
			// Se va calculando el fitness de cada individuo
			this.calculateFitness(newIndividual);
			// Se a�ade a la poblaci�n
			this.population.add(newIndividual);
			count++;
		}
		
		// this.printFitness(); // Para debuguear
	}

	/**
	 * Se selecciona un n�mero determinado de individuos seg�n diferentes algoritmos
	 * Estos individuos se pasan luego en el ciclo de evoluci�n a la fase de cruce ( crossover )
	 * Probamos a seleccionar tan solo dos individuos para su evoluci�n posterior 
	 * @return El arrayList de individuos seleccionados
	 */
	@Override
	protected ArrayList<Individual<ArrayList<Number>>> selection() 
	{
		
		// El array de individuos que se retornan a la funci�n de crossover
		ArrayList<Individual<ArrayList<Number>>> indivualsSelected = new ArrayList<Individual<ArrayList<Number>>>();
		if( this.selectionType == SelectionType.Tournament )
		{
			// Seleccionamos un porcentaje de individuos de los cuales luego por torneo se elegir�n la mitad de ellos
			int selectionNumb = (int)(this.populationSize * 0.4f);
			if( selectionNumb > 4 ) selectionNumb = 4; // introducimos esta l�nea para seleccionar tan solo dos padres para el problema de Pacman
			// Se genera un n�mero equivalente de �ndices aleatorios para extraer los individuos del array de poblaci�n 
			ArrayList<Integer> arrayListSelectionIndex = Utils.generateDifferentValues( selectionNumb, this.populationSize );
			// Los comparamos de dos en dos y seleccionamos el que mejor fitness tenga ( si el �ltimo es impar y por tanto no tiene pareja no se selecciona )
			int count = 0;
			
			while( count < selectionNumb - 1 )
			{
				int index1 = arrayListSelectionIndex.get( count );
				int index2 = arrayListSelectionIndex.get( count + 1 );
				float fitness1 = this.population.get( index1 ).fitness;
				float fitness2 = this.population.get( index2 ).fitness;
				// Se escoge al mejor, el que menor fitness tenga de los dos
				if( fitness1 > fitness2 )
				{
					// Dependiendo del tipo de problema se crean unos individuos u otros
					indivualsSelected.add( this.problemScope.getIndividualCopy( this.population.get( index2 ) ) );
				}
				else
				{
					// Dependiendo del tipo de problema se crean unos individuos u otros
					indivualsSelected.add( this.problemScope.getIndividualCopy( this.population.get( index1 ) ) );
				}
				
				count++;
				count++;
			}
		}
		
		return indivualsSelected;
	}

	/**
	 * El m�todo de cruce de individuos, seg�n el algoritmo prefijado
	 * @return Devuelve el array de individuos que se hayan cruzado ( no todos los que se reciben desde el m�todo de selecci�n )
	 */
	@Override
	protected ArrayList<Individual<ArrayList<Number>>> crossover (ArrayList<Individual<ArrayList<Number>>> parents) 
	{
		// El array de individuos que se retornan a la funci�n de mutaci�n 
		// ( solo se devuelven los que se hayan cruzado, no todos los que se reciben del m�todo de selecci�n )
		ArrayList<Individual<ArrayList<Number>>> mixedIndivuals = new ArrayList<Individual<ArrayList<Number>>>();
		if( this.croosoverType == CroosoverType.Uniforme)
		{
			// Se genera una m�scara aleatoria para el cruce uniforme 
			croosOverMask = this.generateMask();
			// Se cruzan todos los individuos de dos en dos
			// Usamos la m�scara creada aleatoriamente 
			int count = 0;
			while( count < ( parents.size() - 1 ) )
			{
				// Se recorren todos los valores de la m�scara para ver qu� elementos se swapean
				int i = 0;
				for( Integer bin: croosOverMask )
				{
					if( bin == 1 )
					{
						Number aux = 0;
						
						if( this.problemScope.varType == Abstract_Problem.VarType.INTEGER || 
								this.problemScope.varType == Abstract_Problem.VarType.BINARY )
						{
							aux = parents.get(count + 1).genoma.get(i).intValue();
							// swap
							parents.get(count + 1).genoma.set( i, parents.get(count).genoma.get(i).intValue() );
							parents.get(count).genoma.set( i, aux );
						}
						else if( this.problemScope.varType == Abstract_Problem.VarType.FLOAT )
						{
							aux = parents.get(count + 1).genoma.get(i).floatValue();
							// swap
							parents.get(count + 1).genoma.set( i, parents.get(count).genoma.get(i).floatValue() );
							parents.get(count).genoma.set( i, aux );
						}
					}
					i++;
				}
				
				mixedIndivuals.add(parents.get(count));
				mixedIndivuals.add(parents.get(count + 1));
				
				count++;
				count++;
			}
		}
		else if( this.croosoverType == CroosoverType.Bernoulli )
		{
			// Para el c�lculo de probabilidad
			Random rd = new Random();
			// Se recorren todos los individuos y se comparan dos a dos, si son impares el �ltimo individuo se descarta 
			int count = 0;
			while( count < ( parents.size() - 1 ) )
			{
				// Primero se calcula la probabilidad de elegir el alelo de un padre o del otro
				float fitness1 = parents.get(count).fitness;
				float fitness2 = parents.get(count + 1).fitness;
				
				// Cuanto mayor fitness mayor ser� p1, pero seg�n como lo aplicamos luego 
				// menor probabilidad habr� de que se elija un alelo de este individuo
				float p1 = fitness1 / ( fitness1 + fitness2 ); 
				float p2 = 1 - p1;
			
				float p = 0;
				if( fitness1 < fitness2 )
				{
					p = p1;
				}
				else
				{
					p = p2;
				}
				// Se recorren el genoma de ambos padres swapeando seg�n la probabilidad calculada
				for( int i=0; i<parents.get(count).genoma.size(); i++ )
				{
					// Se intercambian los alelos
					if( rd.nextFloat() < p )
					{
						Number aux = 0;
						
						if( this.problemScope.varType == Abstract_Problem.VarType.INTEGER || 
								this.problemScope.varType == Abstract_Problem.VarType.BINARY )
						{
							aux = parents.get(count + 1).genoma.get(i).intValue();
							// swap
							parents.get(count + 1).genoma.set( i, parents.get(count).genoma.get(i).intValue() );
							parents.get(count).genoma.set( i, aux );
						}
						else if( this.problemScope.varType == Abstract_Problem.VarType.FLOAT )
						{
							aux = parents.get(count + 1).genoma.get(i).floatValue();
							// swap
							parents.get(count + 1).genoma.set( i, parents.get(count).genoma.get(i).floatValue() );
							parents.get(count).genoma.set( i, aux );
						}
						
					}
					i++;
				}
				
				// Se seleccionan los dos individuos resultado del cruce 
				mixedIndivuals.add(parents.get(count));
				mixedIndivuals.add(parents.get(count + 1));
				
				count++;
				count++;
			}
		}
		return mixedIndivuals;
	}
	
	/**
	 * El m�todo de mutaci�n.
	 * Solo existir� una probabilidad determinada de que se lleve a cabo la mutaci�n en funci�n del algoritmo prefijado
	 * @return Devuelve el mismo array de individuos recibido del m�todo de crossover pero con las mutaciones que se hayan llevado a cabo
	 */
	@Override
	protected ArrayList<Individual<ArrayList<Number>>> mutation( ArrayList<Individual<ArrayList<Number>>> mixedIndivuals ) 
	{
		// Aplicamos la mutaci�n a los hijos
		Random rd = new Random();
		if( this.mutationType == MutationType.None)
		{
			// No hacemos nada
		}
		else if( this.mutationType == MutationType.Uniforme)
		{
			// Tan solo un tanto por ciento de las veces se efectuar� un n�mero de mutaciones
			if( rd.nextFloat() > 0.5f )
			{
				// Seleccionamos un porcentaje de individuos a los que mutar alguna de las variables de sus genomas respectivos
				// Maximo se muta la mitad de los individuos seleccionados
				int selectionNumb = (int)( mixedIndivuals.size() * rd.nextFloat() * 0.5f );
				if( selectionNumb > 0 )
				{
					ArrayList<Integer> arrayListSelectionIndex = Utils.generateDifferentValues( selectionNumb, mixedIndivuals.size() );
					//System.out.println("Elementos mutados = " + selectionNumb);
					int count = selectionNumb;
					while( count > 0 )
					{
						// Si se est� evolucionando un individuo con genoma formado por variables de tipo binario
						if( this.problemScope.varType == Abstract_Problem.VarType.BINARY )
						{
							// Se elige al azar un elemento del gen�ma binario para mutarlo
							int genIndex = rd.nextInt(this.problemScope.varCount);
							int individualIndex = arrayListSelectionIndex.get(count - 1);
							if( mixedIndivuals.get(individualIndex).genoma.get(genIndex).intValue() == 0 )
							{
								mixedIndivuals.get(individualIndex).genoma.set(genIndex, 1);
							}else mixedIndivuals.get(individualIndex).genoma.set(genIndex, 0);
						}
						count--;
					}
				}
			}
		}
		
		return mixedIndivuals;
	}

	/**
	 * El m�todo de reemplazo
	 * Se recibe la lista de nuevos individuos del proceso de mutaci�n y se insertan en la poblaci�n que evoluciona el AG
	 * De momento est�n implementados los algoritmos de reemplazo aleatorios y por fitness ( los peores )
	 */
	@Override
	protected void replacement(ArrayList<Individual<ArrayList<Number>>> newGeneration) 
	{
		if( this.inmigrationNumber > 0 )
		{
			// Se a�aden los individuos correspondientes al array NewGeneration
			for( int i=0; i<this.inmigrationNumber; i++ )
			{
				newGeneration.add(this.problemScope.getNewIndividual());
			}
		}
		// Los individuos a reemplazar se seleccionan aleatoriamente
		if( this.replacementType == ReplacementType.Random )
		{
			// Se seleccionan aleatoriamente tantos individuos como hayamos modificado
			ArrayList<Integer> arrayListSelectionIndex = Utils.generateDifferentValues( newGeneration.size(), this.populationSize );
			// Subsituimos los individuos seleccionados por los modificados ( newGeneration )
			int count = 0;
			while ( count < newGeneration.size() )
			{
				// Se calcula el fitness de los nuevos individuos generados antes de insertarlos en la poblaci�n
				calculateFitness(newGeneration.get(count));
				int index1 = arrayListSelectionIndex.get(count);
				// Se hace la substituci�n
				this.population.set(index1, newGeneration.get(count));
				count++;
			}
		}
		// Se substituyen los individuos que peor fitness tengan
		else if( this.replacementType == ReplacementType.Worst )
		{
			// Se ordenan los elementos en funci�n del fitness
			// de menor a mayor fitness
			orderPopulation();
			// Se copian los nuevos individuos generados sobreescribiendo los peores
			// se comienza substituyendo el �ltimo y luego se continua ascendentemente las posiciones que se necesiten
			// seg�n el n�mero de individuos modificados que se tengan.
			int index = this.population.size()-1;
			for(Individual<ArrayList<Number>> individual: newGeneration)
			{
				// Se calcula el fitness de los individuos modificados antes de insertarlos en la poblaci�n
				calculateFitness(individual);
				this.population.set(index, individual);
				index--;
			}
		}
		// Se eligen los individuos a sustituir por torneo
		else if( this.replacementType == ReplacementType.Tournament )
		{
	
			// Seleccionamos los individuos aleatorios necesarios de los cuales luego por torneo ser�n sustituidos los peores
			int selectionNumb = newGeneration.size() * 2;
			ArrayList<Integer> arrayListSelectionIndex = Utils.generateDifferentValues( selectionNumb, this.populationSize );
			// Los comparamos de dos en dos y seleccionamos el que peor fitness tenga 
			int count = 0;
			
			// El �ndice para extraer los individuos de la poblaci�n evolucionada
			int newGenerationIndex = 0;
			
			while( count < selectionNumb - 1 )
			{
				// Se calcula el fitness de los individuos modificados antes de insertarlos en la poblaci�n
				calculateFitness( newGeneration.get(newGenerationIndex) );
				
				int index1 = arrayListSelectionIndex.get( count );
				int index2 = arrayListSelectionIndex.get( count + 1 );
				float fitness1 = this.population.get( index1 ).fitness;
				float fitness2 = this.population.get( index2 ).fitness;
				
				// Se escoge al peor, el que peor fitness tenga de los dos
				if( fitness1 < fitness2 )
				{
					
					this.population.set(index2, newGeneration.get(newGenerationIndex) );
				}
				else
				{
					this.population.set(index1, newGeneration.get(newGenerationIndex) );
				}
				
				count++;
				count++;
				newGenerationIndex++;
			}
		}
	}

	/**
	 * Establece la condici�n de parada para el demonio evolutivo
	 * El algoritmo evolutivo se detiene si alguno de los individuos 
	 * de la poblaci�n consigue tener un fitness menor que el margen de error indicado
	 * o si se ha superado el m�ximo establecido de iteraciones sin rebajar el mejor fitness
	 * Por otro lado, no se detiene sin haber completado antes al menos un m�nimo de iteraciones, a menos que
	 * se consiga superar el error indicado.
	 */
	@Override
	protected boolean converge() 
	{
		// Primero se ordena la poblaci�n en funci�n del fitness de cada individuo
		// de mejor a peor fitness ( o sea, de menor a mayor )
		this.orderPopulation();
					
		// Solo se sigue evolucionando la poblaci�n si :
		// 1.- No se han realizado un m�nimo de iteraciones
		// 2.- No se han realizado un m�ximo de iteraciones sin rebajar el mejor fitness
		if( this.iterationsCount < this.min_ED_AG_Iterations || this.badIterationsCount < this.maxBadIterations )
		{
			
			// Se analiza el primero que es el que mejor fitness tiene
			// Si es mejor que el fitness del individuo guardado como mejor hasta el momento
			if( this.population.get(0).fitness < this.bestFitness )
			{
				// Se actualiza el valor del mejor fitness
				this.bestFitness = this.population.get(0).fitness;
				// Se pone a cero el contador de iteraciones fallidas sin bajar el mejor fitness
				this.badIterationsCount = 0;
			}
			else this.badIterationsCount++;
			
			// Si se ha conseguido rebajar el error fijado en un inicio se termina el demonio evolutivo
			// Tambi�n se detiene si se ha sobrepasado el m�ximo de iteraciones totales permitido por el �mbito del problema
			// Se podr�a considerar detener el algoritmo cuando la tasa de mejora bajara de un umbral
			if( this.population.get(0).fitness < this.errorMargin || this.iterationsCount >= this.problemScope.maxTotalIterations )
			{
				System.out.println("***************************************************");
				System.out.println("Se ha rebajado el margen de error o se ha sobrepasado el l�mite de iteraciones m�ximas");
				System.out.println("Iteraciones Totales = " + ( this.iterationsCount ) );
				System.out.println("Iteraciones sin mejorar el fitness = " +  this.badIterationsCount);
				System.out.println("Mejor fitness conseguido = " + this.population.get(0).fitness);
				System.out.println("***************************************************");
				System.out.println("GENOMA DEL INDIVIDUO SELECCIONADO");
				printGenoma(this.population.get(0));
				System.out.println("***************************************************");
				// Se guarda el mejor individuo obtenido despu�s de la evoluci�n en el �mbito del problema
				this.problemScope.finalIndividual = this.population.get(0);
				return true;
			}
		
		}
		else
		{
			System.out.println("**************************************************************");
			System.out.println("**************************************************************");
			System.out.println("No se ha rebajado el margen de error");
			System.out.println("Se ha superado el n�mero de iteraciones m�nimo y ");
			System.out.println("se ha superado el n�mero de iteraciones max permitido sin mejorar el fitness");
			System.out.println("Iteraciones Totales = " + ( this.iterationsCount ) );
			System.out.println("Iteraciones sin mejorar el fitness = " +  this.badIterationsCount);
			System.out.println("Mejor fitness conseguido = " + this.population.get(0).fitness);
			System.out.println("**************************************************************");
			System.out.println("GENOMA DEL INDIVIDUO SELECCIONADO");
			printGenoma(this.population.get(0));
			System.out.println("***************************************************");
			System.out.println("**************************************************************");
			// Se guarda el mejor individuo obtenido despu�s de la evoluci�n en el �mbito del problema
			this.problemScope.finalIndividual = this.population.get(0);
			return true;
		}
		
		this.iterationsCount++;
		
		System.out.println("***************************************************");
		System.out.println("Genetic Algorithm Iteration : " + this.iterationsCount);
		System.out.println("Iteraciones sin mejorar el fitness = " +  this.badIterationsCount);
		System.out.println("Mejor Fitness de la poblaci�n actual : " + this.population.get(0).fitness);
		System.out.println("Mejor Fitness absoluto : " + this.bestFitness);
		System.out.println("***************************************************");
		
		return false;
	}

	/**
	 * Calcula el fitness de un individuo concreto de la poblaci�n y guarda
	 * el valor dentro de este mismo individuo
	 */
	@Override
	protected void calculateFitness( Individual<ArrayList<Number>> individual) 
	{
		// Se calcula el fitness para este individuo
		individual.fitness = this.problemScope.calculateFitness(individual);
	}

	/**
	 * Ordena la poblaci�n en funci�n del fitness ( de menor a mayor fitness )
	 * Se usa el m�todo de la burbuja
	 */
	@Override
	protected void orderPopulation()
	{
		//System.out.println( "*** Antes de ordenar ***" );
		//printFitness();
		for( int i=0; i<this.population.size()-1; i++ )
		{
			for( int j=0; j<this.population.size()-1; j++ )
			{
				if( this.population.get(j+1).fitness < this.population.get(j).fitness )
				{
					Collections.swap( this.population, j, j+1 );
				}
			}
		}
		//System.out.println( "*** Despu�s de ordenar ***" );
		//printFitness();
	}
	
	/**
	 * Genera una m�scara aleatoria para el cruce uniforme en funci�n 
	 * del n�mero de variables que tenga cada individuo en su genoma
	 * @return Un array de 1 y 0 que simboliza la m�scara 
	 */
	private ArrayList<Integer> generateMask()
	{
		ArrayList<Integer> newMask = new ArrayList<Integer>();
		int count = this.problemScope.varCount;
		Random rd = new Random();
		int contador = 0; // Para asegurarnos que al menos va a haber alg�n 1
		while( count > 0 )
		{
			if( rd.nextFloat() > 0.5 )
			{
				newMask.add( 1 );
				contador ++;
			}
			else newMask.add( 0 );
			count--;
		}
		if( contador == 0 ) newMask.set(0, 1); // Para asegurar que la m�scara no resulte todo ceros
		
		return newMask;
	}
	
	/**
	 * Muestra por pantalla el fitness de la lista de individuos para debuguear
	 */
	@SuppressWarnings("unused")
	private void printFitness()
	{
		int index = 0;
		for(Individual<ArrayList<Number>> ind: this.population)
		{
			System.out.println( "index : " + index + " - fitness = " + ind.fitness );
			index++;
		}
	}
	
	/**
	 * Muestra por pantalla el genoma de un individuo para debuguear
	 */
	private void printGenoma(Individual<ArrayList<Number>> _individual)
	{
		int index = 0;
		for( int i=0; i<(_individual.genoma).size(); i++ )
		{
			System.out.println( "var : " + index + " - valor = " + _individual.genoma.get(index) );
			index++;
		}
	}

	
		
	
	
}


