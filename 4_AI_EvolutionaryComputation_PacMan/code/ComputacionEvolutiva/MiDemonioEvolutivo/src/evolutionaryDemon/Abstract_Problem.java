package evolutionaryDemon;


/**
 * Clase abstracta que establece las variables y m�todos que tienen que tener como m�nimo
 * aquellos problemas que se quieran resolver con programaci�n evolutiva
 * Es posible que hubiera que heredar un caso concreto para algoritmos gen�ticos y otro para programaci�n gen�tica
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 * @param <genoma> El tipo de codificaci�n del individuo que se va a evolucionar
 */
public abstract class Abstract_Problem<genoma>
{
	/**
	 *  Para saber si se est� en modo testing o normal
	 *  En modo TEST1 por ejemplo, las redes neuronales se generan con todas las posibles conexiones entre sus neuronas ( o sea con un genoma con todos sus valores a 1 )
	 *  y con los pesos de estas conexiones a 1, incluido el Bias.
	 */
	public enum Mode{ NORMAL, TEST1 };
	/**
	 *  Para saber si se est� en modo testing o normal
	 */
	public Mode mode;
	/**
	 * El n�mero de variables o elementos que tiene el arraylist de cada individuo 
	 */
	int varCount;
	/**
	 * El valor m�ximo que se le puede asignar a cada variable en la creaci�n del individuo
	 */
	int varMax;
	/**
	 * El valor m�nimo que se le puede asignar a cada variable en la creaci�n del individuo
	 */
	int varMin;
	/**
	 * Se permiten crear algoritmos gen�ticos cuyos individuos sean arrays de valores de diferentes tipos
	 * seg�n el problema que se desee resolver y por tanto el fenotipo que tenga que tener cada individuo
	 */
	enum VarType{ NATURAL, INTEGER, BINARY, FLOAT };
	/**
	 * Cada individuo es un array de valores de un tipo determinado ( Int, Float, etc... )
	 */
	VarType varType;
	/**
	 * Indica si se se dispone ya del mejor individuo posible en relaci�n al problema planteado
	 */
	boolean bestIndividualFound;
	/**
	 * Se guarda una referencia al mejor individuo encontrado tras el proceso de creaci�n y evoluci�n
	 */
	public Individual<genoma> finalIndividual;
	/**
	 * El margen de error que se considera admisible para detener tanto el algoritmo gen�tico como el algoritmo de backpropagation
	 * o cualquier otro algoritmo que se dise�ara, en el caso de que un individuo de la poblaci�n consiguiera un fitness por debajo de ese valor
	 */
	protected float marginError = 0;
	/**
	 * El m�ximo de iteraciones que se pueden hacer tanto del algoritmo de backpropagation como del algoritmo gen�tico
	 */
	protected int maxTotalIterations = 0;
	
	/**
	 * Constructor 
	 * @param _mode Se le pasa el modo en el que opera, normal o con diferentes modos de testing
	 */
	public Abstract_Problem( Mode _mode )
	{
		this.mode = _mode;
	}
	
	/**
	 * Calcula el fitness de un individuo concreto
	 * @param _individual El individuo al que se le calcula el fitness
	 * @return El valor del fitness
	 */
	protected abstract float calculateFitness( Individual<genoma> _individual );
	/**
	 * Inicializa los individuos y los evoluciona buscando una soluci�n al problema o un individuo que se adapte lo mejor
	 * posible a las condiciones impuestas
	 */
	protected abstract void getBestIndividual();
	/**
	 * Devuelve un individuo acorde con el problema que se trata de resolver
	 * @return Individual-genoma- El nuevo individuo creado
	 */
	protected abstract Individual<genoma> getNewIndividual();
	/**
	 * Devuelve un individuo acorde con el problema que se trata de resolver
	 * y creado como copia de uno pasado por par�metro
	 * @param _origin El individuo original que se usa para construir uno nuevo como copia de este
	 * @return El individuo reci�n creado
	 */
	protected abstract Individual<genoma> getIndividualCopy( Individual<genoma> _origin );
	
	
}
