package dataRecording;

import pacman.game.Constants;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;
import pacman.game.Game;

/**
 * Le hemos a�adido algunos m�todos para facilitar la pr�ctica
 * y tambi�n algunos atributos propios
 * @author Versi�n por Javier Barreiro P. y Miguel A. Torres
 *
 */
public class DataTuple 
{

	public enum DiscreteTag 
	{
		VERY_LOW, LOW, MEDIUM, HIGH, VERY_HIGH, NONE;

		public static DiscreteTag DiscretizeDouble(double aux) 
		{
			if (aux < 0.1)
				return DiscreteTag.VERY_LOW;
			else if (aux <= 0.3)
				return DiscreteTag.LOW;
			else if (aux <= 0.5)
				return DiscreteTag.MEDIUM;
			else if (aux <= 0.7)
				return DiscreteTag.HIGH;
			else
				return DiscreteTag.VERY_HIGH;
		}
	}

	public MOVE DirectionChosen;

	// General game state this - not normalized!
	public int mazeIndex;
	public int currentLevel;
	public int pacmanPosition;
	public int pacmanLivesLeft;
	public int currentScore;
	public int totalGameTime;
	public int currentLevelTime;
	public int numOfPillsLeft;
	public int numOfPowerPillsLeft;

	// Ghost this, dir, dist, edible - BLINKY, INKY, PINKY, SUE
	public boolean isBlinkyEdible = false;
	public boolean isInkyEdible = false;
	public boolean isPinkyEdible = false;
	public boolean isSueEdible = false;

	public int blinkyDist = -1;
	public int inkyDist = -1;
	public int pinkyDist = -1;
	public int sueDist = -1;

	public MOVE blinkyDir;
	public MOVE inkyDir;
	public MOVE pinkyDir;
	public MOVE sueDir;

	// Util data - useful for normalization
	public int numberOfNodesInLevel;
	public int numberOfTotalPillsInLevel;
	public int numberOfTotalPowerPillsInLevel;
	private int maximumDistance = 150;
	
	// Atributos a�adidos
	
	// Distancia a las p�ldoras
	public int minPillsDistance = -1;
	public int minPowerPillsDistance = -1;
	
	// Direcci�n a las p�ldoras
	public MOVE dirToNearerPill = MOVE.NEUTRAL;
	public MOVE dirToNearerPowerPill = MOVE.NEUTRAL;
	
	// Direcci�n para escapar del fantasma m�s cercano
	public MOVE dirAwayFromClosestGhost;
	
	// Direcci�n para ir a por el fantasma m�s cercano
	// public MOVE dirTowardsClosestGhost;
	

	/**
	 * Constructor al que se le pasa el objeto juego y un movimiento determinado
	 * @param game El objeto juego que guarda una referencia de las propiedades del juego en un instante del mismo
	 * @param move Un objeto de tipo MOVE
	 */
	public DataTuple(Game game, MOVE move) 
	{
		if (move == MOVE.NEUTRAL) 
		{
			move = game.getPacmanLastMoveMade();
		}

		this.DirectionChosen = move;

		this.mazeIndex = game.getMazeIndex();
		this.currentLevel = game.getCurrentLevel();
		this.pacmanPosition = game.getPacmanCurrentNodeIndex();
		this.pacmanLivesLeft = game.getPacmanNumberOfLivesRemaining();
		this.currentScore = game.getScore();
		this.totalGameTime = game.getTotalTime();
		this.currentLevelTime = game.getCurrentLevelTime();
		this.numOfPillsLeft = game.getNumberOfActivePills();
		this.numOfPowerPillsLeft = game.getNumberOfActivePowerPills();

		if (game.getGhostLairTime(GHOST.BLINKY) == 0) {
			this.isBlinkyEdible = game.isGhostEdible(GHOST.BLINKY);
			this.blinkyDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.BLINKY));
		}

		if (game.getGhostLairTime(GHOST.INKY) == 0) {
			this.isInkyEdible = game.isGhostEdible(GHOST.INKY);
			this.inkyDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.INKY));
		}

		if (game.getGhostLairTime(GHOST.PINKY) == 0) {
			this.isPinkyEdible = game.isGhostEdible(GHOST.PINKY);
			this.pinkyDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.PINKY));
		}

		if (game.getGhostLairTime(GHOST.SUE) == 0) {
			this.isSueEdible = game.isGhostEdible(GHOST.SUE);
			this.sueDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.SUE));
		}

		this.blinkyDir = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.BLINKY), DM.PATH);
		this.inkyDir = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.INKY), DM.PATH);
		this.pinkyDir = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.PINKY), DM.PATH);
		this.sueDir = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.SUE), DM.PATH);

		this.numberOfNodesInLevel = game.getNumberOfNodes();
		this.numberOfTotalPillsInLevel = game.getNumberOfPills();
		this.numberOfTotalPowerPillsInLevel = game.getNumberOfPowerPills();
		
		// Atributos a�adidos
		
		// Direcci�n para llegar a las p�ldoras m�s cercanas
		// Si no quedan p�ldoras tomar� los valores con los que se inicializan las variables
		
		// Para las p�ldoras normales
		
		boolean flag = false;
		for( int nodeIndex : game.getActivePillsIndices())
		{
			int auxDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), nodeIndex );
			if( flag == false )
			{
				flag = true;
				this.minPillsDistance = auxDist;
				this.dirToNearerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}
			else if( auxDist < this.minPillsDistance )
			{
				this.minPillsDistance = auxDist;
				this.dirToNearerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}	
		}
		
		// Para las p�ldoras Power
		flag = false;
		for( int nodeIndex : game.getActivePowerPillsIndices() )
		{
			int auxDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), nodeIndex );
			if( flag == false )
			{
				flag = true;
				this.minPowerPillsDistance = auxDist;
				this.dirToNearerPowerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}
			else if( auxDist < this.minPowerPillsDistance )
			{
				this.minPowerPillsDistance = auxDist;
				this.dirToNearerPowerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}	
		}
		
	
		// Direcci�n para escapar del fantasma m�s cercano
		
		if( this.blinkyDist > this.inkyDist && this.blinkyDist > this.sueDist && this.blinkyDist > this.pinkyDist )
		{
			this.dirAwayFromClosestGhost = game.getNextMoveAwayFromTarget( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.BLINKY), DM.PATH);
		}
		else if( this.inkyDist > this.sueDist && this.inkyDist > this.pinkyDist )
		{
			this.dirAwayFromClosestGhost = game.getNextMoveAwayFromTarget( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.PINKY), DM.PATH);
		}
		else if( this.sueDist > this.pinkyDist )
		{
			this.dirAwayFromClosestGhost = game.getNextMoveAwayFromTarget( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.SUE), DM.PATH);
		}
		else 
		{
			this.dirAwayFromClosestGhost = game.getNextMoveAwayFromTarget( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(GHOST.INKY), DM.PATH);
		}
		
		// Direcci�n para ir a por el fantasma m�s cercano
		// this.dirTowardsClosestGhost;

	}
	
	/**
	 * Constructor al que se le pasa el objeto juego 
	 * Este constructor lo usamos cuando queremos recorrer el �rbol de nodos, creamos un objeto DataTuple
	 * para poder hacer uso de los m�todos propios a�adidos a esta clase
	 * Es una copia del anterior pero no se le pasa el objeto MOVE
	 * @param game El objeto juego que guarda una referencia de las propiedades del juego en un instante del mismo
	 */
	public DataTuple(Game game) 
	{
		// Se invoca el constructor original
		this( game, MOVE.NEUTRAL );
	}

	/**
	 * Constructor al que se le pasa una cadena
	 * @param data Los datos de un instante del juego en formato String 
	 */
	public DataTuple(String data) 
	{
		String[] dataSplit = data.split(";");

		this.DirectionChosen = MOVE.valueOf(dataSplit[0]);

		this.mazeIndex = Integer.parseInt(dataSplit[1]);
		this.currentLevel = Integer.parseInt(dataSplit[2]);
		this.pacmanPosition = Integer.parseInt(dataSplit[3]);
		this.pacmanLivesLeft = Integer.parseInt(dataSplit[4]);
		this.currentScore = Integer.parseInt(dataSplit[5]);
		this.totalGameTime = Integer.parseInt(dataSplit[6]);
		this.currentLevelTime = Integer.parseInt(dataSplit[7]);
		this.numOfPillsLeft = Integer.parseInt(dataSplit[8]);
		this.numOfPowerPillsLeft = Integer.parseInt(dataSplit[9]);
		this.isBlinkyEdible = Boolean.parseBoolean(dataSplit[10]);
		this.isInkyEdible = Boolean.parseBoolean(dataSplit[11]);
		this.isPinkyEdible = Boolean.parseBoolean(dataSplit[12]);
		this.isSueEdible = Boolean.parseBoolean(dataSplit[13]);
		this.blinkyDist = Integer.parseInt(dataSplit[14]);
		this.inkyDist = Integer.parseInt(dataSplit[15]);
		this.pinkyDist = Integer.parseInt(dataSplit[16]);
		this.sueDist = Integer.parseInt(dataSplit[17]);
		this.blinkyDir = MOVE.valueOf(dataSplit[18]);
		this.inkyDir = MOVE.valueOf(dataSplit[19]);
		this.pinkyDir = MOVE.valueOf(dataSplit[20]);
		this.sueDir = MOVE.valueOf(dataSplit[21]);
		this.numberOfNodesInLevel = Integer.parseInt(dataSplit[22]);
		this.numberOfTotalPillsInLevel = Integer.parseInt(dataSplit[23]);
		this.numberOfTotalPowerPillsInLevel = Integer.parseInt(dataSplit[24]);
		
		// Atributos propios
		this.dirAwayFromClosestGhost = MOVE.valueOf(dataSplit[25]);
		this.minPillsDistance = Integer.parseInt(dataSplit[26]);
		this.dirToNearerPill = MOVE.valueOf(dataSplit[27]);
		this.minPowerPillsDistance = Integer.parseInt(dataSplit[28]);
		this.dirToNearerPowerPill = MOVE.valueOf(dataSplit[29]);
	}

	/**
	 * Construye una cadena con todos los datos obtenidos en un determinado estado del juego ( un frame )
	 * @return String la cadena con tod
	 */
	public String getSaveString() {
		StringBuilder stringbuilder = new StringBuilder();

		stringbuilder.append(this.DirectionChosen + ";");
		stringbuilder.append(this.mazeIndex + ";");
		stringbuilder.append(this.currentLevel + ";");
		stringbuilder.append(this.pacmanPosition + ";");
		stringbuilder.append(this.pacmanLivesLeft + ";");
		stringbuilder.append(this.currentScore + ";");
		stringbuilder.append(this.totalGameTime + ";");
		stringbuilder.append(this.currentLevelTime + ";");
		stringbuilder.append(this.numOfPillsLeft + ";");
		stringbuilder.append(this.numOfPowerPillsLeft + ";");
		stringbuilder.append(this.isBlinkyEdible + ";");
		stringbuilder.append(this.isInkyEdible + ";");
		stringbuilder.append(this.isPinkyEdible + ";");
		stringbuilder.append(this.isSueEdible + ";");
		stringbuilder.append(this.blinkyDist + ";");
		stringbuilder.append(this.inkyDist + ";");
		stringbuilder.append(this.pinkyDist + ";");
		stringbuilder.append(this.sueDist + ";");
		stringbuilder.append(this.blinkyDir + ";");
		stringbuilder.append(this.inkyDir + ";");
		stringbuilder.append(this.pinkyDir + ";");
		stringbuilder.append(this.sueDir + ";");
		stringbuilder.append(this.numberOfNodesInLevel + ";");
		stringbuilder.append(this.numberOfTotalPillsInLevel + ";");
		stringbuilder.append(this.numberOfTotalPowerPillsInLevel + ";");
		
		// Atributos propios
		stringbuilder.append(this.dirAwayFromClosestGhost + ";");
		stringbuilder.append(this.minPillsDistance + ";");
		stringbuilder.append(this.dirToNearerPill + ";");
		stringbuilder.append(this.minPowerPillsDistance + ";");
		stringbuilder.append(this.dirToNearerPowerPill + ";");
		
		return stringbuilder.toString();
	}
	
	/**
	 * Devuelve True o False en funci�n de si el atributo pasado por par�metro toma el mismo valor que la tupla respecto ese mismo par�metro
	 * @param attribute El atributo de tipo String a evaluar
	 * @param value El valor de tipo String
	 * @return boolean True si toma el mismo valor en la tupla y False en caso contrario
	 */
	public boolean checkSameValue( String attribute, String value )
	{
		if( attribute == "isBlinkyEdible" )
		{
			return ( String.valueOf(this.isBlinkyEdible) == value );
		}
		else if( attribute == "isInkyEdible" )
		{
			return ( String.valueOf(this.isInkyEdible) == value );
		}
		else if( attribute == "isPinkyEdible" )
		{
			return ( String.valueOf(this.isPinkyEdible) == value );
		}
		else if( attribute == "isSueEdible" )
		{
			return ( String.valueOf(this.isSueEdible) == value );
		}
		else if( attribute == "blinkyDist" )
		{
			return ( this.discretizeDistance(this.blinkyDist).toString() == value );
		}
		else if( attribute == "inkyDist" )
		{
			return ( this.discretizeDistance(this.inkyDist).toString() == value );
		}
		else if( attribute == "pinkyDist" )
		{
			return ( this.discretizeDistance(this.pinkyDist).toString() == value );
		}
		else if( attribute == "sueDist" )
		{
			return ( this.discretizeDistance(this.sueDist).toString() == value );
		}
		else if( attribute == "blinkyDir" )
		{
			return ( String.valueOf(this.blinkyDir) == value );
		}
		else if( attribute == "inkyDir" )
		{
			return ( String.valueOf(this.inkyDir) == value );
		}
		else if( attribute == "pinkyDir" )
		{
			return ( String.valueOf(this.pinkyDir) == value );
		}
		else if( attribute == "sueDir" )
		{
			return ( String.valueOf(this.sueDir) == value );
		}
		else if( attribute == "numOfPillsLeft" )
		{
			return ( this.discretizeNumberOfPills(this.numOfPillsLeft).toString() == value );
		}
		else if( attribute == "numOfPowerPillsLeft" )
		{
			return ( this.discretizeNumberOfPowerPills(this.numOfPowerPillsLeft).toString() == value );
		}
		else if( attribute == "dirAwayFromClosestGhost" )
		{
			return ( String.valueOf(this.dirAwayFromClosestGhost) == value );
		}
		else if( attribute == "minPillsDistance" )
		{
			return ( this.discretizeDistance(this.minPillsDistance).toString() == value );
		}
		else if( attribute == "dirToNearerPill" )
		{
			return ( String.valueOf(this.dirToNearerPill) == value );
		}
		else if( attribute == "minPowerPillsDistance" )
		{
			return ( this.discretizeDistance(this.minPowerPillsDistance).toString() == value );
		}
		else if( attribute == "dirToNearerPowerPill" )
		{
			return ( String.valueOf(this.dirToNearerPowerPill) == value );
		}
		
		return false;
	}
	
	/**
	 * M�todo que obtiene el valor convertido a String normalizado de un atributo determinado pas�ndole el nombre como String
	 * @param attribute El atributo de tipo String que ser� devuelto normalizado
	 * @return String el valor normalizado del atributo convertido a String
	 */
	public String getNormalizedAttributeValue( String attribute )
	{
		if( attribute == "isBlinkyEdible" )
		{
			return String.valueOf(this.isBlinkyEdible);
		}
		else if( attribute == "isInkyEdible" )
		{
			return String.valueOf(this.isInkyEdible);
		}
		else if( attribute == "isPinkyEdible" )
		{
			return String.valueOf(this.isPinkyEdible);
		}
		else if( attribute == "isSueEdible" )
		{
			return String.valueOf(this.isSueEdible);
		}
		else if( attribute == "blinkyDist" )
		{
			return this.discretizeDistance(this.blinkyDist).toString();
		}
		else if( attribute == "inkyDist" )
		{
			return this.discretizeDistance(this.inkyDist).toString();
		}
		else if( attribute == "pinkyDist" )
		{
			return this.discretizeDistance(this.pinkyDist).toString();
		}
		else if( attribute == "sueDist" )
		{
			return this.discretizeDistance(this.sueDist).toString();
		}
		else if( attribute == "blinkyDir" )
		{
			return String.valueOf(this.blinkyDir);
		}
		else if( attribute == "inkyDir" )
		{
			return String.valueOf(this.inkyDir);
		}
		else if( attribute == "pinkyDir" )
		{
			return String.valueOf(this.pinkyDir);
		}
		else if( attribute == "sueDir" )
		{
			return String.valueOf(this.sueDir);
		}
		else if( attribute == "numOfPillsLeft" )
		{
			return this.discretizeNumberOfPills(this.numOfPillsLeft).toString();
		}
		else if( attribute == "numOfPowerPillsLeft" )
		{
			return this.discretizeNumberOfPowerPills(this.numOfPowerPillsLeft).toString();
		}
		else if( attribute == "dirAwayFromClosestGhost" )
		{
			return String.valueOf(this.dirAwayFromClosestGhost);
		}
		else if( attribute == "minPillsDistance" )
		{
			return this.discretizeDistance(this.minPillsDistance).toString();
		}
		else if( attribute == "dirToNearerPill" )
		{
			return String.valueOf(this.dirToNearerPill);
		}
		else if( attribute == "minPowerPillsDistance" )
		{
			return this.discretizeDistance(this.minPowerPillsDistance).toString();
		}
		else if( attribute == "dirToNearerPowerPill" )
		{
			return String.valueOf(this.dirToNearerPowerPill);
		}

		return null;
	}
	
	/**
	 * Devuelve el �ndice de la variable de tipo MOVE ( por ejemplo, si es UP devolver� 0, si es DOWN devolver� 2 )
	 * @return El �ndice de la variable que indica la clase ( MOVE ) en formato int
	 */
	public int getNomalizeClass()
	{
		return this.DirectionChosen.ordinal();
	}

	/**
	 * Used to normalize distances. Done via min-max normalization. Supposes
	 * that minimum possible distance is 0. Supposes that the maximum possible
	 * distance is 150.
	 * 
	 * @param dist
	 *            Distance to be normalized
	 * @return Normalized distance
	 */
	public double normalizeDistance(int dist) {
		return ((dist - 0) / (double) (this.maximumDistance - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizeDistance(int dist) {
		if (dist == -1)
			return DiscreteTag.NONE;
		double aux = this.normalizeDistance(dist);
		return DiscreteTag.DiscretizeDouble(aux);
	}

	public double normalizeLevel(int level) {
		return ((level - 0) / (double) (Constants.NUM_MAZES - 0)) * (1 - 0) + 0;
	}

	public double normalizePosition(int position) {
		return ((position - 0) / (double) (this.numberOfNodesInLevel - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizePosition(int pos) {
		double aux = this.normalizePosition(pos);
		return DiscreteTag.DiscretizeDouble(aux);
	}

	public double normalizeBoolean(boolean bool) {
		if (bool) {
			return 1.0;
		} else {
			return 0.0;
		}
	}

	public double normalizeNumberOfPills(int numOfPills) {
		return ((numOfPills - 0) / (double) (this.numberOfTotalPillsInLevel - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizeNumberOfPills(int numOfPills) {
		double aux = this.normalizeNumberOfPills(numOfPills);
		return DiscreteTag.DiscretizeDouble(aux);
	}

	public double normalizeNumberOfPowerPills(int numOfPowerPills) {
		return ((numOfPowerPills - 0) / (double) (this.numberOfTotalPowerPillsInLevel - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizeNumberOfPowerPills(int numOfPowerPills) {
		double aux = this.normalizeNumberOfPowerPills(numOfPowerPills);
		return DiscreteTag.DiscretizeDouble(aux);
	}

	public double normalizeTotalGameTime(int time) {
		return ((time - 0) / (double) (Constants.MAX_TIME - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizeTotalGameTime(int time) {
		double aux = this.normalizeTotalGameTime(time);
		return DiscreteTag.DiscretizeDouble(aux);
	}

	public double normalizeCurrentLevelTime(int time) {
		return ((time - 0) / (double) (Constants.LEVEL_LIMIT - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizeCurrentLevelTime(int time) {
		double aux = this.normalizeCurrentLevelTime(time);
		return DiscreteTag.DiscretizeDouble(aux);
	}

	/**
	 * 
	 * Max score value lifted from highest ranking PacMan controller on PacMan
	 * vs Ghosts website: http://pacman-vs-ghosts.net/controllers/1104
	 * 
	 * @param score
	 * @return
	 */
	public double normalizeCurrentScore(int score) {
		return ((score - 0) / (double) (82180 - 0)) * (1 - 0) + 0;
	}

	public DiscreteTag discretizeCurrentScore(int score) {
		double aux = this.normalizeCurrentScore(score);
		return DiscreteTag.DiscretizeDouble(aux);
	}

}
