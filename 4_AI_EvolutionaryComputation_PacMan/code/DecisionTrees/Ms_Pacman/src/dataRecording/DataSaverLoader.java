package dataRecording;

import pacman.game.util.*;

/**
 * This class uses the IO class in the PacMan framework to do the actual saving/loading of
 * training data.
 * Le hemos a�adido el m�todo que guarda el �rbol de decisi�n como un string en un txt 
 * Tambi�n hemos modificado el m�todo LoadPacManData para que admita el path pasado 
 * por par�metro del archivo que se desea cargar. Esto nos permite cargar otro archivo 
 * txt de tuplas para el test.
 * @author andershh Versi�n por Javier Barreiro P. y Miguel A. Torres
 *
 */
public class DataSaverLoader {
	
	private static String FileName = "trainingData.txt";
	//private static String FileName_Testing = "trainingData_testing.txt";
	
	private static String TreeFileName = "decisionTree.txt";
	
	public static void SavePacManData(DataTuple data)
	{
		IO.saveFile(FileName, data.getSaveString(), true);
	}
	
	public static void SaveDecisionTree( String _string )
	{
		IO.saveFile(TreeFileName, _string, true);
	}
	
	public static DataTuple[] LoadPacManData( String filePath )
	{
		String data = IO.loadFile(filePath);
		String[] dataLine = data.split("\n");
		DataTuple[] dataTuples = new DataTuple[dataLine.length];
		
		for(int i = 0; i < dataLine.length; i++)
		{
			dataTuples[i] = new DataTuple(dataLine[i]);
		}
		
		return dataTuples;
	}
}
