package decisionTree;

import java.util.HashMap;

import dataRecording.DataTuple;

/**
 * Implementa los diferentes m�todos necesarios para los diferentes m�todos de selecci�n de atributos
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class AttributeSelection 
{
	
	public static enum SelectionMethod{ ID3, C4_5, CART };
	
	/**
	 * Selecciona el atributo que ofrece mayor informaci�n de un determinado dataset
	 * Usando un algoritmo determinado pasado por par�metro
	 * @param dTArray El dataset de tipo DataTuple[]
	 * @param hashMap La lista de par�metros ( map ) de tipo HashMap<String, String[]>
	 * @param selectionMethod El algoritmo de selecci�n que se va a usar
	 * @param classNumber El n�mero de resultados diferentes que puede ofrecer cada tupla de tipo int
	 * ( por ejemplo : UP, RIGHT, DOWN, LEFT, NEUTRAL => 5 )
	 * @return Attribute el atributo de mayor relevancia
	 */
	static public Attribute SelectAttribute( DataTuple[] dTArray, HashMap<String, String[]> hashMap, SelectionMethod selectionMethod, int classNumber )
	{
		// Control de seguridad
		if( hashMap.isEmpty() ) return null;
		
		if( selectionMethod == SelectionMethod.ID3 )
		{
			// Primero calculamos la entrop�a del dataset
			// La info necesaria para clasificar una tupla en el dataset
			float entropy = CalculateEntropy( dTArray, classNumber );
			// Luego calculamos la ganancia de info de cada atributo
			float higherAttributeGain = 0.0f;
			Attribute attributeSelected = null;
			String clave;
		    java.util.Iterator<String> iterator = hashMap.keySet().iterator();
		    // Recorremos el mapa de atributos
		    while(iterator.hasNext())
		    {
		        clave = iterator.next();
		        Attribute newAttribute = new Attribute( clave, hashMap.get(clave) );
		        //System.out.println(clave + " - " + hashMap.get(clave));
		        float auxGain = CalculateAttributeGain( dTArray, newAttribute, entropy, classNumber );
		        // Si es el primer atributo del que se calcula su ganancia
		        // tomamos este atributo como seleccionado para luego ir comparando
		        if( attributeSelected == null )
		    	{
		        	attributeSelected = newAttribute;
		        	higherAttributeGain = auxGain;
		    	}
		        else if( auxGain > higherAttributeGain )
		        {
		        	attributeSelected = newAttribute;
		        	higherAttributeGain = auxGain;
		        }
		    }    
		    return attributeSelected;
		}
		else if( selectionMethod == SelectionMethod.C4_5 )
		{
			// Primero calculamos la entrop�a del dataset
			// La info necesaria para clasificar una tupla en el dataset
			float entropy = CalculateEntropy( dTArray, classNumber );
			// Calculamos la tasa de ganancia de cada atributo
			float higherAttributeGainRatio = 0.0f;
			Attribute attributeSelected = null;
			String id;
			java.util.Iterator<String> iterator = hashMap.keySet().iterator();
			// Recorremos los atributos
			while( iterator.hasNext() )
			{
				id = iterator.next();
				Attribute newAttribute = new Attribute( id, hashMap.get(id) );
				
				float auxGainRatio = CalculateAttributeGainRatio( dTArray, newAttribute, entropy, classNumber );
				
				if( attributeSelected == null )
				{
					attributeSelected = newAttribute;
					higherAttributeGainRatio = auxGainRatio;
				}
				else if ( auxGainRatio > higherAttributeGainRatio )
				{
					attributeSelected = newAttribute;
					higherAttributeGainRatio = auxGainRatio;
				}
			}
			
			return attributeSelected;
		}
		return null;
	}
	
	/**
	 * Calcula la ganancia de cada atributo
	 * @param dTArray El dataset de tuplas de inicio
	 * @param attribute El atributo del que se desea saber la ganancia
	 * @param entropy La entrop�a del dataset de inicio
	 * @param classNumber El n�mero de posibilidades en las que se clasifica una tupla
	 * @return float - Devuelve la ganancia del atributo
	 */
	private static float CalculateAttributeGain( DataTuple[] dTArray, Attribute attribute, float entropy, int classNumber )
	{
		float attributeGain = 0.0f;
		float info = 0.0f;
		for( int i = 0; i < attribute.values.length; i++ )
		{
			// Obtenemos el subdataset para este atributo determinado cuando toma este valor determinado
			DataTuple[] subDTArray = DataSetTools.getSubDataSet( dTArray, attribute, attribute.values[i] );
			// La info se incrementa con la que proporcina cada valor
			if( subDTArray != null )
			{
				// float rep = CountAttributeValueRepetition( dTArray, attribute, attribute.values[i] );
				float rep = subDTArray.length;
				info += ( rep / (float)dTArray.length ) * CalculateEntropy( subDTArray, classNumber );
			}
			else
			{
				 // System.out.println("subDTArray = null");
			}
		}
		attributeGain = entropy - info;
		return attributeGain;
	}
	
	/**
	 * Calcula la tasa de ganancia de cada atributo
	 * @param dTArray El dataset de tuplas de inicio
	 * @param attribute El atributo del que se desea saber la tasa de ganancia
	 * @param entropy La entrop�a del dataset de inicio
	 * @param classNumber El n�mero de posibilidades en las que se clasifica una tupla
	 * @return float - Devuelve la tasa de ganancia del atributo
	 */
	private static float CalculateAttributeGainRatio( DataTuple[] dTArray, Attribute attribute, float entropy, int classNumber )
	{
		float attributeGainRatio = 0.0f;
		float attributeGain = 0.0f;
		float splitInfo = 0.0f;
		
		for( int i = 0; i < attribute.values.length; i++ )
		{
			DataTuple[] subDTArray = DataSetTools.getSubDataSet( dTArray, attribute, attribute.values[i] );
			
			if( subDTArray != null )
			{
				float valueTuple = (float)subDTArray.length / (float)dTArray.length;	
				splitInfo += ( -( valueTuple ) * ( Math.log( valueTuple ) / Math.log(2) ) );
			}
		}
		// Calculamos la ganancia del atributo
		attributeGain = CalculateAttributeGain( dTArray, attribute, entropy, classNumber );
		// Calculamos la tasa de ganancia
		attributeGainRatio = attributeGain / splitInfo;
		
		return attributeGainRatio;
	}
	
	/**
	 * Calcula el n�mero de tuplas dentro del dataset que tienen el valor pasado por par�metro para este atributo
	 * @param dTArray El dataset
	 * @param attribute El atributo 
	 * @param value El valor del atributo 
	 * @return
	 */
	private static int CountAttributeValueRepetition( DataTuple[] dTArray, Attribute attribute, String value ) 
	{
		int rep = 0;
		for( int i = 0; i < dTArray.length; i++ )
		{
			if( dTArray[i].checkSameValue(attribute.name, value) ) rep++;
		}
		return rep;
	}

	/**
	 * Calcula la entrop�a de un dataset, o sea, la informaci�n necesaria para clasificar dicho dataset
	 * @param dTArray El dataset
	 * @param classNumber El n�mero de resultados diferentes que puede ofrecer cada tupla
	 * @return float La entrop�a del dataset
	 */
	private static float CalculateEntropy( DataTuple[] dTArray, int classNumber )
	{
		// El valor de entrop�a que devolvemos
		float entropy = 0;
		// Si el dataset est� vac�o devolvemos el valor 0
		if( dTArray.length == 0 ) return 0;
		
		// Bucle con un n�mero de ciclos igual al n�mero de valores en los que se puede clasificar cada dato
		for( int i = 0; i < classNumber; i++ )
		{
			// Para contar el n�mero de veces que se repite un resultado en el dataset
			float count = 0.0f;
			// Recorremos el dataset para contar la repetici�n de cada clase
			for( int j = 0; j < dTArray.length; j++ )
			{
				// Contamos las veces que se repite el resultado normalizado "i" en el dataset
				if( dTArray[j].getNomalizeClass() == i ) count++;
			}
			// Calculamos la probabilidad de que una tupla se clasifique como "i"
			if( count > 0 )
			{
				float probability = count / ( float )dTArray.length;
				entropy += -probability * ( Math.log(probability) / Math.log(2) );
			}
		}
		// Devolvemos el valor
		return entropy;
	}
}
