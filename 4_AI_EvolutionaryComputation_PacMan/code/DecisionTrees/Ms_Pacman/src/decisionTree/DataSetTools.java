package decisionTree;

import java.util.ArrayList;
import java.util.HashMap;

import pacman.game.Constants.MOVE;

import dataRecording.DataTuple;

/**
 * Implementa m�todos que operan sobre un conjunto de tuplas
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class DataSetTools 
{
	/**
	 * Devuelve el conjunto de tuplas en las que un atributo toma un determinado valor
	 * @param DTArray Le pasamos el dataset sobre el que calcular el nuevo subconjunto de tuplas
	 * @param attribute El atributo que se usa para determinar el subdataset
	 * @param attributeValue El valor que tienen que cumplir las tuplas para ser incluidas en el subdataset
	 * @return DataTuple[] un conjunto de tuplas
	 */
	static public DataTuple[] getSubDataSet(DataTuple[] DTArray, Attribute attribute, String attributeValue) 
	{
		// Usamos un ArrayList para crear el nuevo subconjunto de datos
		ArrayList<DataTuple> newDTArrayList = new ArrayList<DataTuple>(); 
		// Recorremos todas las tuplas para comprobar si el atributo toma el mismo valor que el pasado por par�metro
		for( DataTuple dataTuple : DTArray )
		{
			// Si el valor del atributo de la tupla es el mismo que el pasado por par�metro incluimos
			// dicha tupla en el nuevo ArrayList
			String auxValue = dataTuple.getNormalizedAttributeValue(attribute.name);
			// System.out.println("auxValue = " + auxValue);
			// System.out.println("attributeValue = " + attributeValue);
			if( auxValue == attributeValue )
			{
				newDTArrayList.add(dataTuple);
			}
		}
		
		// Convertimos el ArrayList din�mico en un Array est�tico para devolverlo
		if( newDTArrayList.isEmpty() )
		{
			return null;
		}
		else 
		{
			DataTuple[] newDTArray = new DataTuple[ newDTArrayList.size() ];
			newDTArray = newDTArrayList.toArray(newDTArray);
			return newDTArray;
		}
	}
	
	/**
	 * Calcula la clase mayoritaria de un conjunto de tuplas
	 * @param DTArray El dataset para el que se calcula la clase mayoritaria
	 * @return MOVE la clase mayoritaria en este caso de tipo MOVE
	 */
	static public MOVE CalculateMayorityClass(DataTuple[] DTArray) 
	{
		// Creamos un hashMap para ir contando las repeticiones de cada clase
		HashMap<MOVE, Integer> hashMap = new HashMap<MOVE, Integer>();
		
		Integer i = 0;
		hashMap.put( MOVE.UP, 0 );
		hashMap.put( MOVE.RIGHT, 0 );
		hashMap.put( MOVE.DOWN, 0 );
		hashMap.put( MOVE.LEFT, 0 );
		hashMap.put( MOVE.NEUTRAL, 0 );
		
		for( DataTuple dataTuple : DTArray )
		{
			if( dataTuple.DirectionChosen == MOVE.UP )
			{
				if( hashMap.get(MOVE.UP) != null )
				i = (Integer)hashMap.get(MOVE.UP);
				hashMap.put( MOVE.UP, i + 1 );
			}
			else if( dataTuple.DirectionChosen == MOVE.RIGHT )
			{
				if( hashMap.get(MOVE.RIGHT) != null )
				i = (Integer)hashMap.get(MOVE.RIGHT);
				hashMap.put( MOVE.RIGHT, i + 1 );
			}
			else if( dataTuple.DirectionChosen == MOVE.DOWN )
			{
				if( hashMap.get(MOVE.DOWN) != null )
				i = (Integer)hashMap.get(MOVE.DOWN);
				hashMap.put( MOVE.DOWN, i + 1 );
			}
			else if( dataTuple.DirectionChosen == MOVE.LEFT )
			{
				if( hashMap.get(MOVE.LEFT) != null )
				i = (Integer)hashMap.get(MOVE.LEFT);
				hashMap.put( MOVE.LEFT, i + 1 );
			}
			else if( dataTuple.DirectionChosen == MOVE.NEUTRAL )
			{
				if( hashMap.get(MOVE.NEUTRAL) != null )
				i = (Integer)hashMap.get(MOVE.NEUTRAL);
				hashMap.put( MOVE.NEUTRAL, i + 1 );
			}
		}
		
		// Devolvemos la clase que m�s se haya repetido
		if( hashMap.get( MOVE.UP ) >= hashMap.get( MOVE.RIGHT ) && 
			hashMap.get( MOVE.UP ) >= hashMap.get( MOVE.DOWN ) && 
			hashMap.get( MOVE.UP ) >= hashMap.get( MOVE.LEFT ) &&
			hashMap.get( MOVE.UP ) >= hashMap.get( MOVE.NEUTRAL ) )
		{
			return MOVE.UP;
		}
		else if( hashMap.get( MOVE.RIGHT ) >= hashMap.get( MOVE.DOWN ) && 
				 hashMap.get( MOVE.RIGHT ) >= hashMap.get( MOVE.LEFT ) &&
				 hashMap.get( MOVE.RIGHT ) >= hashMap.get( MOVE.NEUTRAL ) )
		{
			return MOVE.RIGHT;
		}
		else if( hashMap.get( MOVE.DOWN ) >= hashMap.get( MOVE.LEFT ) && 
				 hashMap.get( MOVE.DOWN ) >= hashMap.get( MOVE.NEUTRAL ) )
		{
			return MOVE.DOWN;
		}
		else if( hashMap.get( MOVE.LEFT ) >= hashMap.get( MOVE.NEUTRAL ) )
		{
			return MOVE.LEFT;
		}
		else return MOVE.NEUTRAL;
	}
	
	/**
	 * Devuelve True si la clase del conjunto de tuplas e la misma y False si no tienen la misma clase todas las tuplas
	 * @param DTArray El dataset para el que se chequea si todas sus tuplas tienen la misma clase
	 * @return True o False 
	 */
	static public boolean CheckSameClass(DataTuple[] DTArray)
	{
		// Tomamos la clase del primer valor como referencia
		MOVE directionChosenRef =  DTArray[0].DirectionChosen;
		for( DataTuple dataTuple : DTArray )
		{
			if( directionChosenRef != dataTuple.DirectionChosen )
			{
				return false;
			}
		}
		// Si hemos recorrido todos los elementos es que tienen la misma clase
		return true;
	}
	

}
