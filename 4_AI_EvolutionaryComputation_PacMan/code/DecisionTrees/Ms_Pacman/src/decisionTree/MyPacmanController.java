package decisionTree;

import dataRecording.DataTuple;

import pacman.controllers.Controller;
import pacman.game.Constants.MOVE;
import pacman.game.Game;

/**
 * Controlador para Pacman que hace uso de un �rbol de decisi�n para moverlo
 * Implementa la interfaz Controller MOVE
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class MyPacmanController extends Controller<MOVE> 
{
	// Atributos de la clase
	
	/**
	 *  Nodo raiz del �rbol de decisi�n
	 */
	private TreeNode treeRoot;
	
	/**
	 * Constructor
	 */
	public MyPacmanController()
	{
		// Creamos el �rbol de decisi�n y guardamos una referencia del nodo raiz
		Generator myGenerator = new Generator();
		treeRoot = myGenerator.root;
	}
	
	/**
	 * Dado un estado de juego concreto este m�todo devuelve una direcci�n de movimiento para Pacman
	 * Puede devolver tambi�n NEUTRAL que usar� la direcci�n anteriormente devuelta
	 * @param game El estado del juego en un instante concreto
	 * @param timeDue El tiempo transcurrido desde la anterior vez que se invoc� este m�todo
	 */
	public MOVE getMove(Game game, long timeDue) 
	{
		if( treeRoot == null ) return null;
		
		// Primero creamos un DataTuple a partir del objeto juego
		// para poder hacer uso de los m�todos de discretizaci�n de valores
		DataTuple newDataTuple = new DataTuple( game );
		// Devolvemos el movimiento seg�n el estado del juego en ese instante
		// usando el �rbol de decisi�n
		return getNextMove( newDataTuple, treeRoot );
	}
	
	/**
	 * M�todo recursivo que usando la informaci�n del estado del juego en un instante concreto
	 * ofrecida por el objeto "game", va recorriendo el �rbol de decisi�n hasta encontrar un nodo hoja
	 * devolviendo su valor
	 * @param game
	 * @param _node
	 * @return
	 */
	private MOVE getNextMove( DataTuple newDataTuple, TreeNode _treeNode )
	{
		// Si es un nodo hoja devolvemos su valor
		if( _treeNode.name == "Leaf" )
		{
			LeafNode leafNode = (LeafNode)_treeNode;
			return leafNode.move;
		}
		else // Si no es un nodo hoja continuamos recorriendo el �rbol de decisi�n
		{
			// Lo cacheamos a DecisionNode
			DecisionNode newDecisionNode = (DecisionNode)_treeNode;
			// Ahora analizamos este nodo, en funci�n del nombre responder� a un atributo determinado
			// que podr� tomar unos valores determinados
			// Seleccionamos el nodo hijo cuyo valor de parentBranch coincida con el valor que tiene el estado del juego respecto a ese atributo
			// en un instante del mismo
			for( TreeNode treeNode : newDecisionNode.childrenNodesArray )
			{
				if( newDataTuple.checkSameValue( newDecisionNode.name, treeNode.parentBranch ) )
				{
					// Seguimos recorriendo el �rbol de decisi�n por esa rama
					return getNextMove( newDataTuple, treeNode );
				}
			}
		}
		return null;
	}
	

}
