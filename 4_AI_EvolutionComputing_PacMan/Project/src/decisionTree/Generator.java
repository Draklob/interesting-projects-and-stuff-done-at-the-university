package decisionTree;

import jTree.Ventana;

import java.util.HashMap;
import dataRecording.DataSaverLoader;
import dataRecording.DataTuple;

/**
 * Esta clase genera un �rbol de decisi�n en funci�n
 * de unos atributos, unos datos clasificados y un m�todo de obtenci�n de nodos relevantes
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class Generator 
{

	// Atributos de la clase
	
	/**
	 * El nodo raiz del �rbol de decisi�n
	 */
	public TreeNode root;
	
	/**
     * Constructor por defecto para la clase ControllerBasedOnDecisionTree
     */
	public Generator( )
	{
		// Los datos obtenidos de las pruebas se guardan en un archivo de texto.
		// Los cargamos en un array de objetos de tipo DataTuple usando el m�todo est�tico
		// LoadPacManData de la clase DataSaverLoader
		DataTuple[] tuplesArray = DataSaverLoader.LoadPacManData( "trainingData.txt" );
		System.out.println("Muestra usada para generar el �rbol de decisi�n = " + tuplesArray.length);
		// Creamos el array din�mico de atributos
		// La selecci�n de atributos considerados relevantes para crear el �rbol de decisi�n
		AttributeCollection	attributeCollection = new AttributeCollection();
		
		// Construimos el �rbol de decisi�n guardando la referencia al nodo raiz del mismo
		root = GenerateTree( tuplesArray, attributeCollection.hashMap, "" );
		
		// Guardamos el �rbol generado para debuguear
		// SaveTree();
		// Testeamos la eficiencia del �rbol de nodos sobre otros datos cargados desde otro archivo de texto
		Testing();
		// Creamos una representaci�n visual del �rbol de nodos para debuguear
		GenerateJTree();
	}
	
	/**
     * M�todo recursivo que genera el �rbol de decisi�n
     * @param DTArray El array de tuplas que contiene la informaci�n de partida
     * @param hashMap Un mapa de atributos con los que se crear�n los nodos del �rbol
     * @return TreeNode el nodo raiz para posteriormente poder recorrer el �rbol de decici�n generado por este m�todo
     */
	private TreeNode GenerateTree( DataTuple[] DTArray, HashMap<String,String[]> hashMap, String _parentBranch )
	{
		// � Tienen todas las tuplas la misma clase ?
		if( DataSetTools.CheckSameClass(DTArray) )
		{
			// Si todas las tuplas de este dataset tienen la misma clase devolvemos un nodo hoja con esta clase
			LeafNode newLeafNode = new LeafNode( _parentBranch, DTArray[0].DirectionChosen );
			// Devolvemos este nodo hoja reci�n creado
			return newLeafNode;
		}
		else if( hashMap.size() == 0 ) // Si la lista de atributos est� vac�a, en este caso el map 
		{
			// Calculamos la clase mayoritar�a y creamos un nodo hoja con esta clase
			LeafNode newLeafNode = new LeafNode( _parentBranch, DataSetTools.CalculateMayorityClass(DTArray) );
			// Devolvemos este nodo hoja reci�n creado
			return newLeafNode;
		}
		else
		{
			// Aplicamos el m�todo de selecci�n de atributo
			Attribute newAttributeSelected = AttributeSelection.SelectAttribute( DTArray, hashMap, AttributeSelection.SelectionMethod.ID3, 5 );
			// Creamos un nodo de decisi�n y lo etiquetamos con el nombre del atributo seleccionado
			DecisionNode newDecisionNode = new DecisionNode( newAttributeSelected.name, _parentBranch );
			// Eliminamos este atributo de la lista de atributos
			hashMap.remove(newAttributeSelected.name);
			// Recorremos los posibles valores del atributo
			for( String attributeValue : newAttributeSelected.values )
			{
				// Creamos el subconjunto de datos para cada valor del nuevo atributo seleccionado
				DataTuple[] newDTArray = DataSetTools.getSubDataSet( DTArray, newAttributeSelected, attributeValue );
				// Si el subconjunto est� vac�o a�adimos a newDecisionNode una hoja etiquetada con la clase mayoritaria de D
				if( newDTArray == null )
				{
					// Calculamos la clase mayoritar�a y creamos un nodo hoja pas�ndole el valor del atributo por el que se accede desde el padre
					// y el valor de esta clase mayoritaria
					LeafNode newLeafNode = new LeafNode( attributeValue, DataSetTools.CalculateMayorityClass(DTArray) );
					// Se lo a�adimos a newDecisionNode como un hijo.
					newDecisionNode.childrenNodesArray.add(newLeafNode);
				}
				else
				{
					// Obtenemos el nodo raiz del nuevo �rbol que se genera a partir del nodo en el que estamos 
					// pas�ndole el subconjunto de datos generado, la lista de atributos resultante 
					// y el valor del nodo padre por el que se accede a �l.
					// Le tenemos que pasar una copia del mapa de atributos, porque si no pasa una referencia y 
					// despu�s de ejecutar esta funci�n recursiva el mapa de atributos se queda vac�o
					HashMap<String,String[]> hashMapCopy = CopyHashMap( hashMap );
					TreeNode newChildNode = GenerateTree( newDTArray, hashMapCopy, attributeValue );
					// A�adimos este nuevo nodo hijo a newDecisionNode
					newDecisionNode.childrenNodesArray.add(newChildNode);
				}
			}
			// Devolvemos el nodo
			return newDecisionNode;
			
		}
	}
	
	/**
	 * Devuelve una copia de un mapa de atributos
	 * @param hashMap
	 * @return
	 */
	private HashMap<String,String[]> CopyHashMap( HashMap<String,String[]> hashMap )
	{
		HashMap<String,String[]> newHashMap = new HashMap<String,String[]>();
		// Recorremos el mapa de atributos original para volcarlo en el nuevo
		String clave;
	    java.util.Iterator<String> iterator = hashMap.keySet().iterator();
	    // Recorremos el mapa de atributos
	    while(iterator.hasNext())
	    {
	        clave = iterator.next();
	        newHashMap.put( clave, hashMap.get(clave) );
	    }
		return newHashMap;
	}
	
	/**
	 * M�todo que eval�a la fiabilidad del �rbol de decisi�n sobre un conjunto de tuplas
	 * Muestra por pantalla el tanto por ciento de acierto.
	 */
	private void Testing()
	{
		// Cargamos los datos que usamos para testear el �rbol reci�n creado
		DataTuple[] tuplesArray = DataSaverLoader.LoadPacManData( "trainingData_testing.txt" );
		System.out.println("Muestra usada para testear el �rbol de decisi�n = " + tuplesArray.length);
		
		float evaluated = 0;
		float successful = 0;

		// Recorremos las tuplas para contrastar su clase con la que nos devuelve el �rbol de decisi�n
		for( DataTuple dataTuple : tuplesArray )
		{
			evaluated ++;
			if( TestTuple( dataTuple, root ) ) successful ++;
		}
		
		float result = successful/evaluated * 100;
		// Muestra por pantalla el resultado de eficiencia del �rbol de nodos
		// sobre una muestra diferente de tuplas
		System.out.println("Result = " + result);
	}
	
	/**
	 * Recorre el �rbol de nodos siguiendo los valores de la tupla para los atributos 
	 * representados por los diferentes nodos y devuelve true si la clase de la tupla coincide 
	 * con el valor de la hoja a la que se llegue recorriendo dicho �rbol
	 * @param dataTuple
	 * @param _treeNode
	 * @return true si la tupla se clasifica correctamente mediante dicho �rbol y false si no se clasifica correctamente
	 */
	private boolean TestTuple( DataTuple dataTuple, TreeNode _treeNode )
	{
		// Si es un nodo hoja devolvemos true si la clase 
		// es la misma que la de la tupla y false en caso contrario
		if( _treeNode.name == "Leaf" )
		{
			LeafNode leafNode = (LeafNode)_treeNode;
			if( dataTuple.DirectionChosen == leafNode.move )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else // Si no es un nodo hoja continuamos recorriendo el �rbol de decisi�n
		{
			// Lo cacheamos a DecisionNode
			DecisionNode newDecisionNode = (DecisionNode)_treeNode;
			// Ahora analizamos este nodo, en funci�n del nombre responder� a un atributo determinado
			// que podr� tomar unos valores determinados
			// Seleccionamos el nodo hijo cuyo valor de parentBranch coincida con el valor que tiene la tupla respecto a ese atributo
			for( TreeNode treeNode : newDecisionNode.childrenNodesArray )
			{
				if( dataTuple.checkSameValue( newDecisionNode.name, treeNode.parentBranch ) )
				{
					// Seguimos recorriendo el �rbol de decisi�n por esa rama
					return TestTuple( dataTuple, treeNode );
				}
			}
		}
		return false;
	}
		
	/**
	 * M�todo que crea la visualizaci�n en pantalla del �rbol de nodos generado como si fuera un directorio
	 * usando el objeto JTree de la librer�a java.swing
	 */
	private void GenerateJTree()
	{
		Ventana miVentana= new Ventana( root );
		miVentana.setVisible(true);
	}
	
	/**
	 * Guarda el �rbol generado como un string en un archivo de texto
	 * usando la misma clase proporcionada por el programa DataSaverLoader
	 */
	private void SaveTree()
	{
		if ( root.name == "Leaf") return;
		// Casteamos el nodo raiz a nodo de decisi�n
		DecisionNode decisionNode = (DecisionNode)root;
		// El contenido del �rbol lo volcamos sobre un objeto Stringbuilder
		StringBuilder stringbuilder = new StringBuilder();
		ConvertToString( stringbuilder, decisionNode );
		DataSaverLoader.SaveDecisionTree( stringbuilder.toString() );
	}
	
	/**
	 * Recorre recursivamente el �rbol generado convirti�ndolo a StringBuilder
	 * @param stringbuilder El contenedor donde se va volcando el contenido del �rbol de decisi�n
	 * @param newDecisionNode El nodo que se va evaluando recursivamente
	 */
	private void ConvertToString( StringBuilder stringbuilder, DecisionNode newDecisionNode )
	{
		// Recorremos los nodos hijos para agregarlos al string
		for( TreeNode treeNode : newDecisionNode.childrenNodesArray )
		{
			// Si es un nodo hoja lo cacheamos como nodo hoja
			if( treeNode.name == "Leaf" )
			{
				LeafNode leafNode = (LeafNode)treeNode;
				stringbuilder.append("LeafNode" + ";" + "ParentBranch" + ";" + leafNode.parentBranch + ";" + "MOVE" + ";" + leafNode.move + ";" + "       ");
			}
			else
			{
				DecisionNode decisionNode = (DecisionNode)treeNode;
				stringbuilder.append("DecisionNode" + ";" + "Name" + ";" + decisionNode.name + ";" + "ParentBranch" + ";" + decisionNode.parentBranch + ";" + "--------");
				// Seguimos recorriendo el �rbol de decisi�n por esa rama
				ConvertToString( stringbuilder, decisionNode );
			}
		}
	}
	
	
}// Fin de la clase Generator
