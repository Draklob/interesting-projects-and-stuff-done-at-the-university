package decisionTree;

/**
 * Esta clase define un posible atributo para el �rbol de decisi�n
 * Cada atributo tiene un nombre que le identifica y una lista finita ( discreta ) de posibles valores 
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class Attribute 
{
	// Atributos de la clase
	
	/**
	 *  El nombre del atributo ( su id )
	 */
	public String name;
	/**
	 *  La lista de posibles valores
	 */
	public String[] values;
	
	/**
     * Constructor para la clase Attribute
     * @param _name El identificador del atributo de tipo String
     * @param _values Un array de elementos de tipo String con los posibles valores
     */
	public  Attribute(String _name, String[] _values) 
	{
		this.name = _name;
		this.values = _values;
	}
	// Fin del constructor
	
}// Fin de la clase Attibute