package decisionTree;

import java.util.HashMap;

/**
 * Esta clase construye los diferentes atributos que se van a tener en cuenta en la creaci�n
 * del �rbol de decisi�n y guarda una referencia a ellos en un objeto de tipo Map
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class AttributeCollection 
{
	// Atributos de la clase
	
	// El mapa cuya clave es el nombre de los atributos y su valor un array con los posibles valores
	public HashMap<String, String[]> hashMap;
		
	/**
     * Constructor por defecto para la clase AttributeCollection
     * A�adimos directamente los identificadores de atributos ( name ) con sus arrays de valores
     * No creamos previamente los objetos Attribute, pero luego s� usamos estos objetos para recorrer el mapa de atributos
     */
	public  AttributeCollection() 
	{
		hashMap = new HashMap<String, String[]>();
		
		// Array de posibles valores boleanos convertidos a string
		// Lo usamos para los posibles valores que puede tomar 
		// el indicador de que un fantasma puede ser comible
		String[] boolValues = new String[2];
		boolValues[0] = "true";
		boolValues[1] = "false";
		
		// Array de 5 posibles valores discretizados de tipo string
		// Lo usamos para los valores que puede tomar la distancia de Pacman a cada Ghost
		String[] discreteTagValues = new String[6];
		discreteTagValues[0] = "NONE";
		discreteTagValues[1] = "VERY_LOW";
		discreteTagValues[2] = "LOW";
		discreteTagValues[3] = "MEDIUM";
		discreteTagValues[4] = "HIGH";
		discreteTagValues[5] = "VERY_HIGH";
		
		// Array de 5 posibles valores discretizados de tipo string
		// Lo usamos para los valores que pueden tomar la direcci�n de movimiento de cada Ghost
		String[] dirValues = new String[5];
		dirValues[0] = "UP";
		dirValues[1] = "RIGHT";
		dirValues[2] = "DOWN";
		dirValues[3] = "LEFT";
		dirValues[4] = "NEUTRAL";
		
		
		// Attibute isBlinkyEdible
		hashMap.put( "isBlinkyEdible", boolValues );
		// Attibute isInkyEdible
		hashMap.put( "isInkyEdible", boolValues );
		// Attibute isPinkyEdible
		hashMap.put( "isPinkyEdible", boolValues );	
		// Attibute isSueEdible
		hashMap.put( "isSueEdible", boolValues );
		
		// Attibute blinkyDist
		hashMap.put( "blinkyDist", discreteTagValues );
		// Attibute inkyDist
		hashMap.put( "inkyDist", discreteTagValues );
		// Attibute pinkyDist
		hashMap.put( "pinkyDist", discreteTagValues );
		// Attibute sueDist
		hashMap.put( "sueDist", discreteTagValues );
		
		// Attibute blinkyDir
		hashMap.put( "blinkyDir", dirValues );
		// Attibute inkyDir
		hashMap.put( "inkyDir", dirValues );
		// Attibute pinkyDir
		hashMap.put( "pinkyDir", dirValues );
		// Attibute sueDir
		hashMap.put( "sueDir", dirValues );
		
		// Attibute numOfPillsLeft
		/*hashMap.put( "numOfPillsLeft", discreteTagValues);
		
		// Attibute numOfPowerPillsLeft
		hashMap.put( "numOfPowerPillsLeft", discreteTagValues);
		
		// Attibute numOfPowerPillsLeft
		hashMap.put( "dirAwayFromClosestGhost", dirValues);*/
		
		
		// Attibute minPillsDistance
		hashMap.put( "minPillsDistance", discreteTagValues);
		
		// Attibute minPillsDistance
		hashMap.put( "dirToNearerPill", dirValues);
		
		// Attibute minPowerPillsDistance
		hashMap.put( "minPowerPillsDistance", discreteTagValues);
		
		// Attibute minPillsDistance
		hashMap.put( "dirToNearerPowerPill", dirValues);
	}
	// Fin del constructor
	
}
// Fin de la clase AttributeCollection

