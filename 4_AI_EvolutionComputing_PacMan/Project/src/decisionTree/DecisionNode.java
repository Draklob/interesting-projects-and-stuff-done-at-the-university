package decisionTree;

import java.util.ArrayList;

/**
 * Los nodos de decisi�n con los que componemos el �rbol de Decisi�n
 * Hereda de TreeNode y tiene una lista de nodos hijos
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class DecisionNode extends TreeNode 
{
	// Atributos 
	
	/**
	 * Array de nodos hijos
	 */
	public ArrayList<TreeNode> childrenNodesArray;		
	
	/**
     * Constructor para la clase DecisionNode 
     * @param _name El identificador del nodo de tipo String
     */
	public DecisionNode(String _name) 
	{
		// Inicializamos los atributo del padre
		super(_name);
		// Inicializamos el arraylist
		childrenNodesArray = new ArrayList<TreeNode>();
	}// Fin del constructor 
	
	/**
     * Constructor para la clase DecisionNode
     * @param _name El identificador del nodo de tipo String
     * @param _parentBranch El nombre de la rama con la que se conecta a su nodo padre
     */
	public DecisionNode(String _name, String _parentBranch) 
	{
		// Inicializamos los atributo del padre
		super(_name, _parentBranch);
		// Inicializamos el arraylist
		childrenNodesArray = new ArrayList<TreeNode>();
	}// Fin del constructor 

}
