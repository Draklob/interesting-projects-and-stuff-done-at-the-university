/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

import java.util.ArrayList;

import java.lang.Math;

/**
 * Clase que gestiona el dungeon, hereda de Map
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class DFSMap extends Map
{
	//El path encontrado ( guardamos solamente el �ltimo )
	public ArrayList<Cell> path;
	
	/** 
     *	El constructor
     */
	public DFSMap( Map.MapType _mapType, Map.CellType _cellType, int _rows, int _cols )
	{	
		//Llamamos al constructor padre
		super( _mapType, _cellType, _rows, _cols );

		//Creamos el path ( en principio solo para el dungeon )
		path = new ArrayList<Cell>();	
	}
	
	
	/** 
     *	Genera aleatoriamente la conexi�n entre las celdas del dungeon
     *	Al final todas quedan conectadas
     */
	public void generate()
	{
		//Invocamos el m�todo padre que se encarga de resetear las celdas
		super.generate();
		
		//Generamos la fila y la columna de la casilla de salida aleatoriamente
		int row = this.random.nextInt(this.rows);
		int col = this.random.nextInt(this.cols);
		//Invocamos la funci�n recursiva que genera el dungeon con la celda de salida
		chooseAnotherCell( this.cellArray[row][col] );
		//Despues de generar el mapa se quedan las celdas como visitadas con lo que volvemos
		//a ponerlas como no visitadas
		this.setCellsNotVisited();
	}
		
	/** 
     *	M�todo recursivo para generar el dungeon ( B�squeda en profundidad - DFS )
     */
	private void chooseAnotherCell( Cell currentCell )
	{
		//Marcamos esta celda como visitada
		currentCell.visited = true;
		
		//Pintamos el mapa para debuguear
		if( Application.debug == true )
		{
			this.draw();
			//this.Pause();
		}
		//Variable para contar cuantas celdas llevamos analizadas 
		//en busca de una no visitada
		int count = 0;
		
		/***************************************************************************/
	    /*************	Si las celdas son triangulares o cuadradas *****************/
		/***************************************************************************/
		if( this.cellType == Map.CellType.Triangular || this.cellType == Map.CellType.Square )
		{
			//Sacamos un n�mero aleatorio entre 0 y 3 para comenzar a recorrer
			//las celdas vecinas en busca de una que aun no haya sido visitada
			int index = this.random.nextInt(4);
			//El n�mero m�ximo de celdas vecinas es 4
			while( count < 4 )
			{
				//Comprobamos primero que existe una celda en esa direcci�n
				if( currentCell.neighboringCells[index].cell != null )
				{
					//Si no ha sido visitada la selecionamos y seguimos el camino por ah�
					if( ! currentCell.neighboringCells[index].cell.visited )
					{
						//Seleccionamos esta celda vecina
						Cell newCell = currentCell.neighboringCells[index].cell;
						//Abrimos la conexi�n entre ambas celdas
						currentCell.neighboringCells[index].enabled = true;
						newCell.connectWithCell(index);
						//Invocamos la funci�n recursivamente con la nueva celda seleccionada
						chooseAnotherCell( newCell );
					}
				}
				
				//Elegimos otro index siempre en el intervalo de 0 y 3
				if( index < 3 ) index += 1;
				else index = 0;
				
				count ++;
			}
		}
		/************************************************************/
	    /*************	Si las celdas son hexagonales  **************/
		/************************************************************/
		else if( this.cellType == Map.CellType.Hexagonal )
		{
			//Sacamos un n�mero aleatorio entre 0 y 5 para comenzar a recorrer
			//las celdas vecinas en busca de una que aun no haya sido visitada
			int index = this.random.nextInt(6);
			//El n�mero m�ximo de celdas vecinas es 6
			while( count < 6 )
			{
				//Comprobamos primero que existe una celda en esa direcci�n
				if( currentCell.neighboringCells[index].cell != null )
				{
					//Si no ha sido visitada la selecionamos y seguimos el camino por ah�
					if( ! currentCell.neighboringCells[index].cell.visited )
					{
						//Seleccionamos esta celda vecina
						Cell newCell = currentCell.neighboringCells[index].cell;
						//Abrimos la conexi�n entre ambas celdas
						currentCell.neighboringCells[index].enabled = true;
						newCell.connectWithCell(index);
						//Invocamos la funci�n recursivamente con la nueva celda seleccionada
						chooseAnotherCell( newCell );
					}
				}
				
				//Elegimos otro index siempre en el intervalo de 0 y 5
				if( index < 5 ) index += 1;
				else index = 0;
				
				count ++;
			}
		
		}
	}
	
		
	/** 
     *	M�todo que encuentra el camino m�s corto entre una celda origen y otra destino
     *	Recibe como par�metro la celda origen y la celda destino
     */
	public boolean pathfinding( int originRow, int originCol, int targetRow, int targetCol )
	{
		//Primero reseteamos el dungeon
		dungeonReset();
		//Asignamos los valores en las celdas origen y destino para debuguear
		this.cellArray[originRow][originCol].origin = true;
		this.cellArray[targetRow][targetCol].target = true;
		//La variable boolena que indica si hemos encontrado el path
		boolean finded = false;
		//La lista abierta
		ArrayList<Cell> openList = new ArrayList<Cell>();
		//La lista cerrada
		ArrayList<Cell> closedList = new ArrayList<Cell>();
		//El coste que llevamos acumulado
		int cost = 0;
		//Cuenta las celdas vecinas que llevamos extraidas de cada celda que examinamos
		int count = 0;
		
		//Metemos la celda origen en la lista abierta
		openList.add( this.cellArray[originRow][originCol] );
		//Le asignamos unos valores de coste y heur�stica
		this.cellArray[originRow][originCol].h = 0;
		this.cellArray[originRow][originCol].c = 0;
		
		//Nos mantenemos en el bucle hasta que se encuentre el camino o hasta que 
		//la lista abierta est� vac�a en cuyo caso no se habr� encontrado el camino
		while( !finded && !openList.isEmpty() )
		{
			//Como mantengo la lista abierta ordenada por la heur�stica de cada celda
			//extraigo siempre la celda situada en el inicio de la misma
			Cell currentCell = openList.get(0);
			//Y la elimino de esta lista ( lista abierta )
			openList.remove(0);
			//Actualizo el valor del coste
			cost = currentCell.c + 1;
			
			//Si la celda es el destino hemos terminado
			if( currentCell.row == targetRow && currentCell.col == targetCol )
			{
				finded = true;
			}
			//si aun no hemos llegado a la celda destino
			else 
			{
				//Metemos esta celda en la lista cerrada
				closedList.add( currentCell );
				//Para debuguear se lo indicamos en la propia celda
				currentCell.closed = true;
				//Pintamos el mapa para debuguear
				if( Application.debug == true )
				{
					this.draw();
					Game.Pause();
				}
				//Sacamos las celdas vecinas y las metemos en la lista abierta
				//de manera ordenada
				
				//Dependiendo de si el tipo de celda es triangular, cuadrada o hexagonal
				//tenemos que examinar un n�mero determinado de celdas vecinas
				if( this.cellType == Map.CellType.Triangular || this.cellType == Map.CellType.Square )
				{
					//Ponemos a 3 el contador de celdas vecinas extraidas
					count = 3;
				}
				else if( this.cellType == Map.CellType.Hexagonal )
				{
					//Ponemos a 5 el contador de celdas vecinas extraidas
					count = 5;
				}
				//Mientras que queden celdas vecinas por examinar
				while( count >= 0 )
				{
					Cell newCell = currentCell.neighboringCells[count].cell;
					//Comprobamos si la celda vecina es transitable
					if( newCell != null && currentCell.neighboringCells[count].enabled )
					{
						//Comprobamos que no est� ya en la lista cerrada de celdas expandidas ( lista cerrada )
						if( ! closedList.contains( newCell ) )
						{
							//Calculamos su heur�stica
							int distance = Math.abs( targetRow - (int)newCell.row ) + Math.abs( targetCol - (int)newCell.col );
							//Correcci�n para la heur�stica en las hexagonales
							if( this.cellType == Map.CellType.Hexagonal )
							{
								if( targetRow != (int)newCell.row && targetCol != (int)newCell.col )
								{
									distance --;
								}
							}
							//Le asignamos su heur�stica y su coste
							newCell.h = cost + distance;
							newCell.c = cost;
							//Le indicamos su padre
							newCell.parentCell = currentCell;
							//Comprobamos si estaba ya en la lista abierta
							boolean exist = false;
							int index = 0;
							for( int i=0; i < openList.size(); i++ )
							{
								if( newCell.row == openList.get(i).row && newCell.col == openList.get(i).col )
								{
									exist = true;
									index = i;
									break;
								}
							}
							//Si no estaba ya en la lista abierta
							if( ! exist )
							{
								this.insertIntoOpenList( openList, newCell, currentCell );
							}
							//Si ya estaba en la lista abierta
							else if( exist )
							{
								//Si la heur�stica por este camino es mayor no hacemos nada
								//Si es menor nos quedamos con este nuevo camino para esta celda
								if( newCell.h <= openList.get(index).h )
								{
									//Eliminamos el anterior valor
									openList.remove( index );
									//A�adimos este nuevo valor de manera ordenada
									this.insertIntoOpenList( openList, newCell, currentCell );
								}
							}
						}
					}
					//Cambiamos a la siguiente celda
					count --;
				}
			}
		}
		
		//Si se ha encontrado el camino reconstruimos el camino
		if( finded )
		{
			//Primero vaciamos el path por si ya estuviera lleno de antes
			path.clear();
			//Metemos la celda destino
			path.add( this.cellArray[targetRow][targetCol] );
			//Y la tomamos como primera celda a examinar
			Cell parent_Cell = this.cellArray[targetRow][targetCol];
			
			//Mientras no sea la celda origen
			while( parent_Cell.row != originRow || parent_Cell.col != originCol )
			{
				parent_Cell = parent_Cell.parentCell;
				path.add( parent_Cell );
				//Marcamos la celda como visitada
				parent_Cell.visited = true;
			}
		}
		
		//Retornamos si se ha encontrado o no el camino
		return finded;
	}
	
	/** 
     *	M�todo que inserta una nueva celda en la lista abierta de manera ordenada
     */
	private void insertIntoOpenList( ArrayList<Cell> openList, Cell newCell, Cell parentCell )
	{
		//La metemos en ella de manera ordenada y le asignamos su padre
		boolean inserted = false;
		for( int i=0; i < openList.size(); i++ )
		{
			if( newCell.h <= openList.get(i).h )
			{
				openList.add( i, newCell );
				inserted = true;
				break;
			}
		}
		//Si no se ha insertado la metemos al final de la lista
		if( ! inserted )
		{
			openList.add( newCell );
		}
		//Indicamos que ha sido abierta para debuguear
		newCell.open = true;
	}
	
	
	/** 
     *	M�todo que resetea las celdas para poder iniciar el pathfinding
     */
	private void dungeonReset()
	{
		for( int i=0; i < this.rows; i++ )
		{
			for( int j=0; j < this.cols; j++ )
			{
				//Ponemos cada celda como no visitada
				this.cellArray[i][j].origin = false;
				this.cellArray[i][j].target = false;
				this.cellArray[i][j].visited = false;
				this.cellArray[i][j].closed = false;
				this.cellArray[i][j].open = false;
				this.cellArray[i][j].parentCell = null;
			}
		}
	}

}
					
				
		
		
		

