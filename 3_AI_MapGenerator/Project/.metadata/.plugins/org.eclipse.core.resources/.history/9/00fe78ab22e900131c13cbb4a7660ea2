/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

import java.util.Scanner;

/**
 * Clase que heredan de CAMap
 * Evoluciona el aut�mata con tres estados por celda
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class CAMapThreeStates extends CAMap 
{
	
	/** 
     *	El constructor
     *	@param _mapType : el tipo de mapa, _cellType : el tipo de celda, _rows : el n�mero de filas, _cols : el n�mero de columnas
     *  	   _cAutomatonType : el tipo de mapa ( isla, continente, pangea ), _cellStatesCount : el n�mero de estados por c�lula
     *  	   _seedsCount: el n�mero de semillas con la que se comienzan a generar los mapas.
     */
	public CAMapThreeStates( Game _game, Map.MapType _mapType, Map.CellType _cellType, int _rows, int _cols, 
			CAMap.CAutomatonType _cAutomatonType, int _cellStatesCount, int _seedsCount )
	{	
		//Llamamos al constructor padre
		super( _game, _mapType, _cellType, _rows, _cols, _cAutomatonType, _cellStatesCount, _seedsCount );
	}
	

	/**
	 * Evoluciona el aut�mata el n�mero de iteraciones indicado
	 */
	@Override
	void automataEvolve() 
	{
		//Iteramos seg�n vaya indicando el usuario
		int iterationCount = 0;
		//La lista auxiliar donde vamos volcando los resultados de aplicar las reglas a cada celda
		Cell.CellStates[][] cellStatesArray = new Cell.CellStates[this.rows][this.cols];
		//Recorremos las filas y las columnas para crear un estado inicial 
		for( int i=0; i < this.rows; i++ )
		{
			for( int j=0; j < this.cols; j++ )
			{
				cellStatesArray[i][j] = Cell.CellStates.Sea;
			}
		}	
		
		//Creo un objeto de la clase Scanner para leer del teclado
		Scanner sc = new Scanner( System.in );
		//Variable donde almacenamos la opci�n elegida para ser evaluada
		int option;
		
		do
		{
			System.out.println( "1 para aplicar reglas del Aut�mata Celular" );
			System.out.println( "2 para salir" );
			option = sc.nextInt();
			
			if( option == 1 )
			{
				//Aplicamos las reglas establecidas a cada celda del mapa
				//y el resultado lo guardamos en el array auxiliar
				for( int i=0; i < this.rows; i++ )
				{
					for( int j=0; j < this.cols; j++ )
					{
						//Calculamos el n�mero de vecinos de tipo "Plain"
						int plainNeighboringCells = this.cellArray[i][j].countNeighboringCellStates( Cell.CellStates.Plain );
						//Calculamos el n�mero de vecinos de tipo "Mountain"
						int mountainNeighboringCells = this.cellArray[i][j].countNeighboringCellStates( Cell.CellStates.Mountain );
						//Calculamos la suma de celdas vecinas que no sean mar ( terrain )
						int terrainNeighboringCells = plainNeighboringCells + mountainNeighboringCells;
						//En el caso de un mapa de tipo isla
						if( this.cAutomatonType == CAMap.CAutomatonType.Islands )
						{
							//En el caso de celdas cuadradas
							if( this.cellType == Map.CellType.Square )
							{
								//First Rule -> Si la celda tiene al menos una celda vecina con el atributo "cell" igual a null quiere decir que es un extremo
								//en este caso lo cambiamos a sea.
								if( plainNeighboringCells == -1 ) cellStatesArray[i][j] = Cell.CellStates.Sea;
								//Second Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 0 se convierte en sea.
								//Con esto conseguimos que no emerjan islas del mar
								else if( terrainNeighboringCells == 0 )
								{
									//De esta forma lo que sea completamente mar se mantendr� as� para poder conseguir los continentes
									cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Third Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 1 
								else if( terrainNeighboringCells == 1 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(14) > 10 )
									{
										//Si tan solo tiene una vecina de tierra la convertimos en planicie
										cellStatesArray[i][j] = Cell.CellStates.Plain;
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Fourth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 2 
								else if( terrainNeighboringCells == 2 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(20) > 7 )
									{
										//Si tiene dos vecinos de tierra 
										if( this.random.nextInt(18) > 1 )
											cellStatesArray[i][j] = Cell.CellStates.Plain;
										else cellStatesArray[i][j] = Cell.CellStates.Mountain;
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Fifth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 3 
								else if( terrainNeighboringCells == 3 )	
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(15) > 1 )
									{
										//Si tiene tres vecinos de tierra 
										//Si tiene m�s vecinos de monta�a
										if( mountainNeighboringCells > plainNeighboringCells )
										{
											if( this.random.nextInt(7) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}
										else 
										{
											if( this.random.nextInt(20) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}	
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Sixth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 4
								else if( terrainNeighboringCells == 4 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(16) > 1 )
									{
										//Si tiene cuatro vecinos de tierra 
										//Si tiene m�s vecinos de monta�a
										if( mountainNeighboringCells > plainNeighboringCells )
										{
											if( this.random.nextInt(6) > 3 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}
										else 
										{
											if( this.random.nextInt(18) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}	
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
							}
						}
						//En el caso de un mapa de tipo Pangea
						else if( this.cAutomatonType == CAMap.CAutomatonType.Pangea )
						{
							//En el caso de celdas cuadradas
							if( this.cellType == Map.CellType.Square )
							{
								//First Rule -> Si la celda tiene al menos una celda vecina con el atributo "cell" igual a null quiere decir que es un extremo
								//en este caso lo cambiamos a sea.
								if( plainNeighboringCells == -1 ) cellStatesArray[i][j] = Cell.CellStates.Sea;
								//Second Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 0 se convierte en sea.
								//Con esto conseguimos que no emerjan islas del mar
								else if( terrainNeighboringCells == 0 )
								{
									//De esta forma lo que sea completamente mar se mantendr� as� para poder conseguir los continentes
									cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Third Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 1 
								else if( terrainNeighboringCells == 1 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(15) > 10 )
									{
										//Si tan solo tiene una vecina de tierra la convertimos en planicie
										cellStatesArray[i][j] = Cell.CellStates.Plain;
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Fourth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 3 
								else if( terrainNeighboringCells == 2 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(15) > 10 )
									{
										//Si tiene dos vecinos de tierra 
										if( this.random.nextInt(18) > 1 )
											cellStatesArray[i][j] = Cell.CellStates.Plain;
										else cellStatesArray[i][j] = Cell.CellStates.Mountain;
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Fifth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 3 
								else if( terrainNeighboringCells == 3 )	
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(15) > 1 )
									{
										//Si tiene tres vecinos de tierra 
										//Si tiene m�s vecinos de monta�a
										if( mountainNeighboringCells > plainNeighboringCells )
										{
											if( this.random.nextInt(7) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}
										else 
										{
											if( this.random.nextInt(20) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}	
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Sixth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 4
								else if( terrainNeighboringCells == 4 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(15) > 1 )
									{
										//Si tiene cuatro vecinos de tierra 
										//Si tiene m�s vecinos de monta�a
										if( mountainNeighboringCells > plainNeighboringCells )
										{
											if( this.random.nextInt(6) > 3 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}
										else 
										{
											if( this.random.nextInt(18) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}	
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
							}
						}
						//En el caso de un mapa de tipo Continents
						else if( this.cAutomatonType == CAMap.CAutomatonType.Continents )
						{
							//En el caso de celdas cuadradas
							if( this.cellType == Map.CellType.Square )
							{
								//First Rule -> Si la celda tiene al menos una celda vecina con el atributo "cell" igual a null quiere decir que es un extremo
								//en este caso lo cambiamos a sea.
								if( plainNeighboringCells == -1 ) cellStatesArray[i][j] = Cell.CellStates.Sea;
								//Second Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 0 se convierte en sea.
								//Con esto conseguimos que no emerjan islas del mar
								else if( terrainNeighboringCells == 0 )
								{
									//De esta forma lo que sea completamente mar se mantendr� as� para poder conseguir los continentes
									cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Third Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 1 
								else if( terrainNeighboringCells == 1 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(7) > 2 )
									{
										//Si tan solo tiene una vecina de tierra la convertimos en planicie
										cellStatesArray[i][j] = Cell.CellStates.Plain;
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Fourth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 3 
								else if( terrainNeighboringCells == 2 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(7) > 1 )
									{
										//Si tiene dos vecinos de tierra 
										if( this.random.nextInt(18) > 1 )
											cellStatesArray[i][j] = Cell.CellStates.Plain;
										else cellStatesArray[i][j] = Cell.CellStates.Mountain;
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Fifth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 3 
								else if( terrainNeighboringCells == 3 )	
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(7) > 1 )
									{
										//Si tiene tres vecinos de tierra 
										//Si tiene m�s vecinos de monta�a
										if( mountainNeighboringCells > plainNeighboringCells )
										{
											if( this.random.nextInt(7) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}
										else 
										{
											if( this.random.nextInt(20) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}	
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
								//Sixth Rule -> Si el n�mero de celdas vecinas de tipo terrain es exactamente igual a 4
								else if( terrainNeighboringCells == 4 )
								{
									//Aleatorio para desnivelar a favor de mar o tierra
									if( this.random.nextInt(7) > 1 )
									{
										//Si tiene cuatro vecinos de tierra 
										//Si tiene m�s vecinos de monta�a
										if( mountainNeighboringCells > plainNeighboringCells )
										{
											if( this.random.nextInt(6) > 3 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}
										else 
										{
											if( this.random.nextInt(18) > 1 )
												cellStatesArray[i][j] = Cell.CellStates.Plain;
											else cellStatesArray[i][j] = Cell.CellStates.Mountain;
										}	
									}
									else cellStatesArray[i][j] = Cell.CellStates.Sea;
								}
							}
						}
					}
				}	
				//Copiamos el resultado en el array definitivo
				for( int i=0; i < this.rows; i++ )
				{
					for( int j=0; j < this.cols; j++ )
					{
						this.cellArray[i][j].cellularState = cellStatesArray[i][j];
					}
				}	
				//Incrementamos el n�mero de iteraciones
				iterationCount ++;
				//Pintamos el mapa para debuguear antes de cada iteraci�n
				if( Application.debug == true )
				{
					this.draw();
					System.out.println( "\nIterations : " + iterationCount );
					//Game.Pause();
				}
			}
			
		}while( option != 2 );
		
	}
	

}
