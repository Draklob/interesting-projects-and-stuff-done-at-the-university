/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

/**
 * Clase que hereda de Cell
 * Implementa celdas de tipo hexagonal que por tanto tienen seis vecinos posibles
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class HexagonalCell extends Cell
{
	
	/** 
     *	El constructor
     *	@param _row : n�mero de fila
     *  @param _col : n�mero de columna
     *  @param _map : referencia al mapa
     */
	public HexagonalCell( int _row, int _col, Map _map )
	{
		//Las celdas hexagonales tienen seis vecinas 
		super( _row, _col, _map );
	}
	
	/**
	 * M�todo que resetea la celda a sus condiciones iniciales
	 */
	public void reset()
	{
		//Invocamos el m�todo de la clase padre
		super.reset();	
	}
	
	/**
	 * M�todo que asigna las celdas vecinas al array de celdas vecinas
	 * Un objeto NeihboringCell tiene como atributo la celda vecina y otros atributos
	 * ( como por ejemplo si dicha celda est� "enabled", accesible para el mapa dungeon )
	 * Si no existe una celda vecina en una direcci�n se pone a null el atributo cell de dicha celda vecina
	 */
	public void setNeighboringCells()
	{
		//Asignamos las celdas vecinas del array de celdas vecinas
		//De primeras todas las celdas vecinas se resetean luego como inaccesibles para el mapa tipo dungeon
		
		//Usamos dos variables locales para ir desplazando filas y columnas y
		//poder referenciar las celdas vecinas
		int aux_col;
		int aux_row;
		
		//Si la columna es par
		if( this.col % 2 == 0 )
		{
			//Le sumamos uno a la columna
			//Comprobamos que esa columna exista
			aux_col = this.col + 1;
			if( aux_col < this.map.cols )
			{
				//Asignamos la celda vecina que est� en la misma fila ( el "0" es arriba a la derecha )
				this.neighboringCells[0].cell = this.map.cellArray[this.row][aux_col];
				//Comprobamos si existe la fila de abajo
				aux_row = this.row + 1;
				if( aux_row < this.map.rows )
				{
					//Asignamos esta celda vecina ( el "1" es abajo a la derecha )
					this.neighboringCells[1].cell = this.map.cellArray[aux_row][aux_col];
				}
			}
			else //Si no existe la columna
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[0].cell = null;
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[1].cell = null;
			}
			//Le sumamos uno a la fila
			//Comprobamos que esa fila exista
			aux_row = this.row + 1;
			if( aux_row < this.map.rows )
			{
				//Asignamos la celda vecina que est� en la misma columna pero en la fila de abajo ( el "2" es abajo )
				this.neighboringCells[2].cell = this.map.cellArray[aux_row][this.col];
			}
			else //Si no existe la fila
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[2].cell = null;
			}
			//Le restamos uno a la columna
			//Comprobamos que esa columna exista
			aux_col = this.col - 1;
			if( aux_col >= 0 )
			{
				//Asignamos la celda vecina que est� en la misma fila ( el "4" es arriba a la izquierda )
				this.neighboringCells[4].cell = this.map.cellArray[this.row][aux_col];
				//Comprobamos si existe la fila de abajo
				aux_row = this.row + 1;
				if( aux_row < this.map.rows )
				{
					//Asignamos esta celda vecina ( el "3" es abajo a la izquierda )
					this.neighboringCells[3].cell = this.map.cellArray[aux_row][aux_col];
				}
			}
			else //Si no existe la columna
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[4].cell = null;
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[3].cell = null;
			}
			//Le restamos uno a la fila
			//Comprobamos que esa fila exista
			aux_row = this.row - 1;
			if( aux_row >= 0 )
			{
				//Asignamos la celda vecina que est� en la misma columna pero en la fila de arriba ( el "5" es arriba )
				this.neighboringCells[5].cell = this.map.cellArray[aux_row][this.col];
			}
			else //Si no existe la fila
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[5].cell = null;
			}
		}
		else//Si la columna es impar
		{
			//Le sumamos uno a la columna
			//Comprobamos que esa columna exista
			aux_col = this.col + 1;
			if( aux_col < this.map.cols )
			{
				//Asignamos la celda vecina que est� en la misma fila ( el "1" es abajo a la derecha )
				this.neighboringCells[1].cell = this.map.cellArray[this.row][aux_col];
				//Comprobamos si existe la fila de arriba
				aux_row = this.row - 1;
				if( aux_row > 0 )
				{
					//Asignamos esta celda vecina ( el "0" es arriba a la derecha )
					this.neighboringCells[0].cell = this.map.cellArray[aux_row][aux_col];
				}
			}
			else //Si no existe la columna
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[0].cell = null;
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[1].cell = null;
			}
			//Le sumamos uno a la fila
			//Comprobamos que esa fila exista
			aux_row = this.row + 1;
			if( aux_row < this.map.rows )
			{
				//Asignamos la celda vecina que est� en la misma columna pero en la fila de abajo ( el "2" es abajo )
				this.neighboringCells[2].cell = this.map.cellArray[aux_row][this.col];
			}
			else //Si no existe la fila
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[2].cell = null;
			}
			//Le restamos uno a la columna
			//Comprobamos que esa columna exista
			aux_col = this.col - 1;
			if( aux_col >= 0 )
			{
				//Asignamos la celda vecina que est� en la misma fila ( el "3" es abajo a la izquierda )
				this.neighboringCells[3].cell = this.map.cellArray[this.row][aux_col];
				//Comprobamos si existe la fila de arriba
				aux_row = this.row - 1;
				if( aux_row >= 0 )
				{
					//Asignamos esta celda vecina ( el "4" es arriba a la izquierda )
					this.neighboringCells[4].cell = this.map.cellArray[aux_row][aux_col];
				}
			}
			else //Si no existe la columna
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[4].cell = null;
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[3].cell = null;
			}
			//Le restamos uno a la fila
			//Comprobamos que esa fila exista
			aux_row = this.row - 1;
			if( aux_row >= 0 )
			{
				//Asignamos la celda vecina que est� en la misma columna pero en la fila de arriba ( el "5" es arriba )
				this.neighboringCells[5].cell = this.map.cellArray[aux_row][this.col];
			}
			else //Si no existe la fila
			{
				//Si hemos llegado al extremo la ponemos a null
				this.neighboringCells[5].cell = null;
			}
		}
	}
		
	
	/** 
     *	Habilitamos la celda vecina correspondiente al index que le pasamos
     *	Por ejemplo si le pasamos el "0" que ser�a arriba a la derecha
     *	abrir�amos en esta celda la pared izquierda abajo que ser�a la "3"
     *	o sea, la opuesta.
     *	@param index : la pared de la celda vecina que conecta con esta celda
     */
	public void connectWithCell( int index )
	{
		if( index == 0 ) this.neighboringCells[3].enabled = true;
		else if( index == 1 )  this.neighboringCells[4].enabled = true;
		else if( index == 2 )  this.neighboringCells[5].enabled = true;
		else if( index == 3 )  this.neighboringCells[0].enabled = true;
		else if( index == 4 )  this.neighboringCells[1].enabled = true;
		else if( index == 5 )  this.neighboringCells[2].enabled = true;
	}
	
	
	/************************************************************/
    /********* Los m�todos necesarios para dibujar **************/
	/************************************************************/
	
	/** 
     *	Pintamos el contenido de la celda hexagonal
     */
	public void drawHexContent()
	{
		//Invocamos el m�todo de la celda padre para pintar el contenido
		//Lo invocamos dos veces para que quede m�s sim�trico
		System.out.print("   ");
		super.drawCellContent();
		super.drawCellContent();
		System.out.print("   ");
	}
	
	/** 
     *	Pintamos el techo de la celda hexagonal
     */
	public void drawHexUp()
	{
		//Si la celda vecina de arriba ( "5" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[5].enabled )
		{
			System.out.print("+    +");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("+----+");	
		}
	}
	
	/** 
     *	Pintamos el suelo de la celda hexagonal
     */
	public void drawHexBottom()
	{
		//Si la celda vecina de abajo ( "2" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[2].enabled )
		{
			System.out.print("+    +");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("+----+");	
		}
	}
	
	/** 
     *	Pintamos las paredes de la parte de arriba de la celda hexagonal
     */
	public void drawHexUpSides()
	{
		//Si la celda vecina de arriba a la izquierda ( "4" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[4].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("/");	
		}
		//Pintamos cuatro espacios en blanco
		System.out.print("      ");
		//Si la celda vecina de arriba a la derecha ( "0" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[0].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("\\");	
		}
	}
	
	/** 
     *	Pintamos las paredes de la parte de abajo de la celda hexagonal
     */
	public void drawHexBottomSides()
	{
		//Si la celda vecina de abajo a la izquierda ( "3" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[3].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("\\");	
		}
		//Pintamos el espacio en blanco
		System.out.print("      ");
		//Si la celda vecina de abajo a la derecha ( "1" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[1].enabled )
		{
			System.out.print(" ");
		}
		else //Si la celda vecina de arriba a la izquierda ( "5" ) no es accesible pintamos una pared
		{
			System.out.print("/");	
		}
	}
	
	
}
