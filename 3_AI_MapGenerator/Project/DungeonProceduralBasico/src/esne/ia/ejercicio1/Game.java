/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

/*Importamos la clase Scanner para poder leer del teclado*/
import java.util.Scanner;

import esne.ia.ejercicio1.Map.MapType;

/**
 * Clase que gestiona la interfaz de usuario y el loop del juego
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class Game 
{
	//Guardamos una referencia a un tablero generado por b�squeda ciega o por aut�matas celulares
	Map map;
	
	//M�quina de estados para saber si hemos configurado y creado el mapa
	enum MapState{ None, Configured, Generated };
	MapState mapState;
	
	//El objeto de la clase Scanner para poder leer del teclado
	Scanner sc;
	
	public Game()
	{
		//Inicialmente el tablero no est� configurado
		this.mapState = MapState.None;
		
		//Inicializo la clase scanner:
		this.sc = new Scanner( System.in );
	}
	
	/** 
     *	El men� principal de la aplicaci�n
     */
	public void menu()
	{
		
		/*variable para recoger la opcion elegida en el menu principal*/
		int option;	
		
		do{
			/*Menu principal del programa*/
			System.out.println( "\n\n____________________________________________________________________________________\n\n" );
			System.out.println( "                       Men� de Opciones\n" );
			System.out.println( "   1. Configurar mapa" );
			System.out.println( "   2. Generar Mapa" );
			System.out.println( "   3. Pathfinding" );
			System.out.println( "   4. Pintar tablero" );
			System.out.println( "   5. Salir\n" );
			
			option = sc.nextInt();
			
			switch( option )
			{
				case 1://Opcion 1 - Configurar el tablero 
					//( indicar filas, columnas, tipo de celda )
					setup();
					break;
				case 2://Opcion 2 - Generar el tablero 
					//( crea las conexiones entre las diferentes celdas en el caso del dungeon )
					/*Comprobamos primero que el tablero est� configurado*/
					if( this.mapState == MapState.None )
						System.out.println( "\nAun no est� configurado el tipo de mapa." );
					else this.generate();
					break;
				case 3://Opcion 3 - Efectuar pathfinding indicando posici�n de salida y destino
					/*Comprobamos primero que el tablero est� generado*/
					if( this.mapState != MapState.Generated )
						System.out.println( "\nAun no se ha generado el mapa." );
					else if( this.map.mapType == MapType.CellularAutomaton )
						System.out.println( "\nNo est� implementado el pathfinding para este tipo de mapas." );
					else this.pathfinding();
					break;
				case 4://Opcion 4 - Pintar tablero con las �ltimas modificaciones
					/*Comprobamos primero que el tablero est� generado*/
					if( this.mapState != MapState.Generated )
						System.out.println( "\nAun no se ha generado el mapa." );
					else this.draw();
					break;
				case 5://Opcion 5 - Crear Tablero usando Aut�matas Celulares Simples
			}
				
		}while( option != 5 );//Bucle infinito hasta que introduzca 3 y salga del programa
		
	}
	
	/** 
     *	Configura el tablero
     *	Solicita al usuario el tipo de celda ( triangular, cuadrada o hexagonal )
     *	Solicita al usuario el n�mero de filas y columnas del tablero
     */
	private void setup()
	{
		System.out.println( "\n\n____________________________________________________________________________________\n\n" );
		System.out.println( "                       Configuraci�n del tablero\n" );
		
		//Solicitamos c�mo se generar� el tablero ( por aut�matas celulares o por b�squeda ciega )
		int option = 0;
		//La variable en la que guardamos el tipo de mapa
		Map.MapType mapType = Map.MapType.DFS;
		
		while( option != 1 && option != 2 )
		{
			System.out.println("Indique c�mo generar el mapa");
			System.out.println("( 1=>B�squeda Ciega/2=>Aut�matas Celulares )");
			option = sc.nextInt();
			switch( option )
			{
				case 1://Se generar� por b�squeda ciega
					mapType = Map.MapType.DFS;
					break;
				case 2://Las celdas son cuadradas
					mapType = Map.MapType.CellularAutomaton;
					break;
			}
		}
		
		//Solicitamos el tipo de celda
		option = 0;
		//La variable en la que guardamos temporalmente el tipo de celda
		Map.CellType cellType = Map.CellType.Triangular;
		
		while( option != 1 && option != 2 && option != 3 )
		{
			System.out.println("Indique el tipo de celda");
			System.out.println("( 1=>Triangular/2=>Cuadrada/3=>Hexagonal )");
			option = sc.nextInt();
			
			switch( option )
			{
				case 1://Las celdas son triangulares
					cellType = Map.CellType.Triangular;
					break;
				case 2://Las celdas son cuadradas
					cellType = Map.CellType.Square;
					break;
				case 3://Las celdas son hexagonales
					cellType = Map.CellType.Hexagonal;
					break;
			}
		}
		
		//Solicitamos el n�mero de filas y columnas que tendr� el tablero
		int rows = 0;
		int cols = 0;
		while( rows <= 2 )
		{
			System.out.println("Introduce el numero de filas ( mayor que 2 )");
			rows = sc.nextInt();
		}
		while( cols <= 2 )
		{	
			System.out.println("Introduce el numero de columnas ( mayor que 2 )");
			cols = sc.nextInt();
		}
		
		//En el caso de que el mapa se genere por b�squeda ciega
		if( mapType == Map.MapType.DFS )
		{
			//Invocamos el constructor con los datos obtenidos del usuario
			this.map = new DFSMap( this, mapType, cellType, rows, cols );
		}
		
		//En el caso de que el mapa se genere por aut�matas celulares elegimos el tipo de mapa
		else if( mapType == Map.MapType.CellularAutomaton )
		{
			option = 0;
			//Elegimos el tipo
			CAMap.CAutomatonType cAutomatonType = CAMap.CAutomatonType.Islands;
			//Elegimos tambi�n el n�mero de semillas inicial
			int seedsCount = 1;
			while( option != 1 && option != 2 && option != 3 )
			{
				System.out.println("Indique el tipo de mapa que quiere generar");
				System.out.println("( 1=>Islas/2=>Pangea/3=>Continentes )");
				option = sc.nextInt();
				switch( option )
				{
					case 1://El mapa que se generer� ser� de tipo Islas
						cAutomatonType = CAMap.CAutomatonType.Islands;
						do
						{
							System.out.println("Indique el n�mero de islas ( iniciales )");
							System.out.println("M�ximo : " + cols + " - M�nimo : 2");
							seedsCount = sc.nextInt();
						}while( seedsCount > cols || seedsCount < 2 );
						break;
					case 2://El mapa que se generar� ser� de tipo Pangea
						cAutomatonType = CAMap.CAutomatonType.Pangea;
						break;
					case 3://El mapa que se generar� ser� de tipo Continents
						do
						{
							cAutomatonType = CAMap.CAutomatonType.Continents;
							System.out.println("Indique el n�mero de continentes ( iniciales )");
							System.out.println("M�ximo : " + cols/2 + " - M�nimo : 2");
							seedsCount = sc.nextInt();
						}while( seedsCount > cols/2 || seedsCount < 2 );
						break;
				}
			}
			
			option = 0;
			
			//Elegimos los estados que tendr� cada celda ( c�lula )
			
			while( option != 2 && option != 3 )
			{
				System.out.println("Indique c�mo se generar� el mapa");
				System.out.println("( 2=>Dos estados( plain y mountain )" );
				System.out.println("( 3=>Tres estados( plain, sea y mountain )" );
				option = sc.nextInt();
				switch( option )
				{
					case 2://Tendr� dos estados
						//Invocamos el constructor con los datos obtenidos del usuario
						this.map = new CAMapTwoStates( this, mapType, cellType, rows, cols, cAutomatonType, 2, seedsCount );
						break;
					case 3://Las celdas son cuadradas
						//Invocamos el constructor con los datos obtenidos del usuario
						this.map = new CAMapThreeStates( this, mapType, cellType, rows, cols, cAutomatonType, 3, seedsCount );
						break;
				}
			}
			
			
		}
		
		
		//El dungeon est� configurado
		this.mapState = MapState.Configured;
	}
	
	/** 
     *	Inicializa el tablero
     *	Elige una celda aleatoriamente y desde ah� va creando las conexiones 
     *	necesarias hasta conseguir que todas las celdas est�n conectadas 
     */
	private void generate()
	{
		//Invocamos el m�todo que genera las conexiones entre las celdas
		this.map.generate();
		
		//El mapa ha sido generado
		this.mapState = MapState.Generated;
	}
	
	/** 
     *	Calcula un camino y lo muestra en pantalla
     *	Solicita al usuario una posici�n de salida y una de destino
     */
	private void pathfinding()
	{
		System.out.println( "\n\n____________________________________________________________________________________\n\n" );
		System.out.println( "                       Pathfinding\n" );
		
		//Solicitamos el n�mero de fila y columna de la celda de salida
		int originRow = -1;
		int originCol = -1;
		while( originRow < 0 || originRow > this.map.rows - 1 )
		{
			System.out.println("Introduce la fila de la celda de salida");
			originRow = sc.nextInt();
		}
		while( originCol < 0 || originCol > this.map.cols - 1 )
		{	
			System.out.println("Introduce el numero de columna de la celda de salida");
			originCol = sc.nextInt();
		}
		
		//Solicitamos el n�mero de fila y columna de la celda de destino
		int targetRow = -1;
		int targetCol = -1;
		while( targetRow < 0 || targetRow > this.map.rows - 1 )
		{
			System.out.println("Introduce la fila de la celda de destino");
			targetRow = sc.nextInt();
		}
		while( targetCol < 0 || targetCol > this.map.cols - 1 )
		{	
			System.out.println("Introduce el numero de columna de la celda de destino");
			targetCol = sc.nextInt();
		}
		
		//Invocamos el m�todo de dungeon para encontrar el camino m�s corto pas�ndole la celda origen y destino
		boolean result = this.map.pathfinding(originRow, originCol, targetRow, targetCol);
		if( result )
		{
			System.out.println("Camino encontrado !!!");
		}
		else
		{
			System.out.println("No se encontr� el camino !!!");
		}
		
		//Pintamos el tablero 
		this.draw();
	}
	
	/** 
     *	Pinta el tablero con las �ltimas modificaciones
     */
	private void draw()
	{
		map.draw();
	}
	
	/** 
     *	M�todo para simular una pausa
     */
	public void Pause ()
	{
		System.out.println("Press Any Key To Continue...");
		this.sc.nextLine();
		this.sc.nextLine();
	}
}
	

