/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

import java.util.Random;

/**
 * Clase de la que heredan los diferentes tipos de generaci�n de mapa
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
abstract class Map
{
	//Referencia al juego
	protected Game game;
	
	//El tipo de mapa
	public enum MapType{ DFS, CellularAutomaton };
	public MapType mapType;
		
	//El tipo de celda est�tico para que sea accesible desde las celdas
	public enum CellType{ Triangular, Square, Hexagonal };
	public CellType cellType;
	
	//El n�mero de celdas vecinas que tendr� cada celda en funci�n de su tipo
	public int neighboringCount;
	
	//El n�mero de filas y columnas
	protected int rows;
	protected int cols;
	
	//El array de celdas ( cada celda la identificamos por su fila y su columna )
	protected Cell[][] cellArray;
	
	//El objeto de la clase Random para producir n�meros aleatorios
	protected Random random;
		
	//El constructor 
	public Map( Game _game, Map.MapType _mapType, Map.CellType _cellType, int _rows, int _cols )
	{
		this.game = _game;
		
		//Asignamos el tipo de mapa
		this.mapType = _mapType;
		//Asignamos el tipo de celda al mapa
		this.cellType = _cellType;
		//Asignamos el n�mero de filas y columnas que tendr� el mapa
		this.rows = _rows;
		this.cols = _cols;
		
		//Creamos el objeto random que usaremos en distintas partes del c�digo asociado al mapa
		//No hace falta pasarle la semilla, la toma autom�ticamente del tiempo actual
		this.random = new Random();
		
		//Creamos el array de celdas 
		this.cellArray = new Cell[this.rows][this.cols];
		
		//Recorremos las filas y las columnas para crear las celdas y guardar
		//una referencia de ellas en el array
		for( int i=0; i < this.rows; i++ )
		{
			for( int j=0; j < this.cols; j++ )
			{
				//Diferenciamos el tipo de celda que incluimos en el array 
				//A cada celda le pasamos su fila y columna en el constructor
				if( this.cellType == Map.CellType.Triangular )
				{
					//El n�mero de celdas vecinas para cada celda es 4
					this.neighboringCount = 4;
					this.cellArray[i][j] = new TriangularCell( i, j, this );
				}
				else if( this.cellType == Map.CellType.Square )
				{
					//El n�mero de celdas vecinas para cada celda es 4
					this.neighboringCount = 4;
					this.cellArray[i][j] = new SquareCell( i, j, this );
				}
				else if( this.cellType == Map.CellType.Hexagonal )
				{
					//El n�mero de celdas vecinas para cada celda es 6
					this.neighboringCount = 6;
					this.cellArray[i][j] = new HexagonalCell( i, j, this );
				}
			}
		}	
		
		//Una vez que est�n todas las celdas creadas, invocamos el m�todo de cada una de ellas
		//que asigna las celdas vecinas
		for( int i=0; i < this.rows; i++ )
		{
			for( int j=0; j < this.cols; j++ )
			{
				this.cellArray[i][j].setNeighboringCells();
			}	
		}
	}
	
	/** 
     *	Genera el mapa
     */
	public void generate()
	{
		this.resetMap();
	}
	
	/** 
     *	M�todo que resetea las celdas del mapa poni�ndolas como no visitadas 
     */
	protected void resetMap()
	{
		//Recorremos las filas y las columnas para ponerlas como no visitadas
		for( int i=0; i < this.rows; i++ )
		{
			for( int j=0; j < this.cols; j++ )
			{
				//Reseteamos cada c�lula el array
				this.cellArray[i][j].reset();
			}
		}
	}
	
	/** 
     *	M�todo que pone las celdas como no visitadas
     */
	protected void setCellsNotVisited()
	{
		//Recorremos las filas y las columnas para ponerlas como no visitadas
		for( int i=0; i < this.rows; i++ )
		{
			for( int j=0; j < this.cols; j++ )
			{
				//A cada celda le pasamos su fila y columna en el constructor
				this.cellArray[i][j].visited = false;
			}
		}
	}
		
	/** 
     *	M�todo que encuentra el camino m�s corto entre una celda origen y otra destino
     *	Recibe como par�metro la celda origen y la celda destino
     */
	abstract boolean pathfinding( int originRow, int originCol, int targetRow, int targetCol );
	
	/** 
     *	Pinta el tablero 
     */
	public void draw()
	{
		System.out.print("\f");
		System.out.println( "\n\n____________________________________________________________________________________\n\n" );
		System.out.println( "                       MAP\n" );
		
		/************************************************************/
	    /*************	Si las celdas son triangulares **************/
		/************************************************************/
		if( this.cellType == Map.CellType.Triangular )
		{
			//Hacemos una primera pasada para pintar el techo 
			//Tan solo pintamos el de las columnas impares
			//Primero pintamos tres espacios en blanco
			System.out.print("   ");
			for( int j=0; j < this.cols; j++ )
			{
				if( j % 2 != 0 )
				{
					System.out.print("+-----");
				}
				if( j == this.cols-1 ) System.out.print("+");
			}
			
			//Pintamos las filas
			for( int i=0; i < this.rows; i++ )
			{
				//Hacemos salto de l�nea
				System.out.println("");
				
				//Si la fila es par
				if( i % 2 == 0 )
				{
					//Hacemos una primera pasada para pintar la parte de arriba de los tri�ngulos pares
					//y la parte de abajo de los impares con el contenido de estos �ltimos
					//Primero pintamos dos espacios en blanco
					System.out.print("  ");
					for( int j=0; j < this.cols; j++ )
					{
						//Si el tri�ngulo es par
						if( j % 2 == 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangUpSides();
						}
						//Si el tri�ngulo es impar
						else
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangContent();
						}
					}
					//Si las columnas son pares tenemos que a�adir la �ltima pared
					if( this.cols % 2 == 0 ) System.out.print("/");
					
					//Hacemos salto de l�nea
					System.out.println("");
					//Hacemos una segunda pasada para pintar la parte de inferior de los tri�ngulos pares con su contenido
					//y la parte de arriba de los impares
					//Primero pintamos un espacio en blanco
					System.out.print(" ");
					for( int j=0; j < this.cols; j++ )
					{
						//Si el tri�ngulo es par
						if( j % 2 == 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangBottomSides();
						}
						//Si el tri�ngulo es impar
						else
						{
							//Primero pintamos un espacio en blanco
							System.out.print(" ");
						}
					}
					//Si las columnas son pares tenemos que a�adir la �ltima pared
					if( this.cols % 2 == 0 ) System.out.print("/");
					
					//Por �ltimo hacemos otra pasada para pintar el suelo de las celdas pares
					//Hacemos salto de l�nea
					System.out.println("");
					for( int j=0; j < this.cols; j++ )
					{
						//Si el tri�ngulo es par
						if( j % 2 == 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangBottom();
						}
					}
					//A�adimos el signo m�s
					System.out.print("+");
				}
				//Si la fila es impar
				else
				{
					//Primero pintamos un espacio en blanco
					System.out.print(" ");
					//A�adimos la primera pared inferior del tri�ngulo invertido
					System.out.print("\\");
					//Hacemos una primera pasada para pintar la parte de arriba de los tri�ngulos impares
					//y la parte de abajo de los pares con el contenido de estos �ltimos
					for( int j=0; j < this.cols; j++ )
					{
						//Si el tri�ngulo es impar
						if( j % 2 != 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangUpSides();
						}
						//Si el tri�ngulo es impar
						else
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangContent();
						}
					}
					//Si las columnas son impares tenemos que a�adir la �ltima pared
					if( this.cols % 2 != 0 ) System.out.print("/");
					
					//Hacemos salto de l�nea
					System.out.println("");
					//Pintamos dos espacios en blanco
					System.out.print("  ");
					//A�adimos la primera pared inferior del tri�ngulo invertido
					System.out.print("\\");
					//Pintamos un espacio en blanco
					System.out.print(" ");
					//Hacemos una segunda pasada para pintar la parte de inferior de los tri�ngulos impares con su contenido
					//y la parte de arriba de los pares invertidos
					for( int j=0; j < this.cols; j++ )
					{
						//Si el tri�ngulo es par
						if( j % 2 != 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangBottomSides();
							//Pintamos el espacio de los tri�ngulos invertidos
							System.out.print(" ");
						}
					}
					//Si las columnas son impares tenemos que a�adir la �ltima pared
					if( this.cols % 2 != 0 ) System.out.print("/");
					
					//Por �ltimo hacemos otra pasada para pintar el suelo de las celdas impares
					//Hacemos salto de l�nea
					System.out.println("");
					//Pintamos tres espacios en blanco
					System.out.print("   ");
					for( int j=0; j < this.cols; j++ )
					{
						//Si el tri�ngulo es impar
						if( j % 2 != 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((TriangularCell)this.cellArray[i][j]).drawTriangBottom();
						}
					}
					//A�adimos el signo m�s
					System.out.print("+");
				}
				
			}
		}
				
		/************************************************************/
	    /*************	Si las celdas son cuadradas *****************/
		/************************************************************/
		// cada celda pinta su pared derecha y la de abajo 
		if( this.cellType == Map.CellType.Square )
		{
			//Hacemos una primera pasada para pintar el techo 
			for( int j=0; j < this.cols; j++ )
			{
				System.out.print("+---");
				if( j == this.cols-1 ) System.out.print("+");
			}
			//Hacemos un salto de l�nea
			System.out.println("");
			for( int i=0; i < this.rows; i++ )
			{
				//Luego cada celda con su pared derecha ( la izquierda solo en la primera columna )
				for( int j=0; j < this.cols; j++ )
				{
					//Si es la primera columna pintamos primero la pared
					if( j == 0 ) System.out.print("|");
					//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
					((SquareCell)this.cellArray[i][j]).drawSquareCell();
				}
				//Hacemos un salto de l�nea
				System.out.println("");
				//Hacemos una segunda pasada para pintar la pared de abajo de las celdas de esta fila
				System.out.print("+");
				for( int j=0; j < this.cols; j++ )
				{
					//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
					((SquareCell)this.cellArray[i][j]).drawSquareBottom();
				}
				//Hacemos un salto de l�nea
				System.out.println("");
			}
		}
		/************************************************************/
	    /*************	Si las celdas son hexagonales ***************/
		/************************************************************/
		else if( this.cellType == Map.CellType.Hexagonal )
		{
			for( int i=0; i < this.rows; i++ )
			{
				//La primera l�nea se pinta diferente
				if( i == 0 )
				{
					//Hacemos una primera pasada para pintar el techo de las celdas impares
					//Primero pintamos los espacios
					System.out.print("         ");
					for( int j=0; j < this.cols; j++ )
					{
						if( j % 2 != 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((HexagonalCell)this.cellArray[i][j]).drawHexUp();
							//Pintamos los espacios 
							System.out.print("        ");
						}
					}
					
					//Hacemos un salto de l�nea
					System.out.println("");
					//Hacemos una pasada para pintar las paredes superiores de las celdas impares
					//Primero pintamos los espacios 
					System.out.print("        ");
					for( int j=0; j < this.cols; j++ )
					{
						if( j % 2 != 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((HexagonalCell)this.cellArray[i][j]).drawHexUpSides();
							//Pintamos los espacios
							System.out.print("      ");
						}
					}
					
					//Hacemos un salto de l�nea
					System.out.println("");
					//Hacemos una pasada para pintar el techo de las celdas pares
					//y el contenido de las impares
					//Pintamos los espacios
					System.out.print("  ");
					for( int j=0; j < this.cols; j++ )
					{
						if( j % 2 == 0 )
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((HexagonalCell)this.cellArray[i][j]).drawHexUp();
						}
						else
						{
							//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
							((HexagonalCell)this.cellArray[i][j]).drawHexContent();
						}
					}
					//Pintamos el signo m�s que falta si el n�mero de columnas es par
					if( this.cols % 2 == 0 ) 
					System.out.print("+");
				}
				
				//Hacemos un salto de l�nea
				System.out.println("");
				//Hacemos una pasada para pintar las paredes superiores de las celdas pares
				//Pintamos los espacios
				System.out.print(" ");
				for( int j=0; j < this.cols; j++ )
				{
					if( j % 2 == 0 )
					{
						//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
						((HexagonalCell)this.cellArray[i][j]).drawHexUpSides();
						//Pintamos los espacios
						System.out.print("      ");
					}
				}
				//Pintamos la pared que falta si el n�mero de columnas es par
				if( this.cols % 2 == 0 ) 
				{
					System.out.print("/");
				}
				
				//Hacemos un salto de l�nea
				System.out.println("");
				//Hacemos una pasada para pintar el suelo de las celdas impares
				//y el contenido de las pares
				//Primero pintamos el signo de sumar
				System.out.print("+");
				for( int j=0; j < this.cols; j++ )
				{
					if( j % 2 != 0 )
					{
						//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
						((HexagonalCell)this.cellArray[i][j]).drawHexBottom();
					}
					else
					{
						//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
						((HexagonalCell)this.cellArray[i][j]).drawHexContent();
					}
				}
				//Pintamos el signo m�s que falta si el n�mero de columnas es impar
				if( this.cols % 2 != 0 )
				{
					System.out.print("+");
				}
				
				//Hacemos un salto de l�nea
				System.out.println("");
				//Hacemos una pasada para pintar las paredes inferiores de las celdas pares
				//Pintamos los espacios
				System.out.print(" ");
				for( int j=0; j < this.cols; j++ )
				{
					if( j % 2 == 0 )
					{
						//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
						((HexagonalCell)this.cellArray[i][j]).drawHexBottomSides();
						//Pintamos los espacios
						System.out.print("      ");
					}
				}
				
				//Pintamos la pared que falta si el n�mero de columnas es par y no es la �ltima fila
				if( this.cols % 2 == 0 && ( i != ( this.rows - 1 ) ) ) 
				{
					System.out.print("\\");
				}
				
				//Hacemos un salto de l�nea
				System.out.println("");
				//Hacemos una pasada para pintar el suelo de las celdas pares
				//y el contenido de las impares ( Este contenido corresponde a la fila siguiente )
				//Pintamos los espacios
				System.out.print("  ");
				for( int j=0; j < this.cols; j++ )
				{
					if( j % 2 == 0 )
					{
						//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
						((HexagonalCell)this.cellArray[i][j]).drawHexBottom();
					}
					else
					{
						//Si es la �ltima fila pintamos espacios
						if( i == ( this.rows - 1 ) )
						System.out.print("        ");	
						//Cachemos el tipo de celda para invocar el m�todo concreto de dibujo
						else ((HexagonalCell)this.cellArray[i + 1][j]).drawHexContent();
					}
				}
				
				//Pintamos el signo m�s que falta si el n�mero de columnas es par y no es la �ltima fila
				if( this.cols % 2 == 0 && ( i != ( this.rows - 1 ) ))
				{
					System.out.print("+");
				}
			
			}
		
		}
	
	}
}
