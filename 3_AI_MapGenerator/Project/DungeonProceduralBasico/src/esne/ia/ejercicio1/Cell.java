/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;


/**
 * Clase que define una celda del mapa a nivel general
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
abstract class Cell 
{
	
	//Guardamos una referencia al mapa
	protected Map map;
	//El tipo de estado de la celda
	public enum CellStates{ Plain, Mountain, Sea }; 
	protected CellStates cellularState;
	//La fila y la columna con la que identificamos la celda
	protected int row;
	protected int col;
	//Si ha sido o no recorrida en el pathfinding
	//protected enum CellState{ None, Visited };
	public boolean visited;
	//El array de celdas vecinas
	//Si la celda es triangular => right : 0, bottom : 1, left : 2, up : 3 ( cada celda tiene tres vecinos de cuatro tipos diferentes )
	//Si la celda es cuadrada => right : 0, botton : 1, left : 2, up : 3
	//Si la celda es hexagonal => la pared cero es la de la derecha arriba y en sentido del reloj las dem�s ( 0, 1, 2, 3, 4, 5, 6 )
	//Si la celda es hexagonal => las filas comienzan subiendo a la hora de representarse
	protected NeighboringCell[] neighboringCells;
	
	/*Variables para el pathfinding*/
	public int h; //La heur�stica
	public int c; //El coste
	public Cell parentCell;
	public boolean origin;
	public boolean target;
	public boolean closed; // si se ha metido en la lista cerrada
	public boolean open; // si se ha metido en la lista abierta
	
	/** 
     *	El constructor
     *	@param _row : n�mero de fila
     *	@param _col : n�mero de columna
     *	@param _map : referencia al mapa
     */
	public Cell( int _row, int _col, Map _map )
	{
		this.map = _map;
		this.row = _row;
		this.col = _col;
		
		//El estado inicial en el caso de que las celdas se usen en aut�matas celulares
		this.cellularState = CellStates.Sea;
	
		//Creamos el array de celdas vecinas
		//A cada celda se accede en una direcci�n dependiendo de si la
		//fila y la columna son pares o impares
		this.neighboringCells = new NeighboringCell[this.map.neighboringCount];
		for( int i=0; i<this.map.neighboringCount; i++)
		{
			this.neighboringCells[i] = new NeighboringCell();
		}
		
		//Inicialmente todas las celdas parten como no visitadas
		this.visited = false;
		
		//Para el pathfinding
		this.h = 0;
		this.c = 0;
		this.parentCell = null;
		this.origin = false;
		this.target = false;
		this.closed = false;
		this.open = false;
		
	}
	
	/**
	 * M�todo que resetea la celda a sus condiciones iniciales
	 */
	public void reset()
	{
		//El estado inicial en el caso de que las celdas se usen en aut�matas celulares
		this.cellularState = CellStates.Sea;
		
		this.visited = false;
		
		//Para el pathfinding
		this.h = 0;
		this.c = 0;
		this.parentCell = null;
		this.origin = false;
		this.target = false;
		this.closed = false;
		this.open = false;
		
		//Reseteamos las c�lulas vecinas
		for( NeighboringCell neighboringCell : neighboringCells )
		{
			neighboringCell.reset();
		}
	}
	
	
	/**
	 * M�todo que asigna las celdas vecinas al array de celdas vecinas
	 * Un objeto NeihboringCell tiene como atributo la celda vecina y otros atributos
	 * ( como por ejemplo si dicha celda est� "enabled", accesible para el mapa dungeon )
	 * Si no existe una celda vecina en una direcci�n se pone a null
	 */
	abstract void setNeighboringCells();
	
	/** 
     *	Habilitamos la celda vecina correspondiente al index que le pasamos
     *	Por ejemplo si le pasamos el "0" que ser�a "right" abrir�amos la celda 
     *	vecina "left", o sea, la opuesta.
     */
	abstract void connectWithCell( int index );
	
	/**
	 * M�todo que comprueba cu�ntas celdas vecinas tiene esta celda 
	 * con el estado pasado por par�metro
	 * Si tiene ao
	 */
	public int countNeighboringCellStates( Cell.CellStates _cellState )
	{
		//Variable para almacenar el n�mero de celdas vecinas que coinciden 
		//con el estado pasado por par�metro
		int counter = 0;
		//Recorremos todas las celdas vecinas
		for( int i=0; i<this.map.neighboringCount; i++)
		{
			//Si el atributo de la celda vecina "cell" es null 
			if( neighboringCells[i].cell == null )
			{
				//Si tiene al menos una celda vecina con el atributo "cell" a null
				//devolvemos -1 para saber que se tiene que poner a "sea" y as� tener los bordes del mapa con agua
				return -1;
			}
			else if( neighboringCells[i].cell.cellularState == _cellState ) counter ++;
		}	
		return counter;
	}
	
	/************************************************************/
    /********* Los m�todos necesarios para dibujar **************/
	/************************************************************/

	/** 
     *	Pintamos el contenido de la celda
     */
	protected void drawCellContent()
	{
		//Diferenciamos seg�n el tipo de mapa
		if( this.map.mapType == Map.MapType.DFS )
		{
			//Pintamos el hueco de la celda en funci�n de si es la celda origen o destino del pathfinding
			//o si ha sido visitada o no
			if( origin )
			{
				System.out.print("B");
			}
			else if( target )
			{
				System.out.print("E");
			}
			else if( visited )
			{
				System.out.print("X");
			}
			else if( closed )
			{
				System.out.print("C");
			}
			else if( open )
			{
				System.out.print("O");
			}
			else
			{
				System.out.print(" ");
			}
		}
		else if( this.map.mapType == Map.MapType.CellularAutomaton )
		{
			if( this.cellularState == Cell.CellStates.Plain )
			{
				System.out.print("0");
			}
			else if( this.cellularState == Cell.CellStates.Mountain )
			{
				System.out.print("X");
			}
			else if( this.cellularState == Cell.CellStates.Sea )
			{
				System.out.print(" ");
			}
			else
			{
				System.out.print(" ");
			}
		}
	}
	
}
