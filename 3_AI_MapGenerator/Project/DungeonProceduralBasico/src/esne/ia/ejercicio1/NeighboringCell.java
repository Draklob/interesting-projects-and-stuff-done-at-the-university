/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

/**
 * Clase que sirve para guardar la referencia a una celda vecina
 * Guarda la referencia a la celda m�s el atributo de si est� "enabled" o no
 * Si cuando accedamos a esta celda vecina desde otra celda, encontramos que el atributo cell es "null"
 * esto indicar� que no existe celda vecina en esta direcci�n.
 * Implementa celdas de tipo hexagonal que por tanto tienen seis vecinos posibles
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class NeighboringCell
{
	//Variable que almacena la celda vecina
	public Cell cell;
	//Variable que indica si es accesible o no
	public boolean enabled;
		
	//El constructor
	public NeighboringCell()
	{
	}
	
	/**
	 * M�todo que resetea las condiciones iniciales
	 */
	public void reset()
	{
		this.enabled = false;
	}
}
