/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;


/**
 * Clase que hereda de mapa
 * Generaliza un tipo de mapa generado por aut�matas celulares
 * De �l heredan mapas con dos y tres estados
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public abstract class CAMap extends Map
{
	//Si es de tipo aut�mata celular a su vez pueden generarse de tres tipos
	public enum CAutomatonType{ Islands, Pangea, Continents };
	protected CAutomatonType cAutomatonType;
	
	//El n�mero de estados que tendr� cada celda del mapa
	int cellStatesCount;
	
	//El n�mero de iteraciones para evolucionar los aut�matas
	int iterations;
	
	//Si el mapa es de tipo "Islands" elegimos el n�mero de islas iniciales ( seeds ) con el que comenzamos el aut�mata
	//Si el mapa es de tipo "Continents" elegimos el n�mero de continentes ( seeds ) con el que comenzamos el mapa
	//Si es de tipo Pangea comenzamos el mapa con una semilla central
	int seedsCount;
	

	/** 
     *	El constructor
     *	@param _mapType : el tipo de mapa, _cellType : el tipo de celda, _rows : el n�mero de filas, _cols : el n�mero de columnas
     *  	   _cAutomatonType : el tipo de mapa ( isla, continente, pangea ), _cellStatesCount : el n�mero de estados por c�lula
     */
	public CAMap( Game _game, Map.MapType _mapType, Map.CellType _cellType, int _rows, int _cols, 
			CAMap.CAutomatonType _cAutomatonType, int _cellStatesCount, int _seedsCount  )
	{	
		//Llamamos al constructor padre
		super( _game, _mapType, _cellType, _rows, _cols );
		
		//Asignamos el tipo de aut�mata ( Islands, Pangea, Continents )
		this.cAutomatonType = _cAutomatonType;
		
		//Asignamos el n�mero de estados de cada c�lula( plain, mountain o sea )
		this.cellStatesCount = _cellStatesCount;
		
		//El n�mero de iteraciones para evolucionar los aut�matas
		this.iterations = 10;
		
		//El n�mero de semillas para la creaci�n de los mapas
		this.seedsCount = _seedsCount;
	}
	
	/** 
     *	M�todo p�blico que invoca el m�todo apropiado siguiendo la configuraci�n elegida
     *	para generar aleatoriamente un mapa usando aut�matas celulares
     */
	public void generate()
	{
		//Invocamos el m�todo padre que se encarga de resetear las celdas
		super.generate();
		
		//Diferenciamos los tres tipos a la hora de generarlos
		if( cAutomatonType == CAutomatonType.Islands )
		{
			generateIslands();
		}
		else if( cAutomatonType == CAutomatonType.Pangea )
		{
			generatePangea();
		}
		else if( cAutomatonType == CAutomatonType.Continents )
		{
			generateContinents();
		}
		
		//Evolucionamos el mapa seg�n el n�mero indicado por "iterations"
		this.automataEvolve();
	}
	
	/**
	 * Genera aleatoriamente un mapa de tipo Islands
	 */
	protected void generateIslands()
	{
		//Invocamos el m�todo que inicializa un n�mero de celdas pasado por par�metro
		int[][] arrayAux = initializeCells( this.seedsCount );
		
		//Pintamos el mapa para debuguear despues de generar las islas
		if( Application.debug == true )
		{
			this.draw();
			System.out.println( "\nIslands generated : " + this.seedsCount );
			//Game.Pause();
		}
		
		//Creo un objeto de la clase Scanner para leer del teclado
		//Scanner sc = new Scanner( System.in );
		//Variable donde almacenamos la opci�n elegida para ser evaluada
		int option;
		
		//Permitimos al usuario agrandar inicialmente todo lo que quiera los continentes antes de 
		//aplicarles las reglas del aut�mata celular
		do
		{
			System.out.println( "Indique cu�ntas celdas desea a�adir a cada isla ( Max : " + this.cols/2 + " )" );
			System.out.println( "Es recomendable a�adir al menos 8 para asegurarse de que la isla sobreviva" );
			System.out.println( "Es posible que si las celdas de origen est�n muy juntas se terminen fusionando las islas" );
			option = this.game.sc.nextInt();
			
			//Controlamos que introduzca un valor correcto para el n�mero de celdas
			if( option >= 0 && option <= this.cols/2 )
			{
				//Con cada celda inicializada invocamos el m�todo que inicializa las celdas indicadas en torno a ella
				for( int[] coordCell : arrayAux )
				{
					initializeContinent( coordCell[0], coordCell[1], option + 1 );
				}
				
				//Pintamos el mapa para debuguear despues de a�adir las celdas
				if( Application.debug == true )
				{
					this.draw();
					System.out.println( "\nIslands incremented width " + option + " new initialized cells" );
					//Game.Pause();
				}
			}
		}while( option < -1 || option > this.cols/2 );
	}
	
	/**
	 * Genera aleatoriamente un mapa de tipo Pangea
	 */
	protected void generatePangea()
	{
		//Generamos aleatoriamente un n�mero de coordenadas equivalente a un cuarto de las celdas del tablero
		//A estas coordenadas les asignaremos el estado inicial de planicie ( plain - ser�n las islas del mapa )
		int cellCount = this.rows * this.cols / 4;
		
		//Para empezar escogemos la celda central como inicial
		int row = (int)this.rows/2;
		int col = (int)this.cols/2;
		
		//Invocamos el m�todo generar continente con estos valores
		this.initializeContinent( row, col, cellCount);
		
		//Pintamos el mapa para debuguear despues de generar el continente
		if( Application.debug == true )
		{
			this.draw();
			System.out.println( "\nContinent generated" );
			//Game.Pause();
		}
	}
	
	
	
	/**
	 * Genera aleatoriamente un mapa de tipo Continents 
	 * ( se podr�a hacer con el mismo m�todo de generar islas porque en principio es igual )
	 */
	protected void generateContinents()
	{
		//Invocamos el m�todo que inicializa un n�mero de celdas pasado por par�metro
		int[][] arrayAux = initializeCells( this.seedsCount );
		
		//Pintamos el mapa para debuguear despues de generar las islas
		if( Application.debug == true )
		{
			this.draw();
			System.out.println( "\nContinents generated : " + this.seedsCount );
			//Game.Pause();
		}
		
		//Variable donde almacenamos la opci�n elegida para ser evaluada
		int option;
		
		//Permitimos al usuario agrandar inicialmente todo lo que quiera los continentes antes de 
		//aplicarles las reglas del aut�mata celular
		do
		{
			System.out.println( "Indique cu�ntas celdas desea a�adir a cada continente ( Max : " + 2*this.cols/3 + " )" );
			System.out.println( "Es recomendable a�adir al menos 8 para asegurarse de que el continente sobreviva" );
			System.out.println( "Es posible que si las celdas de origen est�n muy juntas se terminen fusionando los continentes" );
			option = this.game.sc.nextInt();
			
			//Controlamos que introduzca un valor correcto para el n�mero de celdas
			if( option >= 0 && option <= 2*this.cols/3 )
			{
				//Con cada celda inicializada invocamos el m�todo que inicializa las celdas indicadas en torno a ella ( en este caso 3 )
				for( int[] coordCell : arrayAux )
				{
					initializeContinent( coordCell[0], coordCell[1], option + 1 );
				}
				
				//Pintamos el mapa para debuguear despues de a�adir las celdas
				if( Application.debug == true )
				{
					this.draw();
					System.out.println( "\nContinents incremented width " + option + " new initialized cells" );
					//Game.Pause();
				}
			}
		}while( option < -1 || option > 2*this.cols/3 );
		
		//Cerramos el objeto scanner 
		//Da error luego en Game.Pause si lo cierro
		//sc.close();
	}
	
	/**
	 * M�todo que inicializa un n�mero determinado de celdas a modo isla 
	 * distribuidas aleatoriamente y de forma aproxim�damente distribuida por el mapa
	 * @param _cellCount : el n�mero de celdas que tienen que ser inicializadas a "Plain"
	 * @return arrayAux : el array de coordenadas que indica las celdas inicializadas
	 */
	protected int[][] initializeCells( int _cellCount )
	{
		//Dividimos el mapa en anchura entre el n�mero de semillas que queremos inicializar
		//Asignamos la separaci�n entre cada zona 
		int horizontalDistance = this.cols / _cellCount;
		//El resto de la divisi�n que luego habr� que irlo sumando aleatoriamente para cubrir todo el mapa en anchura
		int remainder = this.cols % _cellCount;
		//Usamos un array auxiliar de celdas para asegurarnos de no repetir celdas y por tanto inicializar
		//exactamente las celdas indicadas ( seedsCount )
		int[][] arrayAux = new int[_cellCount][2];
		//El contador de celdas asignadas inicialmente como planicie
		int counter = 0;
		//Variables que indican las coordenadas de la celda seleccionada
		int row = 0;
		int col = 0;
		//Para comenzar aleatoriamente por la mitad superior o por la mitad inferior del mapa
		int zone = this.random.nextInt(2);
		while( counter < _cellCount )
		{
			//Inicializamos una celda al azar de entre las que se encuentren en la zona determinada
			//esta zona la calculamos a partir de horizontalDistance y counter
			//si zone es par la escogemos de la mitad superior del tablero
			if( zone % 2 == 0 )
			{
				row = this.random.nextInt(this.rows/2);
				col = this.random.nextInt(horizontalDistance) + ( counter * horizontalDistance );
			}
			//Si counter es impar la escogemos de la mitad inferior del tablero
			else
			{
				row = this.random.nextInt(this.rows/2) + this.rows/2;
				col = this.random.nextInt(horizontalDistance) + ( counter * horizontalDistance );
			}
			
			//Al valor obtenido en la columna le sumamos un n�mero aleatorio entre cero y el resto obtenido
			//de dividir this.cols / this.seedsCount - con esto nos aseguramos que se cubra aleatoriamente todo el mapa en horizontal
			if( remainder > 0 ) col = col + this.random.nextInt(remainder);
			
			//Comprobamos que esas coordenadas no hayan sido ya escogidas
			//Inicialmente presuponemos que las coordenadas no est�n repetidas
			boolean repetead = false;
			//Recorremos el array de coordenadas ya escogidas para comprobar si ya la hemos escogido
			for( int[] point : arrayAux )
			{
				//Si coindicen las coordenadas con alg�n punto del array
				if( row == point[0] && col == point[1] )
				{
					//Escogemos otras coordenadas al azar
					row = this.random.nextInt(this.rows);
					col = this.random.nextInt(this.cols);
					repetead = true;
					break;
				}
			}
			
			//Si finalmente no estaba ya escogida
			if( repetead == false )
			{
				//Agregamos las coordenadas al array para el control de repetici�n
				//y para luego devolver el array con las celdas inicializadas
				arrayAux[counter][0] = row;
				arrayAux[counter][1] = col;
				
				//Asignamos el estado inicial de plain a la celda con estas coordenadas
				this.cellArray[row][col].cellularState = Cell.CellStates.Plain;
				counter ++;
			}
			
			//Le sumamos uno a zone para alternar entre la mitad superior e inferior del mapa
			zone ++;
		}
		
		//Retorno el array de celdas inicializadas
		return arrayAux;
	}
	
	/**
	 * Genera un continente partiendo de una posici�n de celda determinada
	 * @param _row : fila de la celda de partida, _col : columna de la celda de partida
	 * @param _cellCount : n�mero de celdas que se tienen que inicializar para formar el continente de partida
	 */
	protected void initializeContinent( int _row, int _col, int _cellCount )
	{
		//Usamos un array auxiliar de celdas para asegurarnos de no repetir celdas
		//y por tanto siempre inicializar el mismo n�mero de celdas indicadas
		int[][] arrayAux = new int[_cellCount][2];
		//El contador de celdas que llevamos asignadas como planicie
		int counter = 0;
		
		//Para empezar escogemos la celda pasada como par�metro
		int row = _row;
		int col = _col;
		
		//Variables auxiliares para almacenar las filas y columnas de las celdas vecinas
		int neighboringRow = 0;
		int neighboringCol = 0;
		
		while( counter < _cellCount )
		{
			//Seleccionamos una celda vecina al azar de la celda inicial seleccionada( indicada por la fila "row" y la columna "col" )
			int aux = this.random.nextInt( this.neighboringCount ); 
	
			//El contador de vecinas examinadas
			int neighboringCounter = 0;
	
			//Nos mantenemos en el bucle siempre que aun no hayamos inicializado todas las celdas o 
			//no hayamos examinado todas las celdas vecinas
			while( counter < _cellCount && neighboringCounter < this.neighboringCount )
			{
				//Comprobamos que esa celda vecina no tenga el atributo "cell" igual a null
				if( this.cellArray[row][col].neighboringCells[aux].cell != null )
				{
					//Seleccionamos la fila y la columna de esta celda vecina
					neighboringRow = this.cellArray[row][col].neighboringCells[aux].cell.row;
					neighboringCol = this.cellArray[row][col].neighboringCells[aux].cell.col;
					
					//Inicialmente presuponemos que las coordenadas no est�n repetidas
					boolean repetead = false;
					for( int[] point : arrayAux )
					{
						//Si coindicen las coordenadas con alg�n punto del array
						if( neighboringRow == point[0] && neighboringCol == point[1] )
						{
							repetead = true;
							break;
						}
					}
					//Si no est� repetida la inicializamos, la contabilizamos 
					//y la a�adimos al array de control de repetici�n
					if( repetead == false )
					{
						arrayAux[counter][0] = neighboringRow;
						arrayAux[counter][1] = neighboringCol;
						//Asignamos el estado inicial de plain a la celda con estas coordenadas
						this.cellArray[neighboringRow][neighboringCol].cellularState = Cell.CellStates.Plain;
						counter++;
					}
				}
					
				//Vamos recorriendo en el sentido de las agujas del reloj todas las celdas vecinas
				aux ++;
				if( aux == this.neighboringCount ) aux = 0;
				
				//Sumamos uno al contador de vecinas extraidas
				neighboringCounter ++;
			}
				
			//Asignamos como nueva fila y columna de partida las �ltimas vecinas v�lidas seleccionadas
			row = neighboringRow;
			col = neighboringCol;
		}
	}
	
	
	/**
	 * Evoluciona el aut�mata el n�mero de iteraciones indicado
	 */
	abstract void automataEvolve();
	
	/** 
     *	M�todo que encuentra el camino m�s corto entre una celda origen y otra destino
     *	Recibe como par�metro la celda origen y la celda destino
     */
	public boolean pathfinding( int originRow, int originCol, int targetRow, int targetCol )
	{
		return true;
	}

}
