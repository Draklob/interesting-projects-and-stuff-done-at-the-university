/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;


/**
 * Clase que arranca la aplicaci�n
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class Application 
{
	
	//Referencia al gestor principal del juego 
	static Game game;
	
	static boolean debug;
	
	//Entrada a la aplicaci�n
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		//Indicamos si estamos en modo debug
		debug = true;
		
		//Inicializamos el juego 
		game = new Game();
		//Invocamos al men� principal con el loop del juego
		game.menu();
		
		game.sc.close();
		
	}

}
