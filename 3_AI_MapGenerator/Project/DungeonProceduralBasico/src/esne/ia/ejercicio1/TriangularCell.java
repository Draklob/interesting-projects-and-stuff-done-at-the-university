/**
 * Maps Generation
 * Javier Barreiro Portela -> jbarreiro.23@gmail.com
 * Miguel �ngel Torres Fern�ndez-P��ar -> miguelangeltorresfp@gmail.com
 * Group 3.3 VideoGame Programming
 * 31/05/2014
 */

package esne.ia.ejercicio1;

/**
 * Clase que hereda de Cell
 * Implementa celdas de tipo triangular que por tanto tienen cuatro vecinos posibles
 * @version 1.0
 * @author  Miguel �ngel Torres FP
 * 			Asignatura de IA / Esne 2014
 */
public class TriangularCell extends Cell
{
	
	/** 
     *	El constructor
     *	@param _row : n�mero de fila
     *  @param _col : n�mero de columna
     *  @param _map : referencia al mapa
     */
	public TriangularCell( int _row, int _col, Map _map )
	{
		//Las celdas triangulares tienen tres vecinas ( pero cuatro posibles )
		//De los cuatro uno siempre ser� inaccesible
		super( _row, _col, _map );
	}
	
	/**
	 * M�todo que resetea la celda a sus condiciones iniciales
	 */
	public void reset()
	{
		//Invocamos el m�todo de la clase padre
		super.reset();	
	}
	
	
	/**
	 * M�todo que asigna las celdas vecinas al array de celdas vecinas
	 * Un objeto NeihboringCell tiene como atributo la celda vecina y otros atributos
	 * ( como por ejemplo si dicha celda est� "enabled", accesible para el mapa dungeon )
	 * Si no existe una celda vecina en una direcci�n se pone a null el atributo cell de dicha celda vecina
	 */
	public void setNeighboringCells()
	{
		//Asignamos las celdas vecinas del array de celdas vecinas
		//De primeras todas las celdas vecinas se resetean luego como inaccesibles para el mapa tipo dungeon
		
		//Usamos dos variables locales para ir desplazando filas y columnas y
		//poder referenciar las celdas vecinas
		int aux_col;
		int aux_row;
		
		/*LOS LADOS EN HORIZONTAL SON IGUALES PARA TODAS LAS CELDAS*/
		
		//Comprobamos la de la derecha
		//Le sumamos uno a la columna y
		//comprobamos que esa celda exista ( "0" )
		aux_col = this.col + 1;
		if( aux_col < this.map.cols )
		{
			//Asignamos la celda vecina
			this.neighboringCells[0].cell = this.map.cellArray[this.row][aux_col];
		}
		else this.neighboringCells[0].cell = null;
		
		//Comprobamos la de la izquierda
		//Le restamos uno a la columna y
		//comprobamos que esa columna exista ( "2" )
		aux_col = this.col - 1;
		if( aux_col >= 0 )
		{
			//Asignamos la celda vecina
			this.neighboringCells[2].cell = this.map.cellArray[this.row][aux_col];
		}
		else this.neighboringCells[2].cell = null;
		
		/*VARIAN EN LAS CONEXIONES VERTICALES ARRIBA Y ABAJO*/
		
		//Si la fila es par y la columna par
		//o si la fila es impar y la columna impar
		if( ( this.row % 2 == 0 ) & ( this.col % 2 == 0 ) || ( this.row % 2 != 0 ) & ( this.col % 2 != 0 ) )
		{
			//En vertical tiene posible vecino abajo
			//Por tanto primero ponemos a null el de arriba ( "3" )
			this.neighboringCells[3].cell = null;	
			
			//Le sumamos uno a la fila y
			//comprobamos que esa celda exista ( "1" )
			aux_row = this.row + 1;
			if( aux_row < this.map.rows )
			{
				//Asignamos la celda vecina
				this.neighboringCells[1].cell = this.map.cellArray[aux_row][this.col];
			}
			else this.neighboringCells[1].cell = null;
			
		}
		//Si la fila es par y la columna impar
		//o si la fila es impar y la columna par
		else
		{
			//En vertical tiene posibles vecino arriba
			//Por tanto primero ponemos a null el de abajo ( "1" )
			this.neighboringCells[1].cell = null;	
			
			//Le restamos uno a la fila y
			//comprobamos que esa celda exista ( "3" )
			aux_row = this.row - 1;
			if( aux_row >= 0 )
			{
				//Asignamos la celda vecina
				this.neighboringCells[3].cell = this.map.cellArray[aux_row][this.col];
			}
			else this.neighboringCells[3].cell = null;
		}
		
	}
	
	
	/** 
     *	Habilitamos la celda vecina correspondiente al index que le pasamos
     *	Por ejemplo si le pasamos el "0" que ser�a "right" abrir�amos la celda 
     *	vecina "left", o sea, la opuesta.
     */
	public void connectWithCell( int index )
	{
		if( index == 0 ) this.neighboringCells[2].enabled = true;
		else if( index == 1 )  this.neighboringCells[3].enabled = true;
		else if( index == 2 )  this.neighboringCells[0].enabled = true;
		else if( index == 3 )  this.neighboringCells[1].enabled = true;	
	}
	
	
	/************************************************************/
    /********* Los m�todos necesarios para dibujar **************/
	/************************************************************/
	
	/** 
     *	Pintamos la primera pasada de la celda ( las paredes laterales superiores )
     * ( las de los triangulos invertidos no las dibujamos aqu� )
     */
	public void drawTriangUpSides()
	{
		//Si la celda vecina de la izquierda ( "2" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[2].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("/");	
		}
		//Pintamos un espacio en blanco
		System.out.print(" ");
		//Si la celda vecina a la derecha ( "0" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[0].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("\\");	
		}
	}
	
	/** 
     *	Pintamos la segunda pasada de la celda ( las paredes laterales inferiores )
     * ( las de los triangulos invertidos no las dibujamos aqu� )
     */
	public void drawTriangBottomSides()
	{
		//Si la celda vecina de la izquierda ( "2" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[2].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("/");	
		}
		//Pintamos el contenido de la celda
		drawTriangContent();
		//Si la celda vecina a la derecha ( "0" ) es accesible pintamos un espacio en blanco
		if( neighboringCells[0].enabled )
		{
			System.out.print(" ");
		}
		else //Si no es accesible pintamos una pared
		{
			System.out.print("\\");	
		}
	}
	
	/** 
     *	Pintamos el suelo de las celdas 
     * ( no pintamos aqu� los techos de los tri�ngulos invertidos )
     */
	public void drawTriangBottom()
	{
		//Si la celda vecina de abajo ( "1" ) es accesible 
		if( neighboringCells[1].enabled )
		{
			System.out.print("+     ");
		}
		else //Si no es accesible 
		{
			System.out.print("+-----");	
		}
	}
	
	/** 
     *	Pintamos el contenido de la celda ( tres espacios )
     */
	public void drawTriangContent()
	{
		//Invocamos el m�todo de la celda padre para pintar el contenido
		System.out.print(" ");
		super.drawCellContent();
		System.out.print(" ");
	}
}
