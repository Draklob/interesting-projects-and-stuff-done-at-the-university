#!/bin/bash

## INICIO DEL SCRIPT

##    DECLARACION DE LAS FUNCIONES A UTILIZAR  ##

# Función de ayuda del script. Nos mostrará los detalles como utilizarlo.
    use_script()
    {
		echo
		echo "Remember: "
        echo "    memory-check.sh <threshold> <process>"
        echo
    }
    
 # Función log para mostrar por consola si el script se está ejecutando bien o mal
	log_script()
	{
		echo
		case $detailedError in
				umbral)
					echo "El primer parámetro no es un número. Vuelva a iniciar el script y escriba un número correcto."
				;;
                parameters)
                    echo "Parámetros incorrectos. Escribiste $message parámetros."
                ;;
                general)
					echo "Error en $message."
                ;;
                process)
					echo "Lo siento, este proceso que estás buscando no existe."
                ;;
        esac
        
        use_script
	}
	
	# Función que hace toda la comprobación del proceso
	checkProcess()
	{
		# Guardamos el pid del proceso padre del proceso elegido.
		idFatherProcess=`ps fl | grep $nameProcess$ | grep -v bash | head -1 | tr -s ' ' | cut -f 3 -d ' '`
		
		# Si el PID padre coincide con el PID del padre encontrado la 1º vez, ejecuta todo el proceso de comprobación:
		if [ "$idFatherProcess" -eq "$idFather" ] || [ -z "$idFather" ]; then
			# Guardo el PID del proceso padre
			idFather=$idFatherProcess

			# Guardamos todos identificadores ( PID ) de los procesos hijo de su padre.
			childProcesses=`pgrep -P $idFatherProcess`
		
			# Recorremos cada proceso hijo para comprobar si supera el umbral
			for child in $childProcesses
			do
				# Obtenemos el umbral de cada hijo para comprobar si supera el umbral.
				threshold=`ps fl | grep $child | grep $nameProcess$ | grep -v bash | tr -s ' ' | cut -f 8 -d ' '`
				
				if [ "$threshold" -gt "$maxThreshold" ]; then
					# Eliminamos el proceso hijo que supere el umbral elegido.
					kill -9 $child
				fi
			done
			# echo "Fin Procesos"
		else
			# Si el proceso padre ya no existe ( Comparo el PID del padre de la primera vez si sigue siendo el mismo )
			# Sino mato a todos los procesos hijos, que quedaron zombies y ya no tienen padre, que pasan a ser de init.

			# Recorremos cada proceso hijo zombie sin padre y los matamos.
			for child in $childProcesses
			do
				kill -9 $child
			done
			
			# Otra forma posible de matar a todos los hijos sería así:
			# killall $nameProcess
			
			# echo "Hijos eliminados."
		fi
	}
	
##     FIN DE LAS FUNCIONES  ##
	
## INICIO Variables a utilizar    ##

	# Un mensaje de error con más detalles
    detailedError=""
	message=""
	
	# Umbral
	declare -i maxThreshold
	
	# Nombre del proceso
	nameProcess=""
	
	# PID del proceso buscado
	idFather=""
	
	# PID de los procesos hijos del proceso padre buscado
	childProcesses=""
	
## FIN Variables a utilizar    ##

# Si introducimos los 2 parámetros que se piden, continuamos.
if [ $# = 2 ]; then
	# Si es un número, procedemos con otras comprobaciones
	if [[ $1 =~ ^-?[0-9]+$ ]]; then
		# Si esto se cumple, quiere decir que existe el proceso
		# Otra opcion posible => ps fl | grep '\$2$' | head -2 | wc -l
		numProcess=`pidof $2 | wc -w`
		if [ $numProcess -gt 0 ]; then
			# Guardamos el umbral que introduce por parámetro.
			maxThreshold=$1
		
			# Guardamos el nombre del proceso a buscar.
			nameProcess=$2
					
			# Llamamos a la función. Hacemos un bucle para que esté ejecutándose continuamente
			# hasta que solo quede el proceso padre o no quede ningún proceso. Sino el script termina porque sale fuera.
			while [ ! $numProcess -le 1 ];
			do
				checkProcess
				numProcess=`pidof $2 | wc -w`
			done
		else
			detailedError=process
			log_script
		fi
	# Si el 1º parámetro no es un número, lanzamos este mensaje de error.	
	elif [[ ! $1 =~ ^-?[0-9]+$ ]]; then
		detailedError=umbral
		log_script
		
	fi
	
# Si se supera el número de parámetros, lanzamos este mensaje de error.	
elif [ $# > 2 ]; then
	detailedError=parameters
	message=$#
	log_script
		
fi
