#!/bin/bash

## INICIO DEL SCRIPT

##    DECLARACION DE LAS FUNCIONES A UTILIZAR  ##

    # Función de ayuda del script. Nos mostrará los detalles como utilizarlo.
    use_script()
    {
        echo "    backup.sh [ -c | -u <days> | -d <days> ] directory."
        echo -e "\t-c: Complete Backup        =>    -c nameDirectory."
        echo -e "\t-u: Changes in files       =>    -u numDays nameDirectory"
        echo -e "\t-d: Differential Backup    =>    -d numDays nameDirectory"
        echo -e "\t-i: Incremental Backup	  =>	-i ( No parameters. )"
        echo
    }
   
    # Función que configura donde se guardan los backups y el fichero de logs.
    load_config()
    {
        # Configuramos la ruta donde se guardarán el fichero de log de backups
        pathLogBackup=$HOME/log
       
        # Configuramos la ruta donde se guardarán los backups
        pathBackups=$HOME/backups
       
        # Comprobamos si la carpeta "log" no existe, la creamos.
        if [ ! -d $pathLogBackup ]; then
            mkdir $pathLogBackup        # Crea la carpeta log en la carpeta local de dicho usuario.
            # Si ya existe la carpeta "log" no pasa nada.
        fi
       
        # Comprobamos si la carpeta "backups" no existe, la creamos.
        if [ ! -d $pathBackups ]; then
            mkdir $pathBackups        # Crea la carpeta backups en la carpeta local de dicho usuario.
            # Si ya existe la carpeta "backups" no pasa nada.
        fi
    }
   
    # Función que guarda en un archivo log los registros de los backups que se van haciendo.
    log_script()
    {
        # Comprobamos si no existe el archivo "log_backups", lo creamos.
        if [ ! -f $pathLogBackup/log_backups ]; then
            touch $pathLogBackup/log_backups
        fi
           
        # Si existe pero está vacío, no calculamos el index. Sólo sumamos 1.
        if [ ! -s $pathLogBackup/log_backups ]; then
            index+=1;
        # Si existe y tiene ya algún mensaje guardado, calculamos el index.
        else
            # Control para ir aumentando el índice del nº de mensajes en el documento.
            index=`grep -co :: $pathLogBackup/log_backups`
            index=$(($index+1))
        fi
       
        if [ -z "$messageLog" ]; then
            # Errores específicos para diferentes mensajes en el log.
            case $detailedError in
                directory)
                    echo "$index:: `date +%a" "%d" "%b" "%Y" "%R:%S" "%Z` El usuario `whoami`, ejecutó el script backup.sh: ERROR $typeBackup Falló la copia de seguridad sobre el directorio `readlink -f $pathDirectory` porque no existe." >> $pathLogBackup/log_backups
                ;;
                parameters)
                    echo "$index:: `date +%a" "%d" "%b" "%Y" "%R:%S" "%Z` El usuario `whoami`, ejecutó el script backup.sh: ERROR $typeBackup Falló la copia de seguridad porque los parámetros no eran correctos." >> $pathLogBackup/log_backups
                ;;
                sshfail)
					echo "$index:: `date +%a" "%d" "%b" "%Y" "%R:%S" "%Z` El usuario `whoami`, intentó conectarse por ssh pero falló la conexión." >> $pathLogBackup/log_backups
                ;;
                ssh)
                echo "$index:: `date +%a" "%d" "%b" "%Y" "%R:%S" "%Z` El usuario `whoami`, se conectó por ssh guardando una copia del backup en el servidor." >> $pathLogBackup/log_backups
                ;;
            esac
        else
            # Guardamos el mensaje INFO sobre la tarea hecha en el script.
            if [ $messageLog == "true" ]; then
                echo "$index:: `date +%a" "%d" "%b" "%Y" "%R:%S" "%Z` El usuario `whoami`, ejecutó el script backup.sh: INFO $typeBackup La copia de seguridad sobre el directorio `readlink -f $pathDirectory` se completó correctamente." >> $pathLogBackup/log_backups
            fi
           
            # Guardamos el mensaje de ERROR GENERAL sobre la tarea hecha en el script.
            if [ $messageLog == "false" ]; then
                echo "$index:: `date +%a" "%d" "%b" "%Y" "%R:%S" "%Z` El usuario `whoami`, ejecutó el script backup.sh: ERROR $typeBackup Falló la copia de seguridad sobre el directorio, no se pudo completar." >> $pathLogBackup/log_backups
            fi
        fi
       
        # date +%c    =>  Nos muestra todo esto Wed 31 Dec 2014 09:22:13 PM CET
        # date +%a" "%d" "%b" "%Y" "%R:%S" "%Z    => Nos muestra Wed 31 Dec 2014 21:22:13 CET
    }
   
    # Función que ejecuta el Backup Completo. La 1º opción, la "-c".
    bu_Completo()
    {

        # Variable donde guardamos el nombre del archivo que va tener el fichero comprimido.
        nameBackup=backup_`date +%Y-%m-%d`_`date +%H_%M_%S`.tar.gz
       
        # Código que crea el archivo comprimido de la carpeta deseada.
        tar -zcf $pathBackups/$nameBackup $pathDirectory 2> /dev/null
       
        # Creamos el mensaje de que se comprimió correctamente la carpeta elegida.
            # Le decimos que el mensaje es de INFO
            messageLog=true
            # Llamamos a la función
            log_script

        #ssh -q server@192.168.1.28
        ssh -q student@192.168.1.28 exit	# Esto nos devuelve 0 si se conecta bien al ssh y si da error al conectar, devuelve 255.
        # Si falla la conexión ssh:
        if [ $? -eq 255 ]; then
			echo
			# Si sobra tiempo, añadir un log al archivo LOG como un error especifico de conexion ssh
			echo "Error, no se pudo conectar al servidor a través del servicio ssh. Revisa que el servicio esté operativo."
			echo
			
			detailedError=sshfail
            # Creamos el mensaje de log.
            log_script
        # Si el usuario conecta por ssh correctamente:
		else
			#ssh -q server@192.168.1.28 bash -c "'
			ssh -q student@192.168.1.28 bash -c "'
			
			# Si no existe la carpeta, la crea de forma automática.
			if [ ! -d rbackups ]; then
				# Siempre crea la carpeta en el /home/student ( en este caso ) porque al conectar con ssh, siempre vas al home.
				mkdir rbackups
			fi
			
			# Comprobamos también, que si no existe la carpeta del usuario que loguea al servidor para hacer la copia, la crea.
			if [ ! -d rbackups/$USER ]; then
				mkdir rbackups/$USER
			fi
			
			exit
			'"
			
			# Copiamos el archivo del backup completo creado al servidor.
			scp $pathBackups/$nameBackup student@192.168.1.28:rbackups/$USER/
			#scp $pathBackups/$nameBackup server@192.168.1.28:rbackups/$USER/
			
			# Guardamos un mensaje log en el archivo log de los backups.
			detailedError=ssh
            # Creamos el mensaje de log.
            log_script
			
		fi

<<OtroEjemploPosible	# Hay un error que no encontré bien como solucionarlo, pero esta sería otra opción de hacerlo.
			
			ssh -q -p 2345 server@192.168.1.28 <<**
			
			# Si no existe la carpeta, la crea de forma automática.
			if [ ! -d rbackups ]; then
				mkdir rbackups
			fi
			
			# Comprobamos también, que si no existe la carpeta del usuario que loguea al servidor para hacer la copia, la crea.
			if [ ! -d rbackups/$USER ]; then
				mkdir rbackups/$USER
			fi
			
			exit
**
OtroEjemploPosible
        
    }
   
    # Función que ejecuta los Últimos Cambios. La 2º opción, la "-u".
    bu_LastChanges()
    {
        # Variable de la ruta para crear un archivo oculto temporal donde se guardan los nombres de los archivos encontrados para comprimir.
        pathFichero=$pathBackups/.tempFileLastChanges
       
        # Variable de la ruta para crear una carpeta temporal llamada "tempFolder" donde guarda los archivos comprimidos elegidos.
        pathFolder=$pathBackups/tempFolderLastChanges
       
        # Variable donde guardamos el nombre del archivo que va tener el fichero comprimido.
        nameBackup=backupLC_`date +%Y-%m-%d`_`date +%H_%M_%S`.tar.gz
       
        # Creamos la carpeta temporal donde se guardarán los archivos comprimidos
        mkdir $pathFolder
       
        # buscar los ficheros modificados en x dias de un directorio
        find $pathDirectory -type f -mtime -$days | sed 's/^/\/home\/'"$USER"'\//' > $pathFichero
       
        # Creamos un bucle while para recorrer e ir creando cada archivo elegido.
        declare -i idx=`grep -c $pathDirectory $pathFichero`        # Para conocer cuantos archivos encontrados tenemos que recorrer.
        declare -i id=1        # Utilizado para ir recorriendo el while.

        while read
        do
            if (( id <= idx  )); then
				cp `head -$id $pathFichero | tail -1` $pathFolder/`head -$id $pathFichero | tail -1 | tr / @`
            fi
            id=$((id+1))    # Sumamos 1 al id para cambiar al siguiente archivo.       
        done < $pathFichero

        # Creamos la carpeta comprimida con todos los archivos comprimidos.
        tar -czf $pathBackups/$nameBackup -C $pathFolder/ ../tempFolderLastChanges &> /dev/null
       
        # Cuando terminamos de hacer todo el proceso, borramos ambos archivos ocultos, el fichero y la carpeta.
        rm -r $pathFolder    # Borra la carpeta.
        rm $pathFichero        # Borra el fichero
       
        # Creamos el mensaje de que se comprimió correctamente la carpeta elegida.
            # Le decimos que el mensaje es de INFO
            messageLog=true
            # Llamamos a la función
            log_script
    }
   
    # Función que ejecuta el Backup Diferencial. La 3º opción, la "-d".
    bu_Differential()
    {
        # Variable de la ruta para crear un archivo oculto temporal donde se guardan los nombres de los archivos encontrados.
        pathFiles=$pathBackups/.tempFileDifferential
        
         # Variable de la ruta para crear un archivo oculto temporal donde se guardan los nombres de las carpetas encontradas.
        pathFolders=$pathBackups/.tempFoldersDifferential
       
        # Variable de la ruta para crear una carpeta oculta temporal llamada "tempFolder" donde guarda los archivos comprimidos elegidos.
        pathTempFolder=$pathBackups/tempFolderDifferential
       
        # Variable donde guardamos el nombre del archivo que va tener el fichero comprimido.
        nameBackup=backupDiff_`date +%Y-%m-%d`_`date +%H_%M_%S`.tar.gz
       
        # Creamos la carpeta temporal donde se guardarán los archivos comprimidos
        mkdir $pathTempFolder
        
        # Buscamos las carpetas donde están los archivos modificados en esos "x" días.
		find $pathDirectory -mtime -$days -type d > $pathFolders
		
		# Buscamos los archivos que fueron modificados en esos "x" días.
		find $pathDirectory -mtime -$days -type f > $pathFiles
   
		# Creamos un bucle while para recorrer e ir creando cada carpeta elegida.
		declare -i idx=`grep -c $pathDirectory $pathFolders`        # Para conocer cuantas carpetas encontradas tenemos que recorrer y crear.
        declare -i id=1        # Utilizado para ir recorriendo el while.
    
        while read
        do
            if (( id <= idx  )); then
				mkdir $pathTempFolder/`head -$id $pathFolders | tail -1`
            fi
            id=$((id+1))    # Sumamos 1 al id para cambiar al siguiente archivo.
            
        done < $pathFolders
   
   
		idx=`grep -c $pathDirectory $pathFiles`        # Para conocer cuantos archivos encontrados tenemos que recorrer y copiar.
        id=1        # Utilizado para ir recorriendo el while.
        
		# Creamos un bucle while para recorrer e ir creando cada archivo elegido.
        while read
        do
            if (( id <= idx  )); then
				 name=`head -$id $pathFiles | tail -1`
                 cp $name $pathTempFolder/`dirname $name`
            fi
            id=$((id+1))    # Sumamos 1 al id para cambiar al siguiente archivo.       
        done < $pathFiles
        
        # Creamos la carpeta comprimida con todos los archivos comprimidos.
        tar -czPf $pathBackups/$nameBackup -C $pathTempFolder $pathDirectory
       
        # Cuando terminamos de hacer todo el proceso, borramos ambos archivos ocultos, el fichero y la carpeta.
        rm -r $pathTempFolder    # Borra la carpeta temporal.
        rm $pathFiles      		 # Borra el fichero temporal de ficheros.
        rm $pathFolders       	 # Borra el fichero temporal de carpetas.
       
        # Creamos el mensaje de que se comprimió correctamente la carpeta elegida.
            # Le decimos que el mensaje es de INFO
            messageLog=true
            # Llamamos a la función
            log_script
    }
    
    bu_Incremental()
    {
		# Variable de la ruta donde se guarda los directorios que tiene el backup completo.
		pathFolders=$pathBackups/tempFoldersIncremental
		
		# Variable de la ruta donde se guarda los directorios que tiene el backup completo.
		pathFiles=$pathBackups/tempFilesIncremental
		
		 # Variable donde guardamos el nombre del archivo que va tener el fichero comprimido.
        nameBackup=backupIncre_`date +%Y-%m-%d`_`date +%H_%M_%S`.tar.gz
		
		# Creamos la carpeta donde se va descomprimir el backup completo y poder analizarlo.
		mkdir $pathBackups/IncrementalBC
		
		# Descomprimos el último backup completo que exista para actualizar sus datos.
		tar -xzf $bcExist -C $pathBackups/IncrementalBC
		
		# Guardamos el nombre de la carpeta raíz del backup completo. Ya que nos va hacer falta para varias cosas.
		mainFolder=`ls $pathBackups/IncrementalBC`
		
		# Encontramos y copiamos en un archivo las carpetas que tiene el backup completo.
		find $pathBackups/IncrementalBC/* -type d | sed 's/\/home\/'"$USER"'\/backups\/IncrementalBC\///g' > $pathFolders
    
		# Encontramos y copiamos en un archivo los archivos que tiene el backup completo.
		find $pathBackups/IncrementalBC -type f | sed 's/\/home\/'"$USER"'\/backups\/IncrementalBC\///g' > $pathFiles
        
        # Encontramos y copiamos en un archivo las carpetas que tiene la carpeta original elegida.
        find $pathDirectory -type d | sed '1s/\/$//g' > $pathBackups/Folders		# En caso de que en la 1º línea se añada un "/", se lo quitamos.
        # Encontramos y copiamos en un archivo los archivos que tiene la carpeta original elegida.
        find $pathDirectory -type f | sed '/~/d' > $pathBackups/Files
        
        # Declaramos los tamaños de los arrays que vamos a utilizar.
        declare -i lenghtArrayFB=`grep -c $mainFolder $pathFolders`		# Folders Backup
        declare -i lenghtArrayFO=`grep -c $mainFolder backups/Folders`	# Folders Original
        declare -i lenghtArrayAB=`grep -c $mainFolder $pathFiles`		# Archives Backup
        
        # Todo el proceso para analizar y recorrer la carpeta elegida del backup completo.
		for((i=0;i<lenghtArrayFB;i++));
        do
			# Array donde se coge una de las carpetas del backup completo.
			arrayBackupFolders[$i]=`head -$(($i+1)) $pathFolders | tail -1`
			
			for((j=0;j<lenghtArrayFO;j++));
			do
				# Array donde se coge una de las carpetas de la carpeta original.
				arrayHomeFolders[$j]=`head -$(($j+1)) backups/Folders | tail -1`
				
				# Comparamos si ambas carpetas se llaman igual incluyendo su ruta a parte del nombre.
				if [ ${arrayBackupFolders[$i]} = ${arrayHomeFolders[$j]} ]; then
					# Tamaño del array de archivos que hay en dicha carpeta original para poder recorrerlos.
					declare -i lenghtArrayAO=`find ${arrayBackupFolders[$i]} -maxdepth 1 -type f | grep -c $mainFolder`	# Archives Original
					# Guardamos los nombres de los archivos que tiene dicha carpeta.
					filesInFolder=`find ${arrayBackupFolders[$i]} -maxdepth 1 -type f | sed '/~/d'`
					
					for((z=0;z<lenghtArrayAB;z++));
					do
						# Array de los nombres de los archivos que hay en dicha carpeta del backup completo.
						arrayBackupFiles[$z]=`head -$(($z+1)) $pathFiles | tail -1`
						
						for file in $filesInFolder
						do
							# Si el archivo del backup y el original son iguales, procedemos a ver si hay una nueva modificación.
							if [ ${arrayBackupFiles[$z]} = $file ]; then
								# Comparamos si un archivo es más viejo el del backup que el archivo original.
								# Si es así, copiamos el nuevo archivo.
								if [ "$pathBackups/IncrementalBC/${arrayBackupFiles[$z]}" -ot "$file" ]; then
									cp $file $pathBackups/IncrementalBC/${arrayBackupFiles[$z]}
								fi
							fi
						done
					done
					# Comprobamos si alguno de los ficheros en la carpeta original no existe en el backup. Si no existe, lo copiamos.
					for file in $filesInFolder
						do
							if [ ! -f $pathBackups/IncrementalBC/$file ]; then
								cp $file $pathBackups/IncrementalBC/$file
							fi
						done
					
				fi
			done
		done
		
		# Creamos la carpeta comprimida con todos los archivos actualizados o nuevos del backup completo anterior.
	#	tar -zcPf $pathBackups/$nameBackup $pathBackups/IncrementalBC/$mainFolder 2> /dev/null
		tar -zcPf $pathBackups/$nameBackup -C $pathBackups/IncrementalBC $mainFolder 2> /dev/null
	#	tar -czPf $pathBackups/$nameBackup -C $pathTempFolder $pathDirectory
		# Borramos los archivos que se crean para analizar el backup y la carpeta donde se guarda el backup a analizar.
		rm -r $pathBackups/IncrementalBC # Se borra la carpeta donde está el backup descomprimido.
		rm $pathFolders		# Borramos el archivo donde guardamos las carpetas del backup completo.
		rm $pathFiles 		# Borramos el archivo donde guardamos los archivos del backup completo.
		rm $pathBackups/Folders	# Borramos el archivo donde guardamos las carpetas de la carpeta original elegida.
		rm $pathBackups/Files		# Borramos el archivo donde guardamos los archivos de la carpeta original elegida.
		
		# OTRA OPCIÓN POSIBLE Y QUE FUNCIONA ES UTILIZANDO ESTA LÍNEA
		# tar --listed-incremental=snapshot.file -cvzf $nameBackup $pathDirectory &> /dev/null
	}
   
    # Sistema de error que nos permite elegir si queremos ir a la ayuda o no.
    error()
    {
        # Sistema para consultar la ayuda después de un error.
        echo -ne "Puede consutar la ayuda del script escribiendo backup --help \no escribiendo \"SI\" o \"si\" para acceder directamente.\n\n"
        read help
        if [ ! $help ]; then
			exit 
        elif [ $help == SI ] || [ $help == si ]; then
			echo
			use_script
        else
			echo
			echo "Tu respuesta fue: $help"
			echo -e "\nNo elegiste la opción de consultar la ayuda, gracias y hasta luego.\n"
        fi
    }
   
   
##     FIN DE LAS FUNCIONES  ##

## BackUp Completo

    ## INICIO Variables a utilizar    ##
   
        # Variable para guardar la ruta del directorio que queremos comprimir
        pathDirectory=""    # Inicializamos la variable para evitar errores.
       
        # Inicializamos la variable para decidir luego que mensaje mandar.
        messageLog=""        # Inicializamos la variable para evitar errores.
       
        # Variable donde guarda la ruta del fichero de log de los backups
        pathLogBackup=""
       
        # Variable donde guarda la ruta donde se guardan los backups
        pathBackups=""
   
        # El tipo de backup que se hace.
        typeBackup=""
       
        # Un mensaje de error con más detalles
        detailedError=
       
        # El número de días para hacer la búsqueda de archivos.
        declare -i days
       
    ## FIN Variables a utilizar    ##


    ## Ejecución final del script ##
   
    ## Llamamos primero a la función load_config() que configura las rutas de donde se guardan las cosas.
    load_config
   
    # Control de parámetros para que el script haga una tarea u otra.
    case $1 in
		## 1º Opción => Backup Completo
        -c)
            typeBackup="\"Backup Completo\""    # El tipo de backup.
            
            # Si cumple con el requisito de que son 2 parámetros.
            if (( $# == "2" )); then
                # Si la carpeta elegida existe, procedemos a ejecutar la tarea.
                if [ -d $2 ]; then
					pathDirectory=$2    # Guardamos el nombre del directorio a comprimir.
                    bu_Completo
                # Si la carpeta elegida no existe.
                else
                    detailedError=directory
                    # Creamos el mensaje de log.
                    log_script
                fi
            else
                if (( $# == "1" )); then
                    echo -e "\nError, no introduciste el nombre del fichero.\n"
                else
                    echo -e "\nError, sobrepasaste el número de argumentos.\n"
                fi
                # Creamos el mensaje de que falló el script.
                    # Le decimos que el mensaje es de ERROR
                    detailedError=parameters
                    # Llamamos a la función
                    log_script
                error
            fi
        ;;
       
       ## 2º Opción Backup Últimos Cambios
        -u)
            typeBackup="\"Últimos Cambios\""    # El tipo de backup.
            
            # Si cumple con el requisito de que son 3 parámetros.
            if (( $# == "3" )); then
           
                pathDirectory=$3    # Guardamos el nombre del directorio a comprimir.
               
                # Si la carpeta elegida existe y "days" es un numero entero, procedemos a ejecutar la tarea.
                if [ -d $3 ] && [[ $2 =~ ^-?[0-9]+$ ]]; then    # =~ ^-?[0-9]+$ => Chequea que sea un entero.
                    days=$2                # Guardamos el nº de días que puso el usuario para hacer la búsqueda.
                    bu_LastChanges        # Ejecutamos la tarea de Últimos Cambios
                elif [ ! -d $3 ]; then
                    detailedError=directory
                    # Creamos el mensaje de log.
                    log_script
                else
                    messageLog=false
                    # Creamos el mensaje de log.
                    log_script
                fi
            else
                if (( $# < "3" )); then
                    echo -e "\nError, el número de parámetros es incorrecto, falta alguno. Consulta la ayuda\n."
                else
                    echo -e "\nError, el número de parámetros es incorrecto, hay de más. Consulta la ayuda.\n"
                fi
                # Creamos el mensaje de que falló el script.
                    # Le decimos que el mensaje es de ERROR
                    detailedError=parameters
                    # Llamamos a la función
                    log_script
                error
            fi
        ;;
      
       ## 3º Opción Backup Diferencial
        -d)
            typeBackup="\"Backup Diferencial\""    # El tipo de backup.
            
            # Si cumple con el requisito de que son 3 parámetros.
            if (( $# == "3" )); then
           
                pathDirectory=$3    # Guardamos el nombre del directorio a comprimir.
               
                # Si la carpeta elegida existe y "days" es un numero entero, procedemos a ejecutar la tarea.
                if [ -d $3 ] && [[ $2 =~ ^-?[0-9]+$ ]]; then    # =~ ^-?[0-9]+$ => Chequea que sea un entero.
                    days=$2                # Guardamos el nº de días que puso el usuario para hacer la búsqueda.
                    bu_Differential        # Ejecutamos la tarea de Backup Diferencial
                elif [ ! -d $3 ]; then
                    detailedError=directory
                    # Creamos el mensaje de log.
                    log_script
                else
                    messageLog=false
                    # Creamos el mensaje de log.
                    log_script
                fi
            else
                if (( $# < "3" )); then
                    echo -e "\nError, el número de parámetros es incorrecto, falta alguno. Consulta la ayuda\n."
                else
                    echo -e "\nError, el número de parámetros es incorrecto, hay de más. Consulta la ayuda.\n"
                fi
                # Creamos el mensaje de que falló el script.
                # Le decimos que el mensaje es de ERROR
                detailedError=parameters
                # Llamamos a la función
                log_script
                error
            fi
        ;;
        
        ## 4º Opción Backup Incremental
        -i)
        typeBackup="\"Backup Incremental\""    # El tipo de backup.
        
        # Realmente no se utiliza aquí para nada que lea la última línea, pero si comprueba que existe por lo menos un backup completo.
        bcExist=`find backups/backup_* 2>/dev/null | tail -1 | head -1`	 
			if [ -z "$bcExist" ]; then
				echo
				echo "No se puede hacer el Backup Incremental, no existe ningún backup completo anterior."
				echo
				
				messageLog=false
                # Creamos el mensaje de log.
                log_script
			else
				if (( $# == "2" )); then
					# Si la carpeta elegida existe, procedemos a ejecutar la tarea.
					if [ -d $2 ]; then
						pathDirectory=$2    # Guardamos el nombre del directorio a comprimir.
						bu_Incremental
					# Si la carpeta elegida no existe.
					else
						detailedError=directory
						# Creamos el mensaje de log.
						log_script
					fi
				else
					if (( $# < "2" )); then
						echo -e "\nError, el número de parámetros es incorrecto, falta alguno. Consulta la ayuda\n."
					elif (( $# > "2" )); then
						echo -e "\nError, el número de parámetros es incorrecto, hay de más. Consulta la ayuda.\n"
					fi
					# Creamos el mensaje de que falló el script.
					# Le decimos que el mensaje es de ERROR
					detailedError=parameters
					# Llamamos a la función
					log_script
					error
				fi
			fi
        ;;
        
       ## ## Opción de Ayuda
        --help)
            echo
            use_script
        ;;
        
        *)
            if (( $# > "3" )); then
                echo
                echo -e "\nError!! Sobrepasaste el número de parámetros. Consulte la ayuda."
            elif (( $# == "0" )); then
                echo
                echo "Error!! No introduciste ningún parámetro."
            fi
           
            echo
            error
        ;;
    esac
   
## FIN DEL SCRIPT ##
