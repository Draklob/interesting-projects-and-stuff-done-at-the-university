#!/bin/bash

## INICIO DEL SCRIPT

## Función que se encarga de crear la carpeta, el archivo y el mensaje.
message() {
    # Comprobamos si la carpeta "log" no existe, la creamos.
    if [ ! -d "$HOME/log" ]; then
    #    echo "No existe la carpeta"
        mkdir $HOME/log        # Crea la carpeta log en la carpeta local de dicho usuario.
    fi

    # Comprobamos si no existe el archivo ocupacion, por si tenemos que crearlo.
    if [ ! -f $HOME/log/ocupacion ]; then
        index+=1;
    #    echo "No existe este archivo"
        echo "$index:: La revisión del día `date +%d-%m-%Y` a las `date +%H:%M:%S` su disco duro estuvo al $disco%. Por favor, libere el disco." > $HOME/log/ocupacion
    # Si el archivo ya existe, procedemos a guardar solamente el mensaje.
    else
        # Control para ir aumentando el índice del nº de errores en el documento.
        index=`grep -co :: $HOME/log/ocupacion`
        index+=1;

        echo "$index:: La revisión del día `date +%d-%m-%Y` a las `date +%H:%M:%S` su disco duro estuvo al $disco%. Por favor, libere el disco." >> $HOME/log/ocupacion
    fi
}

# Declaramos el entero "index" para enumerar los mensajes.
declare -i index

# Declaramos "ocupado" y calculamos el % ocupado de los discos duros para compararlo.
# Usamos grep para que solo nos de los resultados entre 0 y 9 y en un rango de 1 o 2 números. Y quitamos el carácter '%'.
ocupado=`df -h /dev/sd* | grep -Eo [0-9]\{1,2\}% | tr -d '%'`

# Recorremos los resultados guardados en "ocupado" y los comparamos por si alguno de ellos superó el 90%. Si lo hace, manda un aviso.
for id in $ocupado
do
    if [ $id -ge 90 ]; then
    # Guardamos el % del disco ocupado.
    disco=$id

    echo "LLamando al mensaje de aviso."
    message
    fi
done
