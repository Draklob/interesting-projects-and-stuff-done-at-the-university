package evolutionaryDemon;

import java.util.ArrayList;

import pacman.controllers.Controller;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;
import pacman.game.Game;

/**
 * Controlador para Pacman que devuelve el pr�xmo movimiento haciendo uno de una red neuronal evolucionada 
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 28/1/2015/A
 */
public class RNA_PacmanController extends Controller<MOVE>
{
	/**
	 * Si se usa directamente la funci�n que valora las entradas para devolver unas salidas
	 * o se usa la red neuronal evolucionada que trata de simular dicha funci�n
	 */
	public enum OutputUsed{ DirectFunction, RNA };
	
	/**
	 * Si se usa directamente la funci�n que valora las entradas para devolver unas salidas
	 * o se usa la red neuronal evolucionada que trata de simular dicha funci�n
	 */
	private OutputUsed outputUsed;
	
	/**
	 * Referencia a la clase donde se genera y evolucionan las redes neuronales
	 * hasta obtener una que pueda tomar decisiones correctas para el movimiento de MsPacman
	 */
	private RNA_Problem_Pacman myRNA_Scope;
	
	/**
	 * Valor de referencia para normalizar las distancias de los fantasmas respecto a Pacman
	 */
	private float maximumDistance;
	
	/**
	 * Valor de referencia para normalizar el tiempo que le queda a cada fantasma de ser comestible
	 */
	private float maximumEdibleTime;
	
	/**
	 * Si est� en modo de testeo se ir� comparando el valor de salida de la funci�n de entrenamiento
	 * con el ofrecido por la red neuronal despu�s de ser procesados.
	 */
	private boolean testingMode = false;
	
	/**
	 * Si est� en modo testingMode indicar� el n�mero de aciertos, o sea de veces que la red neuronal proporciona
	 * una salida similar a la funci�n de entrenamiento.
	 */
	private float succeed = 0;
	
	/**
	 * El n�mero de veces que se ha requerido el movimiento
	 */
	private float count = 0;
	
	/**
	 * Si est� en modo testingMode indicar� el n�mero de errores, o sea de veces que la red neuronal proporciona
	 * una salida diferente a la funci�n de entrenamiento.
	 */
	private float error = 0;
	
	/**
	 * Se guarda una referencia a la funci�n de entrenamiento para poder debuguear comparando la respuesta ofrecida
	 * por esta directamente y la respuesta ofrecida por la red neuronal
	 */
	private RNA_Problem_Pacman.TraningFunction trainingFunction;
	
	/**
	 * Para debuguear el porcentaje de acierto no se puede usar el movimiento devuelto, porque podr�a darse el caso
	 * de que el movimiento resultante para el array de valores de salida devuelto por la red neuronal fuera el mismo que para el 
	 * array de valores de salida devuelto directamente por la funci�n de entrenamiento. Es mejor hacerlo comparando la estrategia, incluso
	 * esto podr�a no ser del todo correcto.
	 */
	private enum StrategyType { ETG11, ETG12, ETG21, ETG22, ETG31, ETG32, ETG41, ETG42, ETG5 };
	
	/**
	 * Constructor del controlador para MsPacman que hace uso de una red neuronal entrenada
	 * para generar el movimiento �ptimo para MsPacman
	 * @param _outputUsed Indica si hacer uso de la red neuronal o directamente hacer uso de la funci�n de entrenamiento
	 * para calcular los movimientos de pacman en funci�n de los par�metros del juego en cada instante ( a trav�s del objeto "game" ).
	 * @param _trainingFunction la funci�n de entrenamiento que se usar� en el algoritmo de backpropagation
	 * @param _testingMode Indica si se testea la salida de la red neuronal compar�ndola con la de la funci�n de entrenamiento.
	 */
	public RNA_PacmanController( OutputUsed _outputUsed, RNA_Problem_Pacman.TraningFunction _trainingFunction, boolean _testingMode )
	{
		this.outputUsed = _outputUsed;
		this.testingMode = _testingMode;
		this.succeed = 0;
		this.error = 0;
		this.count = 0;
	
		this.maximumDistance = 95;
		this.maximumEdibleTime = 200;
		
		this.trainingFunction = _trainingFunction;
		// Se crea la clase que genera y evoluciona una poblaci�n de redes neuronales
		// hasta obtener una con un fitness �ptimo para MsPacman
		this.myRNA_Scope = new RNA_Problem_Pacman( _trainingFunction );
		
		if( outputUsed == OutputUsed.RNA )
		{
			this.myRNA_Scope.getBestIndividual();
		}
		else if( outputUsed == OutputUsed.DirectFunction )
		{
			this.myRNA_Scope.bestIndividualFound = true;
		}
	
	}// Fin del constructor 

	/**
	 * Sobrescribe el m�todo getMove que tiene que tener todo controlador de Pacman
	 * Implementa la posibilidad de comparar la estrategia seleccionada para el output devuelto por la red neuronal seleccionada
	 * con la estrategia seleccionada para el output devuelto directamente por la funci�n de entrenamiento seleccionada al ejecutar la aplicaci�n
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public MOVE getMove(Game game, long timeDue) 
	{
		// Se espera a que el algoritmo evolutivo haya terminado para usar la red neuronal
		if( ! this.myRNA_Scope.bestIndividualFound )
			return null;
		else
		{
		
			// Para obtener una estrategia de movimiento para Ms.Pacman
			// se generan las entradas necesarias normalizadas para la red neuronal a partir del objeto game
			
			ArrayList<Float> input = new ArrayList<Float>();
			
			// Se agregan las distancias de los fantasmas a pacman al array de inputs
			for(GHOST ghostType : GHOST.values())
			{
				int dist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(ghostType));
				float distNormalized = this.normalizeDistance(dist);
				input.add(distNormalized);
				//System.out.println("fantasma = " + ghostType);
				//System.out.println("dist = " + dist);
				//System.out.println("distNormalized = " + distNormalized);
			}
			
			// Se agregan los valores para el tiempo que le quedan a los fantasmas de ser comestibles al array de inputs
			for(GHOST ghostType : GHOST.values())
			{
				int time = game.getGhostEdibleTime(ghostType);
				float timeNormalized = this.normalizeEdibleTime(time);
				input.add(timeNormalized);
				//System.out.println("fantasma = " + ghostType);
				//System.out.println("time = " + time);
				//System.out.println("timeNormalized = " + timeNormalized);
			}
			
			// Si se usa la red neuronal para obtener un array de valores de salida
			// para un input concreto ofrecido por el juego en un momento dato
			if( outputUsed == OutputUsed.RNA )
			{
				// Se calculan los outputs de la red neuronal elegida
				ArrayList<Float> RNA_outputs = this.myRNA_Scope.getOutput(input);
				
				// Se procesan los datos de salida recibidos de la red neuronal
				// para esos datos la funci�n getNextMovement devuelve un array con la estrategia seleccionada y el movimiento para pacman
				ArrayList RNA_Result = this.getNextMovement(game, RNA_outputs);
				
				// Si se est� en modo testing
				// se eval�a si la red neuronal ofrece o no la misma salida que la funci�n de entrenamiento 
				if( this.testingMode )
				{
					ArrayList<Float> trainingFunctionOutputs = new ArrayList<Float>();
					if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.CustomFunction )
					{
						trainingFunctionOutputs = this.myRNA_Scope.getCustomTrainingOutputs(input);
					}
					else 
					if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.CustomFunctionHiperbolic )
					{
						
						trainingFunctionOutputs = this.myRNA_Scope.getTrainingOutputsHiperbolic(input);
					}
					else 
					if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.StarterPacmanFunction )
					{
						
						trainingFunctionOutputs = this.myRNA_Scope.getStarterPacmanTrainingOutputs(input);
					}
					// Se procesan los datos de salida recibidos de la funci�n de entrenamiento seleccionada
					// para esos datos la funci�n getNextMovement devuelve un array con la estrategia seleccionada y el movimiento para pacman
					ArrayList trainingFunction_Result = this.getNextMovement(game, trainingFunctionOutputs);
					// En la posici�n "0" del array se guarda la estrategia seleccionada en cada caso
					if( RNA_Result.get(0) == trainingFunction_Result.get(0) ) this.succeed ++;
					else this.error ++;
					float total = this.succeed + this.error;
					float successRate = ( this.succeed / total ) * 100;
					this.count ++; // Se podr�an sumar sin m�s los errores y los aciertos
				
					System.out.println( "-------------------------------------------" );
					System.out.println( "MOVIMIENTO N�MERO : " + this.count );
					System.out.println( "Estrategia esperada : " + trainingFunction_Result.toString() );
					System.out.println( "Estrategia devuelta : " + RNA_Result.toString() );
					System.out.println( "Total Aciertos : " + this.succeed );
					System.out.println( "Total Errores : " + this.error );
					System.out.println( "Promedio Acierto : " + successRate );
				}
				
				return (MOVE)RNA_Result.get(1);
			}
			else // if ( outputUsed == OutputUsed.DirectFunction )
			{
				ArrayList<Float> trainingFunctionOutputs = new ArrayList<Float>();
				if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.CustomFunction )
				{
					trainingFunctionOutputs = this.myRNA_Scope.getCustomTrainingOutputs(input);
				}
				else 
				if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.CustomFunction_Version2 )
				{
					
					trainingFunctionOutputs = this.myRNA_Scope.getCustomTrainingOutputs_version_2(input);
				}
				else 
				if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.CustomFunctionHiperbolic )
				{
					
					trainingFunctionOutputs = this.myRNA_Scope.getTrainingOutputsHiperbolic(input);
				}
				else 
				if( this.trainingFunction == RNA_Problem_Pacman.TraningFunction.StarterPacmanFunction )
				{
					
					trainingFunctionOutputs = this.myRNA_Scope.getStarterPacmanTrainingOutputs(input);
				}
				// Se procesan los datos de salida recibidos de la funci�n de entrenamiento seleccionada
				// para esos datos la funci�n getNextMovement devuelve un array con la estrategia seleccionada y el movimiento para pacman
				ArrayList trainingFunction_Result = this.getNextMovement(game, trainingFunctionOutputs);
				return (MOVE)trainingFunction_Result.get(1);
			}
		}
		
	}
	
	/**
	 * Procesa los valores de salida pasados por par�metro y
	 * devuelve la estrategia seleccionada y el movimiento pr�ximo para Pacman 
	 * @param game
	 * @param _outputs El array de valores de salida ofrecidos por la red neuronal
	 *  o directamente por la funci�n de entrenamiento.
	 * @return Un arraylist con la estrategia seleccionada para pacman y el movimiento correspondiente en funci�n de los valores de salida pasados por par�metro.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList getNextMovement( Game game, ArrayList<Float> _outputs )
	{
		ArrayList outputs = new ArrayList<>();
		
		float ETG1 = _outputs.get(0);
		float absETG1 = Math.abs(ETG1);
		float ETG2 = _outputs.get(1);
		float absETG2 = Math.abs(ETG2);
		float ETG3 = _outputs.get(2);
		float absETG3 = Math.abs(ETG3);
		float ETG4 = _outputs.get(3);
		float absETG4 = Math.abs(ETG4);
		float ETG5 = _outputs.get(4);
		float absETG5 = Math.abs(ETG5);
	
		
		if( absETG1 > absETG2 && absETG1 > absETG3 && absETG1 > absETG4 && absETG1 > absETG5 )
		{
			if( ETG1 > 0 )
			{
				outputs.add(StrategyType.ETG11);
				outputs.add(escapeFromGhost( game, GHOST.BLINKY ));
				return outputs;
			}
			else
			{
				outputs.add(StrategyType.ETG12);
				outputs.add(catchGhost( game, GHOST.BLINKY ));
				return outputs;
			}
		} else if( absETG2 > absETG3 && absETG2 > absETG4 && absETG2 > absETG5 )
		{
			if( ETG2 > 0 )
			{
				outputs.add(StrategyType.ETG21);
				outputs.add(escapeFromGhost( game, GHOST.PINKY ));
				return outputs;
			}
			else
			{
				outputs.add(StrategyType.ETG22);
				outputs.add(catchGhost( game, GHOST.PINKY ));
				return outputs;
			}
		} else if( absETG3 > absETG4 && absETG3 > absETG5 )
		{
			if( ETG3 > 0 )
			{
				outputs.add(StrategyType.ETG31);
				outputs.add(escapeFromGhost( game, GHOST.INKY ));
				return outputs;
			}
			else
			{
				outputs.add(StrategyType.ETG32);
				outputs.add(catchGhost( game, GHOST.INKY ));
				return outputs;
			}
		} else if( absETG4 > absETG5 )
		{
			if( ETG4 > 0 )
			{
				outputs.add(StrategyType.ETG41);
				outputs.add(escapeFromGhost( game, GHOST.SUE ));
				return outputs;
			}
			else
			{
				outputs.add(StrategyType.ETG42);
				outputs.add(catchGhost( game, GHOST.SUE ));
				return outputs;
			}
		} else
		{
			outputs.add(StrategyType.ETG5);
			outputs.add(goToCloserPill( game ));
			return outputs;
		}
	}
	
	/**
	 * Devuelve el momvimiento necesario para ir a por la p�ldora m�s cercana ( normal o super )
	 * @param game El estado del juego
	 * @return El movimiento requerido ( tipo MOVE ) 
	 */
	private MOVE goToCloserPill( Game game )
	{
		//Strategy 5: go after the pills and power pills
		int[] pills=game.getPillIndices();
		int[] powerPills=game.getPowerPillIndices();		
		
		ArrayList<Integer> targets=new ArrayList<Integer>();
		
		for(int i=0;i<pills.length;i++)					//check which pills are available			
			if(game.isPillStillAvailable(i))
				targets.add(pills[i]);
		
		for(int i=0;i<powerPills.length;i++)			//check with power pills are available
			if(game.isPowerPillStillAvailable(i))
				targets.add(powerPills[i]);				
		
		int[] targetsArray=new int[targets.size()];		//convert from ArrayList to array
		
		for(int i=0;i<targetsArray.length;i++)
			targetsArray[i]=targets.get(i);
		
		int current=game.getPacmanCurrentNodeIndex();
		//return the next direction once the closest target has been identified
		return game.getNextMoveTowardsTarget(current,game.getClosestNodeIndexFromNodeIndex(current,targetsArray,DM.PATH),DM.PATH);
	}
	
	/**
	 * Devuelve el movimiento para escapar de un determinado fantasma
	 * @param game El estado del juego
	 * @param ghostType El fantasma del que trata de escapar
	 * @return El movimiento requerido ( tipo MOVE ) 
	 */
	private MOVE escapeFromGhost( Game game, GHOST ghostType )
	{
		return game.getNextMoveAwayFromTarget(game.getPacmanCurrentNodeIndex(),game.getGhostCurrentNodeIndex(ghostType),DM.PATH);
	}
	
	/**
	 * Devuelve el movimieneto para ir a por un determinado fantasma
	 * @param game El estado del juego
	 * @param ghostType El fantasma del que trata de escapar
	 * @return El movimiento requerido ( tipo MOVE )
	 */
	private MOVE catchGhost( Game game, GHOST ghostType )
	{
		return game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(),game.getGhostCurrentNodeIndex(ghostType),DM.PATH);
	}
	
	/**
	 * Used to normalize ghost's edible time. Done via min-max normalization.
	 * Supposes that minimum possible time is 0. Supposes that the maximum possible
	 * time is 200 ( this.maximumEdibleTime )
	 * Every level decreases this time in 0.9 seconds. Esto no lo tenemos en cuenta en esta pr�ctica
	 * 
	 * @param time - Time to be normalized
	 * @return Normalized distance
	 */
	public float normalizeEdibleTime(float time) 
	{
		// Si se pasa habr� que reajustar el maximo del tiempo posible
		if( time > this.maximumEdibleTime )
		{
			//System.out.println("time = " + time);
			//this.maximumEdibleTime = time;
			return 1.f;
		}
		if( time <= 0 ) return 0.f;
		return ((time - 0) / (float) (this.maximumEdibleTime - 0)) * (1 - 0) + 0;
	}

	/**
	 * Used to normalize distances a un valor entre [0,1]. Done via min-max normalization. 
	 * Supposes that minimum possible distance is 0. Supposes that the maximum possible
	 * distance is 95 ( this.maximumDistance ) 
	 * Si es -1 se devuelve el m�ximo normalizado, en este caso 1.
	 * 
	 * @param dist - Distance to be normalized
	 * @return Normalized distance
	 */
	public float normalizeDistance(int dist) 
	{
		// Si se pasa habr� que reajustar el maximo de la distancia posible
		if( dist > this.maximumDistance )
		{
			//System.out.println("dist = " + dist);
			//this.maximumDistance = dist;
			return 1.f;
		}
		if( dist < 0 ) return 1.f;
		return ((dist - 0) / (float) (this.maximumDistance - 0)) * (1 - 0) + 0;
	}
	


}
