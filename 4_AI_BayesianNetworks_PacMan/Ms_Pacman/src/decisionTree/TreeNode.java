package decisionTree;


/**
 * Los nodos con los que componemos el �rbol de Decisi�n
 * Cada treeNode puede ser a su vez o un Decisi�nNode o un LeafNode
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class TreeNode 
{
	// Atributos 
	
	// Nombre del nodo ( si es una hoja se llamar� "Leaf" )
	public String name;		
	// Nombre de la rama hacia el padre
	public String parentBranch;	
	
	/**
     * Constructor para la clase TreeNode 
     * @param _name El identificador del nodo de tipo String
     */
	public TreeNode(String _name) 
	{
		// Inicializamos los atributo
		this.name = _name;
		this.parentBranch = "";
	}// Fin del constructor 
	
	/**
     * Constructor para la clase TreeNode 
     * @param _name El identificador del nodo de tipo String
     * @param _parentBranch El nombre de la rama con la que se conecta a su nodo padre
     */
	public TreeNode(String _name, String _parentBranch) 
	{
		// Inicializamos los atributo
		this.name = _name;
		this.parentBranch = _parentBranch;
	}// Fin del constructor 
}
