package decisionTree;

import pacman.game.Constants.MOVE;

/**
 * Los nodos hoja con los que componemos el �rbol de Decisi�n
 * Guarda la direcci�n de movimiento y hereda de TreeNode
 * @author: Miguel �ngel Torres y Javier Barreiro Portela
 * @version: 21/10/2014/A
 */
public class LeafNode extends TreeNode 
{
	// Atributos 
	
	/**
	 * La direcci�n de movimiento
	 */
	public MOVE move;		
	
	/**
     * Constructor para la clase LeafNode 
     */
	public LeafNode() 
	{
		// Inicializamos los atributo del padre
		super("Leaf");
	}// Fin del constructor 
	
	/**
     * Constructor para la clase LeafNode 
     * @param _move La direcci�n de movimiento que devuelve este nodo hoja
     */
	public LeafNode(MOVE _move) 
	{
		// Inicializamos los atributo del padre
		super("Leaf");
		move = _move;
	}// Fin del constructor 
	
	/**
     * Constructor para la clase LeafNode
     * @param _parentBranch El nombre de la rama con la que se conecta a su nodo padre
     * @param _move La direcci�n de movimiento que devuelve este nodo hoja
     */
	public LeafNode(String _parentBranch, MOVE _move)
	{
		// Inicializamos los atributo del padre
		super("Leaf", _parentBranch);
		move = _move;
	}// Fin del constructor 

}
