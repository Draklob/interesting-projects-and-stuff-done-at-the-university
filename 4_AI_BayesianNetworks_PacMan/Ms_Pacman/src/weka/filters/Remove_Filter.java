package weka.filters;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 * Encapsula la construcci�n de un filtro para eliminar los atributos que no aportan informaci�n relativa
 * 
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version 24/05/2015
 *
 */
public class Remove_Filter extends Abstract_Filter 
{

	Remove remove;
	
	public Remove_Filter( String[] elementsId )
	{
		String[] options = new String[ elementsId.length + 1 ];
		options[0] = "-R";
		
		int index = 1;
		for( String element : elementsId )
		{
			options[index] = element;
			index++;
		}
		
		remove = new Remove();
		try {
			remove.setOptions( options );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	/**
	 * Aplicar el filtro remove anteriormente construido
	 * @param data El datase sobre el que se desea aplicar el filtro
	 */
	public Instances applyFilter( Instances data) {
		
		Instances newData = null;
		
		try {
			remove.setInputFormat( data );
			newData = Filter.useFilter( data, remove );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newData;
	}

}
