package weka.filters;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;;

/**
 * Encapsula la construcci�n de un filtro para discretizar los valores de los atributos
 * 
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version 24/05/2015
 *
 */
public class Discretize_Filter 
{
	/**
	 * Se guarda una referencia al filtro creado para luego poderlo rehusar con los diferentes dataset
	 */
	public Discretize discretize;
	
	/**
	 * Constructor del filtro
	 * @param indices Los �ndices de los atributos que se desean discretizar
	 * ( en principio se discretizan todos, aunque los que son nominales se supone que esto no le afecta )
	 * @param ingoreClass Si es True no discretiza el atributo seleccionado como clase ( el que se usa para clasificar )
	 * @param invert Si es true aplica el filtro sobre los atributos no indicados por los �ndices ( en principio siempre a false )
	 * @param data El dataset que sirve como base para configurar el filtro
	 */
	public Discretize_Filter( String indices, Boolean ingoreClass, Boolean invert, Instances data )
	{
		discretize = new Discretize();
		
		discretize.setAttributeIndices( indices );
		discretize.setIgnoreClass( ingoreClass );
		discretize.setInvertSelection( invert );
		
		try {
			discretize.setInputFormat( data );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Aplica el filtro sobre un dataset determinado
	 * Una vez construido, con este m�todo se puede aplicar tanto para el
	 * trainingData usado para construir la red bayesiana, como para el testingData usado
	 * para el crossvalidation, o tambi�n para discretizar el dataSet de una sola instancia usado para obtener
	 * en tiempo de juego el movimiento de Pacman analizando el objeto proporcionado game
	 * @param data El dataset sobre el que se desea aplicar el filtro
	 * @return Un dataset copia del original con el filtro aplicado
	 */
	public Instances applyFilter( Instances data) 
	{
		
		Instances newData = null;
		
		try {
			newData = Filter.useFilter( data, discretize );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newData;
	}

	
}
