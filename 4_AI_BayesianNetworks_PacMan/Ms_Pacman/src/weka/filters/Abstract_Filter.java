package weka.filters;

import weka.core.Instances;
import weka.filters.Filter;

/**
 * Clase abstracta que permite usar polimorfismo con los filtros
 * 
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version 24/05/2015
 *
 */
public abstract class Abstract_Filter 
{
	/**
	 * Los tipos de filtro que se incluyen en esta demo
	 */
	public enum filter_Type { Remove, Discretize, ReplaceMissingValues };
	
	/**
	 * Se guarda una referencia al filtro configurado para poderlo aplicar a diferentes datasets
	 */
	Filter filter = null;
	
	/**
	 * M�todo que permite aplicar el filtro a un dataset determinado
	 * @param data El dataset sobre el que se desea aplicar el filtro
	 * @return Un dataset copia del original con el filtro aplicado
	 */
	public abstract  Instances applyFilter( Instances data );
}
