package weka.algorithms;

import weka.classifiers.bayes.net.search.SearchAlgorithm;
import weka.classifiers.bayes.net.search.local.K2;
import weka.classifiers.bayes.net.search.local.LocalScoreSearchAlgorithm;
import weka.core.SelectedTag;

/**
 * En esta clase se encapsula la creaci�n de algoritmos locales de b�squeda 
 * para la creaci�n de la estructura de las redes bayesianas
 * 
 * @author Javier Barreiro Portela, Miguel �ngel Torres
 * @version 24/05/2015
 */
public class Searching_Algorithm_Local 
{
	/**
	 * Indican diferentes formas de evaluar las redes bayesianas generadas
	 */
	public enum ScoreType { BAYES, BDEU, MDL, ENTROPY, AIC };
	
	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda de tipo k2
	 * 
	 * @param parents N�mero de padres que tendr� cada nodo de la red bayesiana generada
	 * Si se inicia como una Naive Bayes y el n�mero de padres que se indica es 1, se construir� una 
	 * Naive Bayes Net.
	 * Cuando se establece en 2, se genera un Tree Augmented Bayes Network (TAN)
	 * Cuando se establece mayor que 2, se genera un Bayes Net Augmented Bayes Network (BAN)
	 * Si el n�mero de padres es mayor que el n�mero de nodos no se establece ninguna restricci�n al n�mero de dependencias.
	 * 
	 * @param naiveBayes A True inicia la estructura sin ninguna dependencia o a False 
	 * con una dependencia simple de todos los nodos respecto al nodo clasificador
	 * 
	 * @param markov A True aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador
	 * 
	 * @param order Si es True se establece un orden en los nodos aleatorio, si es False se mantiene el mismo
	 * orden que ten�an los nodos en el dataset. En cualquier caso, si se inicia como Naive Bayes, el primero nodo 
	 * ser� el nodo clasificador.
	 * 
	 * @param scoreType Determina la medida para juzgar la calidad de una estructura generada.
	 * Puede ser Bayes, BDeu, Minimum Description Length (MDL), Akaike Information Criterion (AIC), and Entropy.
	 *
	 * @return El algoritmo construido de tipo k2
	 */
	static public SearchAlgorithm get_K2( int parents, Boolean naiveBayes, Boolean markov, Boolean order, ScoreType scoreType  )
	{
		K2 k2 = new K2();
		
		k2.setMaxNrOfParents( parents );
		k2.setInitAsNaiveBayes( naiveBayes );
		k2.setMarkovBlanketClassifier( markov );
		k2.setRandomOrder( order );
		k2.setScoreType( new SelectedTag( scoreType.ordinal(), LocalScoreSearchAlgorithm.TAGS_SCORE_TYPE ) );
		
		return k2;
	}
}