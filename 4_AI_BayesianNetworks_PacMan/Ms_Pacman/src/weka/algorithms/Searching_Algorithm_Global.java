package weka.algorithms;

import weka.classifiers.bayes.net.search.SearchAlgorithm;
import weka.classifiers.bayes.net.search.global.GlobalScoreSearchAlgorithm;
import weka.classifiers.bayes.net.search.global.HillClimber;
import weka.classifiers.bayes.net.search.global.K2;
import weka.classifiers.bayes.net.search.global.RepeatedHillClimber;
import weka.classifiers.bayes.net.search.global.SimulatedAnnealing;
import weka.classifiers.bayes.net.search.global.TAN;
import weka.classifiers.bayes.net.search.global.TabuSearch;

import weka.core.SelectedTag;

/**
 * En esta clase se encapsula la creaci�n de algoritmos globales de b�squeda 
 * para la creaci�n de la estructura de las redes bayesianas
 * 
 * @author Javier Barreiro Portela, Miguel �ngel Torres
 * @version 24/05/2015
 */
public class Searching_Algorithm_Global 
{
	
	/**
	 * Indican diferentes formas de evaluar las redes bayesianas generadas
	 */
	public enum CVType { LOO_CV, k_Fold_CV, Cumulative_CV };
	

	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda global de tipo Hill Climber.
	 * 
	 * @param cvType : Selecciona la estrategia de validaci�n cruzada que va usar en la b�squeda de redes.
	 * LOO-CV : Deja una fuera de la validaci�n cruzada.
	 * k-Fold-CV : "k" pliegues de validaci�n cruzada.
	 * Cumulative-CV : Validaci�n cruzada cumulativa.
	 * 
	 * @param naiveBayes : A "True" inicia la estructura sin ninguna dependencia o a "False" 
	 * con una dependencia simple de todos los nodos respecto al nodo clasificador.
	 * 
	 * @param markov : A "True" aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador.
	 * 
	 * @param parents : N�mero de padres que tendr� cada nodo de la red bayesiana generada.
	 * Si se inicia como una Naive Bayes y el n�mero de padres que se indica es 1, se construye una Naive Bayes Net.
	 * Cuando se establece en 2, se genera un Tree Augmented Bayes Network (TAN).
	 * Cuando se establece mayor que 2, se genera un Bayes Net Augmented Bayes Network (BAN).
	 * Si el n�mero de padres es mayor que el n�mero de nodos no se establece ninguna restricci�n al n�mero de dependencias.
	 * 
	 * @param arcReversal : A "True" la operaci�n de reversi�n de arco es utilizada en la b�squeda.
	 * 
	 * @param useProb : A "True" la probabilidad de la clase se devuelve en la estimaci�n de la precisi�n.
	 * A "False" la estimaci�n de precisi�n es s�lo incrementanda si el clasificador devuelve exactamente la clase correcta.
	 * 
	 * @return : El algoritmo de b�squeda global construido de tipo Hill Climber.
	 */
	static public SearchAlgorithm get_HillClimber( CVType cvType, Boolean naiveBayes, Boolean markov, int parents, Boolean arcReversal, Boolean useProb )
	{
		HillClimber hc = new HillClimber();
		
		hc.setCVType( new SelectedTag( cvType.ordinal(), GlobalScoreSearchAlgorithm.TAGS_CV_TYPE ));
		hc.setInitAsNaiveBayes( naiveBayes );
		hc.setMarkovBlanketClassifier( markov );
		hc.setMaxNrOfParents( parents );
		hc.setUseArcReversal( arcReversal );
		hc.setUseProb( useProb );
		
		return hc;
	}
	
	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda global de tipo K2.
	 * 
	 * @param parents : N�mero de padres que tendr� cada nodo de la red bayesiana generada.
	 * Si se inicia como una Naive Bayes y el n�mero de padres que se indica es 1, se construye una Naive Bayes Net.
	 * Cuando se establece en 2, se genera un Tree Augmented Bayes Network (TAN).
	 * Cuando se establece mayor que 2, se genera un Bayes Net Augmented Bayes Network (BAN).
	 * Si el n�mero de padres es mayor que el n�mero de nodos no se establece ninguna restricci�n al n�mero de dependencias.
	 * 
	 * @param naiveBayes : A "True" inicia la estructura sin ninguna dependencia o a "False" 
	 * con una dependencia simple de todos los nodos respecto al nodo clasificador.
	 * 
	 * @param markov : A "True" aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador.
	 * 
	 * @param order : Si es "True" se establece un orden en los nodos aleatorio, si es "False" se mantiene el mismo
	 * orden que ten�an los nodos en el dataset. En cualquier caso, si se inicia como Naive Bayes, el primero nodo.
	 * 
	 * @param useProb : A "True" la probabilidad de la clase se devuelve en la estimaci�n de la precisi�n.
	 * A "False" la estimaci�n de precisi�n es s�lo incrementanda si el clasificador devuelve exactamente la clase correcta.
	 * 
	 * @param cvType : Selecciona la estrategia de validaci�n cruzada que va usar en la b�squeda de redes.
	 * LOO-CV : Deja una fuera de la validaci�n cruzada.
	 * k-Fold-CV : "k" pliegues de validaci�n cruzada.
	 * Cumulative-CV : Validaci�n cruzada cumulativa.
	 * 
	 * @return : El algoritmo de b�squeda global construido de tipo K2.
	 */
	static public SearchAlgorithm get_K2( int parents, Boolean naiveBayes, Boolean markov, Boolean order, Boolean useProb, CVType cvType )
	{
		K2 k2 = new K2();
		
		k2.setMaxNrOfParents( parents );
		k2.setInitAsNaiveBayes( naiveBayes );
		k2.setMarkovBlanketClassifier( markov );
		k2.setRandomOrder( order );
		k2.setUseProb( useProb );
		k2.setCVType( new SelectedTag( cvType.ordinal(), GlobalScoreSearchAlgorithm.TAGS_CV_TYPE ) );
		
		return k2;
	}
	
	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda global de tipo Repeated Hill Climber.
	 * 
	 * @param cvType : Selecciona la estrategia de validaci�n cruzada que va usar en la b�squeda de redes.
	 * LOO-CV : Deja una fuera de la validaci�n cruzada.
	 * k-Fold-CV : "k" pliegues de validaci�n cruzada.
	 * Cumulative-CV : Validaci�n cruzada cumulativa.
	 * 
	 * @param naiveBayes : A "True" inicia la estructura sin ninguna dependencia o a "False" 
	 * con una dependencia simple de todos los nodos respecto al nodo clasificador.
	 * 
	 * @param markov : A "True" aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador.
	 * 
	 * @param parents : N�mero de padres que tendr� cada nodo de la red bayesiana generada.
	 * Si se inicia como una Naive Bayes y el n�mero de padres que se indica es 1, se construye una Naive Bayes Net.
	 * Cuando se establece en 2, se genera un Tree Augmented Bayes Network (TAN).
	 * Cuando se establece mayor que 2, se genera un Bayes Net Augmented Bayes Network (BAN).
	 * Si el n�mero de padres es mayor que el n�mero de nodos no se establece ninguna restricci�n al n�mero de dependencias.
	 * 
	 * @param runs : Establece el n�mero de veces que se ejecuta el algoritmo.
	 * 
	 * @param seed : Valor de inicializaci�n para el generador de n�meros aleatorios.
	 * Establecer la semilla permite replicabilidad de experimentos.
	 * 
	 * @param arcReversal : A "True" la operaci�n de reversi�n de arco es utilizada en la b�squeda.
	 * 
	 * @param useProb : A "True" la probabilidad de la clase se devuelve en la estimaci�n de la precisi�n.
	 * A "False" la estimaci�n de precisi�n es s�lo incrementanda si el clasificador devuelve exactamente la clase correcta.
	 * 
	 * @return El algoritmo de b�squeda global construido de tipo Repeated Hill Climber.
	 */
	static public SearchAlgorithm get_RepeatedHillClimber( CVType cvType, Boolean naiveBayes, Boolean markov, int parents, int runs, int seed, Boolean arcReversal, Boolean useProb )
	{
		RepeatedHillClimber rhc = new RepeatedHillClimber();
		
		rhc.setCVType( new SelectedTag( cvType.ordinal(), GlobalScoreSearchAlgorithm.TAGS_CV_TYPE ));
		rhc.setInitAsNaiveBayes( naiveBayes );
		rhc.setMarkovBlanketClassifier( markov );
		rhc.setMaxNrOfParents( parents );
		rhc.setRuns( runs );
		rhc.setSeed( seed );
		rhc.setUseArcReversal( arcReversal );
		rhc.setUseProb( useProb );
		
		return rhc;
	}
	
	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda global de tipo Simulated Annealing.
	 * 
	 * @param cvType : Selecciona la estrategia de validaci�n cruzada que va usar en la b�squeda de redes.
	 * LOO-CV : Deja una fuera de la validaci�n cruzada.
	 * k-Fold-CV : "k" pliegues de validaci�n cruzada.
	 * Cumulative-CV : Validaci�n cruzada cumulativa.
	 * 
	 * @param temp : Ajusta la temperatura inicial del algoritmo. La temperatura inicial determina
	 * la probabilidad de que un paso en la direcci�n "equivocada" en el espacio de b�squeda se acepte.
	 * Cuanto mayor sea la temperatura, mayor ser� la probabilidad de aceptaci�n.
	 * 
	 * @param delta : Establece el factor con el que la temperatura se reduce en cada iteraci�n.
	 * 
	 * @param markov : A "True" aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador.
	 * 
	 * @param runs : Establece el n�mero de veces que se ejecuta el algoritmo.
	 * 
	 * @param seed : Valor de inicializaci�n para el generador de n�meros aleatorios.
	 * Establecer la semilla permite replicabilidad de experimentos.
	 * 
	 * @param useProb : A "True" la probabilidad de la clase se devuelve en la estimaci�n de la precisi�n.
	 * A "False" la estimaci�n de precisi�n es s�lo incrementanda si el clasificador devuelve exactamente la clase correcta.
	 * 
	 * @return El algoritmo de b�squeda global construido de tipo Simulate Annealing.
	 */
	static public SearchAlgorithm get_SimulatedAnnealing( CVType cvType, Double temp, Double delta, Boolean markov, int runs, int seed, Boolean useProb )
	{
		SimulatedAnnealing sA = new SimulatedAnnealing();
		
		sA.setCVType( new SelectedTag( cvType.ordinal(), GlobalScoreSearchAlgorithm.TAGS_CV_TYPE ));
		sA.setTStart( temp );
		sA.setDelta( delta);
		sA.setMarkovBlanketClassifier( markov );
		sA.setRuns( runs );
		sA.setSeed( seed);
		sA.setUseProb( useProb );
		
		return sA;
	}
	
	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda global de tipo Tabu Search.
	 * 
	 * @param cvType : Selecciona la estrategia de validaci�n cruzada que va usar en la b�squeda de redes.
	 * LOO-CV : Deja una fuera de la validaci�n cruzada.
	 * k-Fold-CV : "k" pliegues de validaci�n cruzada.
	 * Cumulative-CV : Validaci�n cruzada cumulativa.
	 * 
	 * @param naiveBayes : A "True" inicia la estructura sin ninguna dependencia o a "False" 
	 * con una dependencia simple de todos los nodos respecto al nodo clasificador.
	 * 
	 * @param markov : A "True" aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador.
	 * 
	 * @param parents : N�mero de padres que tendr� cada nodo de la red bayesiana generada.
	 * Si se inicia como una Naive Bayes y el n�mero de padres que se indica es 1, se construye una Naive Bayes Net.
	 * Cuando se establece en 2, se genera un Tree Augmented Bayes Network (TAN).
	 * Cuando se establece mayor que 2, se genera un Bayes Net Augmented Bayes Network (BAN).
	 * Si el n�mero de padres es mayor que el n�mero de nodos no se establece ninguna restricci�n al n�mero de dependencias.
	 * 
	 * @param runs : Establece el n�mero de veces que se ejecuta el algoritmo.
	 * 
	 * @param list : Establece la longitud de la lista de tabu.
	 * 
	 * @param arcReversal : A "True" la operaci�n de reversi�n de arco es utilizada en la b�squeda.
	 * 
	 * @param useProb : A "True" la probabilidad de la clase se devuelve en la estimaci�n de la precisi�n.
	 * A "False" la estimaci�n de precisi�n es s�lo incrementanda si el clasificador devuelve exactamente la clase correcta.
	 * 
	 * @return El algoritmo de b�squeda global construido de tipo Tabu Search.
	 */
	static public SearchAlgorithm get_TabuSearch( CVType cvType, Boolean naiveBayes, Boolean markov, int parents, int runs, int list, Boolean arcReversal, Boolean useProb )
	{
		TabuSearch ts = new TabuSearch();
		
		ts.setCVType( new SelectedTag( cvType.ordinal(), GlobalScoreSearchAlgorithm.TAGS_CV_TYPE ));
		ts.setInitAsNaiveBayes( naiveBayes );
		ts.setMarkovBlanketClassifier( markov );
		ts.setMaxNrOfParents( parents );
		ts.setRuns( runs );
		ts.setTabuList( list );
		ts.setUseArcReversal( arcReversal );
		ts.setUseProb( useProb );
		
		return ts;
	}
	
	/**
	 * Construcci�n de un objeto para realizar un algoritmo de b�squeda global de tipo TAN.
	 * 
	 * @param markov : A "True" aplica una correcci�n sobre la estructura creada para 
	 * asegurarse de que todos los nodos pertenecen al manto de markov del nodo clasificador.
	 * 
	 * @param cvType : Selecciona la estrategia de validaci�n cruzada que va usar en la b�squeda de redes.
	 * LOO-CV : Deja una fuera de la validaci�n cruzada.
	 * k-Fold-CV : "k" pliegues de validaci�n cruzada.
	 * Cumulative-CV : Validaci�n cruzada cumulativa.
	 * 
	 * @return El algoritmo de b�squeda global construido de tipo TAN.
	 */
	static public SearchAlgorithm get_TAN( Boolean markov, CVType cvType)
	{
		TAN tan = new TAN();
		
		tan.setMarkovBlanketClassifier( markov );
		tan.setCVType( new SelectedTag( cvType.ordinal(), GlobalScoreSearchAlgorithm.TAGS_CV_TYPE ));
		
		return tan;
	}
}