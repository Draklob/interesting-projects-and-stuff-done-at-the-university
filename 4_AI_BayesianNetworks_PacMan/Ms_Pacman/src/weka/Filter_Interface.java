package weka;

import weka.core.Instances;
import weka.filters.Filter;

public interface Filter_Interface {

	public enum filter_Type { Remove, Discretize, ReplaceMissingValues };
	
	Filter filter = null;
	
	public Instances applyFilter( Instances data );
	
}
