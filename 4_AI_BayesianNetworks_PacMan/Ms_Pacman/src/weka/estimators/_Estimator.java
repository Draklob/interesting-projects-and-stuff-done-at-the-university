package weka.estimators;

import weka.classifiers.bayes.net.estimate.BMAEstimator;
import weka.classifiers.bayes.net.estimate.BayesNetEstimator;
import weka.classifiers.bayes.net.estimate.MultiNomialBMAEstimator;
import weka.classifiers.bayes.net.estimate.SimpleEstimator;

/**
 * Encapsula la construcci�n de un estimador para establecer la forma de calcular las tablas de probabilidad 
 * de las redes bayesianas generadas.
 * 
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version 24/05/2015
 *
 */
public class _Estimator 
{
	/**
	 * Construye y devuelve un estimador de tipo BNE ( BayesNet Estimator )
	 * @param alpha La probabilidad inicial de los diferentes valores 
	 * de cada atributo al comenzar el c�lculo de las tablas de probabilidad
	 * @return estimador de tipo BNE
	 */
	static public BayesNetEstimator get_BNE( Double alpha )
	{
		BayesNetEstimator bne = new BayesNetEstimator();
		bne.setAlpha( alpha );;
		
		return bne;
	}
	
	/**
	 * Construye y devuelve un estimador de tipo BMAE ( Bayes Model Averaging Estimator )
	 * @param alpha La probabilidad inicial de los diferentes valores 
	 * de cada atributo al comenzar el c�lculo de las tablas de probabilidad
	 * @param k2prior Parametro sin determinar
	 * @return estimador de tipo BMAE
	 */
	static public BayesNetEstimator get_BMAE( Double alpha, Boolean k2prior )
	{
	  BMAEstimator bmae = new BMAEstimator();
	  
	  bmae.setAlpha( alpha );
	  bmae.setUseK2Prior( k2prior );
	  
	  return bmae;
	}
	
	/**
	 * Construye y devuelve un estimador de tipo MNMBAE ( Multinomial BMA Estimator )
	 * @param alpha La probabilidad inicial de los diferentes valores 
	 * de cada atributo al comenzar el c�lculo de las tablas de probabilidad
	 * @param k2prior Par�metro sin determinar
	 * @return estimador de tipo MNMBAE
	 */
	static public BayesNetEstimator get_MNMBAE( Double alpha, Boolean k2prior )
	{
		MultiNomialBMAEstimator mnmbae = new MultiNomialBMAEstimator();
		mnmbae.setAlpha( alpha );
		mnmbae.setUseK2Prior( k2prior );
		
		return mnmbae;
	}
	
	/**
	 * Construye y devuelve un estimador de tipo SE ( Simple Estimator )
	 * @param alpha La probabilidad inicial de los diferentes valores 
	 * de cada atributo al comenzar el c�lculo de las tablas de probabilidad
	 * @return estimador de tipo SE
	 */
	static public BayesNetEstimator get_SE( Double alpha )
	{
		SimpleEstimator se = new SimpleEstimator();
		se.setAlpha( alpha );
		
		return se;
	}
}
