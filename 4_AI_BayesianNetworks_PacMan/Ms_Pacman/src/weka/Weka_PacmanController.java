package weka;

import pacman.controllers.Controller;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;
import pacman.game.Game;
import weka.core.Instances;

/**
 * Controlador de Pacman que hace uso de una red bayesiana generada usando Weka.
 * 
 * @author Javier Barreiro Portela, Miguel �ngel Torres
 * @version 24/05/2015
 */
public class Weka_PacmanController extends Controller<MOVE>
{
	/**
	 * Objeto de tipo Bayesian_Network.
	 */
	private Bayesian_Network nb;
	
	/**
	 * Los atributos seleccionados son 
	 * 1 - La clase - DirectionChosen
	 * 2 - isBlinkyEdible
	 * 3 - isInkyEdible
	 * 4 - isPinkyEdible
	 * 5 - isSueEdible
	 * 6 - blinkyDist
	 * 7 - inkyDist
	 * 8 - pinkyDist
	 * 9 - sueDist
	 * 10 - dirToNearerPill
	 * 11 - minPillsDistance
	 * 12 - dirToNearerPowerPill
	 * 13 - minPowerPillsDistance
	 * 14 - dirAwayFromClosestGhost
	 */
	
	/**
	 * El data set con una sola instancia que se sobreescribe en cada ciclo
	 * con los valores del estado del juego
	 * Este dataset no se discretiza en ning�n momento para poderle pasar valores  
	 * a los atributos de tipo NUMERIC
	 */
	private Instances oneInstanceData;
	
	/**
	 * Indica el modo en el que se ejecuta el controlador.
	 */
	public enum Mode { Eval, Game };
	
	/**
	 * Constructor
	 * @param _mode Si el juego se ejecuta en modo evaluacion o en modo juego.
	 */
	public Weka_PacmanController( Mode _mode)
	{
		if( _mode == Mode.Eval )
		{
			nb = new Bayesian_Network( "myData/trainingData_8000.arff", "myData/testingData_5100.arff" );
		}
		else if( _mode == Mode.Game )
		{
			nb = new Bayesian_Network( "myData/trainingData_8000.arff", "" );
		}
		
		// Se usa un data set con una sola instancia para ir clasificando los valores del juego
		// Esto es necesario para poder discretizar estos valores, ya que no sabemos discretizar instancias sueltas
		// Pero inicialmente tan solo se aplica el filtro remove
		// No se discretiza para poderle pasar valores a los atributos de tipo NUMERIC
		
		oneInstanceData = nb.get_instances ( nb.readFile( "myData/oneInstanceData.arff" ) );
		oneInstanceData.setClassIndex(0);
		oneInstanceData = nb.get_remove_filter().applyFilter( oneInstanceData );
	}
	
	@Override
	/**
	 * Implementa el m�todo necesario en cualquier controlador de MsPacman
	 * Examina el objeto game para crear una instancia que pueda ser clasificada usando
	 * la red bayesiana creada y poder as� devolver un movimiento para Pacman
	 * @param game El objeto de tipo Game que sirve para extraer los datos en juego en cada instante
	 * @param timeDue El delta time transcurrido entre cada loop del juego
	 */
	public MOVE getMove(Game game, long timeDue) 
	{
	
		int index = 1;
		
		
		// PacmanPosition
	//	oneInstanceData.instance(0).setValue(index, game.getPacmanCurrentNodeIndex());
	//	index++;
		
		/*
		
		// CurrentScore
		oneInstanceData.instance(0).setValue(index, game.getScore());
		index++;
		
		// TotalGameTime
		oneInstanceData.instance(0).setValue(index, game.getTotalTime());
		index++;
		
		// CurrentLevelTime
		oneInstanceData.instance(0).setValue(index, game.getCurrentLevelTime());
		index++;
		
		*/
		
		// Se calcula si los fantasmas son comestibles o no
		
		for(GHOST ghostType : GHOST.values())
		{
			oneInstanceData.instance(0).setValue(index, String.valueOf( game.isGhostEdible(ghostType) ) );
			
			index++;
		}
		
		// Se calcula la distancia a cada fantasma
		
		int minDist = 500; // Lo usamos para calcular la direcci�n para escapar del fantasma m�s cercano
		GHOST nearerGhost = null;
		for(GHOST ghostType : GHOST.values())
		{
			int dist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(ghostType));
			
			if( dist <= minDist )
			{
				minDist = dist;
				nearerGhost = ghostType;
			}
			
			oneInstanceData.instance(0).setValue(index, dist );
			
			index++;
		}
		
		/*
		// Direcci�n de los enemigos.
		for( int enemy = 0; enemy < 4; enemy++ )
		{
			oneInstanceData.instance(0).setValue(index, String.valueOf( ( game.getNextMoveTowardsTarget( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex( GHOST.values()[enemy] ), DM.PATH) ) ).toString() );
			index++;
		}
		*/
		
		// HASTA AQU�
		
		// La distancia a la PILL m�s cercana y la direcci�n para llegar a ella
		
		boolean flag = false;
		int minPillsDistance = 500;
		MOVE dirToNearerPill = MOVE.NEUTRAL;
		for( int nodeIndex : game.getActivePillsIndices())
		{
			int auxDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), nodeIndex );
			if( flag == false )
			{
				flag = true;
				minPillsDistance = auxDist;
				dirToNearerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}
			else if( auxDist < minPillsDistance )
			{
				minPillsDistance = auxDist;
				dirToNearerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}	
		}
		
		// Se establece la direcci�n que tiene que seguir Pacman para ir a la p�ldora ( normal ) m�s cercana
		oneInstanceData.instance(0).setValue(index, String.valueOf( dirToNearerPill.toString() ));
		
		index++;
		
		// Se establece la distancia que tiene que seguir Pacman para ir a la p�ldora ( normal ) m�s cercana
		oneInstanceData.instance(0).setValue(index, minPillsDistance );
		
		index++;
		
		// La distancia a la POWER PILL m�s cercana y la direcci�n para llegar a ella
		
		flag = false;
		int minPowerPillsDistance = 500;
		MOVE dirToNearerPowerPill = MOVE.NEUTRAL;
		for( int nodeIndex : game.getActivePowerPillsIndices() )
		{
			int auxDist = game.getShortestPathDistance(game.getPacmanCurrentNodeIndex(), nodeIndex );
			if( flag == false )
			{
				flag = true;
				minPowerPillsDistance = auxDist;
				dirToNearerPowerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}
			else if( auxDist < minPowerPillsDistance )
			{
				minPowerPillsDistance = auxDist;
				dirToNearerPowerPill = game.getNextMoveTowardsTarget(game.getPacmanCurrentNodeIndex(), nodeIndex, DM.PATH);
			}	
		}
		
		// Se establece la direcci�n que tiene que seguir Pacman para ir a la p�ldora ( normal ) m�s cercana
		oneInstanceData.instance(0).setValue(index, String.valueOf( dirToNearerPowerPill.toString() ));
		
		index++;
		
		// Se establece la distancia que tiene que seguir Pacman para ir a la p�ldora ( normal ) m�s cercana
		oneInstanceData.instance(0).setValue(index, minPowerPillsDistance );
		
		index++;
		
		// La direcci�n para escapar del fantasma m�s cercano
		
		MOVE dirAwayFromClosestGhost = game.getNextMoveAwayFromTarget( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(nearerGhost), DM.PATH);
		
		oneInstanceData.instance(0).setValue(index, String.valueOf( dirAwayFromClosestGhost.toString() ));
		
		// Se discretiza para poder clasificar la instancia a la que se le han asignado los valores obtenidos del objeto game
		// Pero se conserva el dataset original sin discretizar para poderlo seguir usando en las diferentes iteraciones del juego
		
		Instances discretizeInstances = nb.get_discretize_filter().applyFilter(oneInstanceData);
		
		// CLASIFICACI�N
	
		// Call classifyInstance, which returns a double value for the class
		int result = (int) (nb.classify(discretizeInstances.instance(0)));

		if( result == 0 ) return MOVE.LEFT;
		else if( result == 1 ) return MOVE.RIGHT;
		else if( result == 2 ) return MOVE.UP;
		else if( result == 3 ) return MOVE.DOWN;
		else return MOVE.NEUTRAL;
		
	}
	

}