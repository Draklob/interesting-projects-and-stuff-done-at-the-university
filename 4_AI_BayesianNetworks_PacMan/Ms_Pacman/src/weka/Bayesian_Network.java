package weka;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import weka.algorithms.Searching_Algorithm_Global;
import weka.algorithms.Searching_Algorithm_Local;
import weka.core.Instance;
import weka.core.Instances;
import weka.estimators._Estimator;
import weka.filters.Discretize_Filter;
import weka.filters.Remove_Filter;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.net.estimate.BayesNetEstimator;
import weka.classifiers.bayes.net.search.SearchAlgorithm;

/**
 * Encapsula la construcci�n y uso de una red bayesiana usando la API de Weka
 * 
 * @author Miguel �ngel Torres y Javier Barreiro Portela
 * @version 24/05/2015
 */
public class Bayesian_Network 
{
	/**
	 * El dataset usado para generar la red bayesiana
	 */
	Instances trainingData = null;
	
	/**
	 * El filtro usado para eliminar los atributos que no aportan mucha informaci�n
	 */
	Remove_Filter removeFilter; 
	
	/**
	 * El filtro usado para discritizar los valores
	 */
	Discretize_Filter discretizeFilter;
	
	/**
	 * La red bayesiana que se construye para clasificar las instancias
	 */
	BayesNet bayesNet;
	
	/**
	 * Encapsula todo lo relacionado con la construcci�n y uso de la red bayesiana
	 * @param trainingFilePath La ruta al archivo con el dataset para construir la red bayesiana
	 * @param validationFilePath La ruta al archivo con el datase usado para validar la red bayesiana
	 */
	public Bayesian_Network( String trainingFilePath, String validationFilePath )
	{
		/*** CONSTRUCCI�N DE LA RED BAYESIANA ***/
		
		// Se genera un objeto Instances a partir de un fichero de tipo arff
		trainingData = get_instances ( readFile( trainingFilePath ) );
		
		// Se indica que escoja el 1� atributo del dataSet, el del movimiento de Pacman
		trainingData.setClassIndex(0);
		
		// Se eliminan los atributos que no aportan informaci�n
		// Este filtro se guarda para pod�rselo aplicar a los dem�s datasets
		String[] param = {"2-10, 19-25"};
		removeFilter = new Remove_Filter( param );
		trainingData = removeFilter.applyFilter( trainingData );
		
		// Se discretizan los atributos 
		// Este filtro se guarda para pod�rselo aplicar a los dem�s datasets
		discretizeFilter = new Discretize_Filter( "first-last", true, false, trainingData);
		trainingData = discretizeFilter.applyFilter( trainingData );
		
		// Se crea la red bayesiana
		bayesNet = new BayesNet();
		
		// Se construye el algoritmo usado para generar la estructura de la red
		// Se puede elegir cualquiera de los algoritmos que se construyen y pas�rselo a la red bayesiana
		
		// Globales
		SearchAlgorithm global_k2 = Searching_Algorithm_Global.get_K2( 3 , true , true , false , true, Searching_Algorithm_Global.CVType.LOO_CV );
		SearchAlgorithm sa = Searching_Algorithm_Global.get_SimulatedAnnealing( Searching_Algorithm_Global.CVType.LOO_CV, 10.0, 0.999, true, 45, 1, true );
		SearchAlgorithm tan = Searching_Algorithm_Global.get_TAN(true, Searching_Algorithm_Global.CVType.LOO_CV );
		
		// Locales
		SearchAlgorithm local_k2 = Searching_Algorithm_Local.get_K2( 1 , true , false , false , Searching_Algorithm_Local.ScoreType.BAYES );
		
		bayesNet.setSearchAlgorithm( global_k2 );
			
		// Se construye el algoritmo usado para medir las probabilidades de los valores de los atributos
		BayesNetEstimator se = _Estimator.get_SE( 0.3 );
		bayesNet.setEstimator( se );
		
		// Se construye la red pas�ndole el dataset
		try {
			bayesNet.buildClassifier( trainingData );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*** EVALUACI�N DE LA RED BAYESIANA ***/
		
		// Si se ha pasado la ruta del archivo de testing se efect�a la validaci�n correspondiente
		if( validationFilePath != "" )
		{
			// Se prepara el dataset para la validaci�n
			Instances testingData = get_instances ( readFile( validationFilePath ) );
			testingData = removeFilter.applyFilter( testingData );
			testingData = discretizeFilter.applyFilter( testingData );
			testingData.setClassIndex(0);
			
			try {
				
				Evaluation evaluation = new Evaluation(trainingData);
				
				// Se eval�a el dataset testingData
				//evaluation.evaluateModel(bayesNet, testingData);
				evaluation.crossValidateModel(bayesNet, testingData, 10, new Random(1));
				
				// Saca por pantalla el resultado de la evaluaci�n
				System.out.println(evaluation.toSummaryString("Evaluation results:\n", false));
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		// Imprime informaci�n de las instancias del trainingData
		// System.out.println( trainingData.toSummaryString());
	}
	
	/**
	 * Clasifica una instancia usando la red bayesiana generada
	 * @param newInstance La istancia que se desea clasificar
	 * @return La clase en double ( luego hay que convertirla al enumerado correspondiente )
	 */
	public double classify(Instance newInstance)
	{
		double result = 0.0;
		try {
			result = bayesNet.classifyInstance(newInstance);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Devuelve el filtro de discretizaci�n para poderlo usar sobre las instancias en juego
	 * @return discretizeFilter El filtro de discretizaci�n 
	 */
	public Discretize_Filter get_discretize_filter()
	{
		return discretizeFilter;
	}
	
	/**
	 * Devuelve el filtro de remove para poderlo usar sobre las instancias en juego
	 * @return removeFilter El filtro de tipo Remove
	 */
	public Remove_Filter get_remove_filter()
	{
		return removeFilter;
	}
	
	/**
	 * Lee un archivo y devuelve el buffer corresp�ndiente.
	 * @param filePath La ruta del archivo
	 * @return El buffer con el dataset.
	 */
	public BufferedReader readFile(String filePath) {
		BufferedReader inputReader = null;
 
		try {
			inputReader = new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filePath);
		}
 
		return inputReader;
	}
	
	/**
	 * Crea un objeto de tipo Instances a partir de un BufferedReader
	 * @param bufferedReader El buffer de lectura
	 * @return Objeto de tipo Instances
	 */
	public Instances get_instances(BufferedReader bufferedReader)
	{
		Instances data = null;
		
		try {
			data = new Instances( bufferedReader );
		} catch ( IOException e ){
			e.printStackTrace();
		}
		
		return data;
	}
}
