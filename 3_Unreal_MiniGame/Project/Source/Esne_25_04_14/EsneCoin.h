// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "EsneCoin.generated.h"

/**
 * 
 */
UCLASS()
class AEsneCoin : public AActor
{
	GENERATED_UCLASS_BODY()

	// SceneRoot
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
	TSubobjectPtr<USceneComponent> SceneRoot;
	
	// Mesh
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
		TSubobjectPtr<UStaticMeshComponent> Coin;

	// Box
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
		TSubobjectPtr<UBoxComponent> TrigCoin;

	//Velocidad con la que rota la moneda
	UPROPERTY(EditDefaultsOnly, Category = "Esne")
	float RotationRate;

	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, UPrimitiveComponent* a, int32 b);

	//Sobreescribimos el método Tick de AActor
	virtual void Tick(float DeltaTime) OVERRIDE;
};