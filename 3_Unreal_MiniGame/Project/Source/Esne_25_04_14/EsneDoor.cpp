// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.


#include "Esne_25_04_14.h"
#include "EsneDoor.h"
#include "EsnePawnBase.h"
#include "EsneKey.h"
#include "EsneEnemyPawn.h"

AEsneDoor::AEsneDoor(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// SceneRoot
	SceneRoot = PCIP.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneDoor"));
	RootComponent = SceneRoot;

	// Mesh
	Door = PCIP.CreateEditorOnlyDefaultSubobject<UStaticMeshComponent>(this, TEXT("Door"));
	Door->AttachParent = SceneRoot;

	// Box
	TrigDoor = PCIP.CreateEditorOnlyDefaultSubobject<UBoxComponent>(this, TEXT("TrigDoor"));
	TrigDoor->AttachParent = SceneRoot;

	//Registramos los métodos de control del trigger en los delegados correspondientes
	TrigDoor->OnComponentBeginOverlap.AddDynamic(this, &AEsneDoor::OnBeginOverlap);
	TrigDoor->OnComponentEndOverlap.AddDynamic(this, &AEsneDoor::OnEndOverlap);

}

/**
* Se llama cuando un actor entra en el trigger
*/
void AEsneDoor::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent*, int32)
{
	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(-1, .5f, FColor::Red, TEXT("OnBeginOverlap"));

		AEsnePawnBase* pPawn = Cast<AEsnePawnBase>(OtherActor);
		if (pPawn)  // if (pPawn =!NULL) No se suele usar en C++ porque si vale 0 o es nulo, ya no hace falta ponerlo.
		{
			//Si el que ha entrado es un Enemy
			AEsneEnemyPawn* pEnemy = Cast<AEsneEnemyPawn>(OtherActor);
			if (pEnemy)
			{
				//Se intenta abrir la puerta sin necesidad de pulsar una tecla
				TryToOpenDoor(pEnemy);
				pEnemy->hasKey = false;
			}
			else //Si es un player el que entra
			{
				pPawn->DoorDelegate.BindUObject(this, &AEsneDoor::TryToOpenDoor);
			}
		}
	}
}

/**
* Se llama cuando un actor sale del el trigger
*/
void AEsneDoor::OnEndOverlap(class AActor* OtherActor, class UPrimitiveComponent*, int32)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, .5f, FColor::Red, TEXT("OnEndOverlap"));

		AEsnePawnBase* pPawn = Cast<AEsnePawnBase>(OtherActor);
		if (pPawn)  // if (pPawn =!NULL) No se suele usar en C++ porque si vale 0 o es nulo, ya no hace falta ponerlo.
		{
			pPawn->DoorDelegate.Unbind();
		}
	}
}

/**
* Método que se llama para intentar abrir una llave, comprueba que la llave sea del mismo color
*/
void AEsneDoor::TryToOpenDoor(class AEsnePawnBase* pPawn)
{
	for (AEsneItem* eachItem : pPawn->inventory)
	{
		
		if (Cast< AEsneKey >(eachItem))
		{
			AEsneKey* esneKey = Cast< AEsneKey >(eachItem);

			if (esneKey->itemColor.GetValue() == this->itemColor)
			{
				UE_LOG(LogTemp, Warning, TEXT("abrela"));
				//Si abrimos la puerta destruimos la llave 
				//y la eliminamos del inventario
				esneKey->Destroy();
				pPawn->inventory.Remove(esneKey);
				OpenDoor();
			}
			break;
		}
	}
}

/**
* Método que se llama cada vez que se cambia una propiedad del editor
*/
void AEsneDoor::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//Llamamos al método que sobreescribe
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.Property)
	{
		//Cacheamos el nombre de la propiedad que se ha modificado
		FString name;
		name = *PropertyChangedEvent.Property->GetNameCPP();

		//Tan solo hacemos algo si la propiedad es el color o el material
		if (name == "itemColor" || name == "basicMat")
		{
			//Control de seguridad por si aun no hemos seleccionado un material en el slot de basicMat
			if (basicMat != NULL)
			{
				//Inicializamos el material dinámico, que podriamos haber declarado aquí de forma local
				instanceMat = UMaterialInstanceDynamic::Create(basicMat, this);

				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));
				//En función del color modificamos el material dinámico y luego se lo aplicamos 
				//al slot por defecto del mesh, si el basicMat no tiene ese parámetro ( "BaseColor") no hace nada
				switch (itemColor)
				{
					case EItemColor::Red:
					{
						instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0.5, 0, 0, 1));
						Door->SetMaterial(0, instanceMat);
					}
					break;
					case EItemColor::Yellow:
					{
					   instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(1, 0.9, 0, 1));
					   Door->SetMaterial(0, instanceMat);
					}
					break;
					case EItemColor::Blue:
					{
						 instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0, 0, 0.5, 1));
						 Door->SetMaterial(0, instanceMat);
					}
					break;
					case EItemColor::Green:
					{
						 instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0, 0.13, 0, 1));
						 Door->SetMaterial(0, instanceMat);
					}
					break;
				}
			}
		}
	}	
}