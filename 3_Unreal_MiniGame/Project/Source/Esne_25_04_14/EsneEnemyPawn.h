// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsneEnums.h"
#include "EsnePawnBase.h"
#include "EsneEnemyPawn.generated.h"

/**
 * Hereda de AEsnePawnBase y implementa el pawn para el enemigo
 */
UCLASS()
class AEsneEnemyPawn : public AEsnePawnBase
{
	GENERATED_UCLASS_BODY()

	//Referencia al BehaviorTree que le pasamos en el blueprint ( Defaults )
	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* EnemyBehavior;

	virtual bool ToDie();
	
	public:
	//El color de la llave que coge 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	TEnumAsByte<EItemColor::Type> keyColor;
};
