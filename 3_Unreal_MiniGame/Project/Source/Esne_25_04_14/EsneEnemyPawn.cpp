// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "Esne_25_04_14.h"
#include "EsneEnemyPawn.h"
#include "EsneAIController.h"

/**
* Constructor
*/
AEsneEnemyPawn::AEsneEnemyPawn(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	//Asigna el controlador al enemy pawn
	AIControllerClass = AEsneAIController::StaticClass();
}

bool AEsneEnemyPawn::ToDie()
{
	return false;
}