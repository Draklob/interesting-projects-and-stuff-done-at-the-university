// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "Esne_25_04_14.h"
#include "EsneAIController.h"
#include "EsneEnemyPawn.h"
#include "EsneItem.h"
#include "EsneDoor.h"
#include "EsneCoin.h"
#include "EsneEnums.h"

//Constructor
AEsneAIController::AEsneAIController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

	BlackboardComp = PCIP.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	Behaviorcomp = PCIP.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("Behaviorcomp"));
}

//Se llama al principio para asociar el pawn al controller
void AEsneAIController::Possess(class APawn* InPawn)
{
	Super::Possess(InPawn);
	AEsneEnemyPawn* Enemy = Cast<AEsneEnemyPawn>(InPawn);
	if (Enemy && Enemy->EnemyBehavior)
	{
		//Cogemos el BlackboardComp que se asocia al EnemyBehaviorTree definido en EsneEnemyPawn
		BlackboardComp->InitializeBlackboard(Enemy->EnemyBehavior->BlackboardAsset);
		//Asignamos las referencias a los slots del BlackboardComp
		ItemKeyID = BlackboardComp->GetKeyID("TargetName");
		ItemLocationID = BlackboardComp->GetKeyID("Destination");
		//Inicializamos el Behaviorcomp que se asigna en el blueprint del EsneEnemyPawn
		Behaviorcomp->StartTree(Enemy->EnemyBehavior);
	}
}

/**
* Método que llamamos una vez cada tick para actualizar el target que perseguirá el enemy
*/
void AEsneAIController::SearchForTarget()
{
	//Control de seguridad para asegurarnos que está asignado el pawn
	APawn* MyPawn = GetPawn();
	if (MyPawn == NULL)
	{
		return;
	}
	
	//Casteamos a EsneEnemyPawn
	AEsneEnemyPawn* pawn = Cast<AEsneEnemyPawn>(MyPawn);
	//Cacheamos la posición actual del pawn propio
	const FVector MyLoc = pawn->GetActorLocation();
	//Esta distancia marca el umbral a partir del cual el objeto se considerará fuera del alcance del pawn
	float BestDistSq = 10000.f;
	//En un inicio no hay target
	AActor* BestItem = NULL;

	//Si el pawn está muerto el target será el mismo para que no se mueva de donde está
	if (pawn->pawnStates != EPawnStates::Live)
	{
		//BestItem = pawn;
		BlackboardComp->SetValueAsObject(ItemKeyID, NULL);
		BlackboardComp->SetValueAsVector(ItemLocationID, pawn->GetActorLocation());
	}
	else
	{
		//Cacheamos el "world" relativo a este pawn 
		UWorld* world = GEngine->GetWorldFromContextObject(pawn);

		//Iteramos para recorrer todos los actores instanciados del mundo ( world )
		for (FActorIterator It(world); It; ++It)
		{
			AActor* Actor = *It;

			//Control de seguridad
			if (Actor != NULL && !Actor->IsPendingKill())
			{
				//Si el enemigo ha cogido una llave busca una puerta
				if (pawn->hasKey)
				{
					if (Cast<AEsneDoor>(Actor))
					{
						AEsneDoor* door = Cast<AEsneDoor>(Actor);
						//Si la llave es del mismo color que la puerta
						if (door->itemColor.GetValue() == pawn->keyColor)
						{
							//Calcula la distancia del enemigo con la puerta
							const float DistSq = FVector::Dist(door->GetActorLocation(), MyLoc);
							//Guardamos la puerta más cercana
							if (DistSq < BestDistSq)
							{
								BestDistSq = DistSq;
								BestItem = door;
							}
						}
					}
				}
				else if (Cast<AEsneCoin>(Actor)) //Intentamos castear a coin
				{
					AEsneCoin* coin = Cast<AEsneCoin>(Actor);
					//Calcula la distancia entre el actor y el coin
					const float DistSq = FVector::Dist(coin->GetActorLocation(), MyLoc);
					if (DistSq < BestDistSq)
					{
						BestDistSq = DistSq;
						BestItem = coin;
					}
				}
				else if (Cast<AEsneItem>(Actor)) //Intentamos castear a AEsneItem
				{
					//UE_LOG(LogTemp, Warning, TEXT("Items"));

					AEsneItem* item = Cast<AEsneItem>(Actor);
					//Si el objeto no tiene ese objeto
					if (pawn->DoYouWantItem(item))
					{
						//Calculamos la distancia entre el objeto y el enemigo
						const float DistSq = FVector::Dist(item->GetActorLocation(), MyLoc);
						//Guardamos referencia al objeto más cercano
						if (DistSq < BestDistSq)
						{
							BestDistSq = DistSq;
							BestItem = item;
						}
					}
				}
			}
		}
	}
	if (BestItem) //Si al final nos hemos quedado con un objeto como target seleccionado
	{
		SetTarget(BestItem);
	}
}

/**
* Método que llamamos una vez cada tick para actualizar el target que perseguirá el enemy
*/
void AEsneAIController::SetTarget(class AActor *actor)
{
	BlackboardComp->SetValueAsObject(ItemKeyID, actor);
	BlackboardComp->SetValueAsVector(ItemLocationID, actor->GetActorLocation());
}