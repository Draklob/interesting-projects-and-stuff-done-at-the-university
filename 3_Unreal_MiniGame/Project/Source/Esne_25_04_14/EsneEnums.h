// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "EsneEnums.generated.h"

#pragma once

/**
* Enumerados que usamos en el juego
*/
UENUM(BlueprintType)
namespace EItemArea
{
	enum Type
	{
		Back,
		Hands
	};
}

UENUM(BlueprintType)
namespace EPawnStates
{
	enum Type
	{
		Live,
		Dying,
		Dead
	};
}

UENUM()
namespace EItemColor
{
	enum Type
	{
		Red,
		Yellow,
		Blue,
		Green
	};
}