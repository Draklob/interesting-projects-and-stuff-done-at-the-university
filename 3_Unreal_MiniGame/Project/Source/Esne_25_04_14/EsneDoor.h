// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsneEnums.h"
#include "GameFramework/Actor.h"
#include "EsneDoor.generated.h"


#define VectorSetFloat(X,Y,Z);
/**
 * 
 */
UCLASS()
class AEsneDoor : public AActor
{
	GENERATED_UCLASS_BODY()

	// SceneRoot
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
	TSubobjectPtr<USceneComponent> SceneRoot;

	// Mesh
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
	TSubobjectPtr<UStaticMeshComponent> Door;

	// Box
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
	TSubobjectPtr<UBoxComponent> TrigDoor;

	// Mat
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
	UMaterialInterface* basicMat;

	//El Color del Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
	TEnumAsByte<EItemColor::Type> itemColor;

	//Esta variable se podría declarar de forma local en la función que actualiza el color de la puerta
	UMaterialInstanceDynamic* instanceMat;

	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, UPrimitiveComponent* a, int32 b);

	UFUNCTION()
	void OnEndOverlap(class AActor* OtherActor, UPrimitiveComponent* a, int32 b);

	//Este método se llama cuando un pawn tiene una llave del color de la puerta
	UFUNCTION()
	void TryToOpenDoor(class AEsnePawnBase* pPawn);

	//Este método se implementa en blueprint para la animación de abrir la puerta
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Esne")
	bool OpenDoor();

	//Método que se llama cada vez que se cambia una propiedad de la instancia en el editor
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent & PropertyChangedEvent);
};
