// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsneEnums.h"
#include "EsneItem.h"
#include "EsneKey.generated.h"

/**
 * Clase para las llaves de diferentes colores
 * Hereda de AEsneItem
 */
UCLASS()
class AEsneKey : public AEsneItem
{
	GENERATED_UCLASS_BODY()

	//Material
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
	UMaterialInterface* basicMat;

	//Color
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Materials)
	TEnumAsByte<EItemColor::Type> itemColor;

	//Material dinámico
	UMaterialInstanceDynamic* instanceMat;
	
	//Método que se llama cuando se modifica una propiedad en el editor
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent & PropertyChangedEvent);
};
