// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "Esne_25_04_14.h"
#include "EsneItem.h"
#include "EsnePawnBase.h"
#include "EsneEnemyPawn.h"

uint8 AEsneItem::itemCount = 0;

AEsneItem::AEsneItem(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// Mesh
	MyMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MyMesh"));
	RootComponent = MyMesh;

	// Box
	MyBox = PCIP.CreateEditorOnlyDefaultSubobject<UBoxComponent>(this, TEXT("MyBox"));
	MyBox->AttachParent = MyMesh;

	//Registramos los métodos de control del trigger en los delegados correspondientes
	MyBox->OnComponentBeginOverlap.AddDynamic(this, &AEsneItem::OnBeginOverlap);
	MyBox->OnComponentEndOverlap.AddDynamic(this, &AEsneItem::OnEndOverlap);
}


/**
* Este método se llama desde blueprint para llevar un contador de los items que se instancian 
*/
void AEsneItem::PostLoad()
{
	Super::PostLoad();

	itemID = itemCount;
	itemCount++;
}

/**
* Este método se llama cuando un actor entra en el trigger del objeto
*/
void AEsneItem::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent*, int32)
{
	//Control de seguridad
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, .5f, FColor::Red, TEXT("OnBeginOverlap"));

		//Si es de tipo EsnePawnBase
		AEsnePawnBase* pPawn = Cast<AEsnePawnBase>(OtherActor);
		if (pPawn)  // if (pPawn =!NULL) No se suele usar en C++ porque si vale 0 o es nulo, ya no hace falta ponerlo.
		{
			AEsneEnemyPawn* pEnemy = Cast<AEsneEnemyPawn>(OtherActor);
			if (pEnemy) // Si es de tipo EsneEnemyPawn
			{
				//Intentamos abrir la puerta sin necesidad de pulsar teclas
				TryToGiveItem(pEnemy);
			}
			else // Si es de tipo EsnePlayerPawn
			{
				//Registramos el método "TryToGiveItem" en el delegado del pawn
				pPawn->ItemDelegate.AddUObject(this, &AEsneItem::TryToGiveItem);
			}
			
		}
	}
}

/**
* Este método se llama cuando un actor sale del trigger del objeto
*/
void AEsneItem::OnEndOverlap(class AActor* OtherActor, class UPrimitiveComponent*, int32)
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, .5f, FColor::Red, TEXT("OnEndOverlap"));

		AEsnePawnBase* pPawn = Cast<AEsnePawnBase>(OtherActor);
		if (pPawn)  // if (pPawn =!NULL) No se suele usar en C++ porque si vale 0 o es nulo, ya no hace falta ponerlo.
		{
			//Eliminamos el método "TryToGiveItem" del delegado del pawn
			pPawn->ItemDelegate.RemoveUObject(this, &AEsneItem::TryToGiveItem);
		}
	}
}

/**
* Método que comprueba si el pawn no tiene un objeto de ese tipo antes de cogerlo
*/
void AEsneItem::TryToGiveItem(class AEsnePawnBase* pPawn)
{
	if (pPawn->DoYouWantItem(this))
	{
		//Se asocia al pawn y luego se coge
		AssociateToPawn(pPawn);
		pPawn->GiveItem(this);
	}
}

void AEsneItem::AssociateToPawn(class AEsnePawnBase* pPawn)
{
	//Se anulan las físicas del mesh
	MyMesh->SetSimulatePhysics(false);
	//Se anulan las colisiones del mesh
	MyMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//Ignora al pawn 
	MyBox->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	//Asignamos el id del pawn a la variable pawnOwnerID para saber qué pawn lleva el objeto
	pawnOwnerID = pPawn->pawnID;
}

void AEsneItem::DesassociateToPawn()
{
	//Lo quitamos de la jerarquía del padre
	this->DetachRootComponentFromParent(true);
	//Le activamos la física
	MyMesh->SetSimulatePhysics(true);
	//Le activamos las colisiones
	MyMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//Que no colisione con el pawn
	MyMesh->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	//GetWorldTimerManager().SetTimer(this, &AEsneItem::ActivarColision, 2.f, false);
	//El trigger lo ponemos en overlap y que intercepte al pawn.
	MyBox->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	//ya no tiene owner
	pawnOwnerID = NULL;
}

