// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsnePawnBase.h"
#include "GameFramework/Actor.h"
#include "EsneItem.generated.h"


/**
 * Para el Hammer, Shield y Key
 */
UCLASS()
class AEsneItem : public AActor
{
	GENERATED_UCLASS_BODY()

	// Mesh
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
	TSubobjectPtr<UStaticMeshComponent> MyMesh;

	// Box
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Esne")
	TSubobjectPtr<UBoxComponent> MyBox;

	// Name -> de momento no se usa
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	FString nameItem;

	// El pawn que lo lleva -> de momento no se usa
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	TSubclassOf<AEsnePawnBase> pawnBase;

	// El id del pawnOwner
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
		uint8 pawnOwnerID;

	// El id del objeto
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
		uint8 itemID;
	
	// El contador de objetos instanciados
	static uint8 itemCount;


	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, UPrimitiveComponent* a, int32 b);

	UFUNCTION()
	void OnEndOverlap(class AActor* OtherActor, UPrimitiveComponent* a, int32 b);

	/**
	* Método que primero comprueba si el pawn puede coger ese item y luego lo coge
	*/
	UFUNCTION()
	void TryToGiveItem(class AEsnePawnBase* pPawn);

	/**
	* Método que asocia el item al pawn
	*/
	UFUNCTION()
	void AssociateToPawn(class AEsnePawnBase* pPawn);

	/**
	* Método que libera el item del pawn
	*/
	UFUNCTION(BlueprintCallable, Category = "Esne")
	void DesassociateToPawn();

	/**
	* Método que sobreescribe un método que se llama cuando se construye el objeto en blueprint 
	* o como instancia en el editor
	*/
	virtual void PostLoad() OVERRIDE;
};
