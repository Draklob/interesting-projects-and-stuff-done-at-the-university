// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "Esne_25_04_14.h"
#include "EsneCoin.h"
#include "EsnePawnBase.h"


/**
* Constructor
*/
AEsneCoin::AEsneCoin(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PrimaryActorTick.bCanEverTick = true;

	// SceneRoot
	SceneRoot = PCIP.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneDoor"));
	RootComponent = SceneRoot;

	// Mesh
	Coin = PCIP.CreateEditorOnlyDefaultSubobject<UStaticMeshComponent>(this, TEXT("Coin"));
	Coin->AttachParent = SceneRoot;

	// Box
	TrigCoin = PCIP.CreateEditorOnlyDefaultSubobject<UBoxComponent>(this, TEXT("TrigCoin"));
	TrigCoin->AttachParent = SceneRoot;

	RotationRate = 180.f;

	TrigCoin->OnComponentBeginOverlap.AddDynamic(this, &AEsneCoin::OnBeginOverlap);
}

/**
* Se llama cada en cada frame
*/
void AEsneCoin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Creamos un objeto de tipo FRotator
	FRotator MyRot = GetActorRotation();
	//Incrementamos el valor de la rotación para el rotator
	MyRot.Yaw += RotationRate * DeltaTime;
	//Le pasamos la nueva rotación al actor ( moneda )
	SetActorRotation(MyRot);
}

/**
* Se llama cada vez que un objeto entra en el trigger de la moneda
*/
void AEsneCoin::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent*, int32)
{
	if (GEngine)
	{
		//Habría que castear a EsnePawnBase para solo eliminar la moneda en caso de que la coja un pawn
		AEsneCoin::Destroy();
		AEsnePawnBase* pPawn = Cast<AEsnePawnBase>(OtherActor);
		//Le sumamos 10 al contador de monedas del pawn
		pPawn->coins = pPawn->coins + 10;
	}
}