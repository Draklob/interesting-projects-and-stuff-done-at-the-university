// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "Esne_25_04_14.h"
#include "EsneItem.h"
#include "EsnePawnBase.h"

uint8 AEsnePawnBase::pawnCount = 0;

AEsnePawnBase::AEsnePawnBase(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	coins = 0;

}

void AEsnePawnBase::TakeItems()
{
	ItemDelegate.Broadcast(this);
}

void AEsnePawnBase::OpenDoor()
{
	DoorDelegate.ExecuteIfBound(this);
}

void AEsnePawnBase::PostLoad()
{
	Super::PostLoad();

	pawnID = pawnCount;
	pawnCount++;
}

void AEsnePawnBase::Drop()
{
	for (AEsneItem* eachItem : this->inventory)
	{
		eachItem->DesassociateToPawn();
	}
	this->inventory.Empty();
	this->hasKey = false;
	this->hasHammer = false;
}