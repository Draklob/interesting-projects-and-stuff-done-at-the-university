// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsnePawnBase.h"
#include "GameFramework/AIController.h"
#include "EsneAIController.generated.h"

/**
 * Clase que implementa la IA del EsneEnemyPawn
 */
UCLASS()
class AEsneAIController : public AAIController
{
	GENERATED_UCLASS_BODY()

	//Tablas de datos compartidos entre el controller y el behavior-tree
	UPROPERTY(transient)
	TSubobjectPtr<class UBlackboardComponent> BlackboardComp;

	//Referencia al behavior-tree
	UPROPERTY(transient)
	TSubobjectPtr<class UBehaviorTreeComponent> Behaviorcomp;

	//Reimplementación del método que al inicio asigna el pawn al controller
	virtual void Possess(class APawn* InPawn);

	//Método que una vez encontrado el target guarda los valores en el BlackboardComp
	void SetTarget(class AActor *actor);

	//Método que llamamos una vez cada tick para actualizar el target que perseguirá el enemy
	UFUNCTION(BlueprintCallable, Category = Behavior)
		void SearchForTarget();

	protected:
	//Guardamos la referencia del slot del BlackboardComp donde guardamos en cada momento la referencia al target
	uint8 ItemKeyID;
	//Guardamos la referencia del slot del BlackboardComp donde guardamos en cada momento la posición del target
	uint8 ItemLocationID;
};
