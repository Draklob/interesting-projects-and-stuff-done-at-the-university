// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsneEnums.h"
#include "GameFramework/Character.h"
#include "EsnePawnBase.generated.h"

/**
 * Clase base de la que heredan el player y el enemy
 * Hereda de ACharacter
 */

UCLASS()
class AEsnePawnBase : public ACharacter
{
	GENERATED_UCLASS_BODY()

	//Declaramos los dos tipos de delegados, para coger objetos y abrir puertas
	DECLARE_MULTICAST_DELEGATE_OneParam(FTakeItemDelegate, AEsnePawnBase*);
	DECLARE_DELEGATE_OneParam(FTryToOpenDoorDelegate, AEsnePawnBase*);

	//Método que se llama cuando se construye el blueprint y cada uno de las instancias en el editor
	virtual void PostLoad() OVERRIDE;

	//El id del pawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	uint8 pawnID;

	//Contador para los pawn instanciados
	static uint8 pawnCount;

protected:
		//Para saber si está atacando
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
		bool isAttacking;
		
		//Para saber si ha cogido algún hammer
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
		bool hasHammer;
		
		//Para saber donde poner los objetos que lleva ( back o hands )
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
		TEnumAsByte<EItemArea::Type> itemArea;

public:
	//Para saber si lleva una llave
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	bool hasKey;
	
	//El estado del pawn ( Live, Dying, Dead )
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	TEnumAsByte<EPawnStates::Type> pawnStates;

	//El array donde guarda referencias a los objetos que lleva
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	TArray<class AEsneItem*> inventory;
	
	//El número de monedas que ha cogido
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Esne")
	int32 coins;

	//Método que hace un broadcast de los métodos que se registran ( se llama desde blueprint )
	UFUNCTION(BlueprintCallable, Category = "Esne")
	void TakeItems();
	
	//Método que hace un llama al método que se registra ( se llama desde blueprint )
	UFUNCTION(BlueprintCallable, Category = "Esne")
	void OpenDoor();
	
	//Método que comprueba si tenemos un objeto para cogerlo o no
	UFUNCTION(BlueprintImplementableEvent, Category = "Esne")
	bool DoYouWantItem(class AEsneItem* pItem);
	
	//Coge el objeto ( lo añade al inventario y lo coloca en su sitio )
	UFUNCTION(BlueprintImplementableEvent, Category = "Esne")
	void GiveItem(class AEsneItem* pItem);
	
	//Devuelve el nombre del socket para colocar el objeto
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Esne")
	FName GetSocketName(class AEsneItem* item, EItemArea::Type Area);
	
	//Coloca el objeto en su lugar
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Esne")
	bool SetItem(class AEsneItem* item);
	
	//Suelta todos los objetos que llevamos
	UFUNCTION(BlueprintCallable, Category = "Esne")
	void Drop();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Esne")
	virtual bool ToDie();

	//Delegados a los que se registran los métodos de los objetos ( items y puertas )
	FTakeItemDelegate ItemDelegate;
	FTryToOpenDoorDelegate DoorDelegate;
};
