// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EsnePawnBase.h"
#include "EsnePlayerPawn.generated.h"

/**
 * Clase para el player, de momento está completamente implementada en blueprint
 * Heredad de AEsnePawnBase
 */
UCLASS()
class AEsnePlayerPawn : public AEsnePawnBase
{
	GENERATED_UCLASS_BODY()

	virtual bool ToDie();
	
};
