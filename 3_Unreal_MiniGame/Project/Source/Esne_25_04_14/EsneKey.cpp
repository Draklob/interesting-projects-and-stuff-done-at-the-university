// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "Esne_25_04_14.h"
#include "EsneKey.h"

//Constructor
AEsneKey::AEsneKey(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

/**
* Método que se llama cuando se modifica una propiedad en el editor
*/
void AEsneKey::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	//Llamamos al método que sobreescribe
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (PropertyChangedEvent.Property)
	{
		//Cacheamos el nombre de la propiedad que se ha modificado
		FString name;
		name = *PropertyChangedEvent.Property->GetNameCPP();

		//Tan solo hacemos algo si la propiedad es el color o el material
		if (name == "itemColor" || name == "basicMat")
		{
			//Control de seguridad por si aun no hemos seleccionado un material en el slot de basicMat
			if (basicMat != NULL)
			{
				//Inicializamos el material dinámico, que podriamos haber declarado aquí de forma local
				instanceMat = UMaterialInstanceDynamic::Create(basicMat, this);

				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));
				//En función del color modificamos el material dinámico y luego se lo aplicamos 
				//al slot por defecto del mesh, si el basicMat no tiene ese parámetro ( "BaseColor") no hace nada
				switch (itemColor)
				{
				case EItemColor::Red:
				{
					instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0.5, 0, 0, 1));
					MyMesh->SetMaterial(0, instanceMat);
				}
					break;
				case EItemColor::Yellow:
				{
					instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(1, 0.9, 0, 1));
					MyMesh->SetMaterial(0, instanceMat);
				}
					break;
				case EItemColor::Blue:
				{
					instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0, 0, 0.5, 1));
					MyMesh->SetMaterial(0, instanceMat);
				}
					break;
				case EItemColor::Green:
				{
					instanceMat->SetVectorParameterValue(FName(TEXT("BaseColor")), FLinearColor(0, 0.13, 0, 1));
					MyMesh->SetMaterial(0, instanceMat);
				}
					break;
				}
			}
		}
	}
}