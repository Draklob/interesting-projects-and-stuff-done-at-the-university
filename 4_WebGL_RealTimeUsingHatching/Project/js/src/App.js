        

/*
* Objeto para el control mediante el ratón de la cámara
*/
var controls;
/*
* El array que contiene los shaders que se cargarán con 
* la librería que hace uso de ajax
*/
var shaders;

/* 
* Para generar icosaedros aleatorios
*/
var noise = new ImprovedNoise();
var renderer, scene, camera;
var mesh, cube, pMesh, material;
var obj_model;
var objModelMeshArray = [];
var mario;
var skull;
var lightPosition = [0.0,0.0];
var lightAngle = 0.0;
var lightDistance = 100.0;
/*
* Se definen los valores de apertura para la perspectiva de la cámara
*/
var fov = 120, nfov = 45;

var WIDTH = window.innerWidth;
var HEIGHT = window.innerHeight;

var presets = 
{
    'Default': {  model: 5, blendWithBg:false, bgBlendIntensity:0.5, background_image: 7, ambient: 50, ambientColor: [ 187, 187, 187 ], diffuse: 55, diffuseColor: [ 150, 150, 150 ], specularIntensity:0.01, specularColor: [ 235, 213, 247 ], shininess: 1, rim: 17, invertRim: false, displayOutline: true, outlineWidth: 1.2, outlineColor: [ 42, 42, 42 ], useHatching: false, textureRepetition: {x:2,y:2} },
    'Sketch': { model: 5, blendWithBg:true, bgBlendIntensity:0.5, background_image: 2, ambient: 9.8, ambientColor: [ 72, 72, 164 ], diffuse: 100, diffuseColor: [ 72, 72, 164 ], specularIntensity:0.5, specularColor: [ 90, 120, 111 ], shininess: 15, rim: 77, invertRim: true, displayOutline: false, outlineWidth: 1, outlineColor: [ 0, 0, 90 ],useHatching: false, textureRepetition: {x:1,y:1} },
    'Classroom': { model: 2, blendWithBg:true, bgBlendIntensity:0.5, background_image: 3, ambient: 13, ambientColor: [ 72, 72, 164 ], diffuse: 27, diffuseColor: [ 72, 72, 164 ], specularIntensity:0.5, specularColor: [ 90, 120, 111 ], shininess: 15, rim: 77, invertRim: true, displayOutline: true, outlineWidth: 1, outlineColor: [ 0, 0, 90 ], useHatching: false, textureRepetition: {x:1,y:1} },
    'Engraving': { model: 4, blendWithBg:true, bgBlendIntensity:0.5, background_image: 1, ambient: 0, ambientColor: [ 72, 72, 164 ], diffuse: 57, diffuseColor: [ 72, 72, 164 ], specularIntensity:0.5, specularColor: [ 90, 120, 111 ], shininess: 15, rim: 77, invertRim: true, displayOutline: false, outlineWidth: 1, outlineColor: [ 0, 0, 90 ], useHatching: false, textureRepetition: {x:1,y:1} }
};

var Settings = function() 
{
    this.preset = 0;
    this.model = 3;
    this.blendWithBg = false;
    this.bgBlendIntensity = 0.5;
    this.background_image = 7; 
    this.ambient = 1;
    this.ambientColor = [ 0, 0, 90 ];
    this.diffuse = 100;
    this.diffuseColor = [ 0, 0, 90 ];
    this.specularIntensity = 0.5;
    this.specularColor = [ 235, 213, 247 ];
    this.shininess = 1;
    this.rim = 17;
    this.invertRim = true;
    this.displayOutline = false;
    this.outlineWidth = 1;
    this.outlineColor = [ 0, 0, 90 ];
    this.useHatching = false;
    this.textureRepetition = {x:1,y:1};
};
var settings = new Settings();
var gui = new dat.GUI();

        
/*
* Se inicia la aplicación
*/
function init() 
{
    /*
    * Se genera la interfaz gráfica
    */

    var presetSelector = {};
    for( var j in presets ) 
    {
        presetSelector[ j ] = j;
    }

    gui.add( settings, 'preset', presetSelector );
    gui.add( settings, 'model', { Cube: 1, Sphere: 2, TorusKnot: 3, Torus: 4, Distort: 5, Capsule: 6, Man: 7, Mario: 8, Skull: 9 } );
    var f1 = gui.addFolder('background blend');
    f1.add( settings, 'blendWithBg' );
    f1.add( settings, 'bgBlendIntensity', 0.0, 10.0 );
    f1.add( settings, 'background_image', { bg1: 0, bg2: 1, bg3: 2, bg4: 3, bg5: 4, bg6:5, bg7: 6, bg8: 7, bg9: 8 } );
    gui.add( settings, 'ambient', 0.0, 100.0 );
    gui.addColor( settings, 'ambientColor' );
    gui.add( settings, 'diffuse', 0.0, 100.0 );
    gui.addColor( settings, 'diffuseColor' );
    var f2 = gui.addFolder('specular');
    f2.add( settings, 'specularIntensity', 0.0, 20.0 );
    f2.addColor( settings, 'specularColor' );
    f2.add( settings, 'shininess', 1, 100 );
    f2.add( settings, 'rim', 0.0, 100.0 );
    f2.add( settings, 'invertRim' );
    var f3 = gui.addFolder('Outline');
    f3.add( settings, 'displayOutline' );
    f3.add( settings, 'outlineWidth', 0.0, 10.0 );
    f3.addColor( settings, 'outlineColor' );
    gui.add( settings, 'useHatching' );
    
    var f4 = gui.addFolder('textureRepetition');
    f4.add( settings.textureRepetition, 'x', 1.0, 30.0 ).onChange(function(newValue)
        {
            value = [newValue,settings.textureRepetition.y];
            setRepetition( value );
        });
    f4.add( settings.textureRepetition, 'y', 1.0, 30.0 ).onChange(function(newValue)
        {
            value = [settings.textureRepetition.x,newValue];
            setRepetition( value );
        });
    f4.open();
   
    /*
    * Se inicializa la escena de Three.js
    */

    camera = new THREE.PerspectiveCamera( fov, WIDTH / HEIGHT, 1, 1000 );
    camera.position.z = 70;

    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    renderer.setSize( WIDTH, HEIGHT );
    renderer.autoClear = false;

    var container = document.getElementById( 'container' );
    container.appendChild( renderer.domElement );

    /**
    * Se inicializa el control de la cámara
    */
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.addEventListener( 'change', render );

    /*
    * Se genera el material
    */

    // Para formar la ruta de las diferentes texturas para formar el TAM
    var id = 'images/hatching_patterns/hatch_';

    material = new THREE.ShaderMaterial( 
    {

        uniforms:       {
            uShowOutline: { type: 'f', value: 0 },
            uOutlineWidth: { type: 'f', value: 1 },
            uOutlineColor: { type: 'v3', value: new THREE.Vector3( 0, 0,0 ) },
            uAmbientIntensity: { type: 'f', value : 0 },
            uAmbientColor: { type: 'v3', value: new THREE.Vector3( 0, 0, 0 ) },
            uDiffuseIntensity: { type: 'f', value : 1 },
            uDiffuseColor: { type: 'v3', value: new THREE.Vector3( 0, 0, 0 ) },
            uSpecularIntensity: { type: 'f', value : 1 },
            uSpecularColor: { type: 'v3', value: new THREE.Vector3( 0, 0, 0 ) },
            uShininess: { type: 'f', value : 1 },
            uRimWeight: { type: 'f', value : 1 },
            uInvertRim: { type: 'i', value: 0 },
            uUseHatching: { type: 'i', value: 0 },
            uBlendWithBg: { type: 'i', value: 0 },
            uBackground_image: { type: 't', value: THREE.ImageUtils.loadTexture( 'images/backgrounds/placeholder.jpg' ) },
            uBgBlendIntensity: { type: 'f', value : 1 },
            uBgResolution: { type: 'v2', value: new THREE.Vector2( 0, 0 ) },
            uLightPosition: { type: 'v3', value: new THREE.Vector3( -100, 100, 0 ) }, // En esta demo se sitúa una luz fija en esta posición
            uHatch1: { type: 't', value: THREE.ImageUtils.loadTexture( id + '0.jpg' ) },
            uHatch2: { type: 't', value: THREE.ImageUtils.loadTexture( id + '1.jpg' ) },
            uHatch3: { type: 't', value: THREE.ImageUtils.loadTexture( id + '2.jpg' ) },
            uHatch4: { type: 't', value: THREE.ImageUtils.loadTexture( id + '3.jpg' ) },
            uHatch5: { type: 't', value: THREE.ImageUtils.loadTexture( id + '4.jpg' ) },
            uHatch6: { type: 't', value: THREE.ImageUtils.loadTexture( id + '5.jpg' ) },
            uRepetition: { type: 'v2', value: new THREE.Vector2( 1, 1 ) },
            uUseTexture: { type: 'i', value: 0 },
            uTextureIndex: { type: 'i', value: 0 },
            uObjModelDiffuse1: { type: 't', value: THREE.ImageUtils.loadTexture( 'models/male02/male-02-1noCulling.JPG' ) },
            uObjModelDiffuse2: { type: 't', value: THREE.ImageUtils.loadTexture( 'models/male02/01_-_Default1noCulling.JPG' ) },
            uObjModelDiffuse3: { type: 't', value: THREE.ImageUtils.loadTexture( 'models/male02/orig_02_-_Defaul1noCulling.JPG' ) },
            uMarioDiffuse: { type: 't', value: THREE.ImageUtils.loadTexture( 'models/mario/marioD.jpg' ) }
        },
        vertexShader:   shaders.hatching.vertex,
        fragmentShader: shaders.hatching.fragment

    });

    material.uniforms.uBackground_image.value.generateMipmaps = false;
    material.uniforms.uBackground_image.value.magFilter = THREE.LinearFilter;
    material.uniforms.uBackground_image.value.minFilter = THREE.LinearFilter;
    
    /*
    * Se establecen todas las texturas como tileables
    */
    material.uniforms.uHatch1.value.wrapS = material.uniforms.uHatch1.value.wrapT = THREE.RepeatWrapping;
    material.uniforms.uHatch2.value.wrapS = material.uniforms.uHatch2.value.wrapT = THREE.RepeatWrapping;
    material.uniforms.uHatch3.value.wrapS = material.uniforms.uHatch3.value.wrapT = THREE.RepeatWrapping;
    material.uniforms.uHatch4.value.wrapS = material.uniforms.uHatch4.value.wrapT = THREE.RepeatWrapping;
    material.uniforms.uHatch5.value.wrapS = material.uniforms.uHatch5.value.wrapT = THREE.RepeatWrapping;
    material.uniforms.uHatch6.value.wrapS = material.uniforms.uHatch6.value.wrapT = THREE.RepeatWrapping;

 
    /*
    * Se carga el preset por defecto para los valores del material
    */ 
    setPreset( 'Default' );            

    /*
    * Asignamos los controladores de eventos de inputs y resize de pantalla
    */
    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();
   
    // Cargamos a Mario ( formato json )
    var loader = new THREE.ObjectLoader();
    loader.load("./models/mario/mario-sculpture.json", function ( obj ) {

        obj.traverse( function ( child ) {

            if ( child instanceof THREE.Mesh ) {

                child.material = material;
            }

        } );
        mario = obj;
    });

    // Cargamos la cadavera ( formato json )
    var skullLoader = new THREE.ObjectLoader();
    skullLoader.load("./models/skull/skull.json", function ( obj ) {

        obj.traverse( function ( child ) {

            if ( child instanceof THREE.Mesh ) {

                child.material = material;
            }

        } );
        skull = obj;
        skull.scale.set(10,10,10);
    });
  
    // OBJ MODEL

    var manager = new THREE.LoadingManager();
    manager.onProgress = function ( item, loaded, total ) 
    {

        console.log( item, loaded, total );

    };

    var onProgress = function ( xhr ) {
        if ( xhr.lengthComputable ) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            console.log( Math.round(percentComplete, 2) + '% downloaded' );
        }
    };

    var onError = function ( xhr ) {
    };

    that = this;

    var loader = new THREE.OBJLoader( manager );
    path = 'models/male02/male02.obj';
    auxIndex = 0;
    loader.load( path, function ( object ) {

        object.traverse( function ( child ) {

            if ( child instanceof THREE.Mesh ) {

                child.material = material;
                objModelMeshArray[auxIndex] = child;
                auxIndex++;
            }

        } );

        that.obj_model = object;
        object.position.y = - 80;
        //scene.add( object );

    }, onProgress, onError );


    /*
    * Lanzamos el bucle de la aplicación
    */
    animate();

    var ambient = new THREE.AmbientLight( 0x444444 );
        scene.add( ambient );
}

/*
* El loop de la aplicación
*/
function animate() 
{
    requestAnimationFrame( animate );
    controls.update();
    render();

    // Actualizamos la luz
    lightAngle += 0.01;
    lightPosition[0] = lightDistance * Math.cos(lightAngle);
    lightPosition[1] = lightDistance * Math.sin(lightAngle);
}

/*
* Se renderiza la escena
*/
function render() 
{
   
    // Se comprueba si se ha modificado el id de la textura de papel
    // en la interfaz dat.GUI
    var pId = settings.preset;
    if( pId != settings.currentPreset ) setPreset( pId );
    // Se comprueba si se ha modificado el id deL modelo
    // en la interfaz dat.GUI
    var mId = parseInt( settings.model, 10 ); // Se parsea en sistema binario
    if( mId != settings.currentModel ) setModel( mId );

    // Si se quiere mezclar con la imagen de fondo
    // Se comprueba si se ha modificado el id del background
    // en la interfaz dat.GUI
    material.uniforms.uBlendWithBg.value = settings.blendWithBg?1:0;
    material.uniforms.uBgBlendIntensity.value = settings.bgBlendIntensity;
    var pId = parseInt( settings.background_image, 10 ); // Se parsea en sistema binario
    if( pId != settings.currentBackgroundImage ) setBackgroundImage( pId );
    

    material.uniforms.uAmbientIntensity.value = settings.ambient / 100;
    material.uniforms.uAmbientColor.value.set( settings.ambientColor[ 0 ] / 255, settings.ambientColor[ 1 ] / 255, settings.ambientColor[ 2 ] / 255 );
    material.uniforms.uDiffuseIntensity.value = settings.diffuse / 100;
    material.uniforms.uDiffuseColor.value.set( settings.diffuseColor[ 0 ] / 255, settings.diffuseColor[ 1 ] / 255, settings.diffuseColor[ 2 ] / 255 );
    material.uniforms.uSpecularIntensity.value = settings.specularIntensity;
    material.uniforms.uSpecularColor.value.set( settings.specularColor[ 0 ] / 255, settings.specularColor[ 1 ] / 255, settings.specularColor[ 2 ] / 255 );
    material.uniforms.uShininess.value = settings.shininess;
    material.uniforms.uRimWeight.value = settings.rim / 100;
    material.uniforms.uInvertRim.value = settings.invertRim?1:0;
    material.uniforms.uUseHatching.value = settings.useHatching?1:0;
   
    material.uniforms.uLightPosition.value.set( lightPosition[ 0 ], lightPosition[ 1 ], 0.0 );

   

    //renderer.clear();

    // Si se ha seleccionado mostrar el borde, primero se renderiza el objeto plano
    // pero más ancho y luego el objeto normal encima
    if( settings.displayOutline ) 
    {
        material.depthWrite = false;
        material.uniforms.uShowOutline.value = 1;
        material.uniforms.uOutlineWidth.value = settings.outlineWidth;
        material.uniforms.uOutlineColor.value.set( settings.outlineColor[ 0 ] / 255, settings.outlineColor[ 1 ] / 255, settings.outlineColor[ 2 ] / 255, 1 );
        renderer.render( scene, camera );
    }
    material.depthWrite = true;
    material.uniforms.uShowOutline.value = 0;

    // Si hemos elegido el personaje vamos renderizando cada malla con su correspondiente textura
    if( settings.currentModel == 7 )
    {
        // Recorremos todas las mallas cargadas del obj
        for(auxIndex=0;auxIndex<10;auxIndex++)
        {
           
            if( mesh ) scene.remove( mesh );
            mesh = objModelMeshArray[auxIndex];
            material.uniforms.uTextureIndex.value = auxIndex;
            mesh.position.y = - 80;
            scene.add( mesh );
            renderer.render( scene, camera );
            
        }   
    } 
    else renderer.render( scene, camera );

    

}

/*
* Carga un preset determinado
*/
function setPreset( id ) 
{
    for( var j in presets[ id ] ) 
    {
        settings[ j ] = presets[ id ][ j ];
    }
    this.settings.preset = id;
    this.settings.currentPreset = id;
    // Actualiza los valores de la interfaz
    for (var i in gui.__controllers)
    {
       gui.__controllers[i].updateDisplay();
    }
}

function setRepetition( newValue )
{
    settings.textureRepetition.x = newValue[0];
    settings.textureRepetition.y = newValue[1];
    material.uniforms.uRepetition.value.set( newValue[0], newValue[1] );

    // No funciona, no actualiza el valor de textureRepetition en el dat.gui
    for (var i = 0; i < this.gui.__folders.textureRepetition.__controllers.length; i++) {
        this.gui.__folders.textureRepetition.__controllers[i].updateDisplay();
    }
}

/*
* Carga diferentes mallas con diferentes valores para el material definido en el ámbito global
*/
function setModel( id ) 
{

    if( mesh ) scene.remove( mesh );

    switch( id ) {
        case 1: mesh = new THREE.Mesh( new THREE.CubeGeometry( 40, 40, 40 ), material ); 
            newValue = [1,1];
            setRepetition( newValue );
            material.uniforms.uUseTexture.value = 0;
            break;
        case 2: mesh = new THREE.Mesh( new THREE.SphereGeometry( 40, 36, 36 ), material ); 
            newValue = [4,4];
            setRepetition( newValue );
            material.uniforms.uUseTexture.value = 0;
            break;
        case 3: mesh = new THREE.Mesh( new THREE.TorusKnotGeometry( 50, 10, 200, 50, 1 ,3 ), material );
            newValue = [20,2];
            setRepetition( newValue );
            material.uniforms.uUseTexture.value = 0;
            break;
        case 4: mesh = new THREE.Mesh( new THREE.TorusGeometry( 50, 20, 200, 50 ), material ); 
            newValue = [4,2];
            setRepetition( newValue );
            material.uniforms.uUseTexture.value = 0;
            break;
        case 5: 
            mesh = new THREE.Mesh( new THREE.IcosahedronGeometry( 40, 4 ), material ); 
            for( var j = 0; j < mesh.geometry.vertices.length; j++ ) {
                var v = mesh.geometry.vertices[ j ];
                var n = v.clone();
                n.normalize();
                var f = .05;
                var d = 10 * noise.noise( f * v.x, f * v.y, f * v.z );
                v.add( n.multiplyScalar( d ) );
            }
            mesh.geometry.verticesNeedUpdate = true;
           //mesh.geometry.computeCentroids();
            mesh.geometry.computeFaceNormals();
            mesh.geometry.computeVertexNormals();
            newValue = [4,4];
            setRepetition( newValue );
            material.uniforms.uUseTexture.value = 0;
            break;
        case 6:
            var r = 40;
            var capsuleGeometry = new THREE.Geometry();
            var geometry = new THREE.CylinderGeometry( 50, 50, 100, r, r, 1, false );
            var sphereGeometry = new THREE.SphereGeometry( 50, r, r );
            mesh = new THREE.Mesh( geometry, material );
            THREE.GeometryUtils.merge( capsuleGeometry, mesh );
            mesh = new THREE.Mesh( sphereGeometry, material );
            mesh.position.set( 0, -50, 0 );
            THREE.GeometryUtils.merge( capsuleGeometry, mesh );
            mesh = new THREE.Mesh( sphereGeometry, material );
            mesh.position.set( 0, 50, 0 );
            THREE.GeometryUtils.merge( capsuleGeometry, mesh );

            mesh = new THREE.Mesh( capsuleGeometry, material );
            mesh.scale.set( .5, .5, .5 );
            newValue = [2,1];
            setRepetition( newValue );
            material.uniforms.uUseTexture.value = 0;
            break;
        case 7:
            mesh = obj_model;
            material.uniforms.uUseTexture.value = 1;
            break;
        case 8:
            mesh = mario;
            material.uniforms.uTextureIndex.value = 10;
            material.uniforms.uUseTexture.value = 1;
            break;
        case 9:
            mesh = skull;
            material.uniforms.uUseTexture.value = 0;
            break;
    }

    settings.currentModel = id;

    scene.add( mesh );
}

/*
* Se carga la textura de papel para el fondo
*/
function setBackgroundImage( id ) 
{
    // En principio lo del papel de fondo funciona también en Firefox
    // if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) return;

    // Para formar la ruta de las diferentes texturas de background
    var path = 'images/backgrounds/';
    var files = [ path + 'bg1.jpg', path + 'bg2.jpg', path + 'bg3.jpg', path + 'bg4.jpg', path + 'bg5.jpg', path + 'bg6.jpg', path + 'bg7.jpg', path + 'bg8.jpg', path + 'bg9.jpg' ];

    var img = new Image();
    img.addEventListener( 'load', function() 
    {
        material.uniforms.uBackground_image.value = THREE.ImageUtils.loadTexture( files[ id ] );
        material.uniforms.uBackground_image.value.needsUpdate = true;
        document.body.style.backgroundImage = 'url(' + files[ id ] + ')';
        material.uniforms.uBgResolution.value.set( this.width, this.height );
        settings.currentBackgroundImage = id;
    } );
    img.src = files[ id ];
}

/*
* Convierte color de hexadecimal a RGB ( no se usa )
*/
function hexToRgb(hex) 
{
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

/*
* Método que se invoca cuando se cambial tamaño del viewport
*/
function onWindowResize()
{

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

