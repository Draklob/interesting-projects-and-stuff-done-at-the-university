
// Se pasan las coordenadas de textura
varying vec2 vUv;
// Se pasa la normal transformada e interpolada para este fragmento
varying vec3 vTransformedNormal;
// Se pasa la posición interpolada del fragmento
varying vec3 vPosition;

// Las diferentes texturas
uniform sampler2D uHatch1;
uniform sampler2D uHatch2;
uniform sampler2D uHatch3;
uniform sampler2D uHatch4;
uniform sampler2D uHatch5;
uniform sampler2D uHatch6;
uniform sampler2D uBackground_image;
// Cuando cargamos el modelo obj, vamos cambiando este índice para saber qué textura usar
// ( cuerpo, cabez o pelo )
uniform int uTextureIndex;
uniform sampler2D uObjModelDiffuse1;
uniform sampler2D uObjModelDiffuse2;
uniform sampler2D uObjModelDiffuse3;

// La textura de diffuse para Mario
uniform sampler2D uMarioDiffuse;

// Se recoge el vector de repetición de la textura ( indica cuanto se repita la textura en cada eje )
uniform vec2 uRepetition;

// La resolución de la textura del papel para el fondo
uniform vec2 uBgResolution;
// Si se hace blending con la imagen de fondo
uniform int uBlendWithBg;
// La intensidad del blending con la imagen de fondo si es que se hace
uniform float uBgBlendIntensity;
// La posición del punto de luz ( en esta demo permanece fija )
uniform vec3 uLightPosition;
// La intensidad global de la luz ambiente
uniform float uAmbientIntensity;
uniform vec3 uAmbientColor;
// La intensidad global de la luz difusa
uniform float uDiffuseIntensity;
uniform vec3 uDiffuseColor;
// La intensidad de la luz especular
uniform float uSpecularIntensity;
// El color de la luz especular
uniform vec3 uSpecularColor;
uniform float uRimWeight;
uniform float uShininess;
uniform int uInvertRim;
// Si se renderiza con hatching o no
uniform int uUseHatching;
// Outline
uniform float uShowOutline;
uniform vec3 uOutlineColor;
// Si se usa o no textura de para el objeto
uniform int uUseTexture;

vec3 hatching( float shading ) 
{
    vec4 textureColor;
    vec2 uvRepeated = uRepetition * vUv;
    // Se calcula qué mezcla de texturas escoger dependiendo de en qué tramo está el grado de iluminación
    // Si shading es muy claro se mezcla entre la uHatch1 y blanco
    float step = 1.0 / 6.0;
    if( shading <= step ){   
        textureColor = mix( texture2D( uHatch6, uvRepeated ), texture2D( uHatch5, uvRepeated ), 6.0 * shading );
    }
    if( shading > step && shading <= 2.0 * step ){
        textureColor = mix( texture2D( uHatch5, uvRepeated ), texture2D( uHatch4, uvRepeated) , 6.0 * ( shading - step ) );
    }
    if( shading > 2.0 * step && shading <= 3.0 * step ){
        textureColor = mix( texture2D( uHatch4, uvRepeated ), texture2D( uHatch3, uvRepeated ), 6.0 * ( shading - 2.0 * step ) );
    }
    if( shading > 3.0 * step && shading <= 4.0 * step ){
        textureColor = mix( texture2D( uHatch3, uvRepeated ), texture2D( uHatch2, uvRepeated ), 6.0 * ( shading - 3.0 * step ) );
    }
    if( shading > 4.0 * step && shading <= 5.0 * step ){
        textureColor = mix( texture2D( uHatch2, uvRepeated ), texture2D( uHatch1, uvRepeated ), 6.0 * ( shading - 4.0 * step ) );
    }
    if( shading > 5.0 * step && shading <= 6.0 * step ){
        textureColor = mix( texture2D( uHatch1, uvRepeated ), vec4( 1.0 ), 6.0 * ( shading - 5.0 * step ) );
    }
    else if( shading > 6.0 * step ) textureColor = vec4( 1.0 );

    vec3 colorAux = vec3( 0.0 );

   return textureColor.rgb;
}

void main() 
{
    /*
    * Fusión con la imagen de fondo de la pantalla
    */
    vec3 bgImageColor = vec3 ( 1.0 );

    if( uBlendWithBg == 1 )
    {
        // Se calculan las coordenadas de textura del fragmento respecto al background que cubre la pantalla
        // mod es una función built-in de webgl que devuelve el resto de dividir el primer parámetro entre el segundo.
        vec2 nUV = vec2( mod( gl_FragCoord.x, uBgResolution.x ) / uBgResolution.x, mod( gl_FragCoord.y, uBgResolution.y ) / uBgResolution.y );
        // Se extrae el pixel del background_image correspondiente a la posición del fragmento
        bgImageColor = vec3( texture2D( uBackground_image, nUV ).rgb );
    }

    // Se normalizan las interpolaciones de las normales recibidas en el fragment
    vec3 transformedNormal = normalize( vTransformedNormal );

    /*
    * DIFFUSE WEIGHTING
    */
    // Se calcula el vector dirección normalizado de la luz respecto a la posición del fragmento
    vec3 lightDirection = normalize(uLightPosition - vPosition);
    // Se calcula la proyección de este vector sobre la normal a dicho fragmento
    float diffuseLightWeighting = max(dot(transformedNormal, lightDirection), 0.0);
    
    /*
    * SPECULAR WEIGHTING
    */

    // Se calcula la dirección de la luz reflejada 
    vec3 reflectionDirection = reflect(-lightDirection, transformedNormal);
    // Se calcula el vector de posición con respecto a nuestro punto de visión y se normaliza.
    // Nosotros ( la cámara ) siempre estamos colocados en el origen de coordenadas y
    // es la escena la que se transforma incorporando la inversa de las transformaciones que
    // sufra la cámara
    vec3 eyeDirection = normalize(-vPosition);
    // Finalmente calculamos el specular en este punto
    float specularLightWeighting = pow(max(dot(reflectionDirection, eyeDirection), 0.0), uShininess);
    // Calculamos el gradiente del specular
    float rim = max( 0.0, abs( dot( transformedNormal, eyeDirection ) ) );
    if( uInvertRim == 1 ) rim = 1.0 - rim;

    vec3 fragmentColor;

    if (uUseTexture == 1) 
    {
        // Cuerpo ( ropa, zapatos )
        if ( uTextureIndex == 0 || uTextureIndex == 1 || uTextureIndex == 2 || uTextureIndex == 8 || uTextureIndex == 9 )
        fragmentColor = texture2D(uObjModelDiffuse1, vUv).rgb;
        else if ( uTextureIndex == 0 ) fragmentColor = texture2D(uObjModelDiffuse2, vUv).rgb;
        // Pelo
        else if ( uTextureIndex == 3 || uTextureIndex == 4 || uTextureIndex == 5 || uTextureIndex == 6 || uTextureIndex == 7 ) fragmentColor = texture2D(uObjModelDiffuse3, vUv).rgb;
        // Mario
        else if ( uTextureIndex == 10 )
            fragmentColor = texture2D(uMarioDiffuse, vUv).rgb;
    } 
    else 
    {
        fragmentColor = uDiffuseColor;
    }

    // Se calcula el color de la iluminación y su intensidad en este fragmento
    vec3 lightingColor = uAmbientIntensity * uAmbientColor + ( uDiffuseIntensity * diffuseLightWeighting ) * fragmentColor + ( specularLightWeighting + rim * uRimWeight ) * uSpecularColor * uSpecularIntensity;

    if (uUseHatching == 1) 
    {
        // Se calcula la iluminación resultante en este fragmento
        float shading = uAmbientIntensity + uDiffuseIntensity * diffuseLightWeighting + ( uRimWeight * rim + specularLightWeighting ) * uSpecularIntensity;
      
        // Se multiplica el color original por el efecto del hatching
        lightingColor = lightingColor * hatching( shading );
    } 

    // Se hace una pasada primero para el outline y luego se pinta encima el relleno
    vec3 result = uShowOutline * uOutlineColor + ( 1.0 - uShowOutline ) * lightingColor;
   
    // Se mezcla con la imagen de fondo
    vec3 color = result * bgImageColor;

    gl_FragColor = vec4( color, 1.0 );
}

