
// Pasamos las normales transformadas
varying vec3 vTransformedNormal;
// Pasamos las coordenadas de textura
varying vec2 vUv;
// Pasamos la posición transformada con un escalado si es para el outline
varying vec3 vPosition;
// Se recibe la posición de la luz
uniform vec3 uLightPosition;
// Se recibe si se pinta el contorno o no
uniform float uShowOutline;
// Se recibe el grosor del contorno
uniform float uOutlineWidth;

void main() 
{
    // Si está activado el que pinte un contorno se incrementa la posición en la dirección de la normal
    vec3 posInc = vec3( 0.0 );
    if( uShowOutline == 1.0 ) posInc = uOutlineWidth * normal;

    // Se recogen las coordenadas de textura 
    vUv = uv;
    // A las coordenadas de los vértices se le suma el desplazamiento si hay que mostrar el contorno
    // y se multiplican luego por la matriz modelViewMatrix
    vec4 mvPosition = modelViewMatrix * vec4( position + posInc, 1.0 );
    // Se extrae la posición ( las tres primeras coordenadas )
    vPosition = mvPosition.xyz;
    // Se multiplica por la matriz de proyección y se retorna el valor asignándolo a gl_Position
    gl_Position = projectionMatrix * mvPosition;

    // Al transformar el objeto hay que transformar también las normales 
    // Se normalizan mejor en el fragment que es donde se reciben interpoladas
    vTransformedNormal = normalMatrix * normal;   
}
