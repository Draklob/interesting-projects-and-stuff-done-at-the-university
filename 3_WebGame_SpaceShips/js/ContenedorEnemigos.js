(function(scope){
	function ContenedorEnemigos(){
		this.initialize(); // Es necesario utilizar el this para llamar a los metodos de está ContenedorEnemigos.
	}

	ContenedorEnemigos.prototype = new createjs.Container();
	ContenedorEnemigos.prototype.Container_init = ContenedorEnemigos.prototype.initialize;	// Contenedor sin nada dentro pero con muchas propiedades.

	var enemigos = ContenedorEnemigos.prototype;
	var indiceEnemigos = ContenedorEnemigos.prototype;
	var topeEnemigos = ContenedorEnemigos.prototype;
	var filas = ContenedorEnemigos.prototype;
	var pasos = ContenedorEnemigos.prototype;
	var initY = ContenedorEnemigos.prototype;
	var cargado = ContenedorEnemigos.prototype;

	var vX = ContenedorEnemigos.prototype;
	var vY = ContenedorEnemigos.prototype;

	// Implemento la nueva inicializacion del objeto de la clase.
	ContenedorEnemigos.prototype.initialize = function (){
		//this.Container_init();	//

		this.vX = 6;
		this.vY = 70;
		this.velocity = {x:this.vX, y:0};	// De momento solo se mueven de forma horizontal, no usamos la variable vY en este caso.
		this.init();
	}

	ContenedorEnemigos.prototype.init = function(){
		var self = this;
		this.cargado = false;	// Para saber si se ha cargado, es para no tener que estar preguntando continuamente cuantos enemigos siguen vivos.

		console.log("Inicializando ContenedorEnemigos");

		this.x = 0;
		this.y = 0;

		this.enemigos = new Array();	// Array de los enemigos y testear sobre ellos despues con las colisiones.
		this.indiceEnemigos = 0;		// Cuantos enemigos creados hay previamente.
		this.topeEnemigos = 7;			// Cuantos enemigos por fila.
		this.filas = 3;					// 3 filas de enemigos.
		this.pasos = 0;					// Cuando tienen que ir moviendose los enemigos, antes de que bajen y se acercen más al player.
		this.initY = 60;

		this.snapToPixel = true;

		createjs.Ticker.addListener(this.tickCreador);	// Para crear todos los enemigos de uno en uno, haciendo un efecto mucho mejor.
	};

	ContenedorEnemigos.prototype.tickCreador = function(){
		window.app.contenedorEnemigos.creaEnemigo();	// Llamamos al metodo creaEnemigo en la misma clase de esta forma, hay otras formas tambien usadas antes. Esto permite desde cualquier clase llamar a un metodo de otra cualquiera clase.
	};

	// Nos va servir para modificar la posicion del contenedor. Y es llamada continuamente en cada frame.
	ContenedorEnemigos.prototype.onTick = function(){
		this.x += this.velocity.x;		// Nos permite modificar la velocidad
		this.y += this.velocity.y;		// dependiendo de lo que queramos hacer. En este caso, solo cambiaremos de forma horizontal.

		// Los enemigos se moveran a una velocidad y de izquierda a derecha y viceversa. Llegando a un tope en cada lado. Y con el tiempo se irán moviendo más rapido. Todas estas cosas siempre se pueden configurar o cambiar para crear otros enemigos y tengan otras propiedades.
		if(this.x >= 200 ){
			this.velocity.x = this.velocity.x * -1;
		} else if (this.x <= -200){
			this.velocity.x = this.velocity.x * -1;
			if(this.pasos < 3){
				this.pasos ++;
			}else{					// Aquí bajamos a los enemigos hasta que llegan hasta el final y vuelven a aparecer al principio.
				this.pasos = 0;
				this.y += this.vY;
				if(this.y > 550){
					this.y = 0;
				}
			}
		}
	}

	ContenedorEnemigos.prototype.creaEnemigo = function(){
		if(!this.cargado){
			if(this.indiceEnemigos < this.topeEnemigos * this.filas){	// Si el indice generados es menor que el numero total, sino elimina el ticker.
				var dataEnemigo = {
					images: [app.enemigo.image.src ],
					frames: {width:33, height:31},
					animations: {run:[0,12], boom:[13,24,"boom"]}
				};

				var posY = Math.floor(this.indiceEnemigos / this.topeEnemigos);		// Calculamos la posicion x e y del enemigo
				var posX = this.indiceEnemigos - (posY * this.topeEnemigos);		// Calculamos la posicion x e y del enemigo

				var spriteSheet = new createjs.SpriteSheet(dataEnemigo);	// Generamos el spriteSheet
				var enemigo = new Enemigo(spriteSheet);			// Generamos el enemigo con su constructor de clase
				enemigo.x = 50 + posX * 80;						// Colocamos al enemigo en la posicion correcta
				enemigo.y = this.initY + posY * 60;				// según el enemigo que es y los vaya separando en los ambos ejes.
				this.addChild(enemigo);							// Lo agregamos el enemigo al stage.

				this.enemigos.push(enemigo);					// Lo añadimos al array y sumamos en el indice de enemigos para ir controlando a los enemigos.
				this.indiceEnemigos ++;
			}else{
				this.cargado = true;
				createjs.Ticker.removeListener(this.tickCreador);	// Elimina el listener hasta que vuelva a crear los enemigos.
			}
		}
	};

	// Comprobamos la colision entre el disparo y el enemigo
	ContenedorEnemigos.prototype.testDisparoNave = function (disparo){
		if(disparo != null){									// Cuando hay un disparo, entra y vamos
			for(var i = 0; i < this.enemigos.length; i++){		// comprobando cada enemigo del array
				var enemigo = this.enemigos[i];

				col = ndgmr.checkRectCollision(enemigo,disparo);	// Recuperamos el metodo de colision que queremos utilizar entre el enemigo y el disparo.
				if(col){
					enemigo.explota();				// Ejecutamos el metodo explota de enemigo.
					this.enemigos.splice(i, 1);		// Eliminamos el enemigo concreto del array utilizando splice.
					disparo.destroy();				// Autoeliminamos el disparo.
					this.testTotales();				// Si estan todos eliminados, nos salimos directamente cortando el bucle, ya que no tenemos nada más que
					break;							// testear.
				}
			}
		}
	};

	// Comprobamos la colision entre la nave y el enemigo
	ContenedorEnemigos.prototype.testChoqueNave = function(nave){
		if(nave != null){
			for(var i = 0; i < this.enemigos.length; i++){	// Vamos comprobando cada enemigo
				var enemigo = this.enemigos[i];

				col = ndgmr.checkRectCollision(enemigo,nave);	// Recuperamos el metodo de colision que queremos utilizar entre el enemigo y la nave.
				if(col){
					enemigo.explota();	// Ejecutamos el metodo explota para el sonido y la animacion.
					this.enemigos.splice(i, 1);
					nave.explota();		// Eliminamos la nave ejecutando la funcion de nave.
					break;
				}
			}
		}
	};

	// Comprobamos si el numero de enemigos es igual a 0. Si no quedan enemigos en el array.
	ContenedorEnemigos.prototype.testTotales = function(){
		if(this.enemigos.length == 0){		// Si todos los enemigos del array fueron eliminados.
			this.init();				// Reiniciamos la creacion de enemigos.
			this.acelera();				// Modificamos la dificultad dandole mas velocidad a los enemigos, cada vez que se crean unos nuevos.
		}
	};

	ContenedorEnemigos.prototype.acelera = function(){
		this.velocity.x +=2;		// Esto se puede variar para modificar la dificultad.
	}

	scope.ContenedorEnemigos = ContenedorEnemigos;

}(window));