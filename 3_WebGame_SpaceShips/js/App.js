(function(scope){
	function App(){
		//this.init(); // Es necesario utilizar el this para llamar a los metodos de está clase.
	}

	//var canvas = App.prototype;		// Para crear un metodo(funcion), hay que utilizar prototype y para crear cualquier propiedad. Cualquier propiedad que sea especifica de una instancia de un objeto generado, lo guardaremos utilizando el nombre de la clase.prototype. Con esta estructura se crearán las variables.

	// Creamos todas las variables necesarias para nuestras funciones, añadimos las imagenes utilizadas en el juego y los sonidos, para poder cargarlos.
	var stage = App.prototype,
		canvas = App.prototype,
		cargador = App.prototype,
		bitmap = App.prototype,
		fondo = App.prototype,
		boton = App.prototype,
		nave = App.prototype,	// Para almacenar las referencias de la nave. Que la utilizaremos para la deteccion de colisiones.
		contenedorEnemigos = App.prototype,
		disparo = App.prototype,
		_pfs = App.prototype,
		enemigo = App.prototype,
		SOUNDS = App.prototype,		// Almacenamos la referencia de cada sonido cargado.
		sonidosCargados = App.prototype,	// Numero de sonidos cargados.
		temaMenu = App.prototype,
		temaGameOver = App.prototype,
		scene = App.prototype,

		scene = {
			menu: true,
			gamePlay: false,
			gameOver: false
		},

//		Cargamos todos los sonidos e imágenes en el manifiesto para cargarlo en el PreloadJS
		//Cargamos todas las imagenes para el juego
		rutaNave = new createjs.Bitmap("assets/nave.png"),
		rutaEnemigo = new createjs.Bitmap("assets/enemigo.png"),
		backgroundGameplay = new createjs.Bitmap("assets/nebulosa.jpg"),
		backgroundGameover = new createjs.Bitmap("assets/GameOver.jpg");
		rutaDispNave = new createjs.Bitmap("assets/DisparoNave.png"),
		rutaDispEnemigo = new createjs.Bitmap("assets/DisparoEnemigo.png"),
		backgroundMenu = new createjs.Bitmap("assets/MenuSpaceShip.jpg"),
		startGame = new createjs.Bitmap("assets/StartGame.png"),
		startGameOver = new createjs.Bitmap("assets/StartGameOver.png"),
		VolumeOn = new createjs.Bitmap("assets/VolumeOn.png"),
		VolumeOff = new createjs.Bitmap("assets/VolumeOff.png"),
		menuGameover = new createjs.Bitmap("assets/Menu.png"),
		menuGameoverOver = new createjs.Bitmap("assets/MenuOver.png"),
		restartGameover = new createjs.Bitmap("assets/Restart.png"),
		restartGameoverOver = new createjs.Bitmap("assets/RestartOver.png"),

		queue = new createjs.LoadQueue(false),
		registeredPlugins = createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin]),

		// Cargamos todos los sonidos en el manifiesto para el juego, con su url y un identificador para después poder llamarlos
		pathSounds = "sounds/",
		manifestSounds= [{src:pathSounds + "fire.mp3|" + pathSounds + "fire.ogg", id:"fire"},
						{src:pathSounds + "boom.mp3|" + pathSounds + "boom.ogg", id:"boom"},
						{src:pathSounds + "pop.mp3|" + pathSounds + "pop.ogg", id:"pop"},
						{src:pathSounds + "GameOver.mp3|" + pathSounds + "GameOver.ogg", id:"themeGameOver"},
						{src:pathSounds + "loopMenu.mp3|" + pathSounds + "loopMenu.ogg", id:"themeMenu"}],

		pathImages = "assets/",
		manifestImages= [{src:pathImages + "nave.png"},
						{src:pathImages + "enemigo.png"},
						{src:pathImages + "nebulosa.jpg"},
						{src:pathImages + "DisparoNave.png"},
						{src:pathImages + "DisparoEnemigo.png"},
						{src:pathImages + "MenuSpaceShip.jpg"},
						{src:pathImages + "GameOver.jpg"},
						{src:pathImages + "VolumeOn.png"},
						{src:pathImages + "VolumeOff.png"},
						{src:pathImages + "Menu.png"},
						{src:pathImages + "MenuOver.png"},
						{src:pathImages + "Restart.png"},
						{src:pathImages + "RestartOver.png"},
						{src:pathImages + "StartGame.png"},
						{src:pathImages + "StartGameOver.png"}];

		//Precargamos todas las imágenes y sonidos configurando una cola y creando un array.
		queue.loadManifest(manifestImages);
		queue.loadManifest(manifestSounds);

		// Cargamos la cola y detectamos por consola si se completo y el progreso.
		queue.load();
		queue.addEventListener("complete", showManifest);
		queue.addEventListener("progress", showProgress);
		// Nos va mostrando el progreso por pantalla
		function showProgress(evt) {
			var perc = evt.loaded / evt.total;
			console.log(Math.ceil(perc*100).toString());
		}

		// Cuando termina de cargar todo los elementos que utiliza nuestro juego, nos avisa de que los archivos estan cargados y se inicia el juego.
		function showManifest() {
			console.log("Files are loaded");
			app.init();
		}

		// Registramos el manifiesto para poder utilizar los sonidos
		createjs.Sound.registerManifest(manifestSounds);

	// Funcion inicial que establecemos el canvas y arrancamos el juego.
	App.prototype.init = function (){		// Con esta estructura iremos creando los metodos(funciones). Y entre () los parametros que queramos pasar.

		this.canvas = document.getElementById("gamespace");	// Creamos aquí una variable local para que después no la tengamos consumiendo fuera de la función.
		this.stage = new createjs.Stage(this.canvas);	// Declaramos el lienzo. Para crear la zona donde trabaja el framework CreateJS. Una base nueva para trabajar en el canvas y con las propieades que tienen el framework.
		this.sceneMenu();
	};

	// Cargamos el MENU y la scene inicial del juego.
	App.prototype.sceneMenu = function(){
		if(scene.menu){
			console.debug("CargandoMenu");
			var self = this;

			this.temaMenu = createjs.Sound.play("themeMenu");
			this.temaMenu.setVolume(0.6);
			this.temaMenu.addEventListener("complete", repeatMenuSound);

			// Establecemos que la imagen empiece desde la esquina superior izquierda en el ( 0, 0 )
			backgroundMenu.x = 0;
			backgroundMenu.y = 0;

			// Establecemos el icono del volumen en la esquina superior derecha
			VolumeOn.x = VolumeOff.x = this.canvas.width - VolumeOn.image.width - 25;
			VolumeOn.y = VolumeOff.y = 25;

			// Centramos el botón Start Game y StartGameOver en el mismo sitio de la pantalla principal
			startGame.x = startGameOver.x = this.canvas.width / 2 - startGame.image.width / 2;	// Cogemos el centro del canvas para centrarlo.
			startGame.y = startGameOver.y = this.canvas.height / 2 - startGame.image.height / 2;	// Cogemos el centro del canvas para centrarlo.

			this.stage.addChild(backgroundMenu,startGame,VolumeOn);
			this.stage.update();
			//createjs.Tween.get(startGame);

			// Activamos el MouseOver, sino no funciona por defecto.
			this.stage.enableMouseOver(20);

			// Creamos la zona del boton StartGame para hacer eventos de ratón dibujando una área rectangunlar con el tamaño de la imagen.
			var hitAreaStartGame = new createjs.Shape();
			hitAreaStartGame.graphics.beginFill("#000").drawRect(0, 0, startGame.image.width, startGame.image.height);
			startGame.hitArea = hitAreaStartGame;

			// Creamos la zona del boton VolumeOn para hacer eventos de ratón dibujando una área circular con el tamaño de la imagen.
			var hitAreaVolumeOn = new createjs.Shape();
			hitAreaVolumeOn.graphics.beginFill("#000").drawCircle(0, 0, 55);
			VolumeOn.hitArea = hitAreaVolumeOn;
			VolumeOff.hitArea = hitAreaVolumeOn;

			// Eventos para cuando hagamos algo con el ratón sobre Start Game, que lance dicho metodo.
			startGame.addEventListener("mouseover", function(){
				self.stage.addChild(startGameOver);
				self.stage.update(); });
			startGame.addEventListener("mouseout", function(){
				self.stage.removeChild(startGameOver);
				self.stage.addChild(startGame);
				self.stage.update(); });
			startGame.addEventListener("click", function(){
				console.log("Hiciste click en Start Game. Empieza el juego.");
				scene.menu = false;
				scene.gamePlay = true;
				self.clearScene();
				self.sceneGameplay(); });

			VolumeOn.addEventListener("click", function(){ self.desactiveSound(); });
			VolumeOff.addEventListener("click", function(){ self.activeSound(); });

			//createjs.Ticker.addEventListener("tick", this.tick);
		}
		// Función que se encarga de repetir el audio del menu (LOOP)
		function repeatMenuSound() {
			console.log("LoopMenuSound");
		    self.temaMenu.play("themeMenu");
		}
	}

	// Desactivamos el sonido cuando hacemos click en el icono del volumen y está activado.
	App.prototype.desactiveSound = function(){
		if(scene.menu){
			console.log("Sonido Off");
			this.temaMenu = createjs.Sound.stop("themeMenu");
			this.stage.removeChild(VolumeOn);
			this.stage.addChild(VolumeOff);
			this.stage.update();
			removeEventListener("complete", this.repeatSound);
		}
	}

	// Activamos el sonido cuando hacemos click en el icono del volumen y está desactivado.
	App.prototype.activeSound = function(){
		var self = this;
		console.log("Sonido On");
		this.stage.removeChild(VolumeOff);
		this.stage.addChild(VolumeOn);
		this.stage.update();
		this.temaMenu = createjs.Sound.play("themeMenu");
		this.temaMenu.setVolume(0.6);
		this.temaMenu.addEventListener("complete", repeatSound);
		function repeatSound() {
			console.log("LoopMenuSound");
		    self.temaMenu.play("themeMenu");
		}
	}

	// Aquí se encarga de limpiar la escena para eliminar todo lo de la pantalla para luego añadir lo nuevo.
	App.prototype.clearScene = function(){
		// menu = false y gameOver = false
		if(!scene.menu && !scene.gameOver){
			console.log("Escena menu limpiada.");
			this.stage.enableMouseOver(0);		// Desactivamos el mouseover / mouseout.
			createjs.Sound.stop("themeMenu");	// Paramos el sonido del menú
			this.stage.removeAllChildren();		// Borramos y limpiamos todo lo que tenemos en canvas para añadir la nueva escena.
			startGame.removeAllEventListeners();
			startGame.removeAllEventListeners("click");
			createjs.Ticker.addEventListener("tick", this.stage);

		}
		// menu = false y gameplay = false
		if(!scene.menu && !scene.gamePlay){
			console.log("Escena Gameplay limpiada.");
			//createjs.Sound.stop("themeMenu");	// Paramos el sonido del menú
			this.contenedorEnemigos.removeAllChildren();	// Eliminamos todas las imagenes de los enemigos.
			this.contenedorEnemigos.enemigos.length = 0;	// Eliminamos todos los enemigos del contenedor ( Se vacia el Array):
			this.stage.removeAllChildren();		// Borramos y limpiamos todo lo que tenemos en canvas para añadir la nueva escena.
			this.stage.removeAllEventListeners();
			createjs.Ticker.removeListener(app.tickCreaEnemigos);
		}
		// gameplay = false y gameover = false =>> Para ir a menu
		if(!scene.gamePlay && !scene.gameOver){
			console.log("Escena GameOver a Menu limpiada.");
			createjs.Sound.stop("themegameOver");	// Paramos el sonido del menú
			this.stage.removeAllChildren();		// Borramos y limpiamos todo lo que tenemos en canvas para añadir la nueva escena.
			menuGameover.removeAllEventListeners("mouseout");
			menuGameover.removeAllEventListeners("mouseover");
			menuGameover.removeAllEventListeners("click");
			createjs.Ticker.removeEventListener("tick", this.stage);
		}
		// menu = false y gameover = false =>> Para ir a gameplay
		if(!scene.menu && !scene.gameOver){
			console.log("Escena GameOver a Gameplay limpiada.");
			//createjs.Sound.stop("themeMenu");	// Paramos el sonido del menú
			this.stage.removeAllChildren();		// Borramos y limpiamos todo lo que tenemos en canvas para añadir la nueva escena.
			this.stage.enableMouseOver(0);
			menuGameover.removeAllEventListeners("mouseout");
			menuGameover.removeAllEventListeners("mouseover");
			menuGameover.removeAllEventListeners("click");
			createjs.Ticker.addEventListener("tick", this.stage);
		}
	}

	// Método para hacer fadeOut / fadeIn para los cambios de escena
	App.prototype.swapScene = function(){
	}

	// Aqui se inicia la escena de Gameplay y la carga de todo lo necesario para el funcionamiento del juego.
	App.prototype.sceneGameplay = function (){
		this.enemigo = rutaEnemigo;
		var self = this;



		backgroundGameplay.alpha = 0.8;				// Hacemos algo transparente el fondo para que se vean mejor los elementos de encima.
		this.stage.addChild(backgroundGameplay);	// Añadimos al stage el fondo del gameplay.

		// Aquí creo la nave animada.
		// El createJS se encarga de controlar los spritesheets. Como se tienen que visualizar, que animaciones van dentro y otras utilidades.
		var dataNave = {
			images: [rutaNave.image.src],	// Informacion de la imagen, que está guardada en el cargador.
			frames: {width:103, height:90},		// Ancho y alto del objeto
			animations: {run:[0,19], fire:[19,47,"fire"], boom:[48,71,"boom"]}	// Primera animacion run(del Frame 0 al 19), en marcha(del Frame 19 al 47 y la asociamos a un nombre (fire) y la última boom cuando explota (del frame 48,71)). Los frames se saben viéndolos en la linea de tiempo de la animacion creada en Adobe Flash.
		};

		// Para crear el spriteSheet, necesitamos crear un objeto como dataNave con estos datos => Images(añadimos la imagen), frames(el alto y el ancho) y animations(listado de las animaciones). Creamos el spriteSheet con una variable local utilizando la clase SpriteSheet.
		var spriteSheet = new createjs.SpriteSheet(dataNave);	// Le pasamos los datos de la nave dentro.
		this.nave = new Nave(spriteSheet);	// Asociamos a la nave y el spritesheet.
		this.stage.addChild(this.nave);	// Lo añadimos al stage para que se muestre.

		// Después de tener las imagenes cargadas y tener la información sobre ellas.
		var self = this;
		createjs.Ticker.setFPS(30);
		createjs.Ticker.addListener(function(e){
				self.tick();
			}
		);

		// Cargamos los enemigos
		createjs.Ticker.addListener(this.tickCreaEnemigos);	// Agregamos un ticker para crear los enemigos cada cierto tiempo

		//console.debug(this.canvas); // Nos permitiria ver un elemento, en este caso el del canvas. Puede ser tipo log(devuelve información sobre el) o debug(permite directamente, observar todo lo relacionado con ese elemento en la consola).
	}

	// Creamos los enemigos en el mapa
	App.prototype.tickCreaEnemigos = function(){;
		if(scene.gamePlay){
			var punto = app.nave.localToGlobal(app.nave.x, app.nave.y);
			if(punto.y > 800){
				//creo el contenedor donde van los enemigos
				var contenedor = new ContenedorEnemigos();
				app.stage.addChild(contenedor);
				contenedorEnemigos.x = 0;
				contenedorEnemigos.y = 0;
				app.contenedorEnemigos = contenedor;
				createjs.Ticker.removeListener(app.tickCreaEnemigos);

				app.stage.addEventListener("stagemousedown", function(e){	// El stage que esté atento que cuando hacemos click con el raton.
				app.handleMouseDown(e); });
			}
		}
	};

	// Metodo que se ejecuta continuamente en cada tick.
	App.prototype.tick = function (e){
		// Mientras estemos en la pantalla de Menu, entrará aquí.
		if(scene.menu){
			console.log("Estamos en la pantalla de Menú");
			// Cuando scene.menu sea falso, eliminamos el evento de escucha de la pantalla de menu, porque pasamos a otra pantalla.
			if(!scene.menu){
				console.log("Eliminamos evento tick de Menu");
				//removeEventListener("tick", this.tick());
			}
		}
		// Mientras estemos en la pantalla de Gameplay, entrará aquí.
		if(scene.gamePlay){
			console.log("Estamos en la pantalla de Gameplay");
			if(this.contenedorEnemigos && !this.nave.explotando){
				this.contenedorEnemigos.testDisparoNave(this.disparo);	// Ejecutamos el metodo contenedor de disparo de la nave, le pasamos el disparo.
				this.contenedorEnemigos.testChoqueNave(this.nave);		// Comprobamos el metodo contenedor de si choca la nave, pasandole la nave.
			}

			// Le quitamos la velocidad al player para que cuando explota no pueda seguir moviendose.
			if(this.nave.explotando)
			{
				this.nave.velocity.x = 0;
				this.nave.velocity.y = 0;
			}
		}
		if(scene.gameOver){
			console.log("Estamos en la pantalla de GameOver");
		}
	};

	// Mientras la nave esté viva y no explote, responderá a los clicks que hagamos en el stage.
	App.prototype.handleMouseDown = function (e){
		if(!this.nave.explotando)
			this.nave.salta(e);
	};

	// Aqui creamos el disparo de la nave.
	App.prototype.disparoNave = function(posX, posY){
		if(this.disparo == null){	// Sino existe el disparo.
			this.disparo = new DisparoNave(rutaDispNave.image.src, posX, posY);	// Creamos un disparo con la imagen y en la posicion que está la nave.
		}
	};

	// Metodo que nos lleva a la pantalla de Game Over, si la nave es destruida y termina el juego. Preparamos la carga de la escena.
	App.prototype.gameOver = function(){
		scene.gamePlay = false;
		scene.gameOver = true;
		this.clearScene();
		this.sceneGameover();
	};

	// Cargamos toda la escena GameOver.
	App.prototype.sceneGameover = function(){
		if(scene.gameOver){
			console.debug("Cargamos escena GameOver");
			var self = this;

			this.temaGameOver = createjs.Sound.play("themeGameOver");
			this.temaGameOver.setVolume(0.8);
			this.temaGameOver.addEventListener("complete", repeatGameOverSound);

			backgroundGameover.x = 0;
			backgroundGameover.y = 0;

			restartGameover.x = restartGameoverOver.x = this.canvas.width / 2 - restartGameover.image.width / 2;	// Cogemos el centro del canvas para centrarlo.
			restartGameover.y = restartGameoverOver.y = this.canvas.height / 2 - restartGameover.image.height / 2 - 20;	// Cogemos el centro del canvas para centrarlo.
			menuGameover.x = menuGameoverOver.x = this.canvas.width / 2 - menuGameover.image.width / 2;	// Cogemos el centro del canvas para centrarlo.
			menuGameover.y = menuGameoverOver.y = this.canvas.height / 2 - menuGameover.image.height / 2 + 50;	// Cogemos el centro del canvas para centrarlo.

			this.stage.addChild(backgroundGameover,restartGameover,menuGameover);

			// Activamos el MouseOver, sino no funciona por defecto.
			this.stage.enableMouseOver(20);

			// Creamos la zona del boton StartGame para hacer eventos de ratón dibujando una área rectangunlar con el tamaño de la imagen.
			var hitAreaRestartGame = new createjs.Shape();
			hitAreaRestartGame.graphics.beginFill("#000").drawRect(0, 0, restartGameover.image.width, restartGameover.image.height);
			restartGameover.hitArea = hitAreaRestartGame;

			var hitAreaMenuGame = new createjs.Shape();
			hitAreaMenuGame.graphics.beginFill("#000").drawRect(0, 0, menuGameover.image.width, menuGameover.image.height);
			menuGameover.hitArea = hitAreaMenuGame;

			var self = this;
			// Eventos para cuando hagamos algo con el ratón sobre Start Game, que lance dicho metodo.
			//	Eventos ratón para el botón Restart
			restartGameover.addEventListener("mouseover", function(){
				self.stage.addChild(restartGameoverOver);
				self.stage.update(); });
			restartGameover.addEventListener("mouseout", function(){
				self.stage.removeChild(restartGameoverOver);
				self.stage.addChild(restartGameover);
				self.stage.update(); });
			restartGameover.addEventListener("click", function(){
				console.log("Hiciste click en Restart. Reiniciamos el juego.");
				scene.menu = false;
				scene.gameOver = false;
				scene.gamePlay = true;
				self.clearScene();
				self.sceneGameplay(); });

			//	Eventos ratón para el botón Menu
			menuGameover.addEventListener("mouseover", function(){
				self.stage.addChild(menuGameoverOver);
				self.stage.update(); });
			menuGameover.addEventListener("mouseout", function(){
				self.stage.removeChild(menuGameoverOver);
				self.stage.addChild(menuGameover);
				self.stage.update(); });
			menuGameover.addEventListener("click", function(){
				console.log("Hiciste click en Menu.");
				scene.gamePlay = false;
				scene.gameOver = false;
				scene.menu = true;
				self.clearScene();
				self.sceneMenu(); });

			//createjs.Ticker.addEventListener("tick", this.stage);
		}
			function repeatGameOverSound() {
			console.log("LoopGameOverSound");
		    self.temaGameOver.play("themeGameOver");
		}
	};

	scope.App = App; // En el final de la construccion de esta clase, sobre el espacio de nombres que queramos crear esa clase se va añadir una propiedad en este caso que le iguale a la misma función.
}(window));

// No queremos que se ejecute el codigo hasta que se carge todo el html necesario. Para ello utilizamos el evento onload. Hacemos esto =>>

window.onload = function(){
	this.app = new App();	// Ejecutamos la clase y inicia el método init()
}