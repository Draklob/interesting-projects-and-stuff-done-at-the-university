(function(scope){
	// Comstructor de Nave
	function Nave(spriteSheet){
		this.initialize(spriteSheet);
	}

	//var canvas = Nave.prototype;		// Con esta estructura se crearán las variables.
	Nave.prototype = new createjs.BitmapAnimation();	// Creamos un BitmapAnimation con sus propias caracteristicas.
	Nave.prototype.BitmapAnimation_init = Nave.prototype.initialize;	// Llamamos a las tareas del metodo initialize. Su inicializador del BitmapAnimation  es añadiendo _init. Pidiendo que se inicialize.

	var ancho = Nave.prototype;
	var alto = Nave.prototype;
	var explotando = Nave.prototype;
	var x = Nave.prototype;
	var y = Nave.prototype;


	// Inicializamos la Nave!!
	Nave.prototype.initialize = function (spriteSheet){
		this.BitmapAnimation_init(spriteSheet);	// Aqui llamamos al inicializador propio de la clase BitmapAnimation de createJS. Asignándole todos los metodos y propiedades de la clase BitmapAnimation.

		var self = this;

		console.log("Inicializando Nave");	// Para comprobar que se le asignan las dichas propiedades y metodos.

		this.gotoAndPlay("run");	// Para ejecutar una animacion concreta, utilizamos este metodo correr la animacion. Que está definido en App en animations. animations: {run:[0,19], fire:[19,47,"fire"], boom:[48,71,"boom"]}

		// Lo colocamos en un punto concreto de la pantalla
		this.x = 256;
		this.y = -60;
		this.ancho = spriteSheet.getFrame(0).rect.width;	// Almacenamos el tamaño de la nave utilizando el metodo del framework para que devuelva el tamaño
		this.alto = spriteSheet.getFrame(0).rect.height;	//  del rectacntulo de un fotograma concreto devolviendo su alto y su ancho.

		// Ejecuto el resto de valores
		this.snapToPixel = true;	// Esto hace que en el cualquier momento que el valor de una posicion de la nave no son exactos, sino que son decimales. Los pone de una manera exacta para ahorrar problemas de rendimiento y de visualizacion.
		this.velocity = {x:0,y:-15};		// La utilizamos para ponerle una velocidad de entrada a la animacion. Lo que va haciendo utilizando una velocidad. En este caso la "y" es negativa, para que cuando empiece, caiga para abajo.

		this.addEventListener("mousedown", function(e){		// Está atento a los eventos de raton
			self.handleMouseDown(e);	// Y llamamos a la función.
		});
	};

	// Este onTick se utiliza para recargar la imagen del juego, refrescando continuamente. Llama al evento de App tick cada vez que hay un fotograma nuevo.
	Nave.prototype.onTick = function (){
		// Vamos frenando la nave mientras va cayendo al empezar el juego.
		this.velocity.y +=1;
		if(this.velocity.y < 0 || this.y < 500){	// Siempre que esté cayendo ( y negativo) o su posicion sea menor de 500, irá cayendo poco a poco.
			this.y += this.velocity.y;
		}

		// Testeamos la velocidad horizontal para que vaya modificandose. Si es mayor o menor a 0, va reduciendo o sumándole la velocidad horizontal.
		if(this.velocity.x != 0){
			if(this.velocity.x > 0){
				this.velocity.x -=1;
			}else{
				this.velocity.x +=1;
			}

			this.x += this.velocity.x;
		}

		if(this.currentFrame == 44){		// Comprobamos en que frame concreto en el que está la animacion. Esto lo permite hacer el framework.
			this.gotoAndPlay("run");		// Cuando casi termina la animacion del juego, vuelve a run. Esto es variable, podemos cortar o esperar a que
		}else if(this.currentFrame == 71){	// termine la animación, para hacer lo que queramos en dicho frame o ejecutar una animacion en este caso.
			window.app.gameOver();				// Llamamos al metodo Game Over cuando termina la animacion de explosion de la nave.
			//this.parent.removeChild(this);	// Y lo eliminamos del stage.
		}
	};

	Nave.prototype.salta = function (e){	// Dependiendo donde hagamos click en el stage hará una cosa u otra.
		if((e.stageX >= this.x) && (e.stageX < this.x + this.ancho)){	 // En el caso de que hagamos click en la nave, dispará.
		}else if(e.stageX > this.x){	// Esta zona del stageX es para que se mueva de forma horizontal hacia la izquierda o derecha.
			//window.app.playFire();	// Llamamos a la funcion playFire de App.
			this.velocity.x = +10;		// Se mueve hacia la derecha
		}else if(e.stageX < this.x){
			//window.app.playFire();
			this.velocity.x = -10;		// Se mueve hacia la izquierda
		}

		if(e.stageY <= this.y - 20){	// Esta zona es sobre el stageY para que se mueva de forma verticalmente.
			//window.app.playFire();
			this.velocity.y = -15;		// Para que caiga la nave
		}else if(e.stageY > this.y - 20 && e.stageY < this.y + this.alto){ // Si hacemos click sobre la zona del eje Y donde está la nave, parará de subir,
			this.velocity.y = +0;										   // parando la nave y poder controlarla.
		}else{
			//window.app.playFire();
			this.velocity.y = +15;	// Subimos la nave
		}

		this.gotoAndPlay("fire");	// Ejecutamos la animacion de movimiento de la nave.
	};

	// Evitamos que si la nave explota, pueda disparar. Sino, si podrá disparar.
	Nave.prototype.handleMouseDown = function(e){
		if(!this.explotando){
			this.dispara();
		}
	}

	Nave.prototype.dispara = function (e){
		this.velocity.y = 0;
		this.velocity.x = 0;
		this.gotoAndPlay("run");	// Mientras dispara, que no ejecute la animacion de movimiento FIRE.

		window.app.disparoNave(this.x + this.ancho / 2 - 8, this.y);		// Llamamos al metodo para que dispare la nave.
	}

	Nave.prototype.explota = function(){

		this.explotando = true;		// Lo ponemos a true, para que no pueda seguir disparando la nave.
		//window.app.playBoom();
		this.gotoAndPlay("boom");	// Ejecutamos la animacion boom.

	}

	scope.Nave = Nave;

}(window));