(function(scope){
	function DisparoNave(image, posX, posY){
		this.initialize(image, posX, posY);
	}

	DisparoNave.prototype = new createjs.Bitmap();		// Creamos un nuevo objeto bitmap
	DisparoNave.prototype.Bitmap_init = DisparoNave.prototype.initialize;

	var ancho = DisparoNave.prototype;

	DisparoNave.prototype.initialize = function (image, posX, posY){
		this.ancho = 6;

		this.Bitmap_init(image);	// Lo inicializamos el objeto
		this.x = posX;
		this.y = posY;
		app.stage.addChild(this);	// Lo añadimos al stage

		this.snapToPixel = true;
		this.velocity = {x:0, y:-15};

		//window.app.playPop();
	};

	DisparoNave.prototype.onTick = function(){
		if(this.y < 0){		// Si llega arriba de todo se elimina del stage
			this.destroy();
		}else if(this.velocity.y < 0){		// Sino llega y se está moviendo, vamos sumandole la velocidad para que suba. (( No es necesaria))
			this.y += this.velocity.y;		// Se puede cambiar esto para conseguir otro cualquier efecto diferente en el disparo.
		}
	}

	// Metodo para eliminar el disparo cuando sea necesario.
	DisparoNave.prototype.destroy = function(){
			window.app.disparo = null;
			app.stage.removeChild(this);
	}

	scope.DisparoNave = DisparoNave;

}(window));