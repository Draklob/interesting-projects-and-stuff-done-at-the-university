(function(scope){
	function Cargador(){
		this.initialize(); // Es necesario utilizar el this para llamar a los metodos de está clase.
	}

	//var canvas = Cargador.prototype;		// Con esta estructura se crearán las variables.
	var cargadas = Cargador.prototype;
	var totales = Cargador.prototype;
	var onComplete = Cargador.prototype;

	Cargador.prototype.initialize = function (){	// Con esta estructura iremos creando los metodos(funciones). Y entre () los parametros que queramos pasar.
		//console.log("Cargador generado");
	}

	// Creamos un metodo para efectuar dicha carga
	Cargador.prototype.loadImagenes = function (lista){
		this.cargadas = 0;
		this.totales = lista.length;
		for(i = 0; i < this.totales; i++)
		{
			this.cargarImagen(lista[i]);	// Vamos llamando cada imagen a traves de la ruta de la lista
		}
	};

	// Para cargar una imagen concreta
	Cargador.prototype.cargarImagen = function (ruta){
		var self = this;
		var image = new Image();
		this[ruta] = image;
		image.onload = function(e){		// Que nos avise cuando se carge la imagen. Para ello utilizamos un detector de eventos
			self.imagenCargada(e);
		};
		image.src = image.url = ruta;
	};

	// Recorre cada imagen cargada para comprobar que se cargan todas las imagenes
	Cargador.prototype.imagenCargada = function (e){
		this.cargadas ++;
		if(this.cargadas == this.totales){
			if(this.onComplete){
				this.onComplete();
			}
			else{
				console.log("onComplete no definida");
			}
		}
	};

/*	// Creamos un metodo para cargar los sonidos.
	Cargador.prototype.loadSonidos = function(lista){
		this.cargadas = 0;
		this.totales = lista.length;

		var registeredPlugins = createjs.Sound.registerPlugins([	// Registramos todos los plugins que vamos a utilizar.
			createjs.CocoonJSAudioPlugin,
			createjs.WebAudioPlugin,
			createjs.HTMLAudioPlugin
		]);

		for(i = 0; i < this.totales; i++)
		{
			this.cargarSonido(lista[i]);
		}
	}

	Cargador.prototype.cargarSonido = function(ruta){
		var sound = new Sound();
		this[ruta] = sound;
		sound.src = sound.url = ruta;

		// Si son creados correctamente =>>
		if(registeredPlugins){
			createjs.Sound.addEventListener("loadComplete", createjs.proxy(this.soundsLoaded,this)); // Llamamos a la funcion soundsLoad para cargar el sonido.
			SOUNDS.SOUNDS = {};
			SOUNDS.SOUNDS.FIRE = 'sfx/fire.m4a|sfx/fire.ogg';	// Ruta de los archivos del sonido cargado.
			createjs.Sound.registerSound(SOUNDS.SOUNDS.FIRE, "FIRE", 4);	// Aquí registramos el sonido de donde estará almacenado para ponerlo en uso. Le llamamos "FIRE" para utilizarlo y el número de veces que puede sonar ese sonido a la vez.
			SOUNDS.SOUNDS.BOOM = 'sfx/boom.m4a|sfx/boom.ogg';
			createjs.Sound.registerSound(SOUNDS.SOUNDS.BOOM, "BOOM", 3);
			SOUNDS.SOUNDS.POP = 'sfx/pop.m4a|sfx/pop.ogg';
			createjs.Sound.registerSound(SOUNDS.SOUNDS.POP, "POP", 3);
		}
	}
*/

	scope.Cargador = Cargador; // En el final de la construccion de esta clase, sobre el espacio de nombres que queramos crear esa clase se va añadir una propiedad en este caso que le iguale a la misma función.
}(window));