(function(scope){
	//constructor de la clase
	function Enemigo(spriteSheet){
		this.initialize(spriteSheet); // Es necesario utilizar el this para llamar a los metodos de está Enemigo.
	}

	//var canvas = Enemigo.prototype;		// Con esta estructura se crearán las variables.

	Enemigo.prototype = new createjs.BitmapAnimation();
	Enemigo.prototype.BitmapAnimation_init = Enemigo.prototype.initialize;

	var ancho = Enemigo.prototype;
	var alto = Enemigo.prototype;

	// Implemetanmos la nueva inicializacion del objeto
	Enemigo.prototype.initialize = function (spriteSheet){
		this.BitmapAnimation_init(spriteSheet);

		this.gotoAndPlay("run");

		this.ancho = spriteSheet.getFrame(0).rect.width;
		this.alto = spriteSheet.getFrame(0).rect.height;

		this.snapToPixel = true;
	};

	Enemigo.prototype.onTick = function(){
		if(this.currentFrame == 24 ){	// Cuando termina la animacion de explosion, eleminamos al enemigo.
			this.parent.removeChild(this);
		}
	}

	Enemigo.prototype.eliminaEnemigo = function(){

			this.parent.removeChild(this);

	}

	// Metodo para ejecutar el sonido y la animacion de explosion.
	Enemigo.prototype.explota = function(){
		//window.app.playBoom();
		this.gotoAndPlay("boom");
	};

	scope.Enemigo = Enemigo;

}(window));