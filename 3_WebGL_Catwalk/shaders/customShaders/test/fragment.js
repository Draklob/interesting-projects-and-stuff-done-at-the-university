precision highp float;

//Las coordenadas de textura
varying vec2 v_textCoord;
// La textura
uniform sampler2D u_texture;
// El tamaño de la textura
uniform vec2 u_textureSize;

//Un array de 9 elementos ( el texel central se corresponderá con el valor 4 )
uniform float u_kernel[9];

//Para saber qué modificación del shader aplicar e ir cambiando
//a modo de ejemplo
uniform float index;

//Multiplicador que nos da la opacidad
uniform float opacity;

void main(void) 
{
    // Standard sampling procedure. Just make sure
    // you've passed the uv coords varying.
    if( index == 0.0 )
    {
	    gl_FragColor = opacity * texture2D(u_texture, v_textCoord);
    }
	else if( index == 1.0 )
	{
		gl_FragColor = opacity * texture2D(u_texture, v_textCoord).rbga;
	}
	else if( index == 2.0 )
	{
		gl_FragColor = opacity * texture2D(u_texture, v_textCoord).brga;
	}
	else if( index == 3.0 )
	{
		gl_FragColor = opacity * texture2D(u_texture, v_textCoord).bgra;
	}
	else if( index == 4.0 )
	{
		gl_FragColor = opacity * texture2D(u_texture, v_textCoord).grba;
	}
	else if( index == 5.0 )
	{
		gl_FragColor = opacity * texture2D(u_texture, v_textCoord).gbra;
	}
	else if( index == 6.0 ) //Blur sencillo
	{
		// Para mover las coordenadas de textura un texel 
		vec2 oneTexel = vec2(1.0, 1.0) / u_textureSize;
		//Hace un promedio del texel que se recibe del vertex shader 
		//y los de los lados
		/*gl_FragColor = opacity * ( u_texture2D(u_texture, v_textCoord) +
			u_texture2D(u_texture, v_textCoord + vec2(onePixel.x, 0.0)) +
			u_texture2D(u_texture, v_textCoord + vec2(-onePixel.x, 0.0))/3;*/
		
		gl_FragColor = opacity * ( texture2D(u_texture, v_textCoord) 
						+ texture2D(u_texture, v_textCoord + vec2(oneTexel.x, 0.0))
						+ texture2D(u_texture, v_textCoord - vec2(oneTexel.x, 0.0)) )/ 3.0;
	}
	else if( index == 7.0 || index == 8.0 )
	{
		// Para mover las coordenadas de textura un texel 
		vec2 oneTexel = vec2(1.0, 1.0) / u_textureSize;
		//Calculamos el valor de la suma de los ocho texel adyacentes más el propio en el centro
		//Multiplicados por los pesos indicados en la matriz pasada desde la aplicación
		//Esto indica cuánto se tiene en cuenta cada texel
		vec4 colorSum = texture2D(u_texture, v_textCoord + oneTexel * vec2(-1, 1) ) * u_kernel[0] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2( 0,-1) ) * u_kernel[1] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2( 1,-1) ) * u_kernel[2] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2(-1, 0) ) * u_kernel[3] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2( 0, 0) ) * u_kernel[4] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2( 1, 0) ) * u_kernel[5] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2(-1, 1) ) * u_kernel[6] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2( 0, 1) ) * u_kernel[7] +
						texture2D(u_texture, v_textCoord + oneTexel * vec2( 1, 1) ) * u_kernel[8] ;
		//Calculamos el valor de la suma de los pesos
		float kernelWeight = u_kernel[0] +
							 u_kernel[1] +
							 u_kernel[2] +
							 u_kernel[3] +
							 u_kernel[4] +
							 u_kernel[5] +
							 u_kernel[6] +
							 u_kernel[7] +
							 u_kernel[8] ;

		//Control de seguridad para no dividir por cero
		if( kernelWeight <= 0.0 )
		{
			kernelWeight = 1.0;
		}

		gl_FragColor = opacity * vec4( ( colorSum / kernelWeight).rgb, 1.0 );
	
	}
}