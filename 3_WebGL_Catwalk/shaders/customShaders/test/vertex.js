
varying vec2 v_textCoord;

void main() 
{
    v_textCoord = uv;
    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}

/*
attribute position: vec3, the vertex itself
attribute normal: vec3, the normal at the current vertex
attribute uv: vec2, the texture coord
uniform projectionMatrix: mat4, self explanatory
uniform modelMatrix: mat4, object-to-world matrix
uniform viewMatrix: mat4, world-to-camera matrix
uniform modelViewMatrix: mat4, same as viewMatrix*modelMatrix, or object-to-camera matrix
*/

/* Para ver los filtros de las texturas
http://threejs.org/docs/#Reference/Textures/Texture
*/

/* UniformsLib
https://github.com/mrdoob/three.js/blob/master/src/renderers/shaders/UniformsLib.js
*/