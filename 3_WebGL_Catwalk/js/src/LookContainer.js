function LookContainer( _id, _size, _initialPosition, _initialRotation,
							 _paths, _speed )
{
	//Para identificarlo cuando hagamos click sobre un cubo con foto
	this.id = _id;
	//Las dimensiones de cada cubo para mostrar las fotos del look
	this.width = _size.width;
	this.height = _size.height;
	this.depth = _size.depth;
	//La posición inicial del cubo que muestra las fotos del look
	this.initialPosition = _initialPosition;
	//La rotación inicial del cubo que muestra las fotos del look
	this.initialRotation = _initialRotation;
	//Creamos el contenedor 3D para todos los objetos que componen el look
	this.lookContainer3D = new THREE.Object3D();
	this.lookContainer3D.position = this.initialPosition;
	this.lookContainer3D.rotation = this.initialRotation;
	//Los diferentes materiales que intercambiamos
	//para mostrar la foto en la cara delantera del cubo
	this.standardLookMaterial;
	this.glowLookMaterial;
	this.infoPlaneMaterial;
	
	//Guardamos una referencia al cubo con la foto para poder hacer click en ella
	this.pictureCubeMesh;
	//Guardamos una referencia al plano que mostramos cuando hacemos click
	this.infoPlane;
	//Guardamos una referencia al dome del foco 
	this.dome;
	//Máquina de estados para el contenedor ( para saber cuándo hemos hecho click, etc... )
	//La dirección en la que se ha puesto el último suelo
	this.MAINSTATES = { NONE:0, STOP:1, MOVING:2, SELECTED:3 };
	this.currentMainState = this.MAINSTATES.MOVING;
	this.SELECTEDSTATES = { NONE:0, FADE_IN:1, FADE_OUT:2, SHOWING:3 }
	this.currentSelectedStates = this.SELECTEDSTATES.NONE;
	//La velocidad con la que se mueven los looks
	this.speed = _speed;
	//Para controlar las transiciones de FadeIN-FadeOut
	this.transitionTimeFadeIn = 2.0;
	this.transitionTimeFadeOut = 1.0;
	
	//Para mover los looks en su desplazamiento lateral cuando hacemos zoom
	this.horizontalTransitionTime = 1;
	this.currentHorizontalTransitionTime = 0;

	/**** MATERIALS ****/

	// El material standar para el pictureCube
	var lookTexture = new THREE.ImageUtils.loadTexture( _paths );
	this.standardLookMaterial = new THREE.MeshLambertMaterial( { map: lookTexture, color: 0x888787, ambient: 0x0000ff, transparent: true } );

	/*** El material al que le programo el shader ***/
	this.infoPlaneMaterial = APP.materialsContainer.testMaterial.createMaterial( _paths );
	this.infoPlaneMaterial.opacity = 0;
	this.infoPlaneMaterial.visible = false;
	//El contador para saber cuantas veces hemos hecho click 
	this.infoPlaneChangesCount = 0;

	/**** PICTURE CUBE ****/
	this.createPictureCube( this.standardLookMaterial );

	/**** ADITIONAL PLANE ****/
	var planePosition;
	if( this.initialPosition.x < 0 )
	{
		planePosition = { x:150, y:0, z:2 };
	} 
	else
	{
		planePosition = { x:-150, y:0, z:2 };
	} 
	var planeRotation = { x:0, y:0, z:0 };
	var planeSize = { width:this.width, height:this.height };
	this.infoPlane = this.createPlane( this.infoPlaneMaterial, planePosition, planeRotation, planeSize );

	/**** DOME ****/
	// Diferenciamos si el look se ha colocado
	// a la derecha o a la izquierda de la pasarela
	var domePosition = 0;
	var domeRotation = 0;

	if( this.initialPosition.x < 0 )
	{
		domePosition = { x:-50, y:110, z:15 };
		domeRotation = { x:0.5, y:0, z:0.5 };
	} 
	else
	{
		domePosition = { x:50, y:110, z:15 };
		domeRotation = { x:0.5, y:0, z:-0.5 };
	} 
	var material = new THREE.MeshBasicMaterial( { color: 0xff33cc, side: THREE.DoubleSide, transparent: true, opacity: 0.2 } );
	var geometry = new THREE.SphereGeometry( 10, 16, 6, 0, 2 * Math.PI, 0, Math.PI / 2 );
	this.dome = new THREE.Mesh( geometry, material );
	this.dome.position = domePosition;
	this.dome.rotation = domeRotation;
	// Lo añadimos al contenedor
	this.lookContainer3D.add( this.dome );

	/**** SPOTLIGHT ****/
	// Simulación de foco
	var spotlight = new THREE.SpotLight(0xff0000);
	spotlight.position = domePosition;
	//spotlight.shadowCameraVisible = true;
	spotlight.shadowDarkness = 0.25;
	spotlight.intensity = 70;
	spotlight.castShadow = true;
	spotlight.shadowMapHeight = 256;
    spotlight.shadowMapWidth = 256;
    spotlight.exponent = 40.0;
	// Lo añadimos al contenedor
	this.lookContainer3D.add( spotlight );

	// Change the direction this spotlight is facing
	var lightTarget = new THREE.Object3D();
	lightTarget.position = { x:0, y:0, z:-5 };
	// Lo añadimos al contenedor
	this.lookContainer3D.add( lightTarget );

	// Para que el foco de luz apunte en esta dirección
	spotlight.target = lightTarget;
}

LookContainer.prototype.onMouseClick = function()
{
	/***Simple Change Color StandardMaterial**/
	this.standardLookMaterial.color.setHex( 0x000000 );
	this.standardLookMaterial.ambient.setHex( 0xffffff );
	//Cambiamos la máquina de estados
	this.currentMainState = this.MAINSTATES.SELECTED;
	this.currentSelectedStates = this.SELECTEDSTATES.FADE_IN;	

	this.infoPlaneMaterial.visible = true;

	//Hacemos semi transparente todo lo demás del look que no sea el plano
	this.pictureCubeMesh.material.materials.forEach(function(material)
	{
		material.opacity = 1;
		material.needsUpdate = true;
	});
}

//Para cuando deseleccionamos el look
LookContainer.prototype.reset = function()
{
	/***Simple Change Color StandardMaterial**/
	this.standardLookMaterial.color.setHex( 0x888787 ); 
	this.standardLookMaterial.ambient.setHex( 0x0000ff ); 
	//this.createPictureCube( this.standardLookMaterial );
	//Cambiamos la máquina de estados
	this.currentSelectedStates = this.SELECTEDSTATES.FADE_OUT;
	//Ñapa para que no interfiera con el plano y hagas cosas raras aunque sea transparente
	this.pictureCubeMesh.position.z -= 5000;
	//Hacemos transparente el cubo
	this.pictureCubeMesh.material.materials.forEach(function(material)
	{
		material.opacity = 1.0;
		material.needsUpdate = true;
	});
	//Hacemos transparente el plano
	this.infoPlaneMaterial.opacity = 0;
	this.infoPlaneMaterial.visible.visible = false;
	this.infoPlane.material.uniforms.opacity.value = this.infoPlaneMaterial.opacity;

	//Reseteamos el contador que usamos para mostrar diferentes efectos de shader a modo de ejemplos
	this.infoPlaneChangesCount = 0;
	//Reseteamos el shader
	this.infoPlane.material.uniforms.index.value = 0;
}

//Para parar el look cuando hemos seleccionado cualquier look
LookContainer.prototype.stop = function()
{
	//Cambiamos la máquina de estados principal
	this.currentMainState = this.MAINSTATES.STOP;

	//Hacemos semi transparente todo lo demás del look que no sea el plano
	this.pictureCubeMesh.material.materials.forEach(function(material)
	{
		material.opacity = 0.2;
		material.needsUpdate = true;
	});
	//Hacemos semitransparente el dome
	this.dome.material.opacity = 0.2;
	this.dome.needsUpdate = true;
}

//Para volver a activar todos los looks ( este en concreto )
LookContainer.prototype.changeToMovingState = function()
{
	//Cambiamos la máquina de estados
	this.currentMainState = this.MAINSTATES.MOVING;

	this.pictureCubeMesh.material.materials.forEach(function(material)
	{
		material.opacity = 1;
		material.needsUpdate = true;
	});

	//Hacemos opaco el dome
	this.dome.material.opacity = 1;
	this.dome.needsUpdate = true;
}

LookContainer.prototype.tick = function ( delta )
{
	//Según la máquina de estados principal
	if( this.currentMainState == this.MAINSTATES.MOVING )
	{
		var distance = delta * this.speed;
		//this.pictureCubeMesh.translateZ( distance );
		//this.infoPlane.position.z += delta * this.speed;
		//this.lookContainer3D.position.z += delta * this.speed;
	}

	//Según la máquina de estados de objeto seleccionado
	if( this.currentSelectedStates == this.SELECTEDSTATES.FADE_IN )
	{
		if( this.movePictureCubeToCenter( delta ) )
		{
			//Hacemos completamente opaco el plano
			this.infoPlaneMaterial.opacity = 1;
			this.infoPlane.material.uniforms.opacity.value = this.infoPlaneMaterial.opacity;
			//Ñapa para que no interfiera con el plano y hagas cosas raras aunque sea transparente
			this.pictureCubeMesh.position.z += 5000;
			//Hacemos transparente el cubo
			this.pictureCubeMesh.material.materials.forEach(function(material)
			{
				material.opacity = 0.0;
				material.needsUpdate = true;
			});
			this.currentSelectedStates = this.SELECTEDSTATES.SHOWING;
		}
	}
	else if( this.currentSelectedStates == this.SELECTEDSTATES.FADE_OUT )
	{
		if( this.movePictureCubeToSide( delta ) )
		{
			
			this.currentSelectedStates = this.SELECTEDSTATES.NONE;
		}
	}
	else if( this.currentSelectedStates == this.SELECTEDSTATES.SHOWING )
	{

	}
}

//Modifica el shader aplicado al plano
LookContainer.prototype.updateInfoPlaneMaterial = function ()
{
	if( this.infoPlaneChangesCount >= 8 )
	{
		this.infoPlaneChangesCount = 0;
		this.infoPlane.material.uniforms.index.value = 0;
	}
	else
	{
		this.infoPlaneChangesCount += 1;
		this.infoPlane.material.uniforms.index.value = this.infoPlaneChangesCount;
	}

	if( this.infoPlaneChangesCount == 8 )
	{
		//Si el index vale 8 cambiamos la matriz de kernel
		this.infoPlane.material.uniforms.u_kernel.value = [
													       -2, -1,  0,
													       -1,  1,  1,
													        0,  1,  2
													    ];
	}

}


//Crea el cubo con la foto en la cara delantera
LookContainer.prototype.createPictureCube = function ( _lookMaterial )
{
	// Create an array of materials to be used in a cube, one for each side
	// order to add materials: x+,x-,y+,y-,z+,z-
	var materials = [];	
	// order to add materials: x+,x-,y+,y-,z+,z-
	materials.push( new THREE.MeshBasicMaterial( { color: 0xff8800, transparent: true } ) );
	materials.push( new THREE.MeshBasicMaterial( { color: 0xffff33, transparent: true } ) );
	materials.push( new THREE.MeshBasicMaterial( { color: 0x33ff33, transparent: true } ) );
	materials.push( new THREE.MeshBasicMaterial( { color: 0x3333ff, transparent: true } ) );
	//El quinto es el de la cara para mostrar la foto
	materials.push( _lookMaterial );
	materials.push( new THREE.MeshBasicMaterial( { color: 0x8833ff, transparent: true } ) );
	//Un material que se compone de todos los anteriores
	var cubeMaterials = new THREE.MeshFaceMaterial( materials );
	// Geometry parameters: width (x), height (y), depth (z), 
	// (optional) segments along x, segments along y, segments along z
	var geometry = new THREE.CubeGeometry( this.width, this.height, this.depth, 1, 1, 1 );
	this.pictureCubeMesh = new THREE.Mesh( geometry, cubeMaterials );
	this.pictureCubeMesh.castShadow = true;
	this.pictureCubeMesh.receiveShadow = true;
	//Guardamos información en la malla para poder acceder 
	//al objeto padre cuando hagamos click sobre ella
	this.pictureCubeMesh.lookMaterial = _lookMaterial;
	this.pictureCubeMesh.id = this.id;
	// Lo añadimos al contenedor
	this.lookContainer3D.add( this.pictureCubeMesh );
}

LookContainer.prototype.createPlane = function Plane( _lookMaterial, _position, _rotation, _size )
{
	var geometry = new THREE.PlaneGeometry( _size.width, _size.height, 1, 1);
	var mesh = new THREE.Mesh( geometry, _lookMaterial );
	mesh.position = _position;
	mesh.rotation = _rotation;
	// Lo añadimos al contenedor
	this.lookContainer3D.add( mesh );	

	return mesh;
}

//Hace fade in en el material de creación propia que aplicamos al plano con la foto
LookContainer.prototype.customMaterialFadeIn = function( delta )
{
	this.infoPlaneMaterial.opacity += delta / this.transitionTimeFadeIn;
	if( this.infoPlaneMaterial.opacity >= 1 )
	{
		this.infoPlaneMaterial.opacity = 1;
		this.currentSelectedStates = this.SELECTEDSTATES.SHOWING;
	}
	this.infoPlane.material.uniforms.opacity.value = this.infoPlaneMaterial.opacity;
}

//Hace fade out en el material de creación propia que aplicamos al plano con la foto
LookContainer.prototype.customMaterialFadeOut = function( delta )
{
	this.infoPlaneMaterial.opacity -= delta / this.transitionTimeFadeOut;
	if( this.infoPlaneMaterial.opacity <= 0 )
	{
		this.infoPlaneMaterial.opacity = 0;
		this.currentSelectedStates = this.SELECTEDSTATES.NONE;
	}
	this.infoPlane.material.uniforms.opacity.value = this.infoPlaneMaterial.opacity;
}

//Mueve el cubo con la foto al centro de la escena cuando hacemos ZoomIn
LookContainer.prototype.movePictureCubeToCenter = function ( delta )
{
	this.currentHorizontalTransitionTime += delta;
	this.pictureCubeMesh.position.x = - this.currentHorizontalTransitionTime / this.horizontalTransitionTime * this.initialPosition.x;
	if( this.currentHorizontalTransitionTime >= this.horizontalTransitionTime )
	{
		this.pictureCubeMesh.position.x = - this.initialPosition.x;
		this.currentHorizontalTransitionTime = 0;
		return true;
	}
	return false;
}

//Mueve el cubo con la foto a su posición inicial cuando hacemos ZoomIn
LookContainer.prototype.movePictureCubeToSide = function ( delta )
{
	this.currentHorizontalTransitionTime += delta;
	this.pictureCubeMesh.position.x = - this.initialPosition.x + this.currentHorizontalTransitionTime / this.horizontalTransitionTime * this.initialPosition.x;
	if( this.currentHorizontalTransitionTime >= this.horizontalTransitionTime )
	{
		this.pictureCubeMesh.position.x = 0;
		this.currentHorizontalTransitionTime = 0;
		return true;
	}
	return false;
}












