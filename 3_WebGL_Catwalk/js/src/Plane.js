//Constructor
function Plane( _scene, _size, _initialPosition, 
				_initialRotation, _path, _repetition )
{
	
	this.width = _size.width;
	this.height = _size.height;
	this.initialPosition = _initialPosition;
	this.initialRotation = _initialRotation;

	this.mesh;
	this.texture;
	this.repetition = _repetition;
	this.geometry;
	this.material;
	

	this.texture = new THREE.ImageUtils.loadTexture( _path );
	this.texture.wrapS = THREE.RepeatWrapping; 
	this.texture.wrapT = THREE.RepeatWrapping; 
	this.texture.repeat = this.repetition;
	// DoubleSide: render texture on both sides of mesh
	this.material = new THREE.MeshLambertMaterial( { map: this.texture, ambient:0x0000ff, side:THREE.DoubleSide } );
	this.geometry = new THREE.PlaneGeometry( this.width, this.height, 1, 1);
	this.mesh = new THREE.Mesh( this.geometry, this.material );
	this.mesh.position = this.initialPosition;
	this.mesh.rotation = this.initialRotation;
	this.mesh.receiveShadow = true;
	
	_scene.add( this.mesh );
	
}
