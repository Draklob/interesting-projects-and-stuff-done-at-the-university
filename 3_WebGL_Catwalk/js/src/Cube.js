// Constructor
function Cube( _scene, _size, _initialPosition, 
				_initialRotation )
{

	this.scene = _scene;
	this.width = _size.width;
	this.height = _size.height;
	this.depth = _size.depth;
	//La posición inicial
	this.initialPosition = _initialPosition;
	//La rotación inicial
	this.initialRotation = _initialRotation;
	//El equivalente a una figura en Threejs
	this.mesh;
	this.geometry;
	// Create an array of materials to be used in a cube, one for each side
	// order to add materials: x+,x-,y+,y-,z+,z-
	this.materials = [];

}

Cube.prototype.test = function (delta)
{

}

	
