function CameraControl()
{
	//Array para tener referencia de las diferentes cámaras 
	this.camerasArray = [];
	//Referencia a la cámara principal
	this.mainCamera;
	//La velocidad de desplazamiento cuando movemos la rueda del mouse
	this.weelSpeed = 10;

	//Referencia al player para poderlo seguir
	this.player;
	this.playerDistance;

	//La distancia para el zoom
	this.zoomDistance = { x:0, y:0, z:0 };
	//La posición del look seleccionado
	this.zoomTargetPosition = { x:0, y:0, z:0 };
	//La distancia a la que se tiene que colocar del target
	this.targetDistance = { x:0, y:0, z:80 };
	//El tiempo total de transición ( la velocidad será variable en función de la distancia )
	this.transitionTime = 1;
	//El tiempo actual de la transición
	this.currentTransitionTime = 0;
	//La posición de la cámara antes de hacer el ZoomIn, para luego poder hacer el ZoomOut
	this.prevMainCameraPosition = { x:0, y:0, z:0 };
}

//La foto sigue al player siempre que no se esté examinando algún look
CameraControl.prototype.followPlayer = function ( delta )
{
	if( this.player && this.playerDistance )
	{
		//Esto se tendría que calcular para el loop del suelo cuando anda el personaje
		var distance = this.mainCamera.position.z;
		this.mainCamera.position.z = this.player.position.z - this.playerDistance;
		distance -= this.mainCamera.position.z;
		//console.log( "distance " + distance );
	}
}

//*** ZOOM ***//
CameraControl.prototype.zoom = function ( delta )
{
	this.currentTransitionTime += delta;
	this.mainCamera.position.z += delta / this.transitionTime * this.zoomDistance.z;
	this.mainCamera.position.y += delta / this.transitionTime * this.zoomDistance.y;
	if( this.currentTransitionTime >= this.transitionTime )
	{
		this.mainCamera.position.z = this.zoomTargetPosition.z;
		this.mainCamera.position.y = this.zoomTargetPosition.y;
		return true;
	}
	return false
}

//Reseta los valores para este zoom determinado
CameraControl.prototype.resetZoomIN = function( targetPosition )
{
	//Para luego poder hacer ZoomOut
	this.prevMainCameraPosition.x = this.mainCamera.position.x;
	this.prevMainCameraPosition.y = this.mainCamera.position.y;
	this.prevMainCameraPosition.z = this.mainCamera.position.z;

	this.zoomTargetPosition.x = targetPosition.x - this.targetDistance.x;
	this.zoomTargetPosition.y = targetPosition.y - this.targetDistance.y;
	this.zoomTargetPosition.z = targetPosition.z + this.targetDistance.z;
	
	this.zoomDistance.x = this.zoomTargetPosition.x - this.mainCamera.position.x;
	this.zoomDistance.y = this.zoomTargetPosition.y - this.mainCamera.position.y;
	this.zoomDistance.z = this.zoomTargetPosition.z - this.mainCamera.position.z;

	this.currentTransitionTime = 0;
}

//Reseta los valores para este zoom determinado
CameraControl.prototype.resetZoomOUT = function()
{
	this.zoomTargetPosition.x = this.prevMainCameraPosition.x;
	this.zoomTargetPosition.y = this.prevMainCameraPosition.y;
	this.zoomTargetPosition.z = this.prevMainCameraPosition.z;
	
	this.zoomDistance.x = this.zoomTargetPosition.x - this.mainCamera.position.x;
	this.zoomDistance.y = this.zoomTargetPosition.y - this.mainCamera.position.y;
	this.zoomDistance.z = this.zoomTargetPosition.z - this.mainCamera.position.z;

	//Mejor hacemos siempre el mismo zoom por si nos hemos acercado desde una distancia muy lejana
	//No funciona de momento porque habría que cambiar la posición del player
	//ya que luego la cámara se ajusta a la posición de este
	//this.zoomTargetPosition.z = this.mainCamera.position.z + 150;
	//this.zoomDistance.z = 150;

	this.currentTransitionTime = 0;
}

CameraControl.prototype.addCamera = function( camera )
{
	this.camerasArray.push( camera );
}

