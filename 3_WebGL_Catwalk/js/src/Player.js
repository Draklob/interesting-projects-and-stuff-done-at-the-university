function Player( _scene, _size, _initialPosition, 
				_initialRotation, _keyboard, _mainCamera )
{
	//Llamamos el constructor del padre
	Cube.call( this, _scene, _size, _initialPosition, 
				_initialRotation );

	//Tenemos que incluir un objeto 3d ya que el modelo está rotado al revés
	this.playerContainer3D = new THREE.Object3D();
	this.scene.add( this.playerContainer3D );

	//Guardamos una referencia a la animación
	this.animation;

	//El objeto keyboard para poder controlar las teclas que se pulsan
	this.keyboard = _keyboard;

	//Referencia a la cámara principal para poder moverla detrás del personaje
	this.mainCamera = _mainCamera;

	//Los límites de la pantalla para controlar que el player no se salga
	this.leftLimit = -180;
	this.rightLimit = 180;

	//Creamos el cubo
	this.loadWalkingFemale();


}

//Esto hay que ponerlo antes de implementar los métodos propios de Player
//si no se pisa el prototipo por el de Cube
Player.prototype = Object.create( Cube.prototype );
Player.prototype.constructor = Player;

Player.prototype.loadWalkingFemale = function()
{
	var loader = new THREE.JSONLoader();

	that = this;

	loader.load( "../assets/models/human_walk_0_female.js", function ( geometry, materials ) 
	{
		that.materials = materials;
		geometry.computeVertexNormals();
		geometry.computeBoundingBox();

		//ensureLoop( geometry.animation );
		THREE.AnimationHandler.add( geometry.animation );

		for ( var i = 0, il = materials.length; i < il; i ++ ) 
		{

			var originalMaterial = materials[ i ];
			originalMaterial.transparent = true;
			originalMaterial.skinning = true;

			originalMaterial.map = undefined;
			originalMaterial.shading = THREE.SmoothShading;
			originalMaterial.color.setHSL( 0.5, 1.0, 0.5 );
			originalMaterial.ambient.copy( originalMaterial.color );
			originalMaterial.specular.setHSL( 0, 0, 0.1 );
			originalMaterial.shininess = 75;

			originalMaterial.wrapAround = true;
			originalMaterial.wrapRGB.set( 1, 0.5, 0.5 );

		}

		//Establecemos la escala
		var s = 40;

		var material = new THREE.MeshFaceMaterial( materials );
		that.mesh = new THREE.SkinnedMesh( geometry, material, false );
		that.mesh.rotation.y = Math.PI;
		that.playerContainer3D.add( that.mesh );
		that.playerContainer3D.scale.set( s, s, s );
	

		that.playerContainer3D.position.x = that.initialPosition.x;
		that.initialPosition.y = that.playerContainer3D.position.y = -geometry.boundingBox.min.y * s;
		that.playerContainer3D.position.z = that.initialPosition.z;

		that.mesh.userData.delta = 25;

		
		that.animation = new THREE.Animation( that.mesh, "ActionFemale" );
		that.animation.play();
		that.animation.update( 0 );

	} );

}

Player.prototype.tick = function ( delta )
{
	
	if ( this.mesh )
	{
		var moveDistance = 200 * delta; // 200 pixels per second
		var rotateAngle = Math.PI / 2 * delta;   // pi/2 radians (90 degrees) per second

		// move forwards/backwards/left/right
		if ( this.keyboard.pressed("W") )
		{
			THREE.AnimationHandler.update( 0.8 * delta );
			//var initalPosition = this.mesh.position.z;
			this.playerContainer3D.translateZ( -moveDistance );
			//var distance = this.mesh.position.z - initialPosition;
			//this.cameraControl.mainCamera.position.z += distance;
		}
		if ( this.keyboard.pressed("S") )
		{
			//var initalPosition = this.mesh.position.z;
			//this.playerContainer3D.translateZ(  moveDistance );
			//var distance = this.mesh.position.z - initialPosition;
			//this.cameraControl.mainCamera.position.z += distance;
		}
		if ( this.keyboard.pressed("Q") )
			this.playerContainer3D.translateX( -moveDistance );
		if ( this.keyboard.pressed("E") )
			this.playerContainer3D.translateX(  moveDistance );	

		// rotate left/right/up/down
		var rotation_matrix = new THREE.Matrix4().identity();
		if ( this.keyboard.pressed("A") )
			this.playerContainer3D.rotateOnAxis( new THREE.Vector3(0,1,0), rotateAngle);
		if ( this.keyboard.pressed("D") )
			this.playerContainer3D.rotateOnAxis( new THREE.Vector3(0,1,0), -rotateAngle);

		if( this.playerContainer3D.position.x <= this.leftLimit ) this.playerContainer3D.position.x = this.leftLimit;
		if( this.playerContainer3D.position.x >= this.rightLimit ) this.playerContainer3D.position.x = this.rightLimit;
	}
}

Player.prototype.makeInvisible = function ( )
{
	this.materials.forEach(function(material)
	{
		material.opacity = 0.0;
		material.visible = false;
		material.needsUpdate = true;
	});
}

Player.prototype.makeVisible = function ( )
{
	this.materials.forEach(function(material)
	{
		material.opacity = 1.0;
		material.visible = true;
		material.needsUpdate = true;
	});
}




