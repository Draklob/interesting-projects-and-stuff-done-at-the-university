// Constructor
function Floor( _scene )
{
	this.planeArray = [];
	this.plane1;
	this.plane2;
	this.plane3;

	//La distancia recorrida para hacer el loop
	this.distance = 0;

	//La profundidad del suelo
	this.floorDepth = 3000;

	//Cada cuanta distancia recorrida cambiamos los planos
	this.changeDistance = 300;

	//La dirección en la que se ha puesto el último suelo
	this.Direction = { FRONT:0, BACK:1 };
	this.lastDirection = this.Direction.FRONT;

	var position = { x:0, y:0, z:0 };
	var rotation = { x:0.5*Math.PI, y:0, z:0 };
	var size = { width:600, height:this.floorDepth };
	var path = '../assets/images/blueCell3.png';
	var repetition = { x:10, y:20 };

	position.z = position.z + size.height;

	//Creamos tres planos para hacer el loop del suelo	
	for( j = 0; j < 3; j ++ )
	{
		position = { x:0, y:0, z:position.z - ( size.height ) };
		
		this.planeArray[j] = new Plane(
			_scene,
			size,
			position,
			rotation,
			path,
			repetition
		);
		//this.planeArray[j].mesh.receiveShadow = true; 
	}

	this.plane1 = this.planeArray[0];
	this.plane2 = this.planeArray[1];
	this.plane3 = this.planeArray[2]; 

	//Para debuguear
	/*for( j = 0; j < 3; j ++ )
	{
		console.log( "j = " + j);
		console.log( "this.planeArray[j].initialPosition = " + this.planeArray[j].initialPosition.z );
	}*/
}

Floor.prototype.tick = function (delta)
{
	
}

Floor.prototype.checkLoop = function (delta)
{
	this.distance += delta;

	/*console.log( "delta = " + delta );
	console.log( "this.distance = " + this.distance );
	console.log( "this.lastDirection = " + this.lastDirection );
	console.log( "this.plane1.mesh.position.z = " + this.plane1.mesh.position.z );
	console.log( "this.plane2.mesh.position.z = " + this.plane2.mesh.position.z );
	console.log( "this.plane3.mesh.position.z = " + this.plane3.mesh.position.z );*/
	//Diferenciamos según si el último plano cambiado lo colocamos delante o detrás
	//Front es más alejado en profundidad y Back más cerca
	if( this.lastDirection == this.Direction.FRONT )
	{
		if( this.distance >= this.changeDistance )
		{
			//console.log( "ha entrado FRONT" );
			this.plane1.mesh.position.z -= 3 * this.floorDepth;
			plane_aux = this.plane3;
			this.plane3 = this.plane1;
			this.plane1 = this.plane2;
			this.plane2 = plane_aux;
			this.distance = 0;
		}
		else if( this.distance <= 0 )
		{
			//console.log( "ha entrado FRONT to BACK" );
			this.plane3.mesh.position.z += 3 * this.floorDepth;
			plane_aux = this.plane1;
			this.plane1 = this.plane3;
			this.plane3 = this.plane2;
			this.plane2 = plane_aux;
			this.lastDirection = this.Direction.BACK;
		}
	}
	if( this.lastDirection == this.Direction.BACK )
	{
		if( (-this.distance) >= this.changeDistance )
		{
			//console.log( "ha entrado BACK" );
			this.plane3.mesh.position.z += 3 * this.floorDepth;
			plane_aux = this.plane1;
			this.plane1 = this.plane3;
			this.plane3 = this.plane2;
			this.plane2 = plane_aux;
			this.distance = 0;
		}
		else if( this.distance >= 0 )
		{
			//console.log( "ha entrado BACK to FRONT" );
			this.plane1.mesh.position.z -= 3 * this.floorDepth;
			plane_aux = this.plane3;
			this.plane3 = this.plane1;
			this.plane1 = this.plane2;
			this.plane2 = plane_aux;
			this.lastDirection = this.Direction.FRONT;
		}
	}
	
}
