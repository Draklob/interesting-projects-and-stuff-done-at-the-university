var APP = APP || {};

// App properties
APP.screenW = window.innerWidth;
APP.screenH = window.innerHeight;
APP.stats;
APP.clock;
APP.controls;
APP.keyboard;
APP.projector;

//La posición del mouse
//Para seleccionar un cubo con la foto del look
APP.clickeablePictures = [];
//Para poder hacer click en el plano de información del look selecionado
//Es un array, aunque realmente luego metemos tan solo el plano selecionado
APP.clickeableInfoPlane = [];
APP.mouseVector;

// Scene properties
APP.depth = 6000.0;
APP.fov = 110;
APP.clipNear = 0.1;
APP.clipFar = APP.depth;
APP.scene;
APP.cameraControl;
APP.renderer;
APP.shaders;

//Lights
APP.pointLight;
APP.ambientLight;
APP.directionalLight;
APP.spotlight;

//Materials
APP.materialsContainer;

//Stars
APP.starfield;

// Objects
APP.randomLights;
APP.floor;
APP.decoration;
APP.looksContainer3D;
//Array para meter los contenedores de looks
APP.looksArray = [];
APP.camScreen;
APP.player;

//Solo se actualiza el sistema de orbitación una
//vez, es que ya monté la escena con eso y si no se desconfigura
APP.testControl = false;

//Para guardar el id del look anteriormente seleccionado
APP.prevSelectedLookId = -1;

//La máquina de estados de la aplicación
APP.MAINSTATES = { NONE:0, ZOOM_IN:1, SHOWING:2, ZOOM_OUT:3 };
APP.currentState = APP.MAINSTATES.NONE;

APP.uiChanges = false;

/**INIT**/
APP.init = function ()
{
	
	if (!Detector.webgl)
	{
		Detector.addGetWebGLMessage();
	}

	// Init scene
	APP.scene = APP.scene || new THREE.Scene();

	if (!APP.cameraControl)
	{
		APP.cameraControl = new CameraControl();
		var mainCamera = new THREE.PerspectiveCamera(APP.fov, APP.getAspectRatio(), APP.clipNear, APP.clipFar);
		mainCamera.position.set(0, 50, 1350);
		mainCamera.lookAt({x:0,y:3500,z:mainCamera.position.z + 1000});
		APP.cameraControl.addCamera(mainCamera);
		APP.cameraControl.mainCamera = mainCamera;
	}

	if (!APP.renderer)
	{
		APP.renderer = new THREE.WebGLRenderer();
		APP.renderer.setSize(APP.screenW, APP.screenH);
		APP.renderer.setClearColor(0x000000, 1);
		// must enable shadows on the renderer 
		APP.renderer.shadowMapEnabled = true;
		document.getElementById( 'ThreeJS' ).appendChild(APP.renderer.domElement);
		//document.body.appendChild(APP.renderer.domElement);
	}

	/*** LIGHTS ***/

	if(!APP.directionalLight)
	{
		APP.directionalLight = new THREE.DirectionalLight(0x0000ff, 0.1);
		APP.directionalLight.position.set(0,1,0).normalize(); //Ortogonal al suelo
		APP.scene.add( APP.directionalLight );
	}

	if(!APP.ambientLight)
	{
		APP.ambientLight = new THREE.AmbientLight(0xffffff);
		APP.scene.add(APP.ambientLight);
	}

	/*** MATERIALS ***/
	if(!APP.materialsContainer)
	{
		APP.materialsContainer = new MaterialsContainer( APP.shaders );
	}

	/*** CONTROLS ***/

	if(!APP.controls)
	{
		APP.controls = new THREE.OrbitControls( APP.cameraControl.mainCamera, APP.renderer.domElement );
	}

	if(!APP.keyboard)
	{
		APP.keyboard = new THREEx.KeyboardState();
	}

	// Stats tracker used for metrics like FPS
	if (!APP.stats)
	{
		APP.stats = new Stats();
		APP.stats.domElement.style.position = "absolute";
		APP.stats.domElement.style.top = "0px";
		APP.stats.domElement.style.zIndex = 100;
		document.body.appendChild(APP.stats.domElement);
	}

	if (!APP.clock)
	{
		APP.clock = new THREE.Clock();
		APP.clock.start();
	}

	//Stars
	if (!APP.starfield)
	{
		APP.starfield = STARFIELD.create(
			APP.scene,
			APP.screenW,
			APP.screenH,
			APP.depth,
			APP.shaders.particles.vertex,
			APP.shaders.particles.fragment
		);
	}

	/**** OBJECTS ****/

	//Random Lights
	if( !APP.randomLights )
	{
		APP.randomLights = new RandomLights( APP.scene );
	}

	// Floor
	if (!APP.floor)
	{
		APP.floor = new Floor( APP.scene );
	}

	// Decoration
	if (!APP.decoration)
	{
		APP.decoration = new Decoration( APP.scene );
	}

	if (!APP.looksContainer3D)
	{
		//Cargamos los looks
		this.looksLoader();
	}

	var cameraDistance = - 300;
	// Player
	if (!APP.player)
	{
		var position = { x:15, y:25, z:APP.cameraControl.mainCamera.position.z + cameraDistance };
		var rotation = { x:0, y:0, z:0 };
		var size = { width:30, height:50, depth:10 };
		APP.player = new Player(
			APP.scene,
			size,
			position,
			rotation,
			APP.keyboard,
			APP.cameraControl.mainCamera
		);

		//Le pasamos el player al CameraControl
		APP.cameraControl.player = APP.player.playerContainer3D;
		APP.cameraControl.playerDistance = cameraDistance;
	}

	//CamScreen ( donde se visualiza la captura de la webcam )
	if( !APP.camScreen )
	{
		var playerDistance = - 1500;
		cameraDistance += playerDistance;
		var position = { x:0, y:200, z:APP.cameraControl.mainCamera.position.z + cameraDistance };
		var rotation_y = -Math.PI;
		var rotation = { x:0, y:rotation_y, z:0 };
		var size = { width:600, height:400, depth:20 };
		APP.camScreen = new CamScreen(
			APP.scene,
			size,
			position,
			rotation,
			APP.cameraControl.mainCamera,
			cameraDistance
		);
	}

	// initialize object to perform world/screen calculations
	APP.projector = new THREE.Projector();
	APP.mouseVector = new THREE.Vector3();
	// Add event listeners
	addEventListener("resize", APP.onResize, false);
	// Toggle full-screen on given key press
	THREEx.FullScreen.bindKey({ charCode : 'm'.charCodeAt(0) });
	// when the mouse CLICK, call the given function
	document.addEventListener( 'mousedown', this.onDocumentMouseDown, false );
	//Para mover la cámara con la rueda del mouse
	document.addEventListener( 'mousewheel', this.onMouseWheel, false );
	document.addEventListener( 'DOMMouseScroll', this.onMouseWheel, false ); // firefox

	APP.render();
}

APP.render = function ()
{
	//Esto me llevó muchas horas hasta conseguir debuguearlo
	//porque no se mostraba la cámara en firefox
	//Ahora hace falta crearse la clase con la webcam y poner esto como un método de ella
	try 
	{
		if ( video.readyState === video.HAVE_ENOUGH_DATA ) 
		{
			videoImageContext.drawImage( video, 0, 0, videoImage.width, videoImage.height );
			videoTexture.needsUpdate = true;
		}
	} catch (e) 
	{
    	if (e.name == "NS_ERROR_NOT_AVAILABLE") 
    	{
      		//setTimeout(drawVideo, 0);
    	} else 
    	{
      		throw e;
    	}
	}
 
	requestAnimationFrame(APP.render);

	APP.tick();
	
	APP.renderer.render(APP.scene, APP.cameraControl.mainCamera);
}

APP.tick = function ()
{
	//Calculamos el delta time
	var delta = APP.clock.getDelta();

	//Actualizamos las luces
	if( APP.randomLights )
		APP.randomLights.onTick( delta );

	//Actualizamos el control del movimiento de la escena para debuguear
	if( APP.testControl == false )
	{
		APP.testControl = true;
		APP.controls.update();
	}
	APP.stats.update();

	//***Fin debuguear

	// Actualizamos los contenedores de look
	// Según si hemos hecho click en alguno o no,
	// se moverán o se mostrará el look correspondiente
	APP.looksArray.forEach(function(look)
	{
		look.tick( delta );
	});

	//Si no hemos seleccionado ningún look
	if( APP.currentState == APP.MAINSTATES.NONE )
	{
		//Actualizamos el player
		APP.player.tick( delta );

		//Actualizamos la cámara para que siga al player
		APP.cameraControl.followPlayer( delta );

		//Actualizamos la pantalla que muestra la webcam para que siempre se vea al fondo 
		APP.camScreen.followCamera( delta );

		//Actualizamos las partículas
		APP.starfield.tick(delta);
	}
	else if( APP.currentState == APP.MAINSTATES.ZOOM_IN )
	{
		//Actualizamos la cámara y chequeamos cuando termina de hacer zoom
		if( APP.cameraControl.zoom( delta ) )
		{
			APP.currentState = APP.MAINSTATES.SHOWING;
		}
	}
	else if( APP.currentState == APP.MAINSTATES.SHOWING )
	{
		
	}
	else if( APP.currentState == APP.MAINSTATES.ZOOM_OUT )
	{
		//Actualizamos la cámara y chequeamos cuando termina de hacer zoom
		//Devuelve true si ha terminado
		if( APP.cameraControl.zoom( delta ) )
		{
			APP.currentState = APP.MAINSTATES.NONE;
			//Hacemos visible el player
			APP.player.makeVisible();
			// Ponemos en marcha de nuevo todos los looks
			// Paramos todos los looks
			APP.looksArray.forEach(function(look)
			{
				look.changeToMovingState();
			});
		}
	}

}

APP.keyboardControl = function ()
{

}

APP.getAspectRatio = function ()
{
	return (APP.screenH == 0) ? 0.0 : APP.screenW / APP.screenH;
}

APP.loadShaders = function ()
{
	SHADER_LOADER.load(APP.onShadersLoaded);
}

APP.onResize = function ()
{
	APP.screenW = window.innerWidth;
	APP.screenH = window.innerHeight;

	APP.cameraControl.mainCamera.aspect = APP.getAspectRatio();
	APP.cameraControl.mainCamera.updateProjectionMatrix();

	APP.renderer.setSize(APP.screenW, APP.screenH);

	APP.starfield.resize(APP.screenW, APP.screenH);
}

APP.onShadersLoaded = function (shaders)
{
	APP.shaders = shaders;
	APP.init();
}

//Cuando se han cargado los shader iniciamos la aplicación
$(document).ready(APP.loadShaders);

//Controla el evento de hacer click con el mouse
APP.onDocumentMouseDown = function( event ) 
{
	if( APP.uiChanges )
	{
		APP.uiChanges = false;
	}
	else if( ! APP.uiChanges )
	{
		APP.mouseVector.x = ( event.clientX / window.innerWidth ) * 2 - 1;
		APP.mouseVector.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

		var vector = new THREE.Vector3( APP.mouseVector.x, APP.mouseVector.y, 1 );
		APP.projector.unprojectVector( vector, APP.cameraControl.mainCamera );
		var ray = new THREE.Raycaster( APP.cameraControl.mainCamera.position, vector.sub( APP.cameraControl.mainCamera.position ).normalize() );

		//Si no se está examinando ningún look
		if( APP.currentState == APP.MAINSTATES.NONE )
		{
			//Crea un array conteniendo todos los objetos incluidos en el array pasado por parámetro
			//con los cuales intersecta el rayo
			var intersects = ray.intersectObjects( APP.clickeablePictures );

			// if there is one (or more) intersections
			if ( intersects.length > 0 )
			{
				//Hacemos visible la interfaz del shader ( gui )
				//makeGuiVisible();
				//Cambiamos al estado de zoom
				APP.currentState = APP.MAINSTATES.ZOOM_IN;
				//Hacemos invisible el player
				APP.player.makeInvisible();
				//Paramos todos los looks
				APP.looksArray.forEach(function(look)
				{
					look.stop();
				});
		
				// Si previamente ya habíamos seleccionado un look 
				if( APP.prevSelectedLookId >= 0 )
				{
					// Si es el mismo cambiamos nuevamente al estado de ZoomIn
					if( APP.prevSelectedLookId == intersects[0].object.id )
					{
						//Actualizamos la posición a la que se tiene que mover la cámara para hacer el ZoomIn
						APP.cameraControl.resetZoomIN( APP.looksArray[ intersects[0].object.id ].lookContainer3D.position );
						APP.looksArray[ intersects[0].object.id ].onMouseClick();
					}
					else
					{
					 	//Si no es el mismo reseteamos el look anteriormente seleccionado
						//y cambiamos el nuevo
						//APP.looksArray[ APP.prevSelectedLookId ].reset();
						APP.looksArray[ intersects[0].object.id ].onMouseClick();
						//Guardamos el id del look seleccionado para luego cuando
						//seleccionemos otro, poderlo resetear
						APP.prevSelectedLookId = intersects[0].object.id;
						//Actualizamos la posición a la que se tiene que mover la cámara para hacer el ZoomIn
						APP.cameraControl.resetZoomIN( APP.looksArray[ intersects[0].object.id ].lookContainer3D.position );
						//Guardamos la referencia al plano correspondiente para poder detectar cuando hagamos click en él
						APP.clickeableInfoPlane = [];
						APP.clickeableInfoPlane.push( APP.looksArray[ intersects[0].object.id ].infoPlane );
					}
				}
				else // Si no había ningún look seleccionado previamente
				{
					//console.dir( APP.looksArray[ intersects[0].object.id ] );
					APP.looksArray[ intersects[0].object.id ].onMouseClick();
					// Guardamos el id el look seleccionado para luego cuando
					// seleccionemos otro poderlo resetear
					APP.prevSelectedLookId = intersects[0].object.id;
					//Actualizamos la posición a la que se tiene que mover la cámara para hacer el ZoomIn
					APP.cameraControl.resetZoomIN( APP.looksArray[ intersects[0].object.id ].lookContainer3D.position );
					//Guardamos la referencia al plano correspondiente para poder detectar cuando hagamos click en él
					APP.clickeableInfoPlane = [];
					APP.clickeableInfoPlane.push( APP.looksArray[ intersects[0].object.id ].infoPlane );
				}
			}
			else // Si no hemos pulsado en un look 
			{
				// Si había un look seleccionado lo reseteamos
				if( APP.prevSelectedLookId >= 0 )
				{
					//APP.looksArray[ APP.prevSelectedLookId ].reset();
					// Ponemos el indicador de look seleccionado a -1
					APP.prevSelectedLookId = -1;
					// Ponemos en marcha de nuevo todos los looks
					// Paramos todos los looks
					APP.looksArray.forEach(function(look)
					{
						look.changeToMovingState();
					});
				}

			}
		}
		else if( APP.currentState == APP.MAINSTATES.ZOOM_IN )
		{
			
			
		}
		else if( APP.currentState == APP.MAINSTATES.SHOWING )
		{
			//Crea un array conteniendo todos los objetos incluidos en el array pasado por parámetro
			//con los cuales intersecta el rayo
			var intersects = ray.intersectObjects( APP.clickeableInfoPlane );

			// Si he hecho click en el plano
			if ( intersects.length > 0 )
			{
				APP.looksArray[ APP.prevSelectedLookId ].updateInfoPlaneMaterial();
			}
			else // Si no he hecho click en el plano cambio a Zoom_Out
			{
				APP.currentState = APP.MAINSTATES.ZOOM_OUT;
				//Hacemos invisible la interfaz del shader ( gui )
				hideGui();
				//Actualizamos la posición a la que se tiene que mover la cámara para hacer el ZoomOut
				APP.cameraControl.resetZoomOUT();
				//Reseteamos el look anteriormente seleccionado
				APP.looksArray[ APP.prevSelectedLookId ].reset();
			}
		}
	}

}

//Cargador de looks
APP.looksLoader = function ()
{
	//Cargamos un array de imágenes
	var paths = [ 
		'../assets/images/looks/look_0/look_0_image_0_550_800.jpg',
		'../assets/images/looks/look_1/look_1_image_0_550_800.jpg',
		'../assets/images/looks/look_2/look_2_image_0_550_800.jpg',
		'../assets/images/looks/look_3/look_3_image_0_550_800.jpg',
		'../assets/images/looks/look_4/look_4_image_0_550_800.jpg'
	];

	APP.looksContainer3D = new THREE.Object3D();
	APP.scene.add( APP.looksContainer3D );

	var looksCount = 6;
	var pathIndex = 0;

	//Cargamos el número de looks que tengamos según los path recibidos
	for( j = 0; j < looksCount ; j ++ )
	{
		if( j % 2 === 0 ) var position_x = 150;
			else position_x = -150;
		var position = { x:position_x, y:100, z:1200 - ( j * 60 ) };
		var rotation = { x:0, y:0, z:0 };
		var size = { width:100, height:200, depth:4 };
		var speed = 20;
		//Si hemos cargado todas las imágenes empezamos de nuevo por la primera
		if( pathIndex >= paths.length ) pathIndex = 0;
		APP.looksArray.push( new LookContainer(
			j,
			size,
			position,
			rotation,
			paths[pathIndex],
			speed
		) );
		//Incrementamos el índice del path
		pathIndex ++;
		//Añadimos el contenedor correspondiente a la escena
		APP.looksContainer3D.add( APP.looksArray[j].lookContainer3D );
		//Añadimos el cubo con la foto al array clickeablePictures
		APP.clickeablePictures.push( APP.looksArray[j].pictureCubeMesh );
	}
}

//Evento que se lanza cuando movemos la rueda del mouse
APP.onMouseWheel = function ( event )
{
	//Solo movemos la cámara si no hemos selecionado ningún perfil
	if( APP.currentState == APP.MAINSTATES.NONE )
	{
		if ( event.wheelDelta ) { // WebKit / Opera / Explorer 9

			delta = event.wheelDelta;

		} else if ( event.detail ) { // Firefox

			delta = - event.detail;

		}
		var distance = - delta * 10;
		APP.cameraControl.mainCamera.position.z += distance;
		APP.player.playerContainer3D.position.z += distance;
		APP.floor.checkLoop( delta );
	}
}

