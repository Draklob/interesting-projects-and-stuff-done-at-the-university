// custom global variables
var video, videoImage, videoImageContext, videoTexture, camvideo;

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
window.URL = window.URL || window.webkitURL;

camvideo = document.getElementById('monitor');

if (!navigator.getUserMedia) 
{
	document.getElementById('errorMessage').innerHTML = 
		'Sorry. <code>navigator.getUserMedia()</code> is not available.';
} else {
	navigator.getUserMedia({video: true}, gotStream, noStream);
}

video = document.getElementById( 'monitor' );
	
videoImage = document.getElementById( 'videoImage' );
videoImageContext = videoImage.getContext( '2d' );
// background color if no video present
videoImageContext.fillStyle = '#554433';
videoImageContext.fillRect( 0, 0, videoImage.width, videoImage.height );

videoTexture = new THREE.Texture( videoImage );
videoTexture.minFilter = THREE.LinearFilter;
videoTexture.magFilter = THREE.LinearFilter;


function gotStream(stream) 
{
	if (window.URL) 
	{  
	 	camvideo.src = window.URL.createObjectURL(stream);   
	} 
	else // Opera
	{   
		camvideo.src = stream;  
	}

	camvideo.onerror = function(e) 
	{   
		stream.stop();   
	};

	stream.onended = noStream;
}

function noStream(e) 
{
	var msg = 'No camera available.';
	if (e.code == 1) 
	{   msg = 'User denied access to use camera.';   }
	document.getElementById('errorMessage').textContent = msg;
}