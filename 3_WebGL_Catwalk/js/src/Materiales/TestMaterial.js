//*** Test_SHADER_MATERIAL ***//
function TestMaterial( _shaders )
{
	//Mantengo una referencia a los shaders
	this.shaders = _shaders;
}


//*** CREATE MATERIAL ***//
TestMaterial.prototype.createMaterial = function ( _path )
{
	var material = new THREE.ShaderMaterial( 
	{
	    uniforms: 
		{ 
			opacity: {type: 'f', value: 0.0},
			index: {type: 'f', value: 0.0},
			color: {type: 'f', value: 0.5},
			u_texture: {type: "t", value: THREE.ImageUtils.loadTexture( _path )},
			u_textureSize: {type: "v2", value: new THREE.Vector2( 550, 800 )},
			u_kernel: {type: "fv1", value: [ -1, -1, -1, 
										     -1,  8, -1, 
										     -1, -1, -1
											]}
		},
		vertexShader:  this.shaders.test.vertex,
		fragmentShader: this.shaders.test.fragment,
		side: THREE.FrontSide,
		//blending: THREE.AdditiveBlending,
		transparent: true
	});

	return material;
}




