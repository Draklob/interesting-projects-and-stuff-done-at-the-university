//*** Glow_SHADER_MATERIAL ***//
function GlowMaterial( _shaders )
{

	//Llamamos el constructor del padre
	Material.call( _shaders );

	//Parámetros del objeto GUI
	this.parameters = 
	{
		v : 0   // dummy value, only type is important
	};

	var numberList = [1, 2, 3];
	this.gui.add( this.parameters, 'v', numberList ).name('SwapChanels');

}

//Esto hay que ponerlo antes de implementar los métodos propios del padre
//si no se pisa el prototipo del hijo por el del padre
GlowMaterial.prototype = Object.create( Material.prototype );
GlowMaterial.prototype.constructor = GlowMaterial;

//*** CREATE MATERIAL ***//
GlowMaterial.prototype.createGlowMaterial = function ( _path )
{
	var material = new THREE.ShaderMaterial( 
	{
	    uniforms: 
		{ 
			"c":   { type: "f", value: 1.0 },
			"p":   { type: "f", value: 1.4 },
			glowColor: { type: "c", value: new THREE.Color(0xffff00) },
			viewVector: { type: "v3", value: APP.camera.position }
		},
		vertexShader:  this.shaders.glow.vertex,
		fragmentShader: this.shaders.glow.fragment,
		side: THREE.FrontSide,
		blending: THREE.AdditiveBlending,
		transparent: true
	});

	return material;
}

