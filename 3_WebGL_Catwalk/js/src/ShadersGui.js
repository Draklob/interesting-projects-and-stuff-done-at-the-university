
//Referencia al objeto GUI
var gui = new dat.GUI();
//Para ocultar el gui inicialmente
hideGui();

//Parámetros del objeto GUI
gui.parameters = 
{
	v : 0   // dummy value, only type is important
};

var numberList = [1, 2, 3];
var swapChanelsList = gui.add( gui.parameters, 'v', numberList ).name('SwapChanels').listen();

swapChanelsList.onChange(function(value) 
{
	console.log( "ha entrado" );
  	uiChanges = true;
});

//Para ocultar el gui
function hideGui()
{
	if( !this.guiObject )
	{
		this.guiObject = $('.dg.ac');
	}
	this.guiObject.css('visibility','hidden');
}

//Para hacer visible el gui
function makeGuiVisible()
{
	if( !this.guiObject )
	{
		this.guiObject = $('.dg.ac');
	}
	this.guiObject.css('visibility','visible');
}


