function CamScreen( _scene, _size, _initialPosition, 
				_initialRotation, _camera, _cameraDistance )
{
	//Llamamos el constructor del padre
	Cube.call( this, _scene, _size, _initialPosition, 
				_initialRotation );

	//La distancia que mantendrá con respecto al player
	this.cameraDistance = _cameraDistance;
	this.camera = _camera;

	//Creamos el cubo

	// order to add materials: x+,x-,y+,y-,z+,z-
	this.materials.push( new THREE.MeshBasicMaterial( { color: 0xff8800, transparent: true } ) );
	this.materials.push( new THREE.MeshBasicMaterial( { color: 0xffff33, transparent: true } ) );
	this.materials.push( new THREE.MeshBasicMaterial( { color: 0x33ff33, transparent: true } ) );
	this.materials.push( new THREE.MeshBasicMaterial( { color: 0x3333ff, transparent: true } ) );
	this.materials.push( new THREE.MeshBasicMaterial( { color: 0x8833ff, transparent: true } ) );
	//this.materials.push( new THREE.MeshBasicMaterial( { color: 0x8833ff, transparent: true } ) );

	this.movieMaterial = new THREE.MeshBasicMaterial( { map: videoTexture, overdraw: true, side:THREE.DoubleSide, transparent: true } );
	this.materials.push( this.movieMaterial );
	var cubeMaterials = new THREE.MeshFaceMaterial( this.materials );
	// Geometry parameters: width (x), height (y), depth (z), 
	// (optional) segments along x, segments along y, segments along z
	this.geometry = new THREE.CubeGeometry( this.width, this.height, this.depth, 1, 1, 1 );
	this.mesh = new THREE.Mesh( this.geometry, cubeMaterials );
	this.mesh.position.set( this.initialPosition.x, this.initialPosition.y, this.initialPosition.z );
	this.mesh.rotation.set( this.initialRotation.x, this.initialRotation.y, this.initialRotation.z );
	//Lo añadimos a la escena
	this.scene.add( this.mesh );

}

//Esto hay que ponerlo antes de implementar los métodos propios de Player
//si no se pisa el prototipo por el de Cube
CamScreen.prototype = Object.create( Cube.prototype );
CamScreen.prototype.constructor = CamScreen;

//La pantalla sigue al player siempre que no se esté examinando al player
CamScreen.prototype.followCamera = function ( delta )
{
	if( this.camera && this.cameraDistance )
	this.mesh.position.z = this.camera.position.z + this.cameraDistance;
}

CamScreen.prototype.makeInvisible = function ( )
{
	this.materials.forEach(function(material)
	{
		material.opacity = 0.0;
		material.needsUpdate = true;
	});
}

CamScreen.prototype.makeVisible = function ( )
{
	this.materials.forEach(function(material)
	{
		material.opacity = 1.0;
		material.needsUpdate = true;
	});
}



