function RandomLights( _scene )
{
	this.numLights = 20;
	this.lights = [];


	var distance = 120;

	// front light

	var light = new THREE.PointLight( 0xffffff, 1.5, 1.5 * distance );
	//_scene.add( light );
	this.lights.push( light );

	// random lights

	var c = new THREE.Vector3();

	for ( var i = 1; i < this.numLights; i ++ ) {

		var light = new THREE.PointLight( 0xffffff, 2.0, distance );

		c.set( Math.random(), Math.random(), Math.random() ).normalize();
		light.color.setRGB( c.x, c.y, c.z );

		_scene.add( light );
		this.lights.push( light );

	}

	var geometry = new THREE.SphereGeometry( 0.7, 7, 7 );

	for ( var i = 0; i < this.numLights; i ++ ) {

		var light = this.lights[ i ];

		var material = new THREE.MeshBasicMaterial();
		material.color = light.color;

		var emitter = new THREE.Mesh( geometry, material );
		emitter.position = light.position;

		_scene.add( emitter );

	}
}

RandomLights.prototype.onTick = function( delta )
{
	var time = Date.now() * 0.0005;
	var x, y, z;

	for ( var i = 0, il = this.lights.length; i < il; i ++ ) {

		var light = this.lights[ i ];

		if ( i > 0 ) {

			x = Math.sin( time + i * 1.7 ) * 80;
			y = Math.cos( time + i * 1.5 ) * 40;
			z = Math.cos( time + i * 1.3 ) * 30;

		} else {

			x = Math.sin( time * 3 ) * 20;
			y = 15;
			z = Math.cos( time * 3 ) * 25 + 10;

		}

		light.position.set( x, y + 100, z + 1000 );

	}

}