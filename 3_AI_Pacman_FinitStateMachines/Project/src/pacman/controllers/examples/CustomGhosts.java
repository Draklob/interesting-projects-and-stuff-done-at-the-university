package pacman.controllers.examples;

import java.util.EnumMap;

import pacman.states.FSM1.AggressiveState;
import pacman.states.FSM1.DefensiveState;
import pacman.states.FSM1.FSM1State;
import pacman.states.FSM1.SuicideState;
import pacman.states.FSM2.AvoidState;
import pacman.states.FSM2.ChaseState;
import pacman.states.FSM2.FSM2State;
import pacman.states.FSM2.PatrolState;
import pacman.controllers.Controller;
import pacman.game.Game;
import pacman.game.Constants.GHOST;
import static pacman.game.Constants.*;

/*
 * Ghost team controller as part of the starter package - simply upload this file as a zip called
 * MyGhosts.zip and you will be entered into the rankings - as simple as that! Feel free to modify 
 * it or to start from scratch, using the classes supplied with the original software. Best of luck!
 * 
 * This ghost controller does the following:
 * 1. If edible or Ms Pac-Man is close to power pill, run away from Ms Pac-Man
 * 2. If non-edible, attack Ms Pac-Man with certain probability, else choose random direction
 */
public final class CustomGhosts extends Controller<EnumMap<GHOST,MOVE>>
{	
	//Enu de estados para FSM1
	public enum StatesFSM1 { Aggressive, Defensive, Suicide };
	//El estado actual correspondiente a FSM1
	static public StatesFSM1 currentStateFSM1;
	
	//Array de estados para FSM1 para poderlos referenciar con �ndice
	public FSM1State[] FSM1StatesArray;
	
	//Enum de estados para FSM2
	public enum StatesFSM2 { Chase, Patrol, Avoid };
	//Array de estados de tipo FSM2, uno por cada fantasma
	public StatesFSM2[] currentStateFSM2Array;
	
	//Array de estados para FSM2 para poderlos referenciar con un �ndice
	public FSM2State[] FSM2StatesArray;
	
	//Estructura de datos para devolver el movimiento que tiene que efectuar cada fantasma
	EnumMap<GHOST,MOVE> myMoves=new EnumMap<GHOST,MOVE>(GHOST.class);
	
	 /**
     * Constructor
     */
    public CustomGhosts()
    {
    	//Inicializamos el estado correspondiente a la m�quina de estados principal FSM1
    	//com�n a todos los fantasmas
    	currentStateFSM1 = StatesFSM1.Defensive;
    	
    	currentStateFSM2Array = new StatesFSM2[4];
    	//Inicializamos el estado de cada fantasma relativo a FSM2 a Patrol
    	for( int i=0; i<4; i++ )
    	{
    		currentStateFSM2Array[i] = StatesFSM2.Patrol;
    	}
    	
    	//Para poder referenciar los estados mediante un ordinal
    	FSM1StatesArray = new FSM1State[3];
    	FSM1StatesArray[0] = new AggressiveState();
    	FSM1StatesArray[1] = new DefensiveState();
    	FSM1StatesArray[2] = new SuicideState();
    	//Para poder referenciar los estados mediante un ordinal
    	FSM2StatesArray = new FSM2State[3];
    	FSM2StatesArray[0] = new ChaseState();
    	FSM2StatesArray[1] = new PatrolState();
    	FSM2StatesArray[2] = new AvoidState();
    	
    	//A�adimos los estados de la m�quina 2 como observadores de los 
    	//cambios en FSM1
    	for( FSM2State fsm2State : FSM2StatesArray )
    	{
    		FSM1State.addObserver( fsm2State );
    	}
    }
	
	public EnumMap<GHOST,MOVE> getMove(Game game,long timeDue)
	{
		/*** FSM1 ***/
		//El m�todo next nos dice a qu� estado pasar o si nos quedamos en el mismo
		currentStateFSM1 = FSM1StatesArray[currentStateFSM1.ordinal()].next( game, timeDue );
			
		for(GHOST ghost : GHOST.values())	//for each ghost
		{			
			if(game.doesGhostRequireAction(ghost))		//if ghost requires an action
			{
				//System.out.println("FSM1STATE = " + currentStateFSM1);
				/*** FSM1 ***/
				//Action ejecuta la acci�n de un estado concreto 
				FSM1StatesArray[currentStateFSM1.ordinal()].action( game, timeDue, ghost );
				
				/*** FSM2 ***/
				//El m�todo next nos dice a qu� estado pasar o si nos quedamos en el mismo ( cada fantasma tiene su propio estado )
				currentStateFSM2Array[ghost.ordinal()] = FSM2StatesArray[currentStateFSM2Array[ghost.ordinal()].ordinal()].next( game, timeDue, ghost );
				//Action ejecuta la acci�n de un estado concreto
			    myMoves.put( ghost, FSM2StatesArray[currentStateFSM2Array[ghost.ordinal()].ordinal()].action( game, timeDue, ghost ) );
			}
			
		}

		return myMoves;
	}
	
   
}