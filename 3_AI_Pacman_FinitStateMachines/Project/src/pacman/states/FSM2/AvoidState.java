package pacman.states.FSM2;

import pacman.controllers.examples.CustomGhosts;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;

public class AvoidState extends FSM2State 
{
	//Devuelve el estado al que se transitar� desde este estado, en funci�n de las 
	//condiciones actuales de la partida, podr�a permanecer en el mismo
	public CustomGhosts.StatesFSM2 next( Game game, long timeDue, GHOST ghost )
	{
		//System.out.println("Avoid State");
		
		CustomGhosts.StatesFSM2 nextState = CustomGhosts.StatesFSM2.Avoid;
		
		if( ! ( game.getGhostEdibleTime(ghost)>0 || closeToPower(game) ) )
		{
			nextState = CustomGhosts.StatesFSM2.Patrol;
		}
		
		return nextState;
	}
	//Ejecuta la acci�n correspondiente a este estado
	public MOVE action( Game game, long timeDue, GHOST ghost )
	{
		//Calculamos el movimiento para alejarse de PacMan
		MOVE nextMove = game.getApproximateNextMoveAwayFromTarget(game.getGhostCurrentNodeIndex(ghost),
				game.getPacmanCurrentNodeIndex(),game.getGhostLastMoveMade(ghost),DM.PATH);
		
		return nextMove;
	}
}
