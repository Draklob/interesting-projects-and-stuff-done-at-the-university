package pacman.states.FSM2;

import pacman.controllers.examples.CustomGhosts;
import pacman.game.Game;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;

public class PatrolState extends FSM2State 
{
	//Devuelve el estado al que se transitar� desde este estado, en funci�n de las 
	//condiciones actuales de la partida, podr�a permanecer en el mismo
	public CustomGhosts.StatesFSM2 next( Game game, long timeDue, GHOST ghost )
	{
		//System.out.println("Patrol State");
		
		//Valor de partida
		CustomGhosts.StatesFSM2 nextState = CustomGhosts.StatesFSM2.Patrol;
			
		if( game.getGhostEdibleTime(ghost)>0 || closeToPower(game) )
		{
			nextState = CustomGhosts.StatesFSM2.Avoid;
		}
		//Comprobamos si la distancia entre pacman y el fantasma es menor que el umbral en el que cambiamos del estado "Patrol" a "Chase"
		else if( game.getShortestPathDistance( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(ghost) ) <= this.thresholdDist.getThresholdDist() )
		{
			nextState = CustomGhosts.StatesFSM2.Chase;
		}
		
		return nextState;
	}
	
	//Ejecuta la acci�n correspondiente a este estado
	public MOVE action( Game game, long timeDue, GHOST ghost )
	{
		MOVE[] possibleMoves=game.getPossibleMoves(game.getGhostCurrentNodeIndex(ghost),game.getGhostLastMoveMade(ghost));
		MOVE nextMoVE = possibleMoves[rnd.nextInt(possibleMoves.length)];
		return nextMoVE;
	}
}
