package pacman.states.FSM2;

import pacman.controllers.examples.CustomGhosts;
import pacman.game.Game;
import pacman.game.Constants.DM;
import pacman.game.Constants.GHOST;
import pacman.game.Constants.MOVE;

public class ChaseState extends FSM2State 
{
	//Devuelve el estado al que se transitar� desde este estado, en funci�n de las 
	//condiciones actuales de la partida, podr�a permanecer en el mismo
	public CustomGhosts.StatesFSM2 next( Game game, long timeDue, GHOST ghost )
	{
		//System.out.println("Chase State");
		
		//Valor de partida
		CustomGhosts.StatesFSM2 nextState = CustomGhosts.StatesFSM2.Chase;
		
		//Si Ms PacMan est� cerca de un power-up cambiamos el estado a huida
		if( game.getGhostEdibleTime(ghost)>0 || closeToPower(game) )
		{
			nextState = CustomGhosts.StatesFSM2.Avoid;
		}
		//Comprobamos si la distancia entre pacman y el fantasma es mayor que el umbral en el que cambiamos del estado "Chase" a "Patrol"
		else if( game.getShortestPathDistance( game.getPacmanCurrentNodeIndex(), game.getGhostCurrentNodeIndex(ghost) ) >= this.thresholdDist.getThresholdDist() )
		{
			nextState = CustomGhosts.StatesFSM2.Patrol;
		}
		
		return nextState;
	}
	//Ejecuta la acci�n correspondiente a este estado
	public MOVE action( Game game, long timeDue, GHOST ghost )
	{
		MOVE nextMove = game.getApproximateNextMoveTowardsTarget(game.getGhostCurrentNodeIndex(ghost),
				game.getPacmanCurrentNodeIndex(),game.getGhostLastMoveMade(ghost),DM.PATH);
		return nextMove;
	}
}
