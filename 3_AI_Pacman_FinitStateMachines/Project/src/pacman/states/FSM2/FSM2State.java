package pacman.states.FSM2;

import java.util.Random;

import pacman.controllers.examples.CustomGhosts;
import pacman.game.Constants.MOVE;
import pacman.game.Game;
import pacman.game.Constants.GHOST;

public abstract class FSM2State
{
	//Para calcular movimientos aleatorios
	Random rnd=new Random();
	
	//El umbral de distancia a partir del cual los fantasmas
	//cambian de estado "Patrol" a estado "Chase"
	protected enum ThresholdDistance 
	{ 
		MIN(30), MED(70), HIGH(100);
		private final int thresholdDistance;
		private ThresholdDistance( int dist )
		{
			this.thresholdDistance = dist;
		}
		public int getThresholdDist()
		{
			return this.thresholdDistance;
		}
	};
	//Inicializamos el valor de este umbral
	protected ThresholdDistance thresholdDist = ThresholdDistance.MED;
	
	//El umbral de distancia a partir del cual 
	//consideramos que MsPac-Man est� cerca de coger un power-up
	//y el fantasma debe escapar por precauci�n
	protected final int PILL_PROXIMITY=25;		
	
	//Devuelve el estado al que se transitar� desde este estado, en funci�n de las 
	//condiciones actuales de la partida, podr�a permanecer en el mismo
	abstract public CustomGhosts.StatesFSM2 next( Game game, long timeDue, GHOST ghost );
	//Ejecuta la acci�n correspondiente a este estado
	abstract public MOVE action( Game game, long timeDue, GHOST ghost );
	
	//M�todo que es llamado desde el estado de la m�quina superior y 
	//que cambia la distancia a la que se activan los fantasmas seg�n el nivel de dificultad
	//dictaminado por la m�quina superior ( FSM1 )
	public void setDifficult( CustomGhosts.StatesFSM1 fsm1State )
	{
		//Chequeamos el umbral para la distancia en funci�n del estado relativo a FSM1 que est�n los fantasmas
		if( fsm1State == CustomGhosts.StatesFSM1.Aggressive )
		{
			this.thresholdDist = ThresholdDistance.HIGH;
		}
		else if( fsm1State == CustomGhosts.StatesFSM1.Defensive )
		{
			this.thresholdDist = ThresholdDistance.MED;
		}
		else if( fsm1State == CustomGhosts.StatesFSM1.Suicide )
		{
			this.thresholdDist = ThresholdDistance.MIN;
		}
	}
	
    //This helper function checks if Ms Pac-Man is close to an available power pill
	protected boolean closeToPower(Game game)
    {
    	int[] powerPills=game.getPowerPillIndices();
    	
    	for(int i=0;i<powerPills.length;i++)
    	{
    		if(game.isPowerPillStillAvailable(i) && game.getShortestPathDistance(powerPills[i],game.getPacmanCurrentNodeIndex()) <= PILL_PROXIMITY)
    			return true;
    	}

        return false;
    }
	
}
