package pacman.states.FSM1;

import java.util.ArrayList;

import pacman.controllers.examples.CustomGhosts;
import pacman.game.Game;
import pacman.game.Constants.GHOST;
import pacman.states.FSM2.FSM2State;

public abstract class FSM1State
{
	//Si a pacman le quedan m�s pildoras que recoger que este umbral
	//y tan solo le queda una vida, hacemos que los fantasmas sean m�s tontos
	int PillsMaxThreshold = 150;
	//Si a pacman le quedan menos pildoras que recoger que este umbral
	//y le queda m�s de una vida, hacemos que los fantasmas sean muy peligrosos
	int PillsMinThreshold = 60;
	//Devuelve el estado al que se transitar� desde este estado, en funci�n de las 
	//condiciones actuales de la partida, podr�a permanecer en el mismo
	abstract public CustomGhosts.StatesFSM1 next( Game game, long timeDue );
	//Ejecuta la acci�n correspondiente a este estado
	abstract public void action( Game game, long timeDue, GHOST ghost );
	
	//Guardamos una lista de los subestados de FSM2 que estar�n vigilando los cambios
	//en FSM1
	protected static ArrayList<FSM2State> observers = new ArrayList<FSM2State>();
	
	/**
     * Metodo para a�adir un objeto observador al objeto observable,
     * @param observer objeto que implementa la interfaz IObserver.
     */
    public static void addObserver(FSM2State observer)
    {
    	observers.add(observer);
    }
    
    /**
     * Metodo para eliminar un IObserver del ArrayList de observadores del
     * estado del objeto.
     *
     * @param observer objeto IObserver del ArraList "observers" que se desea
     * eliminar.
     */
    public static void removeObserver(FSM2State observer) 
    {
        observers.remove(observer);
    }
	
}
