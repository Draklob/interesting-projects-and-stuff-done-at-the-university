package pacman.states.FSM1;

import pacman.controllers.examples.CustomGhosts;
import pacman.game.Game;
import pacman.game.Constants.GHOST;
import pacman.states.FSM2.FSM2State;

public class SuicideState extends FSM1State 
{
	//Devuelve el estado al que se transitar� desde este estado, en funci�n de las 
	//condiciones actuales de la partida, podr�a permanecer en el mismo
	public CustomGhosts.StatesFSM1 next( Game game, long timeDue )
	{
		//System.out.println("Suicide State");
		
		CustomGhosts.StatesFSM1 nextState = CustomGhosts.StatesFSM1.Suicide;
		
		if( ! ( game.getPacmanNumberOfLivesRemaining() <= 1 && game.getNumberOfActivePills() >= this.PillsMaxThreshold ) )
		{
			nextState = CustomGhosts.StatesFSM1.Defensive;
		}
		
		return nextState;
	}
	
	//Ejecuta la acci�n correspondiente a este estado
	//Modifica los estados de FSM2
	public void action( Game game, long timeDue, GHOST ghost )
	{
		for( FSM2State fsm2State : observers )
    	{
			fsm2State.setDifficult( CustomGhosts.StatesFSM1.Suicide );
    	}
	}
}
